# The First Program (Checkpoint One)

## 1. Init GDT

1. Setup Kernel Segment

    + code segment
    + data segment

2. Setup User Segment

    + code segment
    + data segment

3. Setup TSS

    set_esp0() from 410kern/lib/x86/seg.h

## 2. Setup IDT

1. The Page Fault Handler

## 3. Design pcb/tcb

## 4. Virtual Memory Management

1. Virtual Memory Layout

    + 1 - 16 MB, direct map to kernel space
    + where does kernel stack reside?
    + and what about user stack?

2. Page Frame Management
    + buddy allocation?
    + bit map scanning for free pages
    + frame for kernel and frame for user?
    + interfaces
        - frame_alloc(frame_cnt)
            + allocate page_cnt continuous frames
            + return address of first frame on success otherwise NULL
        - frame_free(addr, frame_cnt)
            + free frame_cnt frame starting from addr

3. Setup Page Table

## 5. Load Executable

1. User Stack
