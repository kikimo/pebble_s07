#include <stdio.h>

void bt()
{
    void **ebp_p;
    void *ebp;

    // get ebp
    __asm__ __volatile__ ("mov %%ebp, %0":"=m"(ebp_p)::"memory");

    while (ebp_p) {
            ebp = *ebp_p;
            printf("%p\n", ebp_p);
            ebp_p = (void **) ebp;
    }
    printf("end with %p\n", ebp_p);
}

void foo()
{
    bt();
}

int main()
{
    foo();

    return 0;
}
