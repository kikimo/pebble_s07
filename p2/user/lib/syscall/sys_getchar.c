#include <syscall_int.h>
#include <syscall_utils.h>

char getchar(void)
{
    char ret;

    ret = _syscall_no_arg(GETCHAR_INT);

	return ret;
}