#include <syscall_int.h>
#include <syscall_utils.h>

int remove_pages(void * addr)
{
    int ret = 0;

    ret = _syscall(REMOVE_PAGES_INT, addr);

    return ret;
}