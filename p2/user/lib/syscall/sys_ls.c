#include <syscall_int.h>
#include <syscall_utils.h>

struct sys_ls_args_t {
    int size;
    char *buf;
};

int ls(int size, char * buf)
{
    int ret = 0;
    struct sys_ls_args_t args = { size, buf };

    ret = _syscall(LS_INT, &args);

	return ret;
}
