#include <syscall_int.h>
#include <syscall_utils.h>

struct sys_exec_args_t {
    char *execname;
    char **argvec;
};

int exec(char *execname, char *argvec[])
{
    int ret = 0;
    struct sys_exec_args_t args = { execname, argvec };

    ret = _syscall(EXEC_INT, &args);

	return ret;
}