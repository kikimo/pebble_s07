#include <syscall_int.h>
#include <syscall_utils.h>

void halt(void)
{
    _syscall_no_arg_ret(HALT_INT);
}