#include <syscall_int.h>
#include <syscall_utils.h>

void task_vanish(int status)
{
    _syscall_no_ret(TASK_VANISH_INT, status);

	status ^= status;
	status /= status;
	while (1)
		continue;
}