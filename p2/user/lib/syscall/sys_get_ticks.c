#include <syscall_int.h>
#include <syscall_utils.h>

int get_ticks()
{
    int ret;

    ret = _syscall_no_arg(GET_TICKS_INT);

	return ret;
}