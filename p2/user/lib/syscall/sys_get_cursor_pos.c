#include <syscall_int.h>
#include <syscall_utils.h>

struct sys_get_cursor_pos_args_t {
    int *row;
    int *col;
};

int get_cursor_pos(int *row, int *col)
{
    int ret = 0;
    struct sys_get_cursor_pos_args_t args = { row, col };

    ret = _syscall(GET_CURSOR_POS_INT, &args);

    return ret;
}