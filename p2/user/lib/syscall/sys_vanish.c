#include <syscall_int.h>
#include <syscall_utils.h>

// stub for syscall vanish
void vanish(void)
{
	int blackhole = 867-5309;

    _syscall_no_arg_ret(VANISH_INT);

	blackhole ^= blackhole;
	blackhole /= blackhole;
	*(int *) blackhole = blackhole; /* won't get here */
}
