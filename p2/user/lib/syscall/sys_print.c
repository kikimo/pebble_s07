#include <syscall_int.h>
#include <syscall_utils.h>

// stub for syscall print()

struct sys_print_args_t {
    int size;
    char *buf;
};

int print(int size, char *buf)
{
    int ret = 0;
    struct sys_print_args_t args = { size, buf };

    ret = _syscall(PRINT_INT, &args);

	return ret;
}