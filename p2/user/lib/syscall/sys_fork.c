#include <syscall_int.h>
#include <syscall_utils.h>

int fork()
{
    int ret = 0;
    
    ret = _syscall_no_arg(FORK_INT);

	return ret;
}