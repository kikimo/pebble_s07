#include <syscall_int.h>
#include <syscall_utils.h>

struct sys_new_pages_args_t {
    void *addr;
    int len;
};

int new_pages(void * addr, int len)
{
    int ret = 0;
    struct sys_new_pages_args_t args = { addr, len };

    ret = _syscall(NEW_PAGES_INT, &args);

	return ret;
}