#include <syscall_int.h>
#include <syscall_utils.h>

void misbehave(int mode)
{
    _syscall_no_ret(MISBEHAVE_INT, mode);
}