#include <syscall_int.h>
#include <syscall_utils.h>

int set_term_color(int color)
{
    int ret = 0;

    ret = _syscall(SET_TERM_COLOR_INT, color);

	return ret;
}