#include <syscall_int.h>
#include <syscall_utils.h>

int yield(int pid)
{
    int ret = 0;

    ret = _syscall(YIELD_INT, pid);

	return ret;
}