#include <syscall_int.h>
#include <syscall_utils.h>

int wait(int *status_ptr)
{
    int ret = 0;

    ret = _syscall(WAIT_INT, status_ptr);

	return ret;
}