#include <syscall_int.h>
#include <syscall_utils.h>

struct sys_readline_args_t {
    int size;
    char *buf;
};

int readline(int size, char *buf)
{
    int ret = 0;
    struct sys_readline_args_t args = { size, buf };

    ret = _syscall(READLINE_INT, &args);

	return ret;
}