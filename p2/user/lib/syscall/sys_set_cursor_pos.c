#include <syscall_int.h>
#include <syscall_utils.h>

struct sys_set_curs_pos_args_t {
    int row;
    int col;
};

int set_cursor_pos(int row, int col)
{
    int ret = 0;
    struct sys_set_curs_pos_args_t args = { row, col };

    ret = _syscall(SET_CURSOR_POS_INT, &args);

	return ret;
}