#include <syscall_int.h>
#include <syscall_utils.h>

// stub for syscall set_status
void set_status(int status)
{
    _syscall_no_ret(SET_STATUS_INT, status);

	return;
}