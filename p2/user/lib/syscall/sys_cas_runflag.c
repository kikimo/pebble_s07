#include <syscall_int.h>
#include <syscall_utils.h>

struct sys_cas_runflag_args_t {
    int tid;
    int *oldp;
    int *expectedp;
    int *newp;
};

int cas_runflag(int tid, int *oldp, int *expectp, int *newp)
{
    int ret = 0;
    struct sys_cas_runflag_args_t args = {
        tid, oldp, expectp, newp
    };

    ret = _syscall(CAS_RUNFLAG_INT, &args);

	return ret;
}