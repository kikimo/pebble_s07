#include <syscall_int.h>
#include <syscall_utils.h>

int gettid(void)
{
    int ret = 0;

    ret = _syscall_no_arg(GETTID_INT);

	return ret;
}
