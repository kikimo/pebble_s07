#include <syscall_int.h>
#include <syscall_utils.h>

int sleep(int ticks)
{
    int ret = 0;

    ret = _syscall(SLEEP_INT, ticks);

	return ret;
}