/** @file sem_type.h
 *  @brief This file defines the type for semaphores.
 */

#ifndef _SEM_TYPE_H
#define _SEM_TYPE_H

#include <cond_type.h>
#include <mutex_type.h>

typedef struct sem {
    int cnt;
    mutex_t cnt_lock;
    cond_t cnt_cond;
} sem_t;

#endif /* _SEM_TYPE_H */
