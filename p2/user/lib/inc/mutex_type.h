/** @file mutex_type.h
 *  @brief This file defines the type for mutexes.
 */

#ifndef _MUTEX_TYPE_H
#define _MUTEX_TYPE_H

#include <list.h>
#include <spinlock.h>

#define MUTEX_LOCKED    1
#define MUTEX_UNLOCKED  0

typedef struct mutex {
    char lock_val;
    spinlock_t wq_lock;   // spinlock protecting waiter_queue
    struct list_head_t waiter_queue;
} mutex_t;

#endif /* _MUTEX_TYPE_H */
