#ifndef _SPINLOCK_H
#define _SPINLOCK_H

#define SPIN_LOCKED 1
#define SPIN_UNLOCKED 0

#include <stdio.h>
#include <thread_utils.h>
#include <syscall.h>

typedef char spinlock_t;

static inline void spin_init(spinlock_t *lock)
{
    *lock= SPIN_UNLOCKED;
}

static inline void spin_lock(spinlock_t *lock)
{
    char lock_val = SPIN_LOCKED;

    // lprintf("%p", lock);
    // if (lock == (void *) 0x1003fff) {
    //     lprintf("holy shit");
    // }

    while (1) {
        lock_val =  xchg(lock, lock_val);

        if (lock_val == SPIN_LOCKED) {
            yield(-1);
        } else {
            break;
        }
    }

    __asm__ __volatile__ ("mfence":::"memory");
}

static inline int spin_try_lock(spinlock_t *lock)
{
    char lock_val;

    lock_val = xchg(lock, SPIN_LOCKED);
    __asm__ __volatile__ ("mfence":::"memory");

    return lock_val  == SPIN_UNLOCKED;
}

static inline void spin_unlock(spinlock_t *lock)
{
    *lock = SPIN_UNLOCKED;
    __asm__ __volatile__ ("mfence":::"memory");
}

#endif  // _SPINLOCK_H
