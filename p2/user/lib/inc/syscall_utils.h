#ifndef _SYSCALL_H
#define _SYSCALL_H

#define _syscall_no_arg_ret(syscall_no) { \
    __asm__ __volatile__ ( "int %0"::"i"((syscall_no))); \
}

#define _syscall_no_ret(syscall_no, arg) { \
    __asm__ __volatile__ ("int %0":: \
            /* inputs */ \
            "i"((syscall_no)), \
            "S"((arg)) \
    ); \
}

#define _syscall_no_arg(syscall_no) ({ \
    int ret = 0; \
    __asm__ __volatile__ ("int %1":"=a"(ret): \
            /* inputs */ \
            "i"((syscall_no)) \
    ); \
    ret; \
})

#define _syscall(syscall_no, arg) ({ \
    int ret = 0; \
    __asm__ __volatile__ ("int %1":"=a"(ret): \
            /* inputs */ \
            "i"((syscall_no)), \
            "S"((arg)) \
    ); \
    ret; \
})

#endif  // _SYSCALL_H
