#ifndef _LIST_H
#define _LIST_H

struct list_head_t {
    struct list_head_t *next;
    struct list_head_t *prev;
};

#define offsetof(TYPE, MEMBER)	((size_t)&((TYPE *)0)->MEMBER)

#define container_of(ptr, type, member) ({ \
        const typeof( ((type *)0)->member ) *__mptr = (ptr); \
        (type *)( (char *)__mptr - offsetof(type,member) );})

#define LIST_HEAD_INIT(name) { &(name), &(name) }

#define LIST_HEAD(name) \
    struct list_head_t name = LIST_HEAD_INIT(name)

#define list_entry(ptr, type, member) \
        container_of(ptr, type, member)

#define list_first_entry(ptr, type, member) \
        list_entry((ptr)->next, type, member)

#define list_for_each_entry(pos, head, member)                          \
        for (pos = list_entry((head)->next, typeof(*pos), member);      \
             &pos->member != (head);    \
             pos = list_entry(pos->member.next, typeof(*pos), member))

static inline void __list_add(struct list_head_t *new,
                            struct list_head_t *prev,
                            struct list_head_t *next)
{
    next->prev = new;
    new->next = next;
    prev->next = new;
    new->prev = prev;
}

static inline void __list_del(struct list_head_t *prev,
                    struct list_head_t *next)
{
    prev->next = next;
    next->prev = prev;
}

static inline void list_init_head(struct list_head_t *head)
{
    head->next = head;
    head->prev = head;
}

static inline void list_add(struct list_head_t *new,
                        struct list_head_t *head)
{
    __list_add(new, head, head->next);
}

static inline void list_add_tail(struct list_head_t *new,
                        struct list_head_t *head)
{
    __list_add(new, head->prev, head);
}

static inline void list_del(struct list_head_t *entry)
{
    __list_del(entry->prev, entry->next);
}

static inline int list_is_empty(struct list_head_t *head)
{
    return head->next == head;
}

#endif  // _LIST_H
