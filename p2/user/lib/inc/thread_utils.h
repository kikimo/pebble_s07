#ifndef _LOCK_UTILS_H
#define _LOCK_UTILS_H

#include <stdlib.h>

static inline char xchg(char *old_val, char new_val)
{
    __asm__ __volatile__ ("lock xchgb %0, %1"
        :  // outputs
            "=r"(new_val)
        :  // inputs
            "m"(*old_val),
            "0"(new_val)
        :  // cobbler
            "memory"
    );

    return new_val;
}

#endif  // _LOCK_UTILS_H
