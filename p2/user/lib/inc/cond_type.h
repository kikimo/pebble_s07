/** @file cond_type.h
 *  @brief This file defines the type for condition variables.
 */

#ifndef _COND_TYPE_H
#define _COND_TYPE_H

#include <spinlock.h>
#include <list.h>

typedef struct cond {
    spinlock_t wq_lock;
    struct list_head_t waiter_queue;
} cond_t;

#endif /* COND_TYPE_H */
