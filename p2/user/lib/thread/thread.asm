
thread.o：     文件格式 elf32-i386


Disassembly of section .text:

00000000 <__list_add>:
   0:	55                   	push   %ebp
   1:	89 e5                	mov    %esp,%ebp
   3:	8b 45 10             	mov    0x10(%ebp),%eax
   6:	8b 55 08             	mov    0x8(%ebp),%edx
   9:	89 50 04             	mov    %edx,0x4(%eax)
   c:	8b 45 08             	mov    0x8(%ebp),%eax
   f:	8b 55 10             	mov    0x10(%ebp),%edx
  12:	89 10                	mov    %edx,(%eax)
  14:	8b 45 0c             	mov    0xc(%ebp),%eax
  17:	8b 55 08             	mov    0x8(%ebp),%edx
  1a:	89 10                	mov    %edx,(%eax)
  1c:	8b 45 08             	mov    0x8(%ebp),%eax
  1f:	8b 55 0c             	mov    0xc(%ebp),%edx
  22:	89 50 04             	mov    %edx,0x4(%eax)
  25:	5d                   	pop    %ebp
  26:	c3                   	ret    

00000027 <__list_del>:
  27:	55                   	push   %ebp
  28:	89 e5                	mov    %esp,%ebp
  2a:	8b 45 08             	mov    0x8(%ebp),%eax
  2d:	8b 55 0c             	mov    0xc(%ebp),%edx
  30:	89 10                	mov    %edx,(%eax)
  32:	8b 45 0c             	mov    0xc(%ebp),%eax
  35:	8b 55 08             	mov    0x8(%ebp),%edx
  38:	89 50 04             	mov    %edx,0x4(%eax)
  3b:	5d                   	pop    %ebp
  3c:	c3                   	ret    

0000003d <list_add>:
  3d:	55                   	push   %ebp
  3e:	89 e5                	mov    %esp,%ebp
  40:	83 ec 0c             	sub    $0xc,%esp
  43:	8b 45 0c             	mov    0xc(%ebp),%eax
  46:	8b 00                	mov    (%eax),%eax
  48:	89 44 24 08          	mov    %eax,0x8(%esp)
  4c:	8b 45 0c             	mov    0xc(%ebp),%eax
  4f:	89 44 24 04          	mov    %eax,0x4(%esp)
  53:	8b 45 08             	mov    0x8(%ebp),%eax
  56:	89 04 24             	mov    %eax,(%esp)
  59:	e8 a2 ff ff ff       	call   0 <__list_add>
  5e:	c9                   	leave  
  5f:	c3                   	ret    

00000060 <list_del>:
  60:	55                   	push   %ebp
  61:	89 e5                	mov    %esp,%ebp
  63:	83 ec 08             	sub    $0x8,%esp
  66:	8b 45 08             	mov    0x8(%ebp),%eax
  69:	8b 10                	mov    (%eax),%edx
  6b:	8b 45 08             	mov    0x8(%ebp),%eax
  6e:	8b 40 04             	mov    0x4(%eax),%eax
  71:	89 54 24 04          	mov    %edx,0x4(%esp)
  75:	89 04 24             	mov    %eax,(%esp)
  78:	e8 aa ff ff ff       	call   27 <__list_del>
  7d:	c9                   	leave  
  7e:	c3                   	ret    

0000007f <xchg>:
  7f:	55                   	push   %ebp
  80:	89 e5                	mov    %esp,%ebp
  82:	83 ec 04             	sub    $0x4,%esp
  85:	8b 45 0c             	mov    0xc(%ebp),%eax
  88:	88 45 fc             	mov    %al,-0x4(%ebp)
  8b:	8b 55 08             	mov    0x8(%ebp),%edx
  8e:	0f b6 45 fc          	movzbl -0x4(%ebp),%eax
  92:	f0 86 02             	lock xchg %al,(%edx)
  95:	88 45 fc             	mov    %al,-0x4(%ebp)
  98:	0f b6 45 fc          	movzbl -0x4(%ebp),%eax
  9c:	c9                   	leave  
  9d:	c3                   	ret    

0000009e <spin_lock>:
  9e:	55                   	push   %ebp
  9f:	89 e5                	mov    %esp,%ebp
  a1:	83 ec 28             	sub    $0x28,%esp
  a4:	c6 45 f7 01          	movb   $0x1,-0x9(%ebp)
  a8:	0f be 45 f7          	movsbl -0x9(%ebp),%eax
  ac:	89 44 24 04          	mov    %eax,0x4(%esp)
  b0:	8b 45 08             	mov    0x8(%ebp),%eax
  b3:	89 04 24             	mov    %eax,(%esp)
  b6:	e8 c4 ff ff ff       	call   7f <xchg>
  bb:	88 45 f7             	mov    %al,-0x9(%ebp)
  be:	80 7d f7 01          	cmpb   $0x1,-0x9(%ebp)
  c2:	75 0e                	jne    d2 <spin_lock+0x34>
  c4:	c7 04 24 ff ff ff ff 	movl   $0xffffffff,(%esp)
  cb:	e8 fc ff ff ff       	call   cc <spin_lock+0x2e>
  d0:	eb d6                	jmp    a8 <spin_lock+0xa>
  d2:	90                   	nop
  d3:	c9                   	leave  
  d4:	c3                   	ret    

000000d5 <spin_try_lock>:
  d5:	55                   	push   %ebp
  d6:	89 e5                	mov    %esp,%ebp
  d8:	83 ec 18             	sub    $0x18,%esp
  db:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
  e2:	00 
  e3:	8b 45 08             	mov    0x8(%ebp),%eax
  e6:	89 04 24             	mov    %eax,(%esp)
  e9:	e8 91 ff ff ff       	call   7f <xchg>
  ee:	88 45 ff             	mov    %al,-0x1(%ebp)
  f1:	80 7d ff 00          	cmpb   $0x0,-0x1(%ebp)
  f5:	0f 94 c0             	sete   %al
  f8:	0f b6 c0             	movzbl %al,%eax
  fb:	c9                   	leave  
  fc:	c3                   	ret    

000000fd <spin_unlock>:
  fd:	55                   	push   %ebp
  fe:	89 e5                	mov    %esp,%ebp
 100:	8b 45 08             	mov    0x8(%ebp),%eax
 103:	c6 00 00             	movb   $0x0,(%eax)
 106:	5d                   	pop    %ebp
 107:	c3                   	ret    

00000108 <last_ebp>:
 108:	55                   	push   %ebp
 109:	89 e5                	mov    %esp,%ebp
 10b:	83 ec 10             	sub    $0x10,%esp
 10e:	89 6d f8             	mov    %ebp,-0x8(%ebp)
 111:	eb 06                	jmp    119 <last_ebp+0x11>
 113:	8b 45 fc             	mov    -0x4(%ebp),%eax
 116:	89 45 f8             	mov    %eax,-0x8(%ebp)
 119:	8b 45 f8             	mov    -0x8(%ebp),%eax
 11c:	8b 00                	mov    (%eax),%eax
 11e:	89 45 fc             	mov    %eax,-0x4(%ebp)
 121:	83 7d fc 00          	cmpl   $0x0,-0x4(%ebp)
 125:	75 ec                	jne    113 <last_ebp+0xb>
 127:	8b 45 f8             	mov    -0x8(%ebp),%eax
 12a:	c9                   	leave  
 12b:	c3                   	ret    

0000012c <get_thread_local>:
 12c:	55                   	push   %ebp
 12d:	89 e5                	mov    %esp,%ebp
 12f:	83 ec 28             	sub    $0x28,%esp
 132:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 139:	e8 ca ff ff ff       	call   108 <last_ebp>
 13e:	89 45 f4             	mov    %eax,-0xc(%ebp)
 141:	8b 45 f4             	mov    -0xc(%ebp),%eax
 144:	8b 40 04             	mov    0x4(%eax),%eax
 147:	3d af be ad de       	cmp    $0xdeadbeaf,%eax
 14c:	74 24                	je     172 <get_thread_local+0x46>
 14e:	8b 45 f4             	mov    -0xc(%ebp),%eax
 151:	8b 40 04             	mov    0x4(%eax),%eax
 154:	8b 55 f4             	mov    -0xc(%ebp),%edx
 157:	89 54 24 08          	mov    %edx,0x8(%esp)
 15b:	89 44 24 04          	mov    %eax,0x4(%esp)
 15f:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
 166:	e8 fc ff ff ff       	call   167 <get_thread_local+0x3b>
 16b:	b8 00 00 00 00       	mov    $0x0,%eax
 170:	eb 03                	jmp    175 <get_thread_local+0x49>
 172:	8b 45 f4             	mov    -0xc(%ebp),%eax
 175:	c9                   	leave  
 176:	c3                   	ret    

00000177 <current_thread>:
 177:	55                   	push   %ebp
 178:	89 e5                	mov    %esp,%ebp
 17a:	83 ec 18             	sub    $0x18,%esp
 17d:	e8 aa ff ff ff       	call   12c <get_thread_local>
 182:	89 45 f4             	mov    %eax,-0xc(%ebp)
 185:	8b 45 f4             	mov    -0xc(%ebp),%eax
 188:	83 c0 08             	add    $0x8,%eax
 18b:	c9                   	leave  
 18c:	c3                   	ret    

0000018d <thr_init_thread>:
 18d:	55                   	push   %ebp
 18e:	89 e5                	mov    %esp,%ebp
 190:	83 ec 18             	sub    $0x18,%esp
 193:	8b 45 08             	mov    0x8(%ebp),%eax
 196:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
 19d:	8b 45 08             	mov    0x8(%ebp),%eax
 1a0:	c6 40 0c 00          	movb   $0x0,0xc(%eax)
 1a4:	8b 45 08             	mov    0x8(%ebp),%eax
 1a7:	c6 40 0f 00          	movb   $0x0,0xf(%eax)
 1ab:	8b 45 08             	mov    0x8(%ebp),%eax
 1ae:	c6 40 0d 01          	movb   $0x1,0xd(%eax)
 1b2:	8b 45 08             	mov    0x8(%ebp),%eax
 1b5:	c6 40 0e 01          	movb   $0x1,0xe(%eax)
 1b9:	8b 45 08             	mov    0x8(%ebp),%eax
 1bc:	83 c0 10             	add    $0x10,%eax
 1bf:	89 04 24             	mov    %eax,(%esp)
 1c2:	e8 fc ff ff ff       	call   1c3 <thr_init_thread+0x36>
 1c7:	8b 45 08             	mov    0x8(%ebp),%eax
 1ca:	83 c0 10             	add    $0x10,%eax
 1cd:	89 04 24             	mov    %eax,(%esp)
 1d0:	e8 fc ff ff ff       	call   1d1 <thr_init_thread+0x44>
 1d5:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
 1dc:	e8 bd fe ff ff       	call   9e <spin_lock>
 1e1:	8b 45 08             	mov    0x8(%ebp),%eax
 1e4:	83 c0 1c             	add    $0x1c,%eax
 1e7:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
 1ee:	00 
 1ef:	89 04 24             	mov    %eax,(%esp)
 1f2:	e8 46 fe ff ff       	call   3d <list_add>
 1f7:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
 1fe:	e8 fa fe ff ff       	call   fd <spin_unlock>
 203:	c9                   	leave  
 204:	c3                   	ret    

00000205 <thr_init>:
 205:	55                   	push   %ebp
 206:	89 e5                	mov    %esp,%ebp
 208:	83 ec 28             	sub    $0x28,%esp
 20b:	c7 04 24 6d 00 00 00 	movl   $0x6d,(%esp)
 212:	e8 fc ff ff ff       	call   213 <thr_init+0xe>
 217:	c7 04 24 80 00 00 00 	movl   $0x80,(%esp)
 21e:	e8 fc ff ff ff       	call   21f <thr_init+0x1a>
 223:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
 22a:	e8 fc ff ff ff       	call   22b <thr_init+0x26>
 22f:	8b 45 08             	mov    0x8(%ebp),%eax
 232:	a3 00 00 00 00       	mov    %eax,0x0
 237:	a1 00 00 00 00       	mov    0x0,%eax
 23c:	3d ff 0f 00 00       	cmp    $0xfff,%eax
 241:	77 0a                	ja     24d <thr_init+0x48>
 243:	c7 05 00 00 00 00 00 	movl   $0x1000,0x0
 24a:	10 00 00 
 24d:	a1 00 00 00 00       	mov    0x0,%eax
 252:	83 e0 fc             	and    $0xfffffffc,%eax
 255:	a3 00 00 00 00       	mov    %eax,0x0
 25a:	a1 00 00 00 00       	mov    0x0,%eax
 25f:	89 44 24 04          	mov    %eax,0x4(%esp)
 263:	c7 04 24 a4 00 00 00 	movl   $0xa4,(%esp)
 26a:	e8 fc ff ff ff       	call   26b <thr_init+0x66>
 26f:	c7 04 24 2c 00 00 00 	movl   $0x2c,(%esp)
 276:	e8 fc ff ff ff       	call   277 <thr_init+0x72>
 27b:	89 45 f4             	mov    %eax,-0xc(%ebp)
 27e:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 282:	75 16                	jne    29a <thr_init+0x95>
 284:	c7 04 24 c8 00 00 00 	movl   $0xc8,(%esp)
 28b:	e8 fc ff ff ff       	call   28c <thr_init+0x87>
 290:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 295:	e9 8e 00 00 00       	jmp    328 <thr_init+0x123>
 29a:	8b 45 f4             	mov    -0xc(%ebp),%eax
 29d:	89 44 24 04          	mov    %eax,0x4(%esp)
 2a1:	c7 04 24 0c 01 00 00 	movl   $0x10c,(%esp)
 2a8:	e8 fc ff ff ff       	call   2a9 <thr_init+0xa4>
 2ad:	8b 45 f4             	mov    -0xc(%ebp),%eax
 2b0:	c7 40 04 af be ad de 	movl   $0xdeadbeaf,0x4(%eax)
 2b7:	8b 45 f4             	mov    -0xc(%ebp),%eax
 2ba:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
 2c0:	c7 04 24 3c 01 00 00 	movl   $0x13c,(%esp)
 2c7:	e8 fc ff ff ff       	call   2c8 <thr_init+0xc3>
 2cc:	e8 37 fe ff ff       	call   108 <last_ebp>
 2d1:	89 45 f0             	mov    %eax,-0x10(%ebp)
 2d4:	8b 45 f0             	mov    -0x10(%ebp),%eax
 2d7:	89 44 24 04          	mov    %eax,0x4(%esp)
 2db:	c7 04 24 74 01 00 00 	movl   $0x174,(%esp)
 2e2:	e8 fc ff ff ff       	call   2e3 <thr_init+0xde>
 2e7:	8b 45 f0             	mov    -0x10(%ebp),%eax
 2ea:	8b 55 f4             	mov    -0xc(%ebp),%edx
 2ed:	89 10                	mov    %edx,(%eax)
 2ef:	8b 45 f4             	mov    -0xc(%ebp),%eax
 2f2:	83 c0 08             	add    $0x8,%eax
 2f5:	89 45 ec             	mov    %eax,-0x14(%ebp)
 2f8:	e8 fc ff ff ff       	call   2f9 <thr_init+0xf4>
 2fd:	8b 55 ec             	mov    -0x14(%ebp),%edx
 300:	89 02                	mov    %eax,(%edx)
 302:	8b 45 ec             	mov    -0x14(%ebp),%eax
 305:	c7 40 04 00 00 00 00 	movl   $0x0,0x4(%eax)
 30c:	8b 45 ec             	mov    -0x14(%ebp),%eax
 30f:	89 04 24             	mov    %eax,(%esp)
 312:	e8 76 fe ff ff       	call   18d <thr_init_thread>
 317:	c7 04 24 99 01 00 00 	movl   $0x199,(%esp)
 31e:	e8 fc ff ff ff       	call   31f <thr_init+0x11a>
 323:	b8 00 00 00 00       	mov    $0x0,%eax
 328:	c9                   	leave  
 329:	c3                   	ret    

0000032a <thr_create>:
 32a:	55                   	push   %ebp
 32b:	89 e5                	mov    %esp,%ebp
 32d:	83 ec 38             	sub    $0x38,%esp
 330:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 337:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 33e:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
 345:	c7 04 24 aa 01 00 00 	movl   $0x1aa,(%esp)
 34c:	e8 fc ff ff ff       	call   34d <thr_create+0x23>
 351:	a1 00 00 00 00       	mov    0x0,%eax
 356:	89 04 24             	mov    %eax,(%esp)
 359:	e8 fc ff ff ff       	call   35a <thr_create+0x30>
 35e:	89 45 f4             	mov    %eax,-0xc(%ebp)
 361:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 365:	75 16                	jne    37d <thr_create+0x53>
 367:	c7 04 24 c0 01 00 00 	movl   $0x1c0,(%esp)
 36e:	e8 fc ff ff ff       	call   36f <thr_create+0x45>
 373:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 378:	e9 96 01 00 00       	jmp    513 <thr_create+0x1e9>
 37d:	8b 45 f4             	mov    -0xc(%ebp),%eax
 380:	89 44 24 04          	mov    %eax,0x4(%esp)
 384:	c7 04 24 00 02 00 00 	movl   $0x200,(%esp)
 38b:	e8 fc ff ff ff       	call   38c <thr_create+0x62>
 390:	a1 00 00 00 00       	mov    0x0,%eax
 395:	83 e8 04             	sub    $0x4,%eax
 398:	03 45 f4             	add    -0xc(%ebp),%eax
 39b:	89 45 e8             	mov    %eax,-0x18(%ebp)
 39e:	8b 45 e8             	mov    -0x18(%ebp),%eax
 3a1:	83 c0 04             	add    $0x4,%eax
 3a4:	c7 00 af be cd ab    	movl   $0xabcdbeaf,(%eax)
 3aa:	c7 04 24 2c 00 00 00 	movl   $0x2c,(%esp)
 3b1:	e8 fc ff ff ff       	call   3b2 <thr_create+0x88>
 3b6:	89 45 f0             	mov    %eax,-0x10(%ebp)
 3b9:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 3bd:	75 16                	jne    3d5 <thr_create+0xab>
 3bf:	c7 04 24 34 02 00 00 	movl   $0x234,(%esp)
 3c6:	e8 fc ff ff ff       	call   3c7 <thr_create+0x9d>
 3cb:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 3d0:	e9 3e 01 00 00       	jmp    513 <thr_create+0x1e9>
 3d5:	8b 45 f0             	mov    -0x10(%ebp),%eax
 3d8:	89 44 24 04          	mov    %eax,0x4(%esp)
 3dc:	c7 04 24 74 02 00 00 	movl   $0x274,(%esp)
 3e3:	e8 fc ff ff ff       	call   3e4 <thr_create+0xba>
 3e8:	8b 45 f0             	mov    -0x10(%ebp),%eax
 3eb:	c7 40 04 af be ad de 	movl   $0xdeadbeaf,0x4(%eax)
 3f2:	8b 45 f0             	mov    -0x10(%ebp),%eax
 3f5:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
 3fb:	8b 45 f0             	mov    -0x10(%ebp),%eax
 3fe:	83 c0 08             	add    $0x8,%eax
 401:	89 45 ec             	mov    %eax,-0x14(%ebp)
 404:	8b 45 e8             	mov    -0x18(%ebp),%eax
 407:	89 44 24 04          	mov    %eax,0x4(%esp)
 40b:	c7 04 24 9d 02 00 00 	movl   $0x29d,(%esp)
 412:	e8 fc ff ff ff       	call   413 <thr_create+0xe9>
 417:	8b 45 e8             	mov    -0x18(%ebp),%eax
 41a:	8b 55 f0             	mov    -0x10(%ebp),%edx
 41d:	89 10                	mov    %edx,(%eax)
 41f:	8b 45 e8             	mov    -0x18(%ebp),%eax
 422:	83 e8 0c             	sub    $0xc,%eax
 425:	89 45 e4             	mov    %eax,-0x1c(%ebp)
 428:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 42b:	89 44 24 04          	mov    %eax,0x4(%esp)
 42f:	c7 04 24 ba 02 00 00 	movl   $0x2ba,(%esp)
 436:	e8 fc ff ff ff       	call   437 <thr_create+0x10d>
 43b:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 43e:	8b 55 08             	mov    0x8(%ebp),%edx
 441:	89 10                	mov    %edx,(%eax)
 443:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 446:	8d 50 04             	lea    0x4(%eax),%edx
 449:	8b 45 0c             	mov    0xc(%ebp),%eax
 44c:	89 02                	mov    %eax,(%edx)
 44e:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 451:	8d 50 08             	lea    0x8(%eax),%edx
 454:	8b 45 ec             	mov    -0x14(%ebp),%eax
 457:	89 02                	mov    %eax,(%edx)
 459:	c7 04 24 d8 02 00 00 	movl   $0x2d8,(%esp)
 460:	e8 fc ff ff ff       	call   461 <thr_create+0x137>
 465:	8b 45 e8             	mov    -0x18(%ebp),%eax
 468:	89 44 24 04          	mov    %eax,0x4(%esp)
 46c:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 46f:	89 04 24             	mov    %eax,(%esp)
 472:	e8 fc ff ff ff       	call   473 <thr_create+0x149>
 477:	89 45 e0             	mov    %eax,-0x20(%ebp)
 47a:	8b 45 e0             	mov    -0x20(%ebp),%eax
 47d:	89 44 24 04          	mov    %eax,0x4(%esp)
 481:	c7 04 24 f8 02 00 00 	movl   $0x2f8,(%esp)
 488:	e8 fc ff ff ff       	call   489 <thr_create+0x15f>
 48d:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
 491:	79 1b                	jns    4ae <thr_create+0x184>
 493:	8b 45 f4             	mov    -0xc(%ebp),%eax
 496:	89 04 24             	mov    %eax,(%esp)
 499:	e8 fc ff ff ff       	call   49a <thr_create+0x170>
 49e:	8b 45 f0             	mov    -0x10(%ebp),%eax
 4a1:	89 04 24             	mov    %eax,(%esp)
 4a4:	e8 fc ff ff ff       	call   4a5 <thr_create+0x17b>
 4a9:	8b 45 e0             	mov    -0x20(%ebp),%eax
 4ac:	eb 65                	jmp    513 <thr_create+0x1e9>
 4ae:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
 4b2:	75 13                	jne    4c7 <thr_create+0x19d>
 4b4:	c7 04 24 2c 03 00 00 	movl   $0x32c,(%esp)
 4bb:	e8 fc ff ff ff       	call   4bc <thr_create+0x192>
 4c0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 4c5:	eb 4c                	jmp    513 <thr_create+0x1e9>
 4c7:	8b 45 ec             	mov    -0x14(%ebp),%eax
 4ca:	8b 55 e0             	mov    -0x20(%ebp),%edx
 4cd:	89 10                	mov    %edx,(%eax)
 4cf:	8b 45 ec             	mov    -0x14(%ebp),%eax
 4d2:	8b 55 f4             	mov    -0xc(%ebp),%edx
 4d5:	89 50 04             	mov    %edx,0x4(%eax)
 4d8:	8b 45 ec             	mov    -0x14(%ebp),%eax
 4db:	89 04 24             	mov    %eax,(%esp)
 4de:	e8 aa fc ff ff       	call   18d <thr_init_thread>
 4e3:	8b 45 e0             	mov    -0x20(%ebp),%eax
 4e6:	89 44 24 04          	mov    %eax,0x4(%esp)
 4ea:	c7 04 24 68 03 00 00 	movl   $0x368,(%esp)
 4f1:	e8 fc ff ff ff       	call   4f2 <thr_create+0x1c8>
 4f6:	8b 45 ec             	mov    -0x14(%ebp),%eax
 4f9:	83 c0 0d             	add    $0xd,%eax
 4fc:	89 04 24             	mov    %eax,(%esp)
 4ff:	e8 f9 fb ff ff       	call   fd <spin_unlock>
 504:	c7 04 24 91 03 00 00 	movl   $0x391,(%esp)
 50b:	e8 fc ff ff ff       	call   50c <thr_create+0x1e2>
 510:	8b 45 e0             	mov    -0x20(%ebp),%eax
 513:	c9                   	leave  
 514:	c3                   	ret    

00000515 <thr_run_child_func>:
 515:	55                   	push   %ebp
 516:	89 e5                	mov    %esp,%ebp
 518:	83 ec 28             	sub    $0x28,%esp
 51b:	c7 04 24 a4 03 00 00 	movl   $0x3a4,(%esp)
 522:	e8 fc ff ff ff       	call   523 <thr_run_child_func+0xe>
 527:	c7 04 24 c0 03 00 00 	movl   $0x3c0,(%esp)
 52e:	e8 fc ff ff ff       	call   52f <thr_run_child_func+0x1a>
 533:	8b 45 10             	mov    0x10(%ebp),%eax
 536:	83 c0 0d             	add    $0xd,%eax
 539:	89 04 24             	mov    %eax,(%esp)
 53c:	e8 5d fb ff ff       	call   9e <spin_lock>
 541:	8b 45 10             	mov    0x10(%ebp),%eax
 544:	8b 00                	mov    (%eax),%eax
 546:	89 44 24 04          	mov    %eax,0x4(%esp)
 54a:	c7 04 24 f0 03 00 00 	movl   $0x3f0,(%esp)
 551:	e8 fc ff ff ff       	call   552 <thr_run_child_func+0x3d>
 556:	8b 45 0c             	mov    0xc(%ebp),%eax
 559:	89 04 24             	mov    %eax,(%esp)
 55c:	8b 45 08             	mov    0x8(%ebp),%eax
 55f:	ff d0                	call   *%eax
 561:	89 45 f4             	mov    %eax,-0xc(%ebp)
 564:	8b 45 10             	mov    0x10(%ebp),%eax
 567:	8b 00                	mov    (%eax),%eax
 569:	89 44 24 04          	mov    %eax,0x4(%esp)
 56d:	c7 04 24 1c 04 00 00 	movl   $0x41c,(%esp)
 574:	e8 fc ff ff ff       	call   575 <thr_run_child_func+0x60>
 579:	8b 45 10             	mov    0x10(%ebp),%eax
 57c:	8b 00                	mov    (%eax),%eax
 57e:	89 44 24 04          	mov    %eax,0x4(%esp)
 582:	c7 04 24 45 04 00 00 	movl   $0x445,(%esp)
 589:	e8 fc ff ff ff       	call   58a <thr_run_child_func+0x75>
 58e:	8b 45 f4             	mov    -0xc(%ebp),%eax
 591:	89 04 24             	mov    %eax,(%esp)
 594:	e8 fc ff ff ff       	call   595 <thr_run_child_func+0x80>
 599:	c9                   	leave  
 59a:	c3                   	ret    

0000059b <thr_join>:
 59b:	55                   	push   %ebp
 59c:	89 e5                	mov    %esp,%ebp
 59e:	83 ec 38             	sub    $0x38,%esp
 5a1:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 5a8:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 5af:	c7 04 24 62 04 00 00 	movl   $0x462,(%esp)
 5b6:	e8 fc ff ff ff       	call   5b7 <thr_join+0x1c>
 5bb:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
 5c2:	e8 d7 fa ff ff       	call   9e <spin_lock>
 5c7:	8b 45 08             	mov    0x8(%ebp),%eax
 5ca:	89 44 24 04          	mov    %eax,0x4(%esp)
 5ce:	c7 04 24 74 04 00 00 	movl   $0x474,(%esp)
 5d5:	e8 fc ff ff ff       	call   5d6 <thr_join+0x3b>
 5da:	a1 00 00 00 00       	mov    0x0,%eax
 5df:	89 45 ec             	mov    %eax,-0x14(%ebp)
 5e2:	8b 45 ec             	mov    -0x14(%ebp),%eax
 5e5:	83 e8 1c             	sub    $0x1c,%eax
 5e8:	89 45 f4             	mov    %eax,-0xc(%ebp)
 5eb:	eb 1c                	jmp    609 <thr_join+0x6e>
 5ed:	8b 45 f4             	mov    -0xc(%ebp),%eax
 5f0:	8b 00                	mov    (%eax),%eax
 5f2:	3b 45 08             	cmp    0x8(%ebp),%eax
 5f5:	74 21                	je     618 <thr_join+0x7d>
 5f7:	8b 45 f4             	mov    -0xc(%ebp),%eax
 5fa:	8b 40 1c             	mov    0x1c(%eax),%eax
 5fd:	89 45 e8             	mov    %eax,-0x18(%ebp)
 600:	8b 45 e8             	mov    -0x18(%ebp),%eax
 603:	83 e8 1c             	sub    $0x1c,%eax
 606:	89 45 f4             	mov    %eax,-0xc(%ebp)
 609:	8b 45 f4             	mov    -0xc(%ebp),%eax
 60c:	83 c0 1c             	add    $0x1c,%eax
 60f:	3d 00 00 00 00       	cmp    $0x0,%eax
 614:	75 d7                	jne    5ed <thr_join+0x52>
 616:	eb 01                	jmp    619 <thr_join+0x7e>
 618:	90                   	nop
 619:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 61d:	74 0a                	je     629 <thr_join+0x8e>
 61f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 622:	8b 00                	mov    (%eax),%eax
 624:	3b 45 08             	cmp    0x8(%ebp),%eax
 627:	74 29                	je     652 <thr_join+0xb7>
 629:	8b 45 08             	mov    0x8(%ebp),%eax
 62c:	89 44 24 04          	mov    %eax,0x4(%esp)
 630:	c7 04 24 a0 04 00 00 	movl   $0x4a0,(%esp)
 637:	e8 fc ff ff ff       	call   638 <thr_join+0x9d>
 63c:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
 643:	e8 b5 fa ff ff       	call   fd <spin_unlock>
 648:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 64d:	e9 78 01 00 00       	jmp    7ca <thr_join+0x22f>
 652:	8b 45 08             	mov    0x8(%ebp),%eax
 655:	89 44 24 04          	mov    %eax,0x4(%esp)
 659:	c7 04 24 d8 04 00 00 	movl   $0x4d8,(%esp)
 660:	e8 fc ff ff ff       	call   661 <thr_join+0xc6>
 665:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
 66c:	e8 8c fa ff ff       	call   fd <spin_unlock>
 671:	8b 45 08             	mov    0x8(%ebp),%eax
 674:	89 44 24 04          	mov    %eax,0x4(%esp)
 678:	c7 04 24 fc 04 00 00 	movl   $0x4fc,(%esp)
 67f:	e8 fc ff ff ff       	call   680 <thr_join+0xe5>
 684:	8b 45 f4             	mov    -0xc(%ebp),%eax
 687:	83 c0 0f             	add    $0xf,%eax
 68a:	89 04 24             	mov    %eax,(%esp)
 68d:	e8 43 fa ff ff       	call   d5 <spin_try_lock>
 692:	85 c0                	test   %eax,%eax
 694:	75 1d                	jne    6b3 <thr_join+0x118>
 696:	8b 45 08             	mov    0x8(%ebp),%eax
 699:	89 44 24 04          	mov    %eax,0x4(%esp)
 69d:	c7 04 24 24 05 00 00 	movl   $0x524,(%esp)
 6a4:	e8 fc ff ff ff       	call   6a5 <thr_join+0x10a>
 6a9:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 6ae:	e9 17 01 00 00       	jmp    7ca <thr_join+0x22f>
 6b3:	8b 45 08             	mov    0x8(%ebp),%eax
 6b6:	89 44 24 04          	mov    %eax,0x4(%esp)
 6ba:	c7 04 24 4c 05 00 00 	movl   $0x54c,(%esp)
 6c1:	e8 fc ff ff ff       	call   6c2 <thr_join+0x127>
 6c6:	8b 45 08             	mov    0x8(%ebp),%eax
 6c9:	89 44 24 04          	mov    %eax,0x4(%esp)
 6cd:	c7 04 24 74 05 00 00 	movl   $0x574,(%esp)
 6d4:	e8 fc ff ff ff       	call   6d5 <thr_join+0x13a>
 6d9:	8b 45 f4             	mov    -0xc(%ebp),%eax
 6dc:	83 c0 10             	add    $0x10,%eax
 6df:	89 04 24             	mov    %eax,(%esp)
 6e2:	e8 fc ff ff ff       	call   6e3 <thr_join+0x148>
 6e7:	8b 45 08             	mov    0x8(%ebp),%eax
 6ea:	89 44 24 04          	mov    %eax,0x4(%esp)
 6ee:	c7 04 24 a4 05 00 00 	movl   $0x5a4,(%esp)
 6f5:	e8 fc ff ff ff       	call   6f6 <thr_join+0x15b>
 6fa:	8b 45 f4             	mov    -0xc(%ebp),%eax
 6fd:	83 c0 0e             	add    $0xe,%eax
 700:	89 04 24             	mov    %eax,(%esp)
 703:	e8 96 f9 ff ff       	call   9e <spin_lock>
 708:	8b 45 f4             	mov    -0xc(%ebp),%eax
 70b:	8b 40 04             	mov    0x4(%eax),%eax
 70e:	85 c0                	test   %eax,%eax
 710:	74 0e                	je     720 <thr_join+0x185>
 712:	8b 45 f4             	mov    -0xc(%ebp),%eax
 715:	8b 40 04             	mov    0x4(%eax),%eax
 718:	89 04 24             	mov    %eax,(%esp)
 71b:	e8 fc ff ff ff       	call   71c <thr_join+0x181>
 720:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
 727:	e8 72 f9 ff ff       	call   9e <spin_lock>
 72c:	8b 45 f4             	mov    -0xc(%ebp),%eax
 72f:	83 c0 1c             	add    $0x1c,%eax
 732:	89 04 24             	mov    %eax,(%esp)
 735:	e8 26 f9 ff ff       	call   60 <list_del>
 73a:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
 741:	e8 b7 f9 ff ff       	call   fd <spin_unlock>
 746:	8b 45 f4             	mov    -0xc(%ebp),%eax
 749:	8b 40 08             	mov    0x8(%eax),%eax
 74c:	89 44 24 04          	mov    %eax,0x4(%esp)
 750:	c7 04 24 d2 05 00 00 	movl   $0x5d2,(%esp)
 757:	e8 fc ff ff ff       	call   758 <thr_join+0x1bd>
 75c:	8b 45 f4             	mov    -0xc(%ebp),%eax
 75f:	8b 50 08             	mov    0x8(%eax),%edx
 762:	8b 45 0c             	mov    0xc(%ebp),%eax
 765:	89 10                	mov    %edx,(%eax)
 767:	8b 45 f4             	mov    -0xc(%ebp),%eax
 76a:	89 45 e4             	mov    %eax,-0x1c(%ebp)
 76d:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 770:	83 e8 08             	sub    $0x8,%eax
 773:	89 45 f0             	mov    %eax,-0x10(%ebp)
 776:	8b 45 f4             	mov    -0xc(%ebp),%eax
 779:	8b 00                	mov    (%eax),%eax
 77b:	89 44 24 08          	mov    %eax,0x8(%esp)
 77f:	8b 45 f0             	mov    -0x10(%ebp),%eax
 782:	89 44 24 04          	mov    %eax,0x4(%esp)
 786:	c7 04 24 ec 05 00 00 	movl   $0x5ec,(%esp)
 78d:	e8 fc ff ff ff       	call   78e <thr_join+0x1f3>
 792:	8b 45 f0             	mov    -0x10(%ebp),%eax
 795:	89 04 24             	mov    %eax,(%esp)
 798:	e8 fc ff ff ff       	call   799 <thr_join+0x1fe>
 79d:	8b 45 f4             	mov    -0xc(%ebp),%eax
 7a0:	8b 00                	mov    (%eax),%eax
 7a2:	89 44 24 08          	mov    %eax,0x8(%esp)
 7a6:	8b 45 f0             	mov    -0x10(%ebp),%eax
 7a9:	89 44 24 04          	mov    %eax,0x4(%esp)
 7ad:	c7 04 24 14 06 00 00 	movl   $0x614,(%esp)
 7b4:	e8 fc ff ff ff       	call   7b5 <thr_join+0x21a>
 7b9:	c7 04 24 44 06 00 00 	movl   $0x644,(%esp)
 7c0:	e8 fc ff ff ff       	call   7c1 <thr_join+0x226>
 7c5:	b8 00 00 00 00       	mov    $0x0,%eax
 7ca:	c9                   	leave  
 7cb:	c3                   	ret    

000007cc <thr_exit>:
 7cc:	55                   	push   %ebp
 7cd:	89 e5                	mov    %esp,%ebp
 7cf:	83 ec 28             	sub    $0x28,%esp
 7d2:	c7 04 24 54 06 00 00 	movl   $0x654,(%esp)
 7d9:	e8 fc ff ff ff       	call   7da <thr_exit+0xe>
 7de:	e8 94 f9 ff ff       	call   177 <current_thread>
 7e3:	89 45 f4             	mov    %eax,-0xc(%ebp)
 7e6:	8b 45 f4             	mov    -0xc(%ebp),%eax
 7e9:	8b 00                	mov    (%eax),%eax
 7eb:	89 44 24 04          	mov    %eax,0x4(%esp)
 7ef:	c7 04 24 68 06 00 00 	movl   $0x668,(%esp)
 7f6:	e8 fc ff ff ff       	call   7f7 <thr_exit+0x2b>
 7fb:	8b 45 f4             	mov    -0xc(%ebp),%eax
 7fe:	8b 55 08             	mov    0x8(%ebp),%edx
 801:	89 50 08             	mov    %edx,0x8(%eax)
 804:	8b 45 f4             	mov    -0xc(%ebp),%eax
 807:	8b 40 04             	mov    0x4(%eax),%eax
 80a:	85 c0                	test   %eax,%eax
 80c:	74 07                	je     815 <thr_exit+0x49>
 80e:	8b 45 f4             	mov    -0xc(%ebp),%eax
 811:	c6 40 0c ff          	movb   $0xff,0xc(%eax)
 815:	8b 45 f4             	mov    -0xc(%ebp),%eax
 818:	8b 00                	mov    (%eax),%eax
 81a:	89 44 24 04          	mov    %eax,0x4(%esp)
 81e:	c7 04 24 8c 06 00 00 	movl   $0x68c,(%esp)
 825:	e8 fc ff ff ff       	call   826 <thr_exit+0x5a>
 82a:	8b 45 f4             	mov    -0xc(%ebp),%eax
 82d:	83 c0 10             	add    $0x10,%eax
 830:	89 04 24             	mov    %eax,(%esp)
 833:	e8 fc ff ff ff       	call   834 <thr_exit+0x68>
 838:	8b 45 f4             	mov    -0xc(%ebp),%eax
 83b:	c6 40 0e 00          	movb   $0x0,0xe(%eax)
 83f:	cd 60                	int    $0x60
 841:	c7 04 24 b4 06 00 00 	movl   $0x6b4,(%esp)
 848:	e8 fc ff ff ff       	call   849 <thr_exit+0x7d>
 84d:	c9                   	leave  
 84e:	c3                   	ret    

0000084f <thr_getid>:
 84f:	55                   	push   %ebp
 850:	89 e5                	mov    %esp,%ebp
 852:	83 ec 18             	sub    $0x18,%esp
 855:	e8 1d f9 ff ff       	call   177 <current_thread>
 85a:	89 45 f4             	mov    %eax,-0xc(%ebp)
 85d:	8b 45 f4             	mov    -0xc(%ebp),%eax
 860:	8b 00                	mov    (%eax),%eax
 862:	c9                   	leave  
 863:	c3                   	ret    

00000864 <thr_yield>:
 864:	55                   	push   %ebp
 865:	89 e5                	mov    %esp,%ebp
 867:	83 ec 18             	sub    $0x18,%esp
 86a:	8b 45 08             	mov    0x8(%ebp),%eax
 86d:	89 04 24             	mov    %eax,(%esp)
 870:	e8 fc ff ff ff       	call   871 <thr_yield+0xd>
 875:	c9                   	leave  
 876:	c3                   	ret    
