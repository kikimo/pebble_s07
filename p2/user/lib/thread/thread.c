#include <list.h>
#include <thread.h>
#include <mutex.h>
#include <stdio.h>
#include <stdlib.h>
#include <syscall_int.h>
#include <syscall.h>

#define STACK_ALIGN             4
#define ALIGN(val, align) \
    ((unsigned int) (((unsigned int) (val)) & (~((align) - 1))))
#define THREAD_LOCAL_MAGIC      0xdeadbeaf
#define THREAD_STACK_MAGIC      0xabcdbeaf
#define THREAD_STATUS_RUNNING   0
#define THREAD_STATUS_DEAD      -1

#define lprintf(...)

int thr_fork(void *child_stack, void *thread_ebp);

/*
int thr_fork(void *stack_top, void *(*func) (void *), void *arg,
    spinlock_t *parent_lock, spinlock_t *child_lock);
*/

mutex_t malloc_lock;
unsigned int stack_size;

struct thread_t {
    int tid;
    void *stack_mem;
    void *ret_status;
    char thread_status;
    spinlock_t ready_lock;
    spinlock_t exit_lock;
    spinlock_t join_lock;
    mutex_t status_lock;
    struct list_head_t list;
};

struct thread_local_t {
    void *ebp;
    unsigned int magic;
    struct thread_t thread;
};

LIST_HEAD(thr_run_queue);
spinlock_t rq_lock = SPIN_UNLOCKED;

/*
 * ebp_1 -> ebp_2 -> ebp_3 -> ebp_4
 */
static inline void *last_ebp()
{
    void *ebp, **ebp_p;

    // lprintf("last_ebp(): start");
    // ebp_p = %ebp
    __asm__ __volatile__ ("mov %%ebp, %0":"=m"(ebp_p)::"memory");
    while ((ebp = *ebp_p) != NULL) {
        // lprintf("last_ebp(): ebp: %p", ebp_p);
        ebp_p = (void **) ebp;
    }

    // lprintf("last_ebp(): end\n");
    return (void *) ebp_p;
}

static inline struct thread_local_t *get_thread_local()
{
    struct thread_local_t *thread_local = NULL;

    // lprintf("get_thread_local(): begin");
    thread_local = (struct thread_local_t *) last_ebp();
    if (thread_local->magic != THREAD_LOCAL_MAGIC) {
        // TODO panic
        lprintf("!!!get_thread_local(): failed getting thread_local, ' \
            'magic incorrect: 0x%x of thread_local: %p",
            thread_local->magic, thread_local);

        return NULL;
    }

    // lprintf("get_thread_local(): end\n");
    return thread_local;
}

static inline struct thread_t *current_thread()
{
    struct thread_local_t *thread_local = get_thread_local();

    return (void *) &thread_local->thread;
}

static inline void thr_init_thread(struct thread_t *thread)
{
    thread->ret_status = NULL;
    thread->thread_status = THREAD_STATUS_RUNNING;
    thread->join_lock = SPIN_UNLOCKED;
    thread->exit_lock = SPIN_LOCKED;
    mutex_init(&thread->status_lock);
    mutex_lock(&thread->status_lock);

    spin_lock(&rq_lock);
    list_add(&thread->list, &thr_run_queue);
    spin_unlock(&rq_lock);
}

int thr_init( unsigned int size )
{
    struct thread_t *current;
    struct thread_local_t *thread_local;
    void **ebp;

    lprintf("thr_init() start");

    // 1. initialize malloc_lock
    lprintf("thr_init(): initialize malloc_lock");
    mutex_init(&malloc_lock);

    // 1. set stack size
    stack_size = size + STACK_ALIGN - 1;
    // if (stack_size < PAGE_SIZE) {
    //     stack_size = PAGE_SIZE;
    // }

    stack_size = (unsigned int) ALIGN(stack_size, STACK_ALIGN);
    lprintf("thr_init(): stack_size set to: 0x%x", stack_size);

    // 2. init stack top
    if ((thread_local = malloc(sizeof(struct thread_local_t))) == NULL) {
        // TODO log
        lprintf("!!!thr_init(): failed allocation thread_local_t for master thread.");
        return -1;
    }
    lprintf("thr_init(): thread_local of master thread: %p", thread_local);

    // init thread_local
    thread_local->magic = THREAD_LOCAL_MAGIC;
    thread_local->ebp = NULL;

    // link ebp and thread_local
    lprintf("thr_init(): setting up thread_local_t for master thread");

    ebp = last_ebp();
    lprintf("thr_init(): ebp of master thread: %p", ebp);
    ebp[0] = thread_local;

    // init thread
    current = &thread_local->thread;
    current->tid = gettid();
    current->stack_mem = NULL;
    current->ready_lock = SPIN_LOCKED;
    thr_init_thread(current);

    lprintf("thr_init(): end\n");

    return 0;
}

// TODO make it thread safe
int thr_create( void *(*func)(void *), void *arg )
{
    int tid;
    void *stack_mem = NULL;
    struct thread_local_t *thread_local = NULL;
    struct thread_t *thread = NULL;
    void **thread_ebp, **child_stack;

    lprintf("thr_create(): start");

    // 1. allocate stack for the new thread
    if ((stack_mem = malloc(stack_size)) == NULL) {
        lprintf("!!!thr_create(): failed allocating stack memory for new thread");
        return -1;
    }
    lprintf("thr_create(): stack allocated for new thread: %p", stack_mem);

    thread_ebp = ((void **) (((char *) stack_mem) + stack_size)) - 1;
    thread_ebp[1] = (void *) THREAD_STACK_MAGIC;

    /*
     * stack layout of new thread
     *     [ THREAD_LOCAL_MAGIC ]
     *     [ thread_local(last_ebp) ]
     *         ...
     *     [ thread_stack (ebp -> point to last_ebp) ]
     *     [ func() ]
     *     [ arg ]
     *     [ thread ]
     */
    if ((thread_local = malloc(sizeof(struct thread_local_t))) == NULL) {
        lprintf("!!!thr_create(): failed allocating thread_local for new thread");
        return -1;
    }
    lprintf("thr_create(): thread_local allocated: %p", thread_local);

    thread_local->magic = THREAD_LOCAL_MAGIC;
    thread_local->ebp = NULL;
    thread = &thread_local->thread;

    // set stack for child thread 
    // set up ebp
    // link ebp to thread_local
    lprintf("thr_create(): thread_ebp: %p", thread_ebp);
    thread_ebp[0] = thread_local;
    child_stack = thread_ebp - 3;

    // push parameters for thr_func
    // thr_run_func(func, arg, parent_lock, child_lock)
    lprintf("thr_create(): child esp: %p", child_stack);
    child_stack[0] = func;
    child_stack[1] = arg;
    child_stack[2] = thread;
    // child_stack[3] = &child_lock;

    // we should lock ready lock before child thread begin running
    // or else invoke of thr_getid() from child thread may result in
    // random value
    thread->ready_lock = SPIN_LOCKED;
    __asm__ __volatile__ ("mfence":::"memory");

    lprintf("thr_create() call thr_fork()...");
    tid = thr_fork(child_stack, thread_ebp);
    lprintf("thr_create() return from thr_fork() with tid: %d", tid);

    if (tid < 0) {
        free(stack_mem);
        free(thread_local);

        return tid;
    }

    if (tid == 0) {
        // within child chread, we should never be here,
        // TODO just panic
        lprintf("!!!thr_create(): should not return from child thread, panic");
        return -1;
    }

    // within parent thread set up child thread and 
    // wait for its execution of func(arg)
    thread->tid = tid;
    thread->stack_mem = stack_mem;
    thr_init_thread(thread);

    // make child continue now
    lprintf("thr_create(): release ready lock for: %d", tid);
    spin_unlock(&thread->ready_lock);

    lprintf("thr_create(): end\n");

    return tid;
}

inline void thr_run_child_func(void *(*func)(void *), void *arg,
    struct thread_t *self)
{
    void *ret_status;

    lprintf("thr_run_child_func(): start");

    // wait for current_thread(struct thread_t) to be set 
    // up; this is done by parent thread
    lprintf("thr_run_child_func(): acquiring ready lock...");
    spin_lock(&self->ready_lock);

    lprintf("thr_run_child_func(): %d running func()...", self->tid);
    ret_status = func(arg);
    lprintf("thr_run_child_func(): %d func() finished", self->tid);

    lprintf("thr_run_child_func(): %d end", self->tid);
    thr_exit(ret_status);
}

// TODO make it thread safe
int thr_join( int tid, void **statusp )
{
    int thr_found = 0;
    struct thread_t *target = NULL;
    struct thread_local_t *thread_local = NULL;

    lprintf("thr_join(): start");

    spin_lock(&rq_lock);
    lprintf("thr_join(): looking for target thread %d", tid);
    list_for_each_entry(target, &thr_run_queue, list) {
        // lprintf("thr_join(): target thread %d", target->tid);
        if (target->tid == tid) {
            thr_found = 1;
            break;
        }
    }

    // if (target == NULL || target->tid != tid) {
    if (!thr_found) {
        lprintf("!!!thr_join(): looking for target thread %d not found", tid);
        spin_unlock(&rq_lock);
        return -1;
    }

    lprintf("thr_join(): target thread %d found", tid);
    spin_unlock(&rq_lock);

    // target thread not found

    // it's an error if this thread has been joined
    // by other thread.
    lprintf("thr_join(): acquiring join_lock of %d", tid);
    if (!spin_try_lock(&target->join_lock)) {
        lprintf("thr_join(): thread %d joined already", tid);
        return -1;
    }
    lprintf("thr_join(): join_lock of %d acquired", tid);

    // wait for target thread to terminate
    lprintf("thr_join(): acquiring status_lock of thread %d", tid);
    mutex_lock(&target->status_lock);
    lprintf("thr_join(): status_lock of thread %d acquired", tid);

    // wait for target thread to terminate execution
    // so that we can free its stack memory
    spin_lock(&target->exit_lock);

    if (target->stack_mem != NULL) {
        free(target->stack_mem);
    }

    // finally
    // we delete this thread from run queue first
    spin_lock(&rq_lock);
    list_del(&target->list);
    spin_unlock(&rq_lock);

    lprintf("thr_join(): ret_status %p", target->ret_status);
    if (statusp != NULL) {
        *statusp = target->ret_status;
    }

    // don't forget to free memory of thread_local
    thread_local = container_of(target, struct thread_local_t, thread);
    lprintf("thr_join(): free thread_local %p for %d", thread_local, target->tid);
    free(thread_local);
    lprintf("thr_join(): done freeing thread_local %p for %d", thread_local, target->tid);

    lprintf("thr_join(): end");

    return 0;
}

void thr_exit( void *status )
{
    struct thread_t *current;

    lprintf("thr_exit(): start");
    current = current_thread();

    // store thread status
    lprintf("thr_exit(): thread %d exiting...", current->tid);
    current->ret_status = status;

    // free stack memory for non master thread
    if (current->stack_mem != NULL) {
        current->thread_status = THREAD_STATUS_DEAD;
        // free(current->stack_mem);
    }

    // wake potential waiter
    lprintf("thr_exit(): releasing status_lock of %d", current->tid);
    // while (1) {yield(0);}
    mutex_unlock(&current->status_lock);

    // stack may be freed by parent, we need assembly now
    current->exit_lock = SPIN_UNLOCKED;
    // __asm__ __volatile__ ("mov %0, %1"::"i"(SPIN_UNLOCKED), "m"(current->exit_lock):"memory");
    __asm__ __volatile__ ( "int %0"::"i"((VANISH_INT)));

    // lprintf("thr_exit(): status_lock of %d released", current->tid);
    // lprintf("thr_exit(): %d end\n", current->tid);
    // vanish();
    lprintf("thr_exit(): opps, we should never be here\n");
}

int thr_getid( void )
{
    struct thread_t *current = current_thread();

    return current->tid;
}

int thr_yield( int tid )
{
    return yield(tid);
}
