
mutex.o:     file format elf32-i386


Disassembly of section .text:

00000000 <__list_add>:
   0:	55                   	push   %ebp
   1:	89 e5                	mov    %esp,%ebp
   3:	8b 45 10             	mov    0x10(%ebp),%eax
   6:	8b 55 08             	mov    0x8(%ebp),%edx
   9:	89 50 04             	mov    %edx,0x4(%eax)
   c:	8b 45 08             	mov    0x8(%ebp),%eax
   f:	8b 55 10             	mov    0x10(%ebp),%edx
  12:	89 10                	mov    %edx,(%eax)
  14:	8b 45 0c             	mov    0xc(%ebp),%eax
  17:	8b 55 08             	mov    0x8(%ebp),%edx
  1a:	89 10                	mov    %edx,(%eax)
  1c:	8b 45 08             	mov    0x8(%ebp),%eax
  1f:	8b 55 0c             	mov    0xc(%ebp),%edx
  22:	89 50 04             	mov    %edx,0x4(%eax)
  25:	5d                   	pop    %ebp
  26:	c3                   	ret    

00000027 <__list_del>:
  27:	55                   	push   %ebp
  28:	89 e5                	mov    %esp,%ebp
  2a:	8b 45 08             	mov    0x8(%ebp),%eax
  2d:	8b 55 0c             	mov    0xc(%ebp),%edx
  30:	89 10                	mov    %edx,(%eax)
  32:	8b 45 0c             	mov    0xc(%ebp),%eax
  35:	8b 55 08             	mov    0x8(%ebp),%edx
  38:	89 50 04             	mov    %edx,0x4(%eax)
  3b:	5d                   	pop    %ebp
  3c:	c3                   	ret    

0000003d <list_init_head>:
  3d:	55                   	push   %ebp
  3e:	89 e5                	mov    %esp,%ebp
  40:	8b 45 08             	mov    0x8(%ebp),%eax
  43:	8b 55 08             	mov    0x8(%ebp),%edx
  46:	89 10                	mov    %edx,(%eax)
  48:	8b 45 08             	mov    0x8(%ebp),%eax
  4b:	8b 55 08             	mov    0x8(%ebp),%edx
  4e:	89 50 04             	mov    %edx,0x4(%eax)
  51:	5d                   	pop    %ebp
  52:	c3                   	ret    

00000053 <list_add_tail>:
  53:	55                   	push   %ebp
  54:	89 e5                	mov    %esp,%ebp
  56:	83 ec 0c             	sub    $0xc,%esp
  59:	8b 45 0c             	mov    0xc(%ebp),%eax
  5c:	8b 40 04             	mov    0x4(%eax),%eax
  5f:	8b 55 0c             	mov    0xc(%ebp),%edx
  62:	89 54 24 08          	mov    %edx,0x8(%esp)
  66:	89 44 24 04          	mov    %eax,0x4(%esp)
  6a:	8b 45 08             	mov    0x8(%ebp),%eax
  6d:	89 04 24             	mov    %eax,(%esp)
  70:	e8 8b ff ff ff       	call   0 <__list_add>
  75:	c9                   	leave  
  76:	c3                   	ret    

00000077 <list_del>:
  77:	55                   	push   %ebp
  78:	89 e5                	mov    %esp,%ebp
  7a:	83 ec 08             	sub    $0x8,%esp
  7d:	8b 45 08             	mov    0x8(%ebp),%eax
  80:	8b 10                	mov    (%eax),%edx
  82:	8b 45 08             	mov    0x8(%ebp),%eax
  85:	8b 40 04             	mov    0x4(%eax),%eax
  88:	89 54 24 04          	mov    %edx,0x4(%esp)
  8c:	89 04 24             	mov    %eax,(%esp)
  8f:	e8 93 ff ff ff       	call   27 <__list_del>
  94:	c9                   	leave  
  95:	c3                   	ret    

00000096 <list_is_empty>:
  96:	55                   	push   %ebp
  97:	89 e5                	mov    %esp,%ebp
  99:	8b 45 08             	mov    0x8(%ebp),%eax
  9c:	8b 00                	mov    (%eax),%eax
  9e:	3b 45 08             	cmp    0x8(%ebp),%eax
  a1:	0f 94 c0             	sete   %al
  a4:	0f b6 c0             	movzbl %al,%eax
  a7:	5d                   	pop    %ebp
  a8:	c3                   	ret    

000000a9 <xchg>:
  a9:	55                   	push   %ebp
  aa:	89 e5                	mov    %esp,%ebp
  ac:	83 ec 04             	sub    $0x4,%esp
  af:	8b 45 0c             	mov    0xc(%ebp),%eax
  b2:	88 45 fc             	mov    %al,-0x4(%ebp)
  b5:	8b 55 08             	mov    0x8(%ebp),%edx
  b8:	0f b6 45 fc          	movzbl -0x4(%ebp),%eax
  bc:	f0 86 02             	lock xchg %al,(%edx)
  bf:	88 45 fc             	mov    %al,-0x4(%ebp)
  c2:	0f b6 45 fc          	movzbl -0x4(%ebp),%eax
  c6:	c9                   	leave  
  c7:	c3                   	ret    

000000c8 <spin_init>:
  c8:	55                   	push   %ebp
  c9:	89 e5                	mov    %esp,%ebp
  cb:	8b 45 08             	mov    0x8(%ebp),%eax
  ce:	c6 00 00             	movb   $0x0,(%eax)
  d1:	5d                   	pop    %ebp
  d2:	c3                   	ret    

000000d3 <spin_lock>:
  d3:	55                   	push   %ebp
  d4:	89 e5                	mov    %esp,%ebp
  d6:	83 ec 28             	sub    $0x28,%esp
  d9:	c6 45 f7 01          	movb   $0x1,-0x9(%ebp)
  dd:	0f be 45 f7          	movsbl -0x9(%ebp),%eax
  e1:	89 44 24 04          	mov    %eax,0x4(%esp)
  e5:	8b 45 08             	mov    0x8(%ebp),%eax
  e8:	89 04 24             	mov    %eax,(%esp)
  eb:	e8 b9 ff ff ff       	call   a9 <xchg>
  f0:	88 45 f7             	mov    %al,-0x9(%ebp)
  f3:	80 7d f7 01          	cmpb   $0x1,-0x9(%ebp)
  f7:	75 0e                	jne    107 <spin_lock+0x34>
  f9:	c7 04 24 ff ff ff ff 	movl   $0xffffffff,(%esp)
 100:	e8 fc ff ff ff       	call   101 <spin_lock+0x2e>
 105:	eb d6                	jmp    dd <spin_lock+0xa>
 107:	90                   	nop
 108:	c9                   	leave  
 109:	c3                   	ret    

0000010a <spin_unlock>:
 10a:	55                   	push   %ebp
 10b:	89 e5                	mov    %esp,%ebp
 10d:	8b 45 08             	mov    0x8(%ebp),%eax
 110:	c6 00 00             	movb   $0x0,(%eax)
 113:	5d                   	pop    %ebp
 114:	c3                   	ret    

00000115 <mutex_init>:
 115:	55                   	push   %ebp
 116:	89 e5                	mov    %esp,%ebp
 118:	83 ec 04             	sub    $0x4,%esp
 11b:	8b 45 08             	mov    0x8(%ebp),%eax
 11e:	c6 00 00             	movb   $0x0,(%eax)
 121:	8b 45 08             	mov    0x8(%ebp),%eax
 124:	83 c0 01             	add    $0x1,%eax
 127:	89 04 24             	mov    %eax,(%esp)
 12a:	e8 99 ff ff ff       	call   c8 <spin_init>
 12f:	8b 45 08             	mov    0x8(%ebp),%eax
 132:	83 c0 04             	add    $0x4,%eax
 135:	89 04 24             	mov    %eax,(%esp)
 138:	e8 00 ff ff ff       	call   3d <list_init_head>
 13d:	b8 00 00 00 00       	mov    $0x0,%eax
 142:	c9                   	leave  
 143:	c3                   	ret    

00000144 <mutex_destroy>:
 144:	55                   	push   %ebp
 145:	89 e5                	mov    %esp,%ebp
 147:	b8 00 00 00 00       	mov    $0x0,%eax
 14c:	5d                   	pop    %ebp
 14d:	c3                   	ret    

0000014e <mutex_lock>:
 14e:	55                   	push   %ebp
 14f:	89 e5                	mov    %esp,%ebp
 151:	83 ec 38             	sub    $0x38,%esp
 154:	e8 fc ff ff ff       	call   155 <mutex_lock+0x7>
 159:	89 45 f4             	mov    %eax,-0xc(%ebp)
 15c:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
 163:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
 16a:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
 171:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
 178:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
 17f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 182:	89 45 e0             	mov    %eax,-0x20(%ebp)
 185:	8b 45 08             	mov    0x8(%ebp),%eax
 188:	83 c0 01             	add    $0x1,%eax
 18b:	89 04 24             	mov    %eax,(%esp)
 18e:	e8 40 ff ff ff       	call   d3 <spin_lock>
 193:	8b 45 08             	mov    0x8(%ebp),%eax
 196:	83 c0 04             	add    $0x4,%eax
 199:	89 44 24 04          	mov    %eax,0x4(%esp)
 19d:	8d 45 d8             	lea    -0x28(%ebp),%eax
 1a0:	89 04 24             	mov    %eax,(%esp)
 1a3:	e8 ab fe ff ff       	call   53 <list_add_tail>
 1a8:	8b 45 08             	mov    0x8(%ebp),%eax
 1ab:	83 c0 01             	add    $0x1,%eax
 1ae:	89 04 24             	mov    %eax,(%esp)
 1b1:	e8 54 ff ff ff       	call   10a <spin_unlock>
 1b6:	eb 23                	jmp    1db <mutex_lock+0x8d>
 1b8:	8d 45 e4             	lea    -0x1c(%ebp),%eax
 1bb:	89 44 24 0c          	mov    %eax,0xc(%esp)
 1bf:	8d 45 e8             	lea    -0x18(%ebp),%eax
 1c2:	89 44 24 08          	mov    %eax,0x8(%esp)
 1c6:	8d 45 ec             	lea    -0x14(%ebp),%eax
 1c9:	89 44 24 04          	mov    %eax,0x4(%esp)
 1cd:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1d0:	89 04 24             	mov    %eax,(%esp)
 1d3:	e8 fc ff ff ff       	call   1d4 <mutex_lock+0x86>
 1d8:	89 45 f0             	mov    %eax,-0x10(%ebp)
 1db:	8b 45 08             	mov    0x8(%ebp),%eax
 1de:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
 1e5:	00 
 1e6:	89 04 24             	mov    %eax,(%esp)
 1e9:	e8 bb fe ff ff       	call   a9 <xchg>
 1ee:	3c 01                	cmp    $0x1,%al
 1f0:	74 c6                	je     1b8 <mutex_lock+0x6a>
 1f2:	8b 45 08             	mov    0x8(%ebp),%eax
 1f5:	83 c0 01             	add    $0x1,%eax
 1f8:	89 04 24             	mov    %eax,(%esp)
 1fb:	e8 d3 fe ff ff       	call   d3 <spin_lock>
 200:	8d 45 d8             	lea    -0x28(%ebp),%eax
 203:	89 04 24             	mov    %eax,(%esp)
 206:	e8 6c fe ff ff       	call   77 <list_del>
 20b:	8b 45 08             	mov    0x8(%ebp),%eax
 20e:	83 c0 01             	add    $0x1,%eax
 211:	89 04 24             	mov    %eax,(%esp)
 214:	e8 f1 fe ff ff       	call   10a <spin_unlock>
 219:	b8 00 00 00 00       	mov    $0x0,%eax
 21e:	c9                   	leave  
 21f:	c3                   	ret    

00000220 <mutex_unlock>:
 220:	55                   	push   %ebp
 221:	89 e5                	mov    %esp,%ebp
 223:	83 ec 38             	sub    $0x38,%esp
 226:	8b 45 08             	mov    0x8(%ebp),%eax
 229:	83 c0 01             	add    $0x1,%eax
 22c:	89 04 24             	mov    %eax,(%esp)
 22f:	e8 9f fe ff ff       	call   d3 <spin_lock>
 234:	8b 45 08             	mov    0x8(%ebp),%eax
 237:	c6 00 00             	movb   $0x0,(%eax)
 23a:	8b 45 08             	mov    0x8(%ebp),%eax
 23d:	83 c0 04             	add    $0x4,%eax
 240:	89 04 24             	mov    %eax,(%esp)
 243:	e8 4e fe ff ff       	call   96 <list_is_empty>
 248:	85 c0                	test   %eax,%eax
 24a:	0f 84 9f 00 00 00    	je     2ef <mutex_unlock+0xcf>
 250:	e9 ac 00 00 00       	jmp    301 <mutex_unlock+0xe1>
 255:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 25c:	8b 45 08             	mov    0x8(%ebp),%eax
 25f:	8b 40 04             	mov    0x4(%eax),%eax
 262:	89 45 f0             	mov    %eax,-0x10(%ebp)
 265:	8b 45 f0             	mov    -0x10(%ebp),%eax
 268:	89 45 f4             	mov    %eax,-0xc(%ebp)
 26b:	eb 73                	jmp    2e0 <mutex_unlock+0xc0>
 26d:	c7 45 e0 ff ff ff ff 	movl   $0xffffffff,-0x20(%ebp)
 274:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
 27b:	8b 45 f4             	mov    -0xc(%ebp),%eax
 27e:	8b 40 08             	mov    0x8(%eax),%eax
 281:	8d 55 dc             	lea    -0x24(%ebp),%edx
 284:	89 54 24 0c          	mov    %edx,0xc(%esp)
 288:	8d 55 e0             	lea    -0x20(%ebp),%edx
 28b:	89 54 24 08          	mov    %edx,0x8(%esp)
 28f:	8d 55 e4             	lea    -0x1c(%ebp),%edx
 292:	89 54 24 04          	mov    %edx,0x4(%esp)
 296:	89 04 24             	mov    %eax,(%esp)
 299:	e8 fc ff ff ff       	call   29a <mutex_unlock+0x7a>
 29e:	89 45 ec             	mov    %eax,-0x14(%ebp)
 2a1:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 2a5:	79 10                	jns    2b7 <mutex_unlock+0x97>
 2a7:	8b 45 f4             	mov    -0xc(%ebp),%eax
 2aa:	8b 00                	mov    (%eax),%eax
 2ac:	89 45 e8             	mov    %eax,-0x18(%ebp)
 2af:	8b 45 e8             	mov    -0x18(%ebp),%eax
 2b2:	89 45 f4             	mov    %eax,-0xc(%ebp)
 2b5:	eb 29                	jmp    2e0 <mutex_unlock+0xc0>
 2b7:	8b 55 e4             	mov    -0x1c(%ebp),%edx
 2ba:	8b 45 e0             	mov    -0x20(%ebp),%eax
 2bd:	39 c2                	cmp    %eax,%edx
 2bf:	74 3f                	je     300 <mutex_unlock+0xe0>
 2c1:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 2c4:	85 c0                	test   %eax,%eax
 2c6:	79 08                	jns    2d0 <mutex_unlock+0xb0>
 2c8:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 2cb:	89 45 e0             	mov    %eax,-0x20(%ebp)
 2ce:	eb ab                	jmp    27b <mutex_unlock+0x5b>
 2d0:	8b 45 f4             	mov    -0xc(%ebp),%eax
 2d3:	8b 40 08             	mov    0x8(%eax),%eax
 2d6:	89 04 24             	mov    %eax,(%esp)
 2d9:	e8 fc ff ff ff       	call   2da <mutex_unlock+0xba>
 2de:	eb 9b                	jmp    27b <mutex_unlock+0x5b>
 2e0:	8b 45 f4             	mov    -0xc(%ebp),%eax
 2e3:	8b 55 08             	mov    0x8(%ebp),%edx
 2e6:	83 c2 04             	add    $0x4,%edx
 2e9:	39 d0                	cmp    %edx,%eax
 2eb:	75 80                	jne    26d <mutex_unlock+0x4d>
 2ed:	eb 01                	jmp    2f0 <mutex_unlock+0xd0>
 2ef:	90                   	nop
 2f0:	8b 45 08             	mov    0x8(%ebp),%eax
 2f3:	0f b6 00             	movzbl (%eax),%eax
 2f6:	84 c0                	test   %al,%al
 2f8:	0f 84 57 ff ff ff    	je     255 <mutex_unlock+0x35>
 2fe:	eb 01                	jmp    301 <mutex_unlock+0xe1>
 300:	90                   	nop
 301:	8b 45 08             	mov    0x8(%ebp),%eax
 304:	83 c0 01             	add    $0x1,%eax
 307:	89 04 24             	mov    %eax,(%esp)
 30a:	e8 fb fd ff ff       	call   10a <spin_unlock>
 30f:	b8 00 00 00 00       	mov    $0x0,%eax
 314:	c9                   	leave  
 315:	c3                   	ret    
