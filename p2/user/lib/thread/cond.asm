
cond.o:     file format elf32-i386


Disassembly of section .text:

00000000 <__list_add>:
   0:	55                   	push   %ebp
   1:	89 e5                	mov    %esp,%ebp
   3:	8b 45 10             	mov    0x10(%ebp),%eax
   6:	8b 55 08             	mov    0x8(%ebp),%edx
   9:	89 50 04             	mov    %edx,0x4(%eax)
   c:	8b 45 08             	mov    0x8(%ebp),%eax
   f:	8b 55 10             	mov    0x10(%ebp),%edx
  12:	89 10                	mov    %edx,(%eax)
  14:	8b 45 0c             	mov    0xc(%ebp),%eax
  17:	8b 55 08             	mov    0x8(%ebp),%edx
  1a:	89 10                	mov    %edx,(%eax)
  1c:	8b 45 08             	mov    0x8(%ebp),%eax
  1f:	8b 55 0c             	mov    0xc(%ebp),%edx
  22:	89 50 04             	mov    %edx,0x4(%eax)
  25:	5d                   	pop    %ebp
  26:	c3                   	ret    

00000027 <__list_del>:
  27:	55                   	push   %ebp
  28:	89 e5                	mov    %esp,%ebp
  2a:	8b 45 08             	mov    0x8(%ebp),%eax
  2d:	8b 55 0c             	mov    0xc(%ebp),%edx
  30:	89 10                	mov    %edx,(%eax)
  32:	8b 45 0c             	mov    0xc(%ebp),%eax
  35:	8b 55 08             	mov    0x8(%ebp),%edx
  38:	89 50 04             	mov    %edx,0x4(%eax)
  3b:	5d                   	pop    %ebp
  3c:	c3                   	ret    

0000003d <list_init_head>:
  3d:	55                   	push   %ebp
  3e:	89 e5                	mov    %esp,%ebp
  40:	8b 45 08             	mov    0x8(%ebp),%eax
  43:	8b 55 08             	mov    0x8(%ebp),%edx
  46:	89 10                	mov    %edx,(%eax)
  48:	8b 45 08             	mov    0x8(%ebp),%eax
  4b:	8b 55 08             	mov    0x8(%ebp),%edx
  4e:	89 50 04             	mov    %edx,0x4(%eax)
  51:	5d                   	pop    %ebp
  52:	c3                   	ret    

00000053 <list_add_tail>:
  53:	55                   	push   %ebp
  54:	89 e5                	mov    %esp,%ebp
  56:	83 ec 0c             	sub    $0xc,%esp
  59:	8b 45 0c             	mov    0xc(%ebp),%eax
  5c:	8b 40 04             	mov    0x4(%eax),%eax
  5f:	8b 55 0c             	mov    0xc(%ebp),%edx
  62:	89 54 24 08          	mov    %edx,0x8(%esp)
  66:	89 44 24 04          	mov    %eax,0x4(%esp)
  6a:	8b 45 08             	mov    0x8(%ebp),%eax
  6d:	89 04 24             	mov    %eax,(%esp)
  70:	e8 8b ff ff ff       	call   0 <__list_add>
  75:	c9                   	leave  
  76:	c3                   	ret    

00000077 <list_del>:
  77:	55                   	push   %ebp
  78:	89 e5                	mov    %esp,%ebp
  7a:	83 ec 08             	sub    $0x8,%esp
  7d:	8b 45 08             	mov    0x8(%ebp),%eax
  80:	8b 10                	mov    (%eax),%edx
  82:	8b 45 08             	mov    0x8(%ebp),%eax
  85:	8b 40 04             	mov    0x4(%eax),%eax
  88:	89 54 24 04          	mov    %edx,0x4(%esp)
  8c:	89 04 24             	mov    %eax,(%esp)
  8f:	e8 93 ff ff ff       	call   27 <__list_del>
  94:	c9                   	leave  
  95:	c3                   	ret    

00000096 <list_is_empty>:
  96:	55                   	push   %ebp
  97:	89 e5                	mov    %esp,%ebp
  99:	8b 45 08             	mov    0x8(%ebp),%eax
  9c:	8b 00                	mov    (%eax),%eax
  9e:	3b 45 08             	cmp    0x8(%ebp),%eax
  a1:	0f 94 c0             	sete   %al
  a4:	0f b6 c0             	movzbl %al,%eax
  a7:	5d                   	pop    %ebp
  a8:	c3                   	ret    

000000a9 <xchg>:
  a9:	55                   	push   %ebp
  aa:	89 e5                	mov    %esp,%ebp
  ac:	83 ec 04             	sub    $0x4,%esp
  af:	8b 45 0c             	mov    0xc(%ebp),%eax
  b2:	88 45 fc             	mov    %al,-0x4(%ebp)
  b5:	8b 55 08             	mov    0x8(%ebp),%edx
  b8:	0f b6 45 fc          	movzbl -0x4(%ebp),%eax
  bc:	f0 86 02             	lock xchg %al,(%edx)
  bf:	88 45 fc             	mov    %al,-0x4(%ebp)
  c2:	0f b6 45 fc          	movzbl -0x4(%ebp),%eax
  c6:	c9                   	leave  
  c7:	c3                   	ret    

000000c8 <spin_init>:
  c8:	55                   	push   %ebp
  c9:	89 e5                	mov    %esp,%ebp
  cb:	8b 45 08             	mov    0x8(%ebp),%eax
  ce:	c6 00 00             	movb   $0x0,(%eax)
  d1:	5d                   	pop    %ebp
  d2:	c3                   	ret    

000000d3 <spin_lock>:
  d3:	55                   	push   %ebp
  d4:	89 e5                	mov    %esp,%ebp
  d6:	83 ec 28             	sub    $0x28,%esp
  d9:	c6 45 f7 01          	movb   $0x1,-0x9(%ebp)
  dd:	0f be 45 f7          	movsbl -0x9(%ebp),%eax
  e1:	89 44 24 04          	mov    %eax,0x4(%esp)
  e5:	8b 45 08             	mov    0x8(%ebp),%eax
  e8:	89 04 24             	mov    %eax,(%esp)
  eb:	e8 b9 ff ff ff       	call   a9 <xchg>
  f0:	88 45 f7             	mov    %al,-0x9(%ebp)
  f3:	80 7d f7 01          	cmpb   $0x1,-0x9(%ebp)
  f7:	75 0e                	jne    107 <spin_lock+0x34>
  f9:	c7 04 24 ff ff ff ff 	movl   $0xffffffff,(%esp)
 100:	e8 fc ff ff ff       	call   101 <spin_lock+0x2e>
 105:	eb d6                	jmp    dd <spin_lock+0xa>
 107:	90                   	nop
 108:	c9                   	leave  
 109:	c3                   	ret    

0000010a <spin_try_lock>:
 10a:	55                   	push   %ebp
 10b:	89 e5                	mov    %esp,%ebp
 10d:	83 ec 18             	sub    $0x18,%esp
 110:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
 117:	00 
 118:	8b 45 08             	mov    0x8(%ebp),%eax
 11b:	89 04 24             	mov    %eax,(%esp)
 11e:	e8 86 ff ff ff       	call   a9 <xchg>
 123:	88 45 ff             	mov    %al,-0x1(%ebp)
 126:	80 7d ff 00          	cmpb   $0x0,-0x1(%ebp)
 12a:	0f 94 c0             	sete   %al
 12d:	0f b6 c0             	movzbl %al,%eax
 130:	c9                   	leave  
 131:	c3                   	ret    

00000132 <spin_unlock>:
 132:	55                   	push   %ebp
 133:	89 e5                	mov    %esp,%ebp
 135:	8b 45 08             	mov    0x8(%ebp),%eax
 138:	c6 00 00             	movb   $0x0,(%eax)
 13b:	5d                   	pop    %ebp
 13c:	c3                   	ret    

0000013d <cond_init>:
 13d:	55                   	push   %ebp
 13e:	89 e5                	mov    %esp,%ebp
 140:	83 ec 04             	sub    $0x4,%esp
 143:	8b 45 08             	mov    0x8(%ebp),%eax
 146:	89 04 24             	mov    %eax,(%esp)
 149:	e8 7a ff ff ff       	call   c8 <spin_init>
 14e:	8b 45 08             	mov    0x8(%ebp),%eax
 151:	83 c0 04             	add    $0x4,%eax
 154:	89 04 24             	mov    %eax,(%esp)
 157:	e8 e1 fe ff ff       	call   3d <list_init_head>
 15c:	b8 00 00 00 00       	mov    $0x0,%eax
 161:	c9                   	leave  
 162:	c3                   	ret    

00000163 <cond_destroy>:
 163:	55                   	push   %ebp
 164:	89 e5                	mov    %esp,%ebp
 166:	b8 00 00 00 00       	mov    $0x0,%eax
 16b:	5d                   	pop    %ebp
 16c:	c3                   	ret    

0000016d <cond_wait>:
 16d:	55                   	push   %ebp
 16e:	89 e5                	mov    %esp,%ebp
 170:	83 ec 28             	sub    $0x28,%esp
 173:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
 17a:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
 181:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 188:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 18f:	c6 45 f4 01          	movb   $0x1,-0xc(%ebp)
 193:	8b 45 08             	mov    0x8(%ebp),%eax
 196:	89 04 24             	mov    %eax,(%esp)
 199:	e8 35 ff ff ff       	call   d3 <spin_lock>
 19e:	8b 45 08             	mov    0x8(%ebp),%eax
 1a1:	83 c0 04             	add    $0x4,%eax
 1a4:	89 44 24 04          	mov    %eax,0x4(%esp)
 1a8:	8d 45 e8             	lea    -0x18(%ebp),%eax
 1ab:	89 04 24             	mov    %eax,(%esp)
 1ae:	e8 a0 fe ff ff       	call   53 <list_add_tail>
 1b3:	8b 45 08             	mov    0x8(%ebp),%eax
 1b6:	89 04 24             	mov    %eax,(%esp)
 1b9:	e8 74 ff ff ff       	call   132 <spin_unlock>
 1be:	8b 45 0c             	mov    0xc(%ebp),%eax
 1c1:	89 04 24             	mov    %eax,(%esp)
 1c4:	e8 fc ff ff ff       	call   1c5 <cond_wait+0x58>
 1c9:	eb 0c                	jmp    1d7 <cond_wait+0x6a>
 1cb:	c7 04 24 ff ff ff ff 	movl   $0xffffffff,(%esp)
 1d2:	e8 fc ff ff ff       	call   1d3 <cond_wait+0x66>
 1d7:	8d 45 e8             	lea    -0x18(%ebp),%eax
 1da:	83 c0 0c             	add    $0xc,%eax
 1dd:	89 04 24             	mov    %eax,(%esp)
 1e0:	e8 25 ff ff ff       	call   10a <spin_try_lock>
 1e5:	85 c0                	test   %eax,%eax
 1e7:	74 e2                	je     1cb <cond_wait+0x5e>
 1e9:	8b 45 0c             	mov    0xc(%ebp),%eax
 1ec:	89 04 24             	mov    %eax,(%esp)
 1ef:	e8 fc ff ff ff       	call   1f0 <cond_wait+0x83>
 1f4:	b8 00 00 00 00       	mov    $0x0,%eax
 1f9:	c9                   	leave  
 1fa:	c3                   	ret    

000001fb <cond_signal>:
 1fb:	55                   	push   %ebp
 1fc:	89 e5                	mov    %esp,%ebp
 1fe:	83 ec 28             	sub    $0x28,%esp
 201:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 208:	8b 45 08             	mov    0x8(%ebp),%eax
 20b:	89 04 24             	mov    %eax,(%esp)
 20e:	e8 c0 fe ff ff       	call   d3 <spin_lock>
 213:	8b 45 08             	mov    0x8(%ebp),%eax
 216:	83 c0 04             	add    $0x4,%eax
 219:	89 04 24             	mov    %eax,(%esp)
 21c:	e8 75 fe ff ff       	call   96 <list_is_empty>
 221:	85 c0                	test   %eax,%eax
 223:	75 2a                	jne    24f <cond_signal+0x54>
 225:	8b 45 08             	mov    0x8(%ebp),%eax
 228:	8b 40 04             	mov    0x4(%eax),%eax
 22b:	89 45 f0             	mov    %eax,-0x10(%ebp)
 22e:	8b 45 f0             	mov    -0x10(%ebp),%eax
 231:	89 45 f4             	mov    %eax,-0xc(%ebp)
 234:	8b 45 f4             	mov    -0xc(%ebp),%eax
 237:	89 04 24             	mov    %eax,(%esp)
 23a:	e8 38 fe ff ff       	call   77 <list_del>
 23f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 242:	83 c0 0c             	add    $0xc,%eax
 245:	89 04 24             	mov    %eax,(%esp)
 248:	e8 e5 fe ff ff       	call   132 <spin_unlock>
 24d:	eb 01                	jmp    250 <cond_signal+0x55>
 24f:	90                   	nop
 250:	8b 45 08             	mov    0x8(%ebp),%eax
 253:	89 04 24             	mov    %eax,(%esp)
 256:	e8 d7 fe ff ff       	call   132 <spin_unlock>
 25b:	b8 00 00 00 00       	mov    $0x0,%eax
 260:	c9                   	leave  
 261:	c3                   	ret    

00000262 <cond_broadcast>:
 262:	55                   	push   %ebp
 263:	89 e5                	mov    %esp,%ebp
 265:	83 ec 28             	sub    $0x28,%esp
 268:	8b 45 08             	mov    0x8(%ebp),%eax
 26b:	89 04 24             	mov    %eax,(%esp)
 26e:	e8 60 fe ff ff       	call   d3 <spin_lock>
 273:	eb 28                	jmp    29d <cond_broadcast+0x3b>
 275:	8b 45 08             	mov    0x8(%ebp),%eax
 278:	8b 40 04             	mov    0x4(%eax),%eax
 27b:	89 45 f4             	mov    %eax,-0xc(%ebp)
 27e:	8b 45 f4             	mov    -0xc(%ebp),%eax
 281:	89 45 f0             	mov    %eax,-0x10(%ebp)
 284:	8b 45 f0             	mov    -0x10(%ebp),%eax
 287:	89 04 24             	mov    %eax,(%esp)
 28a:	e8 e8 fd ff ff       	call   77 <list_del>
 28f:	8b 45 f0             	mov    -0x10(%ebp),%eax
 292:	83 c0 0c             	add    $0xc,%eax
 295:	89 04 24             	mov    %eax,(%esp)
 298:	e8 95 fe ff ff       	call   132 <spin_unlock>
 29d:	8b 45 08             	mov    0x8(%ebp),%eax
 2a0:	83 c0 04             	add    $0x4,%eax
 2a3:	89 04 24             	mov    %eax,(%esp)
 2a6:	e8 eb fd ff ff       	call   96 <list_is_empty>
 2ab:	85 c0                	test   %eax,%eax
 2ad:	74 c6                	je     275 <cond_broadcast+0x13>
 2af:	8b 45 08             	mov    0x8(%ebp),%eax
 2b2:	89 04 24             	mov    %eax,(%esp)
 2b5:	e8 78 fe ff ff       	call   132 <spin_unlock>
 2ba:	b8 00 00 00 00       	mov    $0x0,%eax
 2bf:	c9                   	leave  
 2c0:	c3                   	ret    
