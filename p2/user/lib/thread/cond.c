#include <cond.h>
#include <thread.h>
#include <cond_type.h>

#define DUMB_COND

struct cond_waiter_t {
    struct list_head_t list;
    int tid;
    spinlock_t wake_lock;
};

int cond_init( cond_t *cv )
{
    spin_init(&cv->wq_lock);
    list_init_head(&cv->waiter_queue);

    return 0;
}

int cond_destroy( cond_t *cv )
{
    // TODO
    return 0;
}

int cond_wait( cond_t *cv, mutex_t *mp )
{
#ifdef DUMB_COND
    struct cond_waiter_t waiter = {
        .wake_lock = SPIN_LOCKED,
    };

    spin_lock(&cv->wq_lock);
    list_add_tail(&waiter.list, &cv->waiter_queue);
    spin_unlock(&cv->wq_lock);

    mutex_unlock(mp);

    while (spin_try_lock(&waiter.wake_lock) == 0) {
        thr_yield(-1);
    }

    mutex_lock(mp);

    return 0;
#else
    int tid = gettid();
    int old_flag, expected_flag, ret;
    struct cond_waiter_t self = { .tid = tid, .status = -tid };

    spin_lock(&cv->wq_lock);
    mutex_unlock(mp);
    list_add_tail(&self.list, &cv->waiter_queue);
    spin_unlock(&cv->wq_lock);

    expected_flag = 0;
    while (1) {
        ret = cas_runflag(tid, &old_flag, &expected_flag, &self.status);

        if (ret >= 0) {
            if (old_flag != expected_flag) {
                if (old_flag >= 0) {
                    expected_flag = old_flag;
                }
            } else {
                break;
            }
        }
    }

    mutex_lock(mp);

    return 0;

#endif  // DUMB_COND
}

int cond_signal( cond_t *cv )
{
#ifdef DUMB_COND

    struct cond_waiter_t *waiter = NULL;

    spin_lock(&cv->wq_lock);

    if (list_is_empty(&cv->waiter_queue)) {
        goto end;
    }

    waiter = list_first_entry(&cv->waiter_queue, struct cond_waiter_t, list);
    list_del(&waiter->list);
    spin_unlock(&waiter->wake_lock);

end:
    spin_unlock(&cv->wq_lock);

    return 0;

#else
    int tid, old_flag, expected_flag, new_flag, ret;

    spin_lock(&cv->wq_lock);

    while (1) {
        struct cond_waiter_t *waiter = NULL;

        waiter = list_first_entry(&cv->waiter_queue, struct cond_waiter_t, list);
        list_del(&waiter->list);
        tid = waiter->tid;
        expected_flag = waiter->status;
        new_flag = 0;

        ret = cas_runflag(tid, &old_flag, &expected_flag, &new_flag);
        if (ret >= 0 && old_flag == expected_flag) {
            break;
        } else {
            list_add_tail(&waiter->list, &cv->waiter_queue);
            thr_yield(tid);
        }
    }

    spin_unlock(&cv->wq_lock);

    return 0;
#endif  // DUMB_COND
}

int cond_broadcast( cond_t *cv )
{
#ifdef DUMB_COND
    struct cond_waiter_t *waiter;

    spin_lock(&cv->wq_lock);
    
    while (!list_is_empty(&cv->waiter_queue)) {
        waiter = list_first_entry(&cv->waiter_queue, struct cond_waiter_t, list);
        list_del(&waiter->list);
        spin_unlock(&waiter->wake_lock);
    }

    spin_unlock(&cv->wq_lock);

    return 0;

#else
    int tid, old_flag, expected_flag, new_flag, ret;

    spin_lock(&cv->wq_lock);

    while (!list_is_empty(&cv->waiter_queue)) {
        struct cond_waiter_t *waiter = NULL;

        waiter = list_first_entry(&cv->waiter_queue, struct cond_waiter_t, list);
        list_del(&waiter->list);
        tid = waiter->tid;
        expected_flag = waiter->status;
        new_flag = 0;

        ret = cas_runflag(tid, &old_flag, &expected_flag, &new_flag);
        if (ret < 0 || old_flag != expected_flag) {
            list_add_tail(&waiter->list, &cv->waiter_queue);
            thr_yield(tid);
        }
    }

    spin_unlock(&cv->wq_lock);

    return 0;

#endif  // DUMB_COND
}
