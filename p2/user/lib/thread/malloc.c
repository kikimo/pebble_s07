/*
 * these functions should be thread safe.
 * It is up to you to rewrite them
 * to make them thread safe.
 *
 */

#include <stdlib.h>
#include <mutex.h>

extern mutex_t malloc_lock;

void *_malloc(size_t __size);
void *_calloc(size_t __nelt, size_t __eltsize);
void *_realloc(void *__buf, size_t __new_size);
void _free(void *__buf);

void *malloc(size_t __size)
{
    void *mem = NULL;

    mutex_lock(&malloc_lock);
    mem = _malloc(__size);
    mutex_unlock(&malloc_lock);

    return mem;
}

void *calloc(size_t __nelt, size_t __eltsize)
{
    void *mem = NULL;

    mutex_lock(&malloc_lock);
    mem = _calloc(__nelt, __eltsize);
    mutex_unlock(&malloc_lock);

    return mem;
}

void *realloc(void *__buf, size_t __new_size)
{
    void *mem = NULL;

    mutex_lock(&malloc_lock);
    mem = _realloc(__buf, __new_size);
    mutex_unlock(&malloc_lock);

    return mem;
}

void free(void *__buf)
{
    mutex_lock(&malloc_lock);
    _free(__buf);
    mutex_unlock(&malloc_lock);
}
