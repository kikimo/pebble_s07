#include <sem.h>
#include <mutex.h>
#include <cond.h>

int sem_init( sem_t *sem, int count )
{
    int ret = 0;

    if (count <= 0) {
        return -1;
    }

    sem->cnt = count;

    if ((ret = mutex_init(&sem->cnt_lock)) != 0) {
        return ret;
    }

    if ((ret = cond_init(&sem->cnt_cond)) != 0) {
        return ret;
    }

    return ret;
}

int sem_wait( sem_t *sem )
{
    mutex_lock(&sem->cnt_lock);

    while (sem->cnt <= 0) {
        cond_wait(&sem->cnt_cond, &sem->cnt_lock);
    }

    sem->cnt--;
    mutex_unlock(&sem->cnt_lock);

    return 0;
}

int sem_signal( sem_t *sem )
{
    mutex_lock(&sem->cnt_lock);
    sem->cnt++;
    mutex_unlock(&sem->cnt_lock);

    cond_signal(&sem->cnt_cond);

    return 0;
}

int sem_destroy( sem_t *sem )
{
    // TODO what should we do here?
    return 0;
}
