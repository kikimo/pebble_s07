#include <mutex.h>
#include <syscall.h>
#include <thread.h>
#include <thread_utils.h>
#include <stdio.h>

#define USE_DUMB 1

#define lprintf(...)

struct waiter_t {
    struct list_head_t list;
    int tid;    // thread id
};

int mutex_init( mutex_t *mp )
{
    mp->lock_val = MUTEX_UNLOCKED;
    spin_init(&mp->wq_lock);
    list_init_head(&mp->waiter_queue);

    return 0;
}

int mutex_destroy( mutex_t *mp )
{
    // TODO implementation
    if (mp->lock_val == MUTEX_LOCKED) {
        // TODO shit in the ass, throw error
    }

    if (mp->waiter_queue.next != &mp->waiter_queue) {
        // TODO shit in the ass, throw error
    }

    return 0;
}

int mutex_lock( mutex_t *mp )
{
    
#ifdef USE_DUMB
    int lock_val = MUTEX_LOCKED;

    while (xchg(&mp->lock_val, lock_val) == MUTEX_LOCKED) {
        thr_yield(-1);
    }

    return 0;

#else
    int tid = gettid();
    int old_flag, expected_flag = 0, new_flag = -1, ret;
    struct waiter_t waiter = { .tid = tid };

    lprintf("mutex_lock(): mutext %p start, tid: %d", mp, tid);

    spin_lock(&mp->wq_lock);
    list_add_tail(&waiter.list, &mp->waiter_queue);
    spin_unlock(&mp->wq_lock);

    lprintf("mutex_lock(): acquiring locked == %d, mp %p, tid: %d",
        mp->lock_val == MUTEX_LOCKED, mp, tid);

    while (xchg(&mp->lock_val, MUTEX_LOCKED) == MUTEX_LOCKED) {
        // goto sleep

        lprintf("mutex_lock(): try locked failed, mp %p, tid: %d, sleeping", mp, tid);

        ret = cas_runflag(tid, &old_flag, &expected_flag, &new_flag);
        if (ret < 0) {
            lprintf("mutex_lock(): error sleeping thread %d", tid);
        }

        lprintf("mutex_lock(): tid: %d, old: %d, expected: %d, new: %d",
            tid, old_flag, expected_flag, new_flag);
    }

    lprintf("mutex_lock(): lock of mp %p acquired", mp);

    spin_lock(&mp->wq_lock);
    list_del(&waiter.list);
    spin_unlock(&mp->wq_lock);

    lprintf("mutex_lock(): mutex %p end", mp);

    return 0;
#endif  // USE_DUMB

}

int mutex_unlock( mutex_t *mp )
{
#ifdef USE_DUMB

    mp->lock_val = MUTEX_UNLOCKED;

    return 0;

#else
    // int tid = gettid();
    int old_flag, expected_flag, new_flag, ret;

    lprintf("mutex_unlock(): start mp %p, tid %d", mp, tid);

    spin_lock(&mp->wq_lock);

    mp->lock_val = MUTEX_UNLOCKED;
    lprintf("mutex_unlock(): mp %p, free lock now", mp);

    if (list_is_empty(&mp->waiter_queue)) {
        lprintf("mutex_unlock(): no waiters, return now, mp %p", mp);
        goto end;
    }

    lprintf("mutex_unlock(): mp %p, waking waiters now", mp);
    while (mp->lock_val == MUTEX_UNLOCKED) {
        struct waiter_t *waiter = NULL;

        list_for_each_entry(waiter, &mp->waiter_queue, list) {
            lprintf("mutex_unlock(): waking thread %d", waiter->tid);
            expected_flag = -1;
            new_flag = 0;

again:    
            ret = cas_runflag(waiter->tid, &old_flag, &expected_flag, &new_flag);
            if (ret < 0) {
                lprintf("mutex_unlock(): error waking thread %d", waiter->tid);
                continue;
            }

            lprintf("mutex_unlock(): tid: %d, old: %d, expected: %d, new: %d",
                waiter->tid, old_flag, expected_flag, new_flag);

            if (old_flag != expected_flag) {
                if (old_flag < 0) {
                    expected_flag = old_flag;
                } else {
                    yield(waiter->tid);
                }

                goto again;
            }

            goto end;
        }
    }

end:
    spin_unlock(&mp->wq_lock);

    lprintf("mutex_unlock(): end, mp %p, tid %d", mp, tid);

    return 0;
#endif  // USE_DUMB
}
