
agility_drill:     file format elf32-i386


Disassembly of section .text:

01000000 <chase>:
 1000000:	55                   	push   %ebp
 1000001:	89 e5                	mov    %esp,%ebp
 1000003:	83 ec 28             	sub    $0x28,%esp
 1000006:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 100000d:	c7 45 f0 ff ff ff ff 	movl   $0xffffffff,-0x10(%ebp)
 1000014:	8b 45 08             	mov    0x8(%ebp),%eax
 1000017:	89 45 ec             	mov    %eax,-0x14(%ebp)
 100001a:	8b 45 ec             	mov    -0x14(%ebp),%eax
 100001d:	89 44 24 04          	mov    %eax,0x4(%esp)
 1000021:	c7 04 24 f8 2a 00 01 	movl   $0x1002af8,(%esp)
 1000028:	e8 78 1c 00 00       	call   1001ca5 <lprintf>
 100002d:	e9 a4 00 00 00       	jmp    10000d6 <chase+0xd6>
 1000032:	a1 00 40 00 01       	mov    0x1004000,%eax
 1000037:	39 45 f4             	cmp    %eax,-0xc(%ebp)
 100003a:	7d 38                	jge    1000074 <chase+0x74>
 100003c:	8b 0d 80 40 00 01    	mov    0x1004080,%ecx
 1000042:	8b 55 f4             	mov    -0xc(%ebp),%edx
 1000045:	89 d0                	mov    %edx,%eax
 1000047:	01 c0                	add    %eax,%eax
 1000049:	01 d0                	add    %edx,%eax
 100004b:	c1 e0 02             	shl    $0x2,%eax
 100004e:	01 c8                	add    %ecx,%eax
 1000050:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
 1000054:	89 04 24             	mov    %eax,(%esp)
 1000057:	e8 49 0c 00 00       	call   1000ca5 <mutex_lock>
 100005c:	85 c0                	test   %eax,%eax
 100005e:	74 14                	je     1000074 <chase+0x74>
 1000060:	c7 44 24 04 5f 00 00 	movl   $0x5f,0x4(%esp)
 1000067:	00 
 1000068:	c7 04 24 28 2b 00 01 	movl   $0x1002b28,(%esp)
 100006f:	e8 ec 24 00 00       	call   1002560 <panic>
 1000074:	8b 45 ec             	mov    -0x14(%ebp),%eax
 1000077:	83 e0 03             	and    $0x3,%eax
 100007a:	85 c0                	test   %eax,%eax
 100007c:	75 16                	jne    1000094 <chase+0x94>
 100007e:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1000081:	83 e0 03             	and    $0x3,%eax
 1000084:	85 c0                	test   %eax,%eax
 1000086:	75 0c                	jne    1000094 <chase+0x94>
 1000088:	c7 04 24 ff ff ff ff 	movl   $0xffffffff,(%esp)
 100008f:	e8 bc 29 00 00       	call   1002a50 <yield>
 1000094:	a1 08 40 00 01       	mov    0x1004008,%eax
 1000099:	39 45 f4             	cmp    %eax,-0xc(%ebp)
 100009c:	7c 38                	jl     10000d6 <chase+0xd6>
 100009e:	8b 0d 80 40 00 01    	mov    0x1004080,%ecx
 10000a4:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
 10000a8:	8b 55 f0             	mov    -0x10(%ebp),%edx
 10000ab:	89 d0                	mov    %edx,%eax
 10000ad:	01 c0                	add    %eax,%eax
 10000af:	01 d0                	add    %edx,%eax
 10000b1:	c1 e0 02             	shl    $0x2,%eax
 10000b4:	01 c8                	add    %ecx,%eax
 10000b6:	89 04 24             	mov    %eax,(%esp)
 10000b9:	e8 19 0c 00 00       	call   1000cd7 <mutex_unlock>
 10000be:	85 c0                	test   %eax,%eax
 10000c0:	74 14                	je     10000d6 <chase+0xd6>
 10000c2:	c7 44 24 04 69 00 00 	movl   $0x69,0x4(%esp)
 10000c9:	00 
 10000ca:	c7 04 24 90 2b 00 01 	movl   $0x1002b90,(%esp)
 10000d1:	e8 8a 24 00 00       	call   1002560 <panic>
 10000d6:	a1 00 40 00 01       	mov    0x1004000,%eax
 10000db:	83 e8 01             	sub    $0x1,%eax
 10000de:	3b 45 f0             	cmp    -0x10(%ebp),%eax
 10000e1:	0f 8f 4b ff ff ff    	jg     1000032 <chase+0x32>
 10000e7:	8b 45 ec             	mov    -0x14(%ebp),%eax
 10000ea:	89 44 24 04          	mov    %eax,0x4(%esp)
 10000ee:	c7 04 24 fc 2b 00 01 	movl   $0x1002bfc,(%esp)
 10000f5:	e8 ab 1b 00 00       	call   1001ca5 <lprintf>
 10000fa:	b8 01 00 00 00       	mov    $0x1,%eax
 10000ff:	c9                   	leave  
 1000100:	c3                   	ret    

01000101 <main>:
 1000101:	55                   	push   %ebp
 1000102:	89 e5                	mov    %esp,%ebp
 1000104:	83 e4 f0             	and    $0xfffffff0,%esp
 1000107:	83 ec 20             	sub    $0x20,%esp
 100010a:	8b 45 08             	mov    0x8(%ebp),%eax
 100010d:	83 f8 03             	cmp    $0x3,%eax
 1000110:	74 21                	je     1000133 <main+0x32>
 1000112:	83 f8 04             	cmp    $0x4,%eax
 1000115:	74 07                	je     100011e <main+0x1d>
 1000117:	83 f8 02             	cmp    $0x2,%eax
 100011a:	74 2c                	je     1000148 <main+0x47>
 100011c:	eb 41                	jmp    100015f <main+0x5e>
 100011e:	8b 45 0c             	mov    0xc(%ebp),%eax
 1000121:	83 c0 0c             	add    $0xc,%eax
 1000124:	8b 00                	mov    (%eax),%eax
 1000126:	89 04 24             	mov    %eax,(%esp)
 1000129:	e8 f6 23 00 00       	call   1002524 <atol>
 100012e:	a3 08 40 00 01       	mov    %eax,0x1004008
 1000133:	8b 45 0c             	mov    0xc(%ebp),%eax
 1000136:	83 c0 08             	add    $0x8,%eax
 1000139:	8b 00                	mov    (%eax),%eax
 100013b:	89 04 24             	mov    %eax,(%esp)
 100013e:	e8 e1 23 00 00       	call   1002524 <atol>
 1000143:	a3 04 40 00 01       	mov    %eax,0x1004004
 1000148:	8b 45 0c             	mov    0xc(%ebp),%eax
 100014b:	83 c0 04             	add    $0x4,%eax
 100014e:	8b 00                	mov    (%eax),%eax
 1000150:	89 04 24             	mov    %eax,(%esp)
 1000153:	e8 cc 23 00 00       	call   1002524 <atol>
 1000158:	a3 00 40 00 01       	mov    %eax,0x1004000
 100015d:	eb 00                	jmp    100015f <main+0x5e>
 100015f:	90                   	nop
 1000160:	83 7d 08 04          	cmpl   $0x4,0x8(%ebp)
 1000164:	7f 2c                	jg     1000192 <main+0x91>
 1000166:	a1 08 40 00 01       	mov    0x1004008,%eax
 100016b:	85 c0                	test   %eax,%eax
 100016d:	7e 23                	jle    1000192 <main+0x91>
 100016f:	a1 04 40 00 01       	mov    0x1004004,%eax
 1000174:	83 f8 06             	cmp    $0x6,%eax
 1000177:	7e 19                	jle    1000192 <main+0x91>
 1000179:	a1 00 40 00 01       	mov    0x1004000,%eax
 100017e:	83 f8 02             	cmp    $0x2,%eax
 1000181:	7e 0f                	jle    1000192 <main+0x91>
 1000183:	8b 15 00 40 00 01    	mov    0x1004000,%edx
 1000189:	a1 08 40 00 01       	mov    0x1004008,%eax
 100018e:	39 c2                	cmp    %eax,%edx
 1000190:	7f 4d                	jg     10001df <main+0xde>
 1000192:	8b 45 0c             	mov    0xc(%ebp),%eax
 1000195:	8b 00                	mov    (%eax),%eax
 1000197:	89 44 24 04          	mov    %eax,0x4(%esp)
 100019b:	c7 04 24 34 2c 00 01 	movl   $0x1002c34,(%esp)
 10001a2:	e8 cd 18 00 00       	call   1001a74 <printf>
 10001a7:	c7 04 24 7c 2c 00 01 	movl   $0x1002c7c,(%esp)
 10001ae:	e8 c1 18 00 00       	call   1001a74 <printf>
 10001b3:	c7 44 24 04 07 00 00 	movl   $0x7,0x4(%esp)
 10001ba:	00 
 10001bb:	c7 04 24 c8 2c 00 01 	movl   $0x1002cc8,(%esp)
 10001c2:	e8 ad 18 00 00       	call   1001a74 <printf>
 10001c7:	c7 04 24 18 2d 00 01 	movl   $0x1002d18,(%esp)
 10001ce:	e8 a1 18 00 00       	call   1001a74 <printf>
 10001d3:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
 10001da:	e8 69 23 00 00       	call   1002548 <exit>
 10001df:	c7 04 24 00 10 00 00 	movl   $0x1000,(%esp)
 10001e6:	e8 91 06 00 00       	call   100087c <thr_init>
 10001eb:	c7 04 24 60 40 00 01 	movl   $0x1004060,(%esp)
 10001f2:	e8 39 01 00 00       	call   1000330 <thrgrp_init_group>
 10001f7:	a1 00 40 00 01       	mov    0x1004000,%eax
 10001fc:	c7 44 24 04 0c 00 00 	movl   $0xc,0x4(%esp)
 1000203:	00 
 1000204:	89 04 24             	mov    %eax,(%esp)
 1000207:	e8 dc 03 00 00       	call   10005e8 <calloc>
 100020c:	a3 80 40 00 01       	mov    %eax,0x1004080
 1000211:	c7 44 24 1c 00 00 00 	movl   $0x0,0x1c(%esp)
 1000218:	00 
 1000219:	eb 3a                	jmp    1000255 <main+0x154>
 100021b:	8b 0d 80 40 00 01    	mov    0x1004080,%ecx
 1000221:	8b 54 24 1c          	mov    0x1c(%esp),%edx
 1000225:	89 d0                	mov    %edx,%eax
 1000227:	01 c0                	add    %eax,%eax
 1000229:	01 d0                	add    %edx,%eax
 100022b:	c1 e0 02             	shl    $0x2,%eax
 100022e:	01 c8                	add    %ecx,%eax
 1000230:	89 04 24             	mov    %eax,(%esp)
 1000233:	e8 34 0a 00 00       	call   1000c6c <mutex_init>
 1000238:	85 c0                	test   %eax,%eax
 100023a:	74 14                	je     1000250 <main+0x14f>
 100023c:	c7 44 24 04 98 00 00 	movl   $0x98,0x4(%esp)
 1000243:	00 
 1000244:	c7 04 24 64 2d 00 01 	movl   $0x1002d64,(%esp)
 100024b:	e8 10 23 00 00       	call   1002560 <panic>
 1000250:	83 44 24 1c 01       	addl   $0x1,0x1c(%esp)
 1000255:	a1 00 40 00 01       	mov    0x1004000,%eax
 100025a:	39 44 24 1c          	cmp    %eax,0x1c(%esp)
 100025e:	7c bb                	jl     100021b <main+0x11a>
 1000260:	c7 44 24 1c 00 00 00 	movl   $0x0,0x1c(%esp)
 1000267:	00 
 1000268:	eb 39                	jmp    10002a3 <main+0x1a2>
 100026a:	8b 44 24 1c          	mov    0x1c(%esp),%eax
 100026e:	89 44 24 08          	mov    %eax,0x8(%esp)
 1000272:	c7 44 24 04 00 00 00 	movl   $0x1000000,0x4(%esp)
 1000279:	01 
 100027a:	c7 04 24 60 40 00 01 	movl   $0x1004060,(%esp)
 1000281:	e8 0d 02 00 00       	call   1000493 <thrgrp_create>
 1000286:	85 c0                	test   %eax,%eax
 1000288:	79 14                	jns    100029e <main+0x19d>
 100028a:	c7 44 24 04 9b 00 00 	movl   $0x9b,0x4(%esp)
 1000291:	00 
 1000292:	c7 04 24 b8 2d 00 01 	movl   $0x1002db8,(%esp)
 1000299:	e8 c2 22 00 00       	call   1002560 <panic>
 100029e:	83 44 24 1c 01       	addl   $0x1,0x1c(%esp)
 10002a3:	a1 04 40 00 01       	mov    0x1004004,%eax
 10002a8:	39 44 24 1c          	cmp    %eax,0x1c(%esp)
 10002ac:	7c bc                	jl     100026a <main+0x169>
 10002ae:	c7 44 24 1c 00 00 00 	movl   $0x0,0x1c(%esp)
 10002b5:	00 
 10002b6:	eb 31                	jmp    10002e9 <main+0x1e8>
 10002b8:	8d 44 24 18          	lea    0x18(%esp),%eax
 10002bc:	89 44 24 04          	mov    %eax,0x4(%esp)
 10002c0:	c7 04 24 60 40 00 01 	movl   $0x1004060,(%esp)
 10002c7:	e8 2b 02 00 00       	call   10004f7 <thrgrp_join>
 10002cc:	85 c0                	test   %eax,%eax
 10002ce:	74 14                	je     10002e4 <main+0x1e3>
 10002d0:	c7 44 24 04 9e 00 00 	movl   $0x9e,0x4(%esp)
 10002d7:	00 
 10002d8:	c7 04 24 18 2e 00 01 	movl   $0x1002e18,(%esp)
 10002df:	e8 7c 22 00 00       	call   1002560 <panic>
 10002e4:	83 44 24 1c 01       	addl   $0x1,0x1c(%esp)
 10002e9:	a1 04 40 00 01       	mov    0x1004004,%eax
 10002ee:	39 44 24 1c          	cmp    %eax,0x1c(%esp)
 10002f2:	7c c4                	jl     10002b8 <main+0x1b7>
 10002f4:	a1 80 40 00 01       	mov    0x1004080,%eax
 10002f9:	89 04 24             	mov    %eax,(%esp)
 10002fc:	e8 65 03 00 00       	call   1000666 <free>
 1000301:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
 1000308:	e8 3b 22 00 00       	call   1002548 <exit>
 100030d:	90                   	nop
 100030e:	90                   	nop
 100030f:	90                   	nop

01000310 <_main>:
 1000310:	55                   	push   %ebp
 1000311:	89 e5                	mov    %esp,%ebp
 1000313:	83 ec 18             	sub    $0x18,%esp
 1000316:	8b 45 0c             	mov    0xc(%ebp),%eax
 1000319:	89 44 24 04          	mov    %eax,0x4(%esp)
 100031d:	8b 45 08             	mov    0x8(%ebp),%eax
 1000320:	89 04 24             	mov    %eax,(%esp)
 1000323:	e8 d9 fd ff ff       	call   1000101 <main>
 1000328:	89 04 24             	mov    %eax,(%esp)
 100032b:	e8 18 22 00 00       	call   1002548 <exit>

01000330 <thrgrp_init_group>:
 1000330:	55                   	push   %ebp
 1000331:	89 e5                	mov    %esp,%ebp
 1000333:	83 ec 28             	sub    $0x28,%esp
 1000336:	8b 45 08             	mov    0x8(%ebp),%eax
 1000339:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
 1000340:	8b 45 08             	mov    0x8(%ebp),%eax
 1000343:	c7 40 10 00 00 00 00 	movl   $0x0,0x10(%eax)
 100034a:	8b 45 08             	mov    0x8(%ebp),%eax
 100034d:	83 c0 14             	add    $0x14,%eax
 1000350:	89 04 24             	mov    %eax,(%esp)
 1000353:	e8 14 09 00 00       	call   1000c6c <mutex_init>
 1000358:	89 45 f4             	mov    %eax,-0xc(%ebp)
 100035b:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 100035f:	74 05                	je     1000366 <thrgrp_init_group+0x36>
 1000361:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000364:	eb 1e                	jmp    1000384 <thrgrp_init_group+0x54>
 1000366:	8b 45 08             	mov    0x8(%ebp),%eax
 1000369:	89 04 24             	mov    %eax,(%esp)
 100036c:	e8 ec 0a 00 00       	call   1000e5d <cond_init>
 1000371:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1000374:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 1000378:	74 05                	je     100037f <thrgrp_init_group+0x4f>
 100037a:	8b 45 f4             	mov    -0xc(%ebp),%eax
 100037d:	eb 05                	jmp    1000384 <thrgrp_init_group+0x54>
 100037f:	b8 00 00 00 00       	mov    $0x0,%eax
 1000384:	c9                   	leave  
 1000385:	c3                   	ret    

01000386 <thrgrp_destroy_group>:
 1000386:	55                   	push   %ebp
 1000387:	89 e5                	mov    %esp,%ebp
 1000389:	83 ec 28             	sub    $0x28,%esp
 100038c:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 1000393:	8b 45 08             	mov    0x8(%ebp),%eax
 1000396:	83 c0 14             	add    $0x14,%eax
 1000399:	89 04 24             	mov    %eax,(%esp)
 100039c:	e8 04 09 00 00       	call   1000ca5 <mutex_lock>
 10003a1:	8b 45 08             	mov    0x8(%ebp),%eax
 10003a4:	8b 40 0c             	mov    0xc(%eax),%eax
 10003a7:	85 c0                	test   %eax,%eax
 10003a9:	75 0a                	jne    10003b5 <thrgrp_destroy_group+0x2f>
 10003ab:	8b 45 08             	mov    0x8(%ebp),%eax
 10003ae:	8b 40 10             	mov    0x10(%eax),%eax
 10003b1:	85 c0                	test   %eax,%eax
 10003b3:	74 07                	je     10003bc <thrgrp_destroy_group+0x36>
 10003b5:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)
 10003bc:	8b 45 08             	mov    0x8(%ebp),%eax
 10003bf:	83 c0 14             	add    $0x14,%eax
 10003c2:	89 04 24             	mov    %eax,(%esp)
 10003c5:	e8 0d 09 00 00       	call   1000cd7 <mutex_unlock>
 10003ca:	8b 45 08             	mov    0x8(%ebp),%eax
 10003cd:	83 c0 14             	add    $0x14,%eax
 10003d0:	89 04 24             	mov    %eax,(%esp)
 10003d3:	e8 c3 08 00 00       	call   1000c9b <mutex_destroy>
 10003d8:	09 45 f4             	or     %eax,-0xc(%ebp)
 10003db:	8b 45 08             	mov    0x8(%ebp),%eax
 10003de:	89 04 24             	mov    %eax,(%esp)
 10003e1:	e8 9d 0a 00 00       	call   1000e83 <cond_destroy>
 10003e6:	09 45 f4             	or     %eax,-0xc(%ebp)
 10003e9:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10003ec:	c9                   	leave  
 10003ed:	c3                   	ret    

010003ee <thrgrp_bottom>:
 10003ee:	55                   	push   %ebp
 10003ef:	89 e5                	mov    %esp,%ebp
 10003f1:	83 ec 38             	sub    $0x38,%esp
 10003f4:	8b 45 08             	mov    0x8(%ebp),%eax
 10003f7:	89 45 f4             	mov    %eax,-0xc(%ebp)
 10003fa:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10003fd:	8b 00                	mov    (%eax),%eax
 10003ff:	89 45 f0             	mov    %eax,-0x10(%ebp)
 1000402:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000405:	8b 40 04             	mov    0x4(%eax),%eax
 1000408:	89 45 ec             	mov    %eax,-0x14(%ebp)
 100040b:	8b 45 f4             	mov    -0xc(%ebp),%eax
 100040e:	8b 40 08             	mov    0x8(%eax),%eax
 1000411:	89 45 e8             	mov    %eax,-0x18(%ebp)
 1000414:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000417:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
 100041d:	e8 df 07 00 00       	call   1000c01 <thr_getid>
 1000422:	8b 55 f4             	mov    -0xc(%ebp),%edx
 1000425:	89 42 04             	mov    %eax,0x4(%edx)
 1000428:	8b 45 ec             	mov    -0x14(%ebp),%eax
 100042b:	89 04 24             	mov    %eax,(%esp)
 100042e:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1000431:	ff d0                	call   *%eax
 1000433:	89 45 e4             	mov    %eax,-0x1c(%ebp)
 1000436:	8b 45 e8             	mov    -0x18(%ebp),%eax
 1000439:	83 c0 14             	add    $0x14,%eax
 100043c:	89 04 24             	mov    %eax,(%esp)
 100043f:	e8 61 08 00 00       	call   1000ca5 <mutex_lock>
 1000444:	8b 45 e8             	mov    -0x18(%ebp),%eax
 1000447:	8b 40 0c             	mov    0xc(%eax),%eax
 100044a:	85 c0                	test   %eax,%eax
 100044c:	74 0b                	je     1000459 <thrgrp_bottom+0x6b>
 100044e:	8b 45 e8             	mov    -0x18(%ebp),%eax
 1000451:	8b 40 0c             	mov    0xc(%eax),%eax
 1000454:	8b 55 f4             	mov    -0xc(%ebp),%edx
 1000457:	89 10                	mov    %edx,(%eax)
 1000459:	8b 55 f4             	mov    -0xc(%ebp),%edx
 100045c:	8b 45 e8             	mov    -0x18(%ebp),%eax
 100045f:	89 50 0c             	mov    %edx,0xc(%eax)
 1000462:	8b 45 e8             	mov    -0x18(%ebp),%eax
 1000465:	8b 40 10             	mov    0x10(%eax),%eax
 1000468:	85 c0                	test   %eax,%eax
 100046a:	75 09                	jne    1000475 <thrgrp_bottom+0x87>
 100046c:	8b 55 f4             	mov    -0xc(%ebp),%edx
 100046f:	8b 45 e8             	mov    -0x18(%ebp),%eax
 1000472:	89 50 10             	mov    %edx,0x10(%eax)
 1000475:	8b 45 e8             	mov    -0x18(%ebp),%eax
 1000478:	89 04 24             	mov    %eax,(%esp)
 100047b:	e8 9b 0a 00 00       	call   1000f1b <cond_signal>
 1000480:	8b 45 e8             	mov    -0x18(%ebp),%eax
 1000483:	83 c0 14             	add    $0x14,%eax
 1000486:	89 04 24             	mov    %eax,(%esp)
 1000489:	e8 49 08 00 00       	call   1000cd7 <mutex_unlock>
 100048e:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 1000491:	c9                   	leave  
 1000492:	c3                   	ret    

01000493 <thrgrp_create>:
 1000493:	55                   	push   %ebp
 1000494:	89 e5                	mov    %esp,%ebp
 1000496:	83 ec 28             	sub    $0x28,%esp
 1000499:	c7 04 24 0c 00 00 00 	movl   $0xc,(%esp)
 10004a0:	e8 0b 01 00 00       	call   10005b0 <malloc>
 10004a5:	89 45 f4             	mov    %eax,-0xc(%ebp)
 10004a8:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 10004ac:	75 07                	jne    10004b5 <thrgrp_create+0x22>
 10004ae:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 10004b3:	eb 40                	jmp    10004f5 <thrgrp_create+0x62>
 10004b5:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10004b8:	8b 55 0c             	mov    0xc(%ebp),%edx
 10004bb:	89 10                	mov    %edx,(%eax)
 10004bd:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10004c0:	8b 55 10             	mov    0x10(%ebp),%edx
 10004c3:	89 50 04             	mov    %edx,0x4(%eax)
 10004c6:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10004c9:	8b 55 08             	mov    0x8(%ebp),%edx
 10004cc:	89 50 08             	mov    %edx,0x8(%eax)
 10004cf:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10004d2:	89 44 24 04          	mov    %eax,0x4(%esp)
 10004d6:	c7 04 24 ee 03 00 01 	movl   $0x10003ee,(%esp)
 10004dd:	e8 45 04 00 00       	call   1000927 <thr_create>
 10004e2:	89 45 f0             	mov    %eax,-0x10(%ebp)
 10004e5:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 10004e9:	79 05                	jns    10004f0 <thrgrp_create+0x5d>
 10004eb:	8b 45 f0             	mov    -0x10(%ebp),%eax
 10004ee:	eb 05                	jmp    10004f5 <thrgrp_create+0x62>
 10004f0:	b8 00 00 00 00       	mov    $0x0,%eax
 10004f5:	c9                   	leave  
 10004f6:	c3                   	ret    

010004f7 <thrgrp_join>:
 10004f7:	55                   	push   %ebp
 10004f8:	89 e5                	mov    %esp,%ebp
 10004fa:	83 ec 28             	sub    $0x28,%esp
 10004fd:	8b 45 08             	mov    0x8(%ebp),%eax
 1000500:	83 c0 14             	add    $0x14,%eax
 1000503:	89 04 24             	mov    %eax,(%esp)
 1000506:	e8 9a 07 00 00       	call   1000ca5 <mutex_lock>
 100050b:	eb 25                	jmp    1000532 <thrgrp_join+0x3b>
 100050d:	8b 45 08             	mov    0x8(%ebp),%eax
 1000510:	8d 50 14             	lea    0x14(%eax),%edx
 1000513:	8b 45 08             	mov    0x8(%ebp),%eax
 1000516:	89 54 24 04          	mov    %edx,0x4(%esp)
 100051a:	89 04 24             	mov    %eax,(%esp)
 100051d:	e8 6b 09 00 00       	call   1000e8d <cond_wait>
 1000522:	85 c0                	test   %eax,%eax
 1000524:	74 0c                	je     1000532 <thrgrp_join+0x3b>
 1000526:	c7 04 24 78 2e 00 01 	movl   $0x1002e78,(%esp)
 100052d:	e8 2e 20 00 00       	call   1002560 <panic>
 1000532:	8b 45 08             	mov    0x8(%ebp),%eax
 1000535:	8b 40 10             	mov    0x10(%eax),%eax
 1000538:	85 c0                	test   %eax,%eax
 100053a:	74 d1                	je     100050d <thrgrp_join+0x16>
 100053c:	8b 45 08             	mov    0x8(%ebp),%eax
 100053f:	8b 40 10             	mov    0x10(%eax),%eax
 1000542:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1000545:	8b 45 08             	mov    0x8(%ebp),%eax
 1000548:	8b 50 10             	mov    0x10(%eax),%edx
 100054b:	8b 45 08             	mov    0x8(%ebp),%eax
 100054e:	8b 40 0c             	mov    0xc(%eax),%eax
 1000551:	39 c2                	cmp    %eax,%edx
 1000553:	75 16                	jne    100056b <thrgrp_join+0x74>
 1000555:	8b 45 08             	mov    0x8(%ebp),%eax
 1000558:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
 100055f:	8b 45 08             	mov    0x8(%ebp),%eax
 1000562:	c7 40 10 00 00 00 00 	movl   $0x0,0x10(%eax)
 1000569:	eb 0e                	jmp    1000579 <thrgrp_join+0x82>
 100056b:	8b 45 08             	mov    0x8(%ebp),%eax
 100056e:	8b 40 10             	mov    0x10(%eax),%eax
 1000571:	8b 10                	mov    (%eax),%edx
 1000573:	8b 45 08             	mov    0x8(%ebp),%eax
 1000576:	89 50 10             	mov    %edx,0x10(%eax)
 1000579:	8b 45 08             	mov    0x8(%ebp),%eax
 100057c:	83 c0 14             	add    $0x14,%eax
 100057f:	89 04 24             	mov    %eax,(%esp)
 1000582:	e8 50 07 00 00       	call   1000cd7 <mutex_unlock>
 1000587:	8b 45 f4             	mov    -0xc(%ebp),%eax
 100058a:	8b 40 04             	mov    0x4(%eax),%eax
 100058d:	89 45 f0             	mov    %eax,-0x10(%ebp)
 1000590:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000593:	89 04 24             	mov    %eax,(%esp)
 1000596:	e8 cb 00 00 00       	call   1000666 <free>
 100059b:	8b 45 0c             	mov    0xc(%ebp),%eax
 100059e:	89 44 24 04          	mov    %eax,0x4(%esp)
 10005a2:	8b 45 f0             	mov    -0x10(%ebp),%eax
 10005a5:	89 04 24             	mov    %eax,(%esp)
 10005a8:	e8 da 04 00 00       	call   1000a87 <thr_join>
 10005ad:	c9                   	leave  
 10005ae:	c3                   	ret    
 10005af:	90                   	nop

010005b0 <malloc>:
 10005b0:	55                   	push   %ebp
 10005b1:	89 e5                	mov    %esp,%ebp
 10005b3:	83 ec 28             	sub    $0x28,%esp
 10005b6:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 10005bd:	c7 04 24 84 40 00 01 	movl   $0x1004084,(%esp)
 10005c4:	e8 dc 06 00 00       	call   1000ca5 <mutex_lock>
 10005c9:	8b 45 08             	mov    0x8(%ebp),%eax
 10005cc:	89 04 24             	mov    %eax,(%esp)
 10005cf:	e8 10 0a 00 00       	call   1000fe4 <_malloc>
 10005d4:	89 45 f4             	mov    %eax,-0xc(%ebp)
 10005d7:	c7 04 24 84 40 00 01 	movl   $0x1004084,(%esp)
 10005de:	e8 f4 06 00 00       	call   1000cd7 <mutex_unlock>
 10005e3:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10005e6:	c9                   	leave  
 10005e7:	c3                   	ret    

010005e8 <calloc>:
 10005e8:	55                   	push   %ebp
 10005e9:	89 e5                	mov    %esp,%ebp
 10005eb:	83 ec 28             	sub    $0x28,%esp
 10005ee:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 10005f5:	c7 04 24 84 40 00 01 	movl   $0x1004084,(%esp)
 10005fc:	e8 a4 06 00 00       	call   1000ca5 <mutex_lock>
 1000601:	8b 45 0c             	mov    0xc(%ebp),%eax
 1000604:	89 44 24 04          	mov    %eax,0x4(%esp)
 1000608:	8b 45 08             	mov    0x8(%ebp),%eax
 100060b:	89 04 24             	mov    %eax,(%esp)
 100060e:	e8 07 0a 00 00       	call   100101a <_calloc>
 1000613:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1000616:	c7 04 24 84 40 00 01 	movl   $0x1004084,(%esp)
 100061d:	e8 b5 06 00 00       	call   1000cd7 <mutex_unlock>
 1000622:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000625:	c9                   	leave  
 1000626:	c3                   	ret    

01000627 <realloc>:
 1000627:	55                   	push   %ebp
 1000628:	89 e5                	mov    %esp,%ebp
 100062a:	83 ec 28             	sub    $0x28,%esp
 100062d:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 1000634:	c7 04 24 84 40 00 01 	movl   $0x1004084,(%esp)
 100063b:	e8 65 06 00 00       	call   1000ca5 <mutex_lock>
 1000640:	8b 45 0c             	mov    0xc(%ebp),%eax
 1000643:	89 44 24 04          	mov    %eax,0x4(%esp)
 1000647:	8b 45 08             	mov    0x8(%ebp),%eax
 100064a:	89 04 24             	mov    %eax,(%esp)
 100064d:	e8 2b 0a 00 00       	call   100107d <_realloc>
 1000652:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1000655:	c7 04 24 84 40 00 01 	movl   $0x1004084,(%esp)
 100065c:	e8 76 06 00 00       	call   1000cd7 <mutex_unlock>
 1000661:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000664:	c9                   	leave  
 1000665:	c3                   	ret    

01000666 <free>:
 1000666:	55                   	push   %ebp
 1000667:	89 e5                	mov    %esp,%ebp
 1000669:	83 ec 18             	sub    $0x18,%esp
 100066c:	c7 04 24 84 40 00 01 	movl   $0x1004084,(%esp)
 1000673:	e8 2d 06 00 00       	call   1000ca5 <mutex_lock>
 1000678:	8b 45 08             	mov    0x8(%ebp),%eax
 100067b:	89 04 24             	mov    %eax,(%esp)
 100067e:	e8 24 0a 00 00       	call   10010a7 <_free>
 1000683:	c7 04 24 84 40 00 01 	movl   $0x1004084,(%esp)
 100068a:	e8 48 06 00 00       	call   1000cd7 <mutex_unlock>
 100068f:	c9                   	leave  
 1000690:	c3                   	ret    
 1000691:	90                   	nop
 1000692:	90                   	nop
 1000693:	90                   	nop

01000694 <__list_add>:
 1000694:	55                   	push   %ebp
 1000695:	89 e5                	mov    %esp,%ebp
 1000697:	8b 45 10             	mov    0x10(%ebp),%eax
 100069a:	8b 55 08             	mov    0x8(%ebp),%edx
 100069d:	89 50 04             	mov    %edx,0x4(%eax)
 10006a0:	8b 45 08             	mov    0x8(%ebp),%eax
 10006a3:	8b 55 10             	mov    0x10(%ebp),%edx
 10006a6:	89 10                	mov    %edx,(%eax)
 10006a8:	8b 45 0c             	mov    0xc(%ebp),%eax
 10006ab:	8b 55 08             	mov    0x8(%ebp),%edx
 10006ae:	89 10                	mov    %edx,(%eax)
 10006b0:	8b 45 08             	mov    0x8(%ebp),%eax
 10006b3:	8b 55 0c             	mov    0xc(%ebp),%edx
 10006b6:	89 50 04             	mov    %edx,0x4(%eax)
 10006b9:	5d                   	pop    %ebp
 10006ba:	c3                   	ret    

010006bb <__list_del>:
 10006bb:	55                   	push   %ebp
 10006bc:	89 e5                	mov    %esp,%ebp
 10006be:	8b 45 08             	mov    0x8(%ebp),%eax
 10006c1:	8b 55 0c             	mov    0xc(%ebp),%edx
 10006c4:	89 10                	mov    %edx,(%eax)
 10006c6:	8b 45 0c             	mov    0xc(%ebp),%eax
 10006c9:	8b 55 08             	mov    0x8(%ebp),%edx
 10006cc:	89 50 04             	mov    %edx,0x4(%eax)
 10006cf:	5d                   	pop    %ebp
 10006d0:	c3                   	ret    

010006d1 <list_add>:
 10006d1:	55                   	push   %ebp
 10006d2:	89 e5                	mov    %esp,%ebp
 10006d4:	83 ec 0c             	sub    $0xc,%esp
 10006d7:	8b 45 0c             	mov    0xc(%ebp),%eax
 10006da:	8b 00                	mov    (%eax),%eax
 10006dc:	89 44 24 08          	mov    %eax,0x8(%esp)
 10006e0:	8b 45 0c             	mov    0xc(%ebp),%eax
 10006e3:	89 44 24 04          	mov    %eax,0x4(%esp)
 10006e7:	8b 45 08             	mov    0x8(%ebp),%eax
 10006ea:	89 04 24             	mov    %eax,(%esp)
 10006ed:	e8 a2 ff ff ff       	call   1000694 <__list_add>
 10006f2:	c9                   	leave  
 10006f3:	c3                   	ret    

010006f4 <list_del>:
 10006f4:	55                   	push   %ebp
 10006f5:	89 e5                	mov    %esp,%ebp
 10006f7:	83 ec 08             	sub    $0x8,%esp
 10006fa:	8b 45 08             	mov    0x8(%ebp),%eax
 10006fd:	8b 10                	mov    (%eax),%edx
 10006ff:	8b 45 08             	mov    0x8(%ebp),%eax
 1000702:	8b 40 04             	mov    0x4(%eax),%eax
 1000705:	89 54 24 04          	mov    %edx,0x4(%esp)
 1000709:	89 04 24             	mov    %eax,(%esp)
 100070c:	e8 aa ff ff ff       	call   10006bb <__list_del>
 1000711:	c9                   	leave  
 1000712:	c3                   	ret    

01000713 <xchg>:
 1000713:	55                   	push   %ebp
 1000714:	89 e5                	mov    %esp,%ebp
 1000716:	83 ec 04             	sub    $0x4,%esp
 1000719:	8b 45 0c             	mov    0xc(%ebp),%eax
 100071c:	88 45 fc             	mov    %al,-0x4(%ebp)
 100071f:	8b 55 08             	mov    0x8(%ebp),%edx
 1000722:	0f b6 45 fc          	movzbl -0x4(%ebp),%eax
 1000726:	f0 86 02             	lock xchg %al,(%edx)
 1000729:	88 45 fc             	mov    %al,-0x4(%ebp)
 100072c:	0f b6 45 fc          	movzbl -0x4(%ebp),%eax
 1000730:	c9                   	leave  
 1000731:	c3                   	ret    

01000732 <spin_lock>:
 1000732:	55                   	push   %ebp
 1000733:	89 e5                	mov    %esp,%ebp
 1000735:	83 ec 28             	sub    $0x28,%esp
 1000738:	c6 45 f7 01          	movb   $0x1,-0x9(%ebp)
 100073c:	0f be 45 f7          	movsbl -0x9(%ebp),%eax
 1000740:	89 44 24 04          	mov    %eax,0x4(%esp)
 1000744:	8b 45 08             	mov    0x8(%ebp),%eax
 1000747:	89 04 24             	mov    %eax,(%esp)
 100074a:	e8 c4 ff ff ff       	call   1000713 <xchg>
 100074f:	88 45 f7             	mov    %al,-0x9(%ebp)
 1000752:	80 7d f7 01          	cmpb   $0x1,-0x9(%ebp)
 1000756:	75 0e                	jne    1000766 <spin_lock+0x34>
 1000758:	c7 04 24 ff ff ff ff 	movl   $0xffffffff,(%esp)
 100075f:	e8 ec 22 00 00       	call   1002a50 <yield>
 1000764:	eb d6                	jmp    100073c <spin_lock+0xa>
 1000766:	90                   	nop
 1000767:	c9                   	leave  
 1000768:	c3                   	ret    

01000769 <spin_try_lock>:
 1000769:	55                   	push   %ebp
 100076a:	89 e5                	mov    %esp,%ebp
 100076c:	83 ec 18             	sub    $0x18,%esp
 100076f:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
 1000776:	00 
 1000777:	8b 45 08             	mov    0x8(%ebp),%eax
 100077a:	89 04 24             	mov    %eax,(%esp)
 100077d:	e8 91 ff ff ff       	call   1000713 <xchg>
 1000782:	88 45 ff             	mov    %al,-0x1(%ebp)
 1000785:	80 7d ff 00          	cmpb   $0x0,-0x1(%ebp)
 1000789:	0f 94 c0             	sete   %al
 100078c:	0f b6 c0             	movzbl %al,%eax
 100078f:	c9                   	leave  
 1000790:	c3                   	ret    

01000791 <spin_unlock>:
 1000791:	55                   	push   %ebp
 1000792:	89 e5                	mov    %esp,%ebp
 1000794:	8b 45 08             	mov    0x8(%ebp),%eax
 1000797:	c6 00 00             	movb   $0x0,(%eax)
 100079a:	5d                   	pop    %ebp
 100079b:	c3                   	ret    

0100079c <last_ebp>:
 100079c:	55                   	push   %ebp
 100079d:	89 e5                	mov    %esp,%ebp
 100079f:	83 ec 10             	sub    $0x10,%esp
 10007a2:	89 6d f8             	mov    %ebp,-0x8(%ebp)
 10007a5:	eb 06                	jmp    10007ad <last_ebp+0x11>
 10007a7:	8b 45 fc             	mov    -0x4(%ebp),%eax
 10007aa:	89 45 f8             	mov    %eax,-0x8(%ebp)
 10007ad:	8b 45 f8             	mov    -0x8(%ebp),%eax
 10007b0:	8b 00                	mov    (%eax),%eax
 10007b2:	89 45 fc             	mov    %eax,-0x4(%ebp)
 10007b5:	83 7d fc 00          	cmpl   $0x0,-0x4(%ebp)
 10007b9:	75 ec                	jne    10007a7 <last_ebp+0xb>
 10007bb:	8b 45 f8             	mov    -0x8(%ebp),%eax
 10007be:	c9                   	leave  
 10007bf:	c3                   	ret    

010007c0 <get_thread_local>:
 10007c0:	55                   	push   %ebp
 10007c1:	89 e5                	mov    %esp,%ebp
 10007c3:	83 ec 10             	sub    $0x10,%esp
 10007c6:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 10007cd:	e8 ca ff ff ff       	call   100079c <last_ebp>
 10007d2:	89 45 fc             	mov    %eax,-0x4(%ebp)
 10007d5:	8b 45 fc             	mov    -0x4(%ebp),%eax
 10007d8:	8b 40 04             	mov    0x4(%eax),%eax
 10007db:	3d af be ad de       	cmp    $0xdeadbeaf,%eax
 10007e0:	74 07                	je     10007e9 <get_thread_local+0x29>
 10007e2:	b8 00 00 00 00       	mov    $0x0,%eax
 10007e7:	eb 03                	jmp    10007ec <get_thread_local+0x2c>
 10007e9:	8b 45 fc             	mov    -0x4(%ebp),%eax
 10007ec:	c9                   	leave  
 10007ed:	c3                   	ret    

010007ee <current_thread>:
 10007ee:	55                   	push   %ebp
 10007ef:	89 e5                	mov    %esp,%ebp
 10007f1:	83 ec 10             	sub    $0x10,%esp
 10007f4:	e8 c7 ff ff ff       	call   10007c0 <get_thread_local>
 10007f9:	89 45 fc             	mov    %eax,-0x4(%ebp)
 10007fc:	8b 45 fc             	mov    -0x4(%ebp),%eax
 10007ff:	83 c0 08             	add    $0x8,%eax
 1000802:	c9                   	leave  
 1000803:	c3                   	ret    

01000804 <thr_init_thread>:
 1000804:	55                   	push   %ebp
 1000805:	89 e5                	mov    %esp,%ebp
 1000807:	83 ec 18             	sub    $0x18,%esp
 100080a:	8b 45 08             	mov    0x8(%ebp),%eax
 100080d:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
 1000814:	8b 45 08             	mov    0x8(%ebp),%eax
 1000817:	c6 40 0c 00          	movb   $0x0,0xc(%eax)
 100081b:	8b 45 08             	mov    0x8(%ebp),%eax
 100081e:	c6 40 0f 00          	movb   $0x0,0xf(%eax)
 1000822:	8b 45 08             	mov    0x8(%ebp),%eax
 1000825:	c6 40 0d 01          	movb   $0x1,0xd(%eax)
 1000829:	8b 45 08             	mov    0x8(%ebp),%eax
 100082c:	c6 40 0e 01          	movb   $0x1,0xe(%eax)
 1000830:	8b 45 08             	mov    0x8(%ebp),%eax
 1000833:	83 c0 10             	add    $0x10,%eax
 1000836:	89 04 24             	mov    %eax,(%esp)
 1000839:	e8 2e 04 00 00       	call   1000c6c <mutex_init>
 100083e:	8b 45 08             	mov    0x8(%ebp),%eax
 1000841:	83 c0 10             	add    $0x10,%eax
 1000844:	89 04 24             	mov    %eax,(%esp)
 1000847:	e8 59 04 00 00       	call   1000ca5 <mutex_lock>
 100084c:	c7 04 24 40 40 00 01 	movl   $0x1004040,(%esp)
 1000853:	e8 da fe ff ff       	call   1000732 <spin_lock>
 1000858:	8b 45 08             	mov    0x8(%ebp),%eax
 100085b:	83 c0 1c             	add    $0x1c,%eax
 100085e:	c7 44 24 04 0c 40 00 	movl   $0x100400c,0x4(%esp)
 1000865:	01 
 1000866:	89 04 24             	mov    %eax,(%esp)
 1000869:	e8 63 fe ff ff       	call   10006d1 <list_add>
 100086e:	c7 04 24 40 40 00 01 	movl   $0x1004040,(%esp)
 1000875:	e8 17 ff ff ff       	call   1000791 <spin_unlock>
 100087a:	c9                   	leave  
 100087b:	c3                   	ret    

0100087c <thr_init>:
 100087c:	55                   	push   %ebp
 100087d:	89 e5                	mov    %esp,%ebp
 100087f:	83 ec 28             	sub    $0x28,%esp
 1000882:	c7 04 24 84 40 00 01 	movl   $0x1004084,(%esp)
 1000889:	e8 de 03 00 00       	call   1000c6c <mutex_init>
 100088e:	8b 45 08             	mov    0x8(%ebp),%eax
 1000891:	a3 90 40 00 01       	mov    %eax,0x1004090
 1000896:	a1 90 40 00 01       	mov    0x1004090,%eax
 100089b:	3d ff 0f 00 00       	cmp    $0xfff,%eax
 10008a0:	77 0a                	ja     10008ac <thr_init+0x30>
 10008a2:	c7 05 90 40 00 01 00 	movl   $0x1000,0x1004090
 10008a9:	10 00 00 
 10008ac:	a1 90 40 00 01       	mov    0x1004090,%eax
 10008b1:	83 e0 fc             	and    $0xfffffffc,%eax
 10008b4:	a3 90 40 00 01       	mov    %eax,0x1004090
 10008b9:	c7 04 24 2c 00 00 00 	movl   $0x2c,(%esp)
 10008c0:	e8 eb fc ff ff       	call   10005b0 <malloc>
 10008c5:	89 45 f4             	mov    %eax,-0xc(%ebp)
 10008c8:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 10008cc:	75 07                	jne    10008d5 <thr_init+0x59>
 10008ce:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 10008d3:	eb 50                	jmp    1000925 <thr_init+0xa9>
 10008d5:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10008d8:	c7 40 04 af be ad de 	movl   $0xdeadbeaf,0x4(%eax)
 10008df:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10008e2:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
 10008e8:	e8 af fe ff ff       	call   100079c <last_ebp>
 10008ed:	89 45 f0             	mov    %eax,-0x10(%ebp)
 10008f0:	8b 45 f0             	mov    -0x10(%ebp),%eax
 10008f3:	8b 55 f4             	mov    -0xc(%ebp),%edx
 10008f6:	89 10                	mov    %edx,(%eax)
 10008f8:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10008fb:	83 c0 08             	add    $0x8,%eax
 10008fe:	89 45 ec             	mov    %eax,-0x14(%ebp)
 1000901:	e8 82 21 00 00       	call   1002a88 <gettid>
 1000906:	8b 55 ec             	mov    -0x14(%ebp),%edx
 1000909:	89 02                	mov    %eax,(%edx)
 100090b:	8b 45 ec             	mov    -0x14(%ebp),%eax
 100090e:	c7 40 04 00 00 00 00 	movl   $0x0,0x4(%eax)
 1000915:	8b 45 ec             	mov    -0x14(%ebp),%eax
 1000918:	89 04 24             	mov    %eax,(%esp)
 100091b:	e8 e4 fe ff ff       	call   1000804 <thr_init_thread>
 1000920:	b8 00 00 00 00       	mov    $0x0,%eax
 1000925:	c9                   	leave  
 1000926:	c3                   	ret    

01000927 <thr_create>:
 1000927:	55                   	push   %ebp
 1000928:	89 e5                	mov    %esp,%ebp
 100092a:	83 ec 38             	sub    $0x38,%esp
 100092d:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 1000934:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 100093b:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
 1000942:	a1 90 40 00 01       	mov    0x1004090,%eax
 1000947:	89 04 24             	mov    %eax,(%esp)
 100094a:	e8 61 fc ff ff       	call   10005b0 <malloc>
 100094f:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1000952:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 1000956:	75 0a                	jne    1000962 <thr_create+0x3b>
 1000958:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 100095d:	e9 f4 00 00 00       	jmp    1000a56 <thr_create+0x12f>
 1000962:	a1 90 40 00 01       	mov    0x1004090,%eax
 1000967:	83 e8 04             	sub    $0x4,%eax
 100096a:	03 45 f4             	add    -0xc(%ebp),%eax
 100096d:	89 45 e8             	mov    %eax,-0x18(%ebp)
 1000970:	8b 45 e8             	mov    -0x18(%ebp),%eax
 1000973:	83 c0 04             	add    $0x4,%eax
 1000976:	c7 00 af be cd ab    	movl   $0xabcdbeaf,(%eax)
 100097c:	c7 04 24 2c 00 00 00 	movl   $0x2c,(%esp)
 1000983:	e8 28 fc ff ff       	call   10005b0 <malloc>
 1000988:	89 45 f0             	mov    %eax,-0x10(%ebp)
 100098b:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 100098f:	75 0a                	jne    100099b <thr_create+0x74>
 1000991:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 1000996:	e9 bb 00 00 00       	jmp    1000a56 <thr_create+0x12f>
 100099b:	8b 45 f0             	mov    -0x10(%ebp),%eax
 100099e:	c7 40 04 af be ad de 	movl   $0xdeadbeaf,0x4(%eax)
 10009a5:	8b 45 f0             	mov    -0x10(%ebp),%eax
 10009a8:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
 10009ae:	8b 45 f0             	mov    -0x10(%ebp),%eax
 10009b1:	83 c0 08             	add    $0x8,%eax
 10009b4:	89 45 ec             	mov    %eax,-0x14(%ebp)
 10009b7:	8b 45 e8             	mov    -0x18(%ebp),%eax
 10009ba:	8b 55 f0             	mov    -0x10(%ebp),%edx
 10009bd:	89 10                	mov    %edx,(%eax)
 10009bf:	8b 45 e8             	mov    -0x18(%ebp),%eax
 10009c2:	83 e8 0c             	sub    $0xc,%eax
 10009c5:	89 45 e4             	mov    %eax,-0x1c(%ebp)
 10009c8:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 10009cb:	8b 55 08             	mov    0x8(%ebp),%edx
 10009ce:	89 10                	mov    %edx,(%eax)
 10009d0:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 10009d3:	8d 50 04             	lea    0x4(%eax),%edx
 10009d6:	8b 45 0c             	mov    0xc(%ebp),%eax
 10009d9:	89 02                	mov    %eax,(%edx)
 10009db:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 10009de:	8d 50 08             	lea    0x8(%eax),%edx
 10009e1:	8b 45 ec             	mov    -0x14(%ebp),%eax
 10009e4:	89 02                	mov    %eax,(%edx)
 10009e6:	8b 45 e8             	mov    -0x18(%ebp),%eax
 10009e9:	89 44 24 04          	mov    %eax,0x4(%esp)
 10009ed:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 10009f0:	89 04 24             	mov    %eax,(%esp)
 10009f3:	e8 f0 02 00 00       	call   1000ce8 <thr_fork>
 10009f8:	89 45 e0             	mov    %eax,-0x20(%ebp)
 10009fb:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
 10009ff:	79 1b                	jns    1000a1c <thr_create+0xf5>
 1000a01:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000a04:	89 04 24             	mov    %eax,(%esp)
 1000a07:	e8 5a fc ff ff       	call   1000666 <free>
 1000a0c:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1000a0f:	89 04 24             	mov    %eax,(%esp)
 1000a12:	e8 4f fc ff ff       	call   1000666 <free>
 1000a17:	8b 45 e0             	mov    -0x20(%ebp),%eax
 1000a1a:	eb 3a                	jmp    1000a56 <thr_create+0x12f>
 1000a1c:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
 1000a20:	75 07                	jne    1000a29 <thr_create+0x102>
 1000a22:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 1000a27:	eb 2d                	jmp    1000a56 <thr_create+0x12f>
 1000a29:	8b 45 ec             	mov    -0x14(%ebp),%eax
 1000a2c:	8b 55 e0             	mov    -0x20(%ebp),%edx
 1000a2f:	89 10                	mov    %edx,(%eax)
 1000a31:	8b 45 ec             	mov    -0x14(%ebp),%eax
 1000a34:	8b 55 f4             	mov    -0xc(%ebp),%edx
 1000a37:	89 50 04             	mov    %edx,0x4(%eax)
 1000a3a:	8b 45 ec             	mov    -0x14(%ebp),%eax
 1000a3d:	89 04 24             	mov    %eax,(%esp)
 1000a40:	e8 bf fd ff ff       	call   1000804 <thr_init_thread>
 1000a45:	8b 45 ec             	mov    -0x14(%ebp),%eax
 1000a48:	83 c0 0d             	add    $0xd,%eax
 1000a4b:	89 04 24             	mov    %eax,(%esp)
 1000a4e:	e8 3e fd ff ff       	call   1000791 <spin_unlock>
 1000a53:	8b 45 e0             	mov    -0x20(%ebp),%eax
 1000a56:	c9                   	leave  
 1000a57:	c3                   	ret    

01000a58 <thr_run_child_func>:
 1000a58:	55                   	push   %ebp
 1000a59:	89 e5                	mov    %esp,%ebp
 1000a5b:	83 ec 28             	sub    $0x28,%esp
 1000a5e:	8b 45 10             	mov    0x10(%ebp),%eax
 1000a61:	83 c0 0d             	add    $0xd,%eax
 1000a64:	89 04 24             	mov    %eax,(%esp)
 1000a67:	e8 c6 fc ff ff       	call   1000732 <spin_lock>
 1000a6c:	8b 45 0c             	mov    0xc(%ebp),%eax
 1000a6f:	89 04 24             	mov    %eax,(%esp)
 1000a72:	8b 45 08             	mov    0x8(%ebp),%eax
 1000a75:	ff d0                	call   *%eax
 1000a77:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1000a7a:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000a7d:	89 04 24             	mov    %eax,(%esp)
 1000a80:	e8 3b 01 00 00       	call   1000bc0 <thr_exit>
 1000a85:	c9                   	leave  
 1000a86:	c3                   	ret    

01000a87 <thr_join>:
 1000a87:	55                   	push   %ebp
 1000a88:	89 e5                	mov    %esp,%ebp
 1000a8a:	83 ec 38             	sub    $0x38,%esp
 1000a8d:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 1000a94:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 1000a9b:	c7 04 24 40 40 00 01 	movl   $0x1004040,(%esp)
 1000aa2:	e8 8b fc ff ff       	call   1000732 <spin_lock>
 1000aa7:	a1 0c 40 00 01       	mov    0x100400c,%eax
 1000aac:	89 45 ec             	mov    %eax,-0x14(%ebp)
 1000aaf:	8b 45 ec             	mov    -0x14(%ebp),%eax
 1000ab2:	83 e8 1c             	sub    $0x1c,%eax
 1000ab5:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1000ab8:	eb 1c                	jmp    1000ad6 <thr_join+0x4f>
 1000aba:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000abd:	8b 00                	mov    (%eax),%eax
 1000abf:	3b 45 08             	cmp    0x8(%ebp),%eax
 1000ac2:	74 21                	je     1000ae5 <thr_join+0x5e>
 1000ac4:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000ac7:	8b 40 1c             	mov    0x1c(%eax),%eax
 1000aca:	89 45 e8             	mov    %eax,-0x18(%ebp)
 1000acd:	8b 45 e8             	mov    -0x18(%ebp),%eax
 1000ad0:	83 e8 1c             	sub    $0x1c,%eax
 1000ad3:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1000ad6:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000ad9:	83 c0 1c             	add    $0x1c,%eax
 1000adc:	3d 0c 40 00 01       	cmp    $0x100400c,%eax
 1000ae1:	75 d7                	jne    1000aba <thr_join+0x33>
 1000ae3:	eb 01                	jmp    1000ae6 <thr_join+0x5f>
 1000ae5:	90                   	nop
 1000ae6:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 1000aea:	74 0a                	je     1000af6 <thr_join+0x6f>
 1000aec:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000aef:	8b 00                	mov    (%eax),%eax
 1000af1:	3b 45 08             	cmp    0x8(%ebp),%eax
 1000af4:	74 16                	je     1000b0c <thr_join+0x85>
 1000af6:	c7 04 24 40 40 00 01 	movl   $0x1004040,(%esp)
 1000afd:	e8 8f fc ff ff       	call   1000791 <spin_unlock>
 1000b02:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 1000b07:	e9 b2 00 00 00       	jmp    1000bbe <thr_join+0x137>
 1000b0c:	c7 04 24 40 40 00 01 	movl   $0x1004040,(%esp)
 1000b13:	e8 79 fc ff ff       	call   1000791 <spin_unlock>
 1000b18:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000b1b:	83 c0 0f             	add    $0xf,%eax
 1000b1e:	89 04 24             	mov    %eax,(%esp)
 1000b21:	e8 43 fc ff ff       	call   1000769 <spin_try_lock>
 1000b26:	85 c0                	test   %eax,%eax
 1000b28:	75 0a                	jne    1000b34 <thr_join+0xad>
 1000b2a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 1000b2f:	e9 8a 00 00 00       	jmp    1000bbe <thr_join+0x137>
 1000b34:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000b37:	83 c0 10             	add    $0x10,%eax
 1000b3a:	89 04 24             	mov    %eax,(%esp)
 1000b3d:	e8 63 01 00 00       	call   1000ca5 <mutex_lock>
 1000b42:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000b45:	83 c0 0e             	add    $0xe,%eax
 1000b48:	89 04 24             	mov    %eax,(%esp)
 1000b4b:	e8 e2 fb ff ff       	call   1000732 <spin_lock>
 1000b50:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000b53:	8b 40 04             	mov    0x4(%eax),%eax
 1000b56:	85 c0                	test   %eax,%eax
 1000b58:	74 0e                	je     1000b68 <thr_join+0xe1>
 1000b5a:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000b5d:	8b 40 04             	mov    0x4(%eax),%eax
 1000b60:	89 04 24             	mov    %eax,(%esp)
 1000b63:	e8 fe fa ff ff       	call   1000666 <free>
 1000b68:	c7 04 24 40 40 00 01 	movl   $0x1004040,(%esp)
 1000b6f:	e8 be fb ff ff       	call   1000732 <spin_lock>
 1000b74:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000b77:	83 c0 1c             	add    $0x1c,%eax
 1000b7a:	89 04 24             	mov    %eax,(%esp)
 1000b7d:	e8 72 fb ff ff       	call   10006f4 <list_del>
 1000b82:	c7 04 24 40 40 00 01 	movl   $0x1004040,(%esp)
 1000b89:	e8 03 fc ff ff       	call   1000791 <spin_unlock>
 1000b8e:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 1000b92:	74 0b                	je     1000b9f <thr_join+0x118>
 1000b94:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000b97:	8b 50 08             	mov    0x8(%eax),%edx
 1000b9a:	8b 45 0c             	mov    0xc(%ebp),%eax
 1000b9d:	89 10                	mov    %edx,(%eax)
 1000b9f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000ba2:	89 45 e4             	mov    %eax,-0x1c(%ebp)
 1000ba5:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 1000ba8:	83 e8 08             	sub    $0x8,%eax
 1000bab:	89 45 f0             	mov    %eax,-0x10(%ebp)
 1000bae:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1000bb1:	89 04 24             	mov    %eax,(%esp)
 1000bb4:	e8 ad fa ff ff       	call   1000666 <free>
 1000bb9:	b8 00 00 00 00       	mov    $0x0,%eax
 1000bbe:	c9                   	leave  
 1000bbf:	c3                   	ret    

01000bc0 <thr_exit>:
 1000bc0:	55                   	push   %ebp
 1000bc1:	89 e5                	mov    %esp,%ebp
 1000bc3:	83 ec 28             	sub    $0x28,%esp
 1000bc6:	e8 23 fc ff ff       	call   10007ee <current_thread>
 1000bcb:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1000bce:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000bd1:	8b 55 08             	mov    0x8(%ebp),%edx
 1000bd4:	89 50 08             	mov    %edx,0x8(%eax)
 1000bd7:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000bda:	8b 40 04             	mov    0x4(%eax),%eax
 1000bdd:	85 c0                	test   %eax,%eax
 1000bdf:	74 07                	je     1000be8 <thr_exit+0x28>
 1000be1:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000be4:	c6 40 0c ff          	movb   $0xff,0xc(%eax)
 1000be8:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000beb:	83 c0 10             	add    $0x10,%eax
 1000bee:	89 04 24             	mov    %eax,(%esp)
 1000bf1:	e8 e1 00 00 00       	call   1000cd7 <mutex_unlock>
 1000bf6:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000bf9:	c6 40 0e 00          	movb   $0x0,0xe(%eax)
 1000bfd:	cd 60                	int    $0x60
 1000bff:	c9                   	leave  
 1000c00:	c3                   	ret    

01000c01 <thr_getid>:
 1000c01:	55                   	push   %ebp
 1000c02:	89 e5                	mov    %esp,%ebp
 1000c04:	83 ec 10             	sub    $0x10,%esp
 1000c07:	e8 e2 fb ff ff       	call   10007ee <current_thread>
 1000c0c:	89 45 fc             	mov    %eax,-0x4(%ebp)
 1000c0f:	8b 45 fc             	mov    -0x4(%ebp),%eax
 1000c12:	8b 00                	mov    (%eax),%eax
 1000c14:	c9                   	leave  
 1000c15:	c3                   	ret    

01000c16 <thr_yield>:
 1000c16:	55                   	push   %ebp
 1000c17:	89 e5                	mov    %esp,%ebp
 1000c19:	83 ec 18             	sub    $0x18,%esp
 1000c1c:	8b 45 08             	mov    0x8(%ebp),%eax
 1000c1f:	89 04 24             	mov    %eax,(%esp)
 1000c22:	e8 29 1e 00 00       	call   1002a50 <yield>
 1000c27:	c9                   	leave  
 1000c28:	c3                   	ret    
 1000c29:	90                   	nop
 1000c2a:	90                   	nop
 1000c2b:	90                   	nop

01000c2c <list_init_head>:
 1000c2c:	55                   	push   %ebp
 1000c2d:	89 e5                	mov    %esp,%ebp
 1000c2f:	8b 45 08             	mov    0x8(%ebp),%eax
 1000c32:	8b 55 08             	mov    0x8(%ebp),%edx
 1000c35:	89 10                	mov    %edx,(%eax)
 1000c37:	8b 45 08             	mov    0x8(%ebp),%eax
 1000c3a:	8b 55 08             	mov    0x8(%ebp),%edx
 1000c3d:	89 50 04             	mov    %edx,0x4(%eax)
 1000c40:	5d                   	pop    %ebp
 1000c41:	c3                   	ret    

01000c42 <xchg>:
 1000c42:	55                   	push   %ebp
 1000c43:	89 e5                	mov    %esp,%ebp
 1000c45:	83 ec 04             	sub    $0x4,%esp
 1000c48:	8b 45 0c             	mov    0xc(%ebp),%eax
 1000c4b:	88 45 fc             	mov    %al,-0x4(%ebp)
 1000c4e:	8b 55 08             	mov    0x8(%ebp),%edx
 1000c51:	0f b6 45 fc          	movzbl -0x4(%ebp),%eax
 1000c55:	f0 86 02             	lock xchg %al,(%edx)
 1000c58:	88 45 fc             	mov    %al,-0x4(%ebp)
 1000c5b:	0f b6 45 fc          	movzbl -0x4(%ebp),%eax
 1000c5f:	c9                   	leave  
 1000c60:	c3                   	ret    

01000c61 <spin_init>:
 1000c61:	55                   	push   %ebp
 1000c62:	89 e5                	mov    %esp,%ebp
 1000c64:	8b 45 08             	mov    0x8(%ebp),%eax
 1000c67:	c6 00 00             	movb   $0x0,(%eax)
 1000c6a:	5d                   	pop    %ebp
 1000c6b:	c3                   	ret    

01000c6c <mutex_init>:
 1000c6c:	55                   	push   %ebp
 1000c6d:	89 e5                	mov    %esp,%ebp
 1000c6f:	83 ec 04             	sub    $0x4,%esp
 1000c72:	8b 45 08             	mov    0x8(%ebp),%eax
 1000c75:	c6 00 00             	movb   $0x0,(%eax)
 1000c78:	8b 45 08             	mov    0x8(%ebp),%eax
 1000c7b:	83 c0 01             	add    $0x1,%eax
 1000c7e:	89 04 24             	mov    %eax,(%esp)
 1000c81:	e8 db ff ff ff       	call   1000c61 <spin_init>
 1000c86:	8b 45 08             	mov    0x8(%ebp),%eax
 1000c89:	83 c0 04             	add    $0x4,%eax
 1000c8c:	89 04 24             	mov    %eax,(%esp)
 1000c8f:	e8 98 ff ff ff       	call   1000c2c <list_init_head>
 1000c94:	b8 00 00 00 00       	mov    $0x0,%eax
 1000c99:	c9                   	leave  
 1000c9a:	c3                   	ret    

01000c9b <mutex_destroy>:
 1000c9b:	55                   	push   %ebp
 1000c9c:	89 e5                	mov    %esp,%ebp
 1000c9e:	b8 00 00 00 00       	mov    $0x0,%eax
 1000ca3:	5d                   	pop    %ebp
 1000ca4:	c3                   	ret    

01000ca5 <mutex_lock>:
 1000ca5:	55                   	push   %ebp
 1000ca6:	89 e5                	mov    %esp,%ebp
 1000ca8:	83 ec 18             	sub    $0x18,%esp
 1000cab:	eb 0c                	jmp    1000cb9 <mutex_lock+0x14>
 1000cad:	c7 04 24 ff ff ff ff 	movl   $0xffffffff,(%esp)
 1000cb4:	e8 5d ff ff ff       	call   1000c16 <thr_yield>
 1000cb9:	8b 45 08             	mov    0x8(%ebp),%eax
 1000cbc:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
 1000cc3:	00 
 1000cc4:	89 04 24             	mov    %eax,(%esp)
 1000cc7:	e8 76 ff ff ff       	call   1000c42 <xchg>
 1000ccc:	3c 01                	cmp    $0x1,%al
 1000cce:	74 dd                	je     1000cad <mutex_lock+0x8>
 1000cd0:	b8 00 00 00 00       	mov    $0x0,%eax
 1000cd5:	c9                   	leave  
 1000cd6:	c3                   	ret    

01000cd7 <mutex_unlock>:
 1000cd7:	55                   	push   %ebp
 1000cd8:	89 e5                	mov    %esp,%ebp
 1000cda:	8b 45 08             	mov    0x8(%ebp),%eax
 1000cdd:	c6 00 00             	movb   $0x0,(%eax)
 1000ce0:	b8 00 00 00 00       	mov    $0x0,%eax
 1000ce5:	5d                   	pop    %ebp
 1000ce6:	c3                   	ret    
 1000ce7:	90                   	nop

01000ce8 <thr_fork>:
 1000ce8:	56                   	push   %esi
 1000ce9:	57                   	push   %edi
 1000cea:	53                   	push   %ebx
 1000ceb:	bb 27 09 48 88       	mov    $0x88480927,%ebx
 1000cf0:	8b 44 24 10          	mov    0x10(%esp),%eax
 1000cf4:	66 87 db             	xchg   %bx,%bx
 1000cf7:	5b                   	pop    %ebx
 1000cf8:	8b 74 24 0c          	mov    0xc(%esp),%esi
 1000cfc:	8b 7c 24 10          	mov    0x10(%esp),%edi
 1000d00:	cd 52                	int    $0x52
 1000d02:	89 c2                	mov    %eax,%edx
 1000d04:	85 d2                	test   %edx,%edx
 1000d06:	78 0b                	js     1000d13 <end>
 1000d08:	75 09                	jne    1000d13 <end>
 1000d0a:	89 f4                	mov    %esi,%esp
 1000d0c:	89 fd                	mov    %edi,%ebp
 1000d0e:	e8 45 fd ff ff       	call   1000a58 <thr_run_child_func>

01000d13 <end>:
 1000d13:	53                   	push   %ebx
 1000d14:	bb 28 09 48 88       	mov    $0x88480928,%ebx
 1000d19:	66 87 db             	xchg   %bx,%bx
 1000d1c:	5b                   	pop    %ebx
 1000d1d:	5f                   	pop    %edi
 1000d1e:	5e                   	pop    %esi
 1000d1f:	c3                   	ret    

01000d20 <__list_add>:
 1000d20:	55                   	push   %ebp
 1000d21:	89 e5                	mov    %esp,%ebp
 1000d23:	8b 45 10             	mov    0x10(%ebp),%eax
 1000d26:	8b 55 08             	mov    0x8(%ebp),%edx
 1000d29:	89 50 04             	mov    %edx,0x4(%eax)
 1000d2c:	8b 45 08             	mov    0x8(%ebp),%eax
 1000d2f:	8b 55 10             	mov    0x10(%ebp),%edx
 1000d32:	89 10                	mov    %edx,(%eax)
 1000d34:	8b 45 0c             	mov    0xc(%ebp),%eax
 1000d37:	8b 55 08             	mov    0x8(%ebp),%edx
 1000d3a:	89 10                	mov    %edx,(%eax)
 1000d3c:	8b 45 08             	mov    0x8(%ebp),%eax
 1000d3f:	8b 55 0c             	mov    0xc(%ebp),%edx
 1000d42:	89 50 04             	mov    %edx,0x4(%eax)
 1000d45:	5d                   	pop    %ebp
 1000d46:	c3                   	ret    

01000d47 <__list_del>:
 1000d47:	55                   	push   %ebp
 1000d48:	89 e5                	mov    %esp,%ebp
 1000d4a:	8b 45 08             	mov    0x8(%ebp),%eax
 1000d4d:	8b 55 0c             	mov    0xc(%ebp),%edx
 1000d50:	89 10                	mov    %edx,(%eax)
 1000d52:	8b 45 0c             	mov    0xc(%ebp),%eax
 1000d55:	8b 55 08             	mov    0x8(%ebp),%edx
 1000d58:	89 50 04             	mov    %edx,0x4(%eax)
 1000d5b:	5d                   	pop    %ebp
 1000d5c:	c3                   	ret    

01000d5d <list_init_head>:
 1000d5d:	55                   	push   %ebp
 1000d5e:	89 e5                	mov    %esp,%ebp
 1000d60:	8b 45 08             	mov    0x8(%ebp),%eax
 1000d63:	8b 55 08             	mov    0x8(%ebp),%edx
 1000d66:	89 10                	mov    %edx,(%eax)
 1000d68:	8b 45 08             	mov    0x8(%ebp),%eax
 1000d6b:	8b 55 08             	mov    0x8(%ebp),%edx
 1000d6e:	89 50 04             	mov    %edx,0x4(%eax)
 1000d71:	5d                   	pop    %ebp
 1000d72:	c3                   	ret    

01000d73 <list_add_tail>:
 1000d73:	55                   	push   %ebp
 1000d74:	89 e5                	mov    %esp,%ebp
 1000d76:	83 ec 0c             	sub    $0xc,%esp
 1000d79:	8b 45 0c             	mov    0xc(%ebp),%eax
 1000d7c:	8b 40 04             	mov    0x4(%eax),%eax
 1000d7f:	8b 55 0c             	mov    0xc(%ebp),%edx
 1000d82:	89 54 24 08          	mov    %edx,0x8(%esp)
 1000d86:	89 44 24 04          	mov    %eax,0x4(%esp)
 1000d8a:	8b 45 08             	mov    0x8(%ebp),%eax
 1000d8d:	89 04 24             	mov    %eax,(%esp)
 1000d90:	e8 8b ff ff ff       	call   1000d20 <__list_add>
 1000d95:	c9                   	leave  
 1000d96:	c3                   	ret    

01000d97 <list_del>:
 1000d97:	55                   	push   %ebp
 1000d98:	89 e5                	mov    %esp,%ebp
 1000d9a:	83 ec 08             	sub    $0x8,%esp
 1000d9d:	8b 45 08             	mov    0x8(%ebp),%eax
 1000da0:	8b 10                	mov    (%eax),%edx
 1000da2:	8b 45 08             	mov    0x8(%ebp),%eax
 1000da5:	8b 40 04             	mov    0x4(%eax),%eax
 1000da8:	89 54 24 04          	mov    %edx,0x4(%esp)
 1000dac:	89 04 24             	mov    %eax,(%esp)
 1000daf:	e8 93 ff ff ff       	call   1000d47 <__list_del>
 1000db4:	c9                   	leave  
 1000db5:	c3                   	ret    

01000db6 <list_is_empty>:
 1000db6:	55                   	push   %ebp
 1000db7:	89 e5                	mov    %esp,%ebp
 1000db9:	8b 45 08             	mov    0x8(%ebp),%eax
 1000dbc:	8b 00                	mov    (%eax),%eax
 1000dbe:	3b 45 08             	cmp    0x8(%ebp),%eax
 1000dc1:	0f 94 c0             	sete   %al
 1000dc4:	0f b6 c0             	movzbl %al,%eax
 1000dc7:	5d                   	pop    %ebp
 1000dc8:	c3                   	ret    

01000dc9 <xchg>:
 1000dc9:	55                   	push   %ebp
 1000dca:	89 e5                	mov    %esp,%ebp
 1000dcc:	83 ec 04             	sub    $0x4,%esp
 1000dcf:	8b 45 0c             	mov    0xc(%ebp),%eax
 1000dd2:	88 45 fc             	mov    %al,-0x4(%ebp)
 1000dd5:	8b 55 08             	mov    0x8(%ebp),%edx
 1000dd8:	0f b6 45 fc          	movzbl -0x4(%ebp),%eax
 1000ddc:	f0 86 02             	lock xchg %al,(%edx)
 1000ddf:	88 45 fc             	mov    %al,-0x4(%ebp)
 1000de2:	0f b6 45 fc          	movzbl -0x4(%ebp),%eax
 1000de6:	c9                   	leave  
 1000de7:	c3                   	ret    

01000de8 <spin_init>:
 1000de8:	55                   	push   %ebp
 1000de9:	89 e5                	mov    %esp,%ebp
 1000deb:	8b 45 08             	mov    0x8(%ebp),%eax
 1000dee:	c6 00 00             	movb   $0x0,(%eax)
 1000df1:	5d                   	pop    %ebp
 1000df2:	c3                   	ret    

01000df3 <spin_lock>:
 1000df3:	55                   	push   %ebp
 1000df4:	89 e5                	mov    %esp,%ebp
 1000df6:	83 ec 28             	sub    $0x28,%esp
 1000df9:	c6 45 f7 01          	movb   $0x1,-0x9(%ebp)
 1000dfd:	0f be 45 f7          	movsbl -0x9(%ebp),%eax
 1000e01:	89 44 24 04          	mov    %eax,0x4(%esp)
 1000e05:	8b 45 08             	mov    0x8(%ebp),%eax
 1000e08:	89 04 24             	mov    %eax,(%esp)
 1000e0b:	e8 b9 ff ff ff       	call   1000dc9 <xchg>
 1000e10:	88 45 f7             	mov    %al,-0x9(%ebp)
 1000e13:	80 7d f7 01          	cmpb   $0x1,-0x9(%ebp)
 1000e17:	75 0e                	jne    1000e27 <spin_lock+0x34>
 1000e19:	c7 04 24 ff ff ff ff 	movl   $0xffffffff,(%esp)
 1000e20:	e8 2b 1c 00 00       	call   1002a50 <yield>
 1000e25:	eb d6                	jmp    1000dfd <spin_lock+0xa>
 1000e27:	90                   	nop
 1000e28:	c9                   	leave  
 1000e29:	c3                   	ret    

01000e2a <spin_try_lock>:
 1000e2a:	55                   	push   %ebp
 1000e2b:	89 e5                	mov    %esp,%ebp
 1000e2d:	83 ec 18             	sub    $0x18,%esp
 1000e30:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
 1000e37:	00 
 1000e38:	8b 45 08             	mov    0x8(%ebp),%eax
 1000e3b:	89 04 24             	mov    %eax,(%esp)
 1000e3e:	e8 86 ff ff ff       	call   1000dc9 <xchg>
 1000e43:	88 45 ff             	mov    %al,-0x1(%ebp)
 1000e46:	80 7d ff 00          	cmpb   $0x0,-0x1(%ebp)
 1000e4a:	0f 94 c0             	sete   %al
 1000e4d:	0f b6 c0             	movzbl %al,%eax
 1000e50:	c9                   	leave  
 1000e51:	c3                   	ret    

01000e52 <spin_unlock>:
 1000e52:	55                   	push   %ebp
 1000e53:	89 e5                	mov    %esp,%ebp
 1000e55:	8b 45 08             	mov    0x8(%ebp),%eax
 1000e58:	c6 00 00             	movb   $0x0,(%eax)
 1000e5b:	5d                   	pop    %ebp
 1000e5c:	c3                   	ret    

01000e5d <cond_init>:
 1000e5d:	55                   	push   %ebp
 1000e5e:	89 e5                	mov    %esp,%ebp
 1000e60:	83 ec 04             	sub    $0x4,%esp
 1000e63:	8b 45 08             	mov    0x8(%ebp),%eax
 1000e66:	89 04 24             	mov    %eax,(%esp)
 1000e69:	e8 7a ff ff ff       	call   1000de8 <spin_init>
 1000e6e:	8b 45 08             	mov    0x8(%ebp),%eax
 1000e71:	83 c0 04             	add    $0x4,%eax
 1000e74:	89 04 24             	mov    %eax,(%esp)
 1000e77:	e8 e1 fe ff ff       	call   1000d5d <list_init_head>
 1000e7c:	b8 00 00 00 00       	mov    $0x0,%eax
 1000e81:	c9                   	leave  
 1000e82:	c3                   	ret    

01000e83 <cond_destroy>:
 1000e83:	55                   	push   %ebp
 1000e84:	89 e5                	mov    %esp,%ebp
 1000e86:	b8 00 00 00 00       	mov    $0x0,%eax
 1000e8b:	5d                   	pop    %ebp
 1000e8c:	c3                   	ret    

01000e8d <cond_wait>:
 1000e8d:	55                   	push   %ebp
 1000e8e:	89 e5                	mov    %esp,%ebp
 1000e90:	83 ec 28             	sub    $0x28,%esp
 1000e93:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
 1000e9a:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
 1000ea1:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 1000ea8:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 1000eaf:	c6 45 f4 01          	movb   $0x1,-0xc(%ebp)
 1000eb3:	8b 45 08             	mov    0x8(%ebp),%eax
 1000eb6:	89 04 24             	mov    %eax,(%esp)
 1000eb9:	e8 35 ff ff ff       	call   1000df3 <spin_lock>
 1000ebe:	8b 45 08             	mov    0x8(%ebp),%eax
 1000ec1:	83 c0 04             	add    $0x4,%eax
 1000ec4:	89 44 24 04          	mov    %eax,0x4(%esp)
 1000ec8:	8d 45 e8             	lea    -0x18(%ebp),%eax
 1000ecb:	89 04 24             	mov    %eax,(%esp)
 1000ece:	e8 a0 fe ff ff       	call   1000d73 <list_add_tail>
 1000ed3:	8b 45 08             	mov    0x8(%ebp),%eax
 1000ed6:	89 04 24             	mov    %eax,(%esp)
 1000ed9:	e8 74 ff ff ff       	call   1000e52 <spin_unlock>
 1000ede:	8b 45 0c             	mov    0xc(%ebp),%eax
 1000ee1:	89 04 24             	mov    %eax,(%esp)
 1000ee4:	e8 ee fd ff ff       	call   1000cd7 <mutex_unlock>
 1000ee9:	eb 0c                	jmp    1000ef7 <cond_wait+0x6a>
 1000eeb:	c7 04 24 ff ff ff ff 	movl   $0xffffffff,(%esp)
 1000ef2:	e8 1f fd ff ff       	call   1000c16 <thr_yield>
 1000ef7:	8d 45 e8             	lea    -0x18(%ebp),%eax
 1000efa:	83 c0 0c             	add    $0xc,%eax
 1000efd:	89 04 24             	mov    %eax,(%esp)
 1000f00:	e8 25 ff ff ff       	call   1000e2a <spin_try_lock>
 1000f05:	85 c0                	test   %eax,%eax
 1000f07:	74 e2                	je     1000eeb <cond_wait+0x5e>
 1000f09:	8b 45 0c             	mov    0xc(%ebp),%eax
 1000f0c:	89 04 24             	mov    %eax,(%esp)
 1000f0f:	e8 91 fd ff ff       	call   1000ca5 <mutex_lock>
 1000f14:	b8 00 00 00 00       	mov    $0x0,%eax
 1000f19:	c9                   	leave  
 1000f1a:	c3                   	ret    

01000f1b <cond_signal>:
 1000f1b:	55                   	push   %ebp
 1000f1c:	89 e5                	mov    %esp,%ebp
 1000f1e:	83 ec 28             	sub    $0x28,%esp
 1000f21:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 1000f28:	8b 45 08             	mov    0x8(%ebp),%eax
 1000f2b:	89 04 24             	mov    %eax,(%esp)
 1000f2e:	e8 c0 fe ff ff       	call   1000df3 <spin_lock>
 1000f33:	8b 45 08             	mov    0x8(%ebp),%eax
 1000f36:	83 c0 04             	add    $0x4,%eax
 1000f39:	89 04 24             	mov    %eax,(%esp)
 1000f3c:	e8 75 fe ff ff       	call   1000db6 <list_is_empty>
 1000f41:	85 c0                	test   %eax,%eax
 1000f43:	75 2a                	jne    1000f6f <cond_signal+0x54>
 1000f45:	8b 45 08             	mov    0x8(%ebp),%eax
 1000f48:	8b 40 04             	mov    0x4(%eax),%eax
 1000f4b:	89 45 f0             	mov    %eax,-0x10(%ebp)
 1000f4e:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1000f51:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1000f54:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000f57:	89 04 24             	mov    %eax,(%esp)
 1000f5a:	e8 38 fe ff ff       	call   1000d97 <list_del>
 1000f5f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000f62:	83 c0 0c             	add    $0xc,%eax
 1000f65:	89 04 24             	mov    %eax,(%esp)
 1000f68:	e8 e5 fe ff ff       	call   1000e52 <spin_unlock>
 1000f6d:	eb 01                	jmp    1000f70 <cond_signal+0x55>
 1000f6f:	90                   	nop
 1000f70:	8b 45 08             	mov    0x8(%ebp),%eax
 1000f73:	89 04 24             	mov    %eax,(%esp)
 1000f76:	e8 d7 fe ff ff       	call   1000e52 <spin_unlock>
 1000f7b:	b8 00 00 00 00       	mov    $0x0,%eax
 1000f80:	c9                   	leave  
 1000f81:	c3                   	ret    

01000f82 <cond_broadcast>:
 1000f82:	55                   	push   %ebp
 1000f83:	89 e5                	mov    %esp,%ebp
 1000f85:	83 ec 28             	sub    $0x28,%esp
 1000f88:	8b 45 08             	mov    0x8(%ebp),%eax
 1000f8b:	89 04 24             	mov    %eax,(%esp)
 1000f8e:	e8 60 fe ff ff       	call   1000df3 <spin_lock>
 1000f93:	eb 28                	jmp    1000fbd <cond_broadcast+0x3b>
 1000f95:	8b 45 08             	mov    0x8(%ebp),%eax
 1000f98:	8b 40 04             	mov    0x4(%eax),%eax
 1000f9b:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1000f9e:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000fa1:	89 45 f0             	mov    %eax,-0x10(%ebp)
 1000fa4:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1000fa7:	89 04 24             	mov    %eax,(%esp)
 1000faa:	e8 e8 fd ff ff       	call   1000d97 <list_del>
 1000faf:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1000fb2:	83 c0 0c             	add    $0xc,%eax
 1000fb5:	89 04 24             	mov    %eax,(%esp)
 1000fb8:	e8 95 fe ff ff       	call   1000e52 <spin_unlock>
 1000fbd:	8b 45 08             	mov    0x8(%ebp),%eax
 1000fc0:	83 c0 04             	add    $0x4,%eax
 1000fc3:	89 04 24             	mov    %eax,(%esp)
 1000fc6:	e8 eb fd ff ff       	call   1000db6 <list_is_empty>
 1000fcb:	85 c0                	test   %eax,%eax
 1000fcd:	74 c6                	je     1000f95 <cond_broadcast+0x13>
 1000fcf:	8b 45 08             	mov    0x8(%ebp),%eax
 1000fd2:	89 04 24             	mov    %eax,(%esp)
 1000fd5:	e8 78 fe ff ff       	call   1000e52 <spin_unlock>
 1000fda:	b8 00 00 00 00       	mov    $0x0,%eax
 1000fdf:	c9                   	leave  
 1000fe0:	c3                   	ret    
 1000fe1:	90                   	nop
 1000fe2:	90                   	nop
 1000fe3:	90                   	nop

01000fe4 <_malloc>:
 1000fe4:	55                   	push   %ebp
 1000fe5:	89 e5                	mov    %esp,%ebp
 1000fe7:	83 ec 18             	sub    $0x18,%esp
 1000fea:	a1 44 40 00 01       	mov    0x1004044,%eax
 1000fef:	85 c0                	test   %eax,%eax
 1000ff1:	75 1a                	jne    100100d <_malloc+0x29>
 1000ff3:	e8 df 00 00 00       	call   10010d7 <mm_init>
 1000ff8:	85 c0                	test   %eax,%eax
 1000ffa:	79 07                	jns    1001003 <_malloc+0x1f>
 1000ffc:	b8 00 00 00 00       	mov    $0x0,%eax
 1001001:	eb 15                	jmp    1001018 <_malloc+0x34>
 1001003:	c7 05 44 40 00 01 01 	movl   $0x1,0x1004044
 100100a:	00 00 00 
 100100d:	8b 45 08             	mov    0x8(%ebp),%eax
 1001010:	89 04 24             	mov    %eax,(%esp)
 1001013:	e8 52 01 00 00       	call   100116a <mm_malloc>
 1001018:	c9                   	leave  
 1001019:	c3                   	ret    

0100101a <_calloc>:
 100101a:	55                   	push   %ebp
 100101b:	89 e5                	mov    %esp,%ebp
 100101d:	83 ec 28             	sub    $0x28,%esp
 1001020:	a1 44 40 00 01       	mov    0x1004044,%eax
 1001025:	85 c0                	test   %eax,%eax
 1001027:	75 1a                	jne    1001043 <_calloc+0x29>
 1001029:	e8 a9 00 00 00       	call   10010d7 <mm_init>
 100102e:	85 c0                	test   %eax,%eax
 1001030:	79 07                	jns    1001039 <_calloc+0x1f>
 1001032:	b8 00 00 00 00       	mov    $0x0,%eax
 1001037:	eb 42                	jmp    100107b <_calloc+0x61>
 1001039:	c7 05 44 40 00 01 01 	movl   $0x1,0x1004044
 1001040:	00 00 00 
 1001043:	8b 45 08             	mov    0x8(%ebp),%eax
 1001046:	0f af 45 0c          	imul   0xc(%ebp),%eax
 100104a:	89 04 24             	mov    %eax,(%esp)
 100104d:	e8 18 01 00 00       	call   100116a <mm_malloc>
 1001052:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1001055:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 1001059:	75 07                	jne    1001062 <_calloc+0x48>
 100105b:	b8 00 00 00 00       	mov    $0x0,%eax
 1001060:	eb 19                	jmp    100107b <_calloc+0x61>
 1001062:	8b 45 08             	mov    0x8(%ebp),%eax
 1001065:	0f af 45 0c          	imul   0xc(%ebp),%eax
 1001069:	89 44 24 04          	mov    %eax,0x4(%esp)
 100106d:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1001070:	89 04 24             	mov    %eax,(%esp)
 1001073:	e8 f4 18 00 00       	call   100296c <bzero>
 1001078:	8b 45 f4             	mov    -0xc(%ebp),%eax
 100107b:	c9                   	leave  
 100107c:	c3                   	ret    

0100107d <_realloc>:
 100107d:	55                   	push   %ebp
 100107e:	89 e5                	mov    %esp,%ebp
 1001080:	83 ec 18             	sub    $0x18,%esp
 1001083:	a1 44 40 00 01       	mov    0x1004044,%eax
 1001088:	85 c0                	test   %eax,%eax
 100108a:	75 07                	jne    1001093 <_realloc+0x16>
 100108c:	b8 00 00 00 00       	mov    $0x0,%eax
 1001091:	eb 12                	jmp    10010a5 <_realloc+0x28>
 1001093:	8b 45 0c             	mov    0xc(%ebp),%eax
 1001096:	89 44 24 04          	mov    %eax,0x4(%esp)
 100109a:	8b 45 08             	mov    0x8(%ebp),%eax
 100109d:	89 04 24             	mov    %eax,(%esp)
 10010a0:	e8 c4 01 00 00       	call   1001269 <mm_realloc>
 10010a5:	c9                   	leave  
 10010a6:	c3                   	ret    

010010a7 <_free>:
 10010a7:	55                   	push   %ebp
 10010a8:	89 e5                	mov    %esp,%ebp
 10010aa:	83 ec 18             	sub    $0x18,%esp
 10010ad:	a1 44 40 00 01       	mov    0x1004044,%eax
 10010b2:	85 c0                	test   %eax,%eax
 10010b4:	74 0d                	je     10010c3 <_free+0x1c>
 10010b6:	8b 45 08             	mov    0x8(%ebp),%eax
 10010b9:	89 04 24             	mov    %eax,(%esp)
 10010bc:	e8 60 01 00 00       	call   1001221 <mm_free>
 10010c1:	eb 01                	jmp    10010c4 <_free+0x1d>
 10010c3:	90                   	nop
 10010c4:	c9                   	leave  
 10010c5:	c3                   	ret    
 10010c6:	90                   	nop
 10010c7:	90                   	nop

010010c8 <min>:
 10010c8:	55                   	push   %ebp
 10010c9:	89 e5                	mov    %esp,%ebp
 10010cb:	8b 45 08             	mov    0x8(%ebp),%eax
 10010ce:	39 45 0c             	cmp    %eax,0xc(%ebp)
 10010d1:	0f 46 45 0c          	cmovbe 0xc(%ebp),%eax
 10010d5:	5d                   	pop    %ebp
 10010d6:	c3                   	ret    

010010d7 <mm_init>:
 10010d7:	55                   	push   %ebp
 10010d8:	89 e5                	mov    %esp,%ebp
 10010da:	83 ec 18             	sub    $0x18,%esp
 10010dd:	c7 04 24 ff ff ff ff 	movl   $0xffffffff,(%esp)
 10010e4:	e8 6b 07 00 00       	call   1001854 <mem_init>
 10010e9:	c7 04 24 10 00 00 00 	movl   $0x10,(%esp)
 10010f0:	e8 c1 07 00 00       	call   10018b6 <mem_sbrk>
 10010f5:	a3 48 40 00 01       	mov    %eax,0x1004048
 10010fa:	a1 48 40 00 01       	mov    0x1004048,%eax
 10010ff:	85 c0                	test   %eax,%eax
 1001101:	75 07                	jne    100110a <mm_init+0x33>
 1001103:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 1001108:	eb 5e                	jmp    1001168 <mm_init+0x91>
 100110a:	a1 48 40 00 01       	mov    0x1004048,%eax
 100110f:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
 1001115:	a1 48 40 00 01       	mov    0x1004048,%eax
 100111a:	83 c0 04             	add    $0x4,%eax
 100111d:	c7 00 09 00 00 00    	movl   $0x9,(%eax)
 1001123:	a1 48 40 00 01       	mov    0x1004048,%eax
 1001128:	83 c0 08             	add    $0x8,%eax
 100112b:	c7 00 09 00 00 00    	movl   $0x9,(%eax)
 1001131:	a1 48 40 00 01       	mov    0x1004048,%eax
 1001136:	83 c0 0c             	add    $0xc,%eax
 1001139:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
 100113f:	a1 48 40 00 01       	mov    0x1004048,%eax
 1001144:	83 c0 08             	add    $0x8,%eax
 1001147:	a3 48 40 00 01       	mov    %eax,0x1004048
 100114c:	c7 04 24 00 04 00 00 	movl   $0x400,(%esp)
 1001153:	e8 63 02 00 00       	call   10013bb <extend_heap>
 1001158:	85 c0                	test   %eax,%eax
 100115a:	75 07                	jne    1001163 <mm_init+0x8c>
 100115c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 1001161:	eb 05                	jmp    1001168 <mm_init+0x91>
 1001163:	b8 00 00 00 00       	mov    $0x0,%eax
 1001168:	c9                   	leave  
 1001169:	c3                   	ret    

0100116a <mm_malloc>:
 100116a:	55                   	push   %ebp
 100116b:	89 e5                	mov    %esp,%ebp
 100116d:	83 ec 28             	sub    $0x28,%esp
 1001170:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
 1001174:	7f 0a                	jg     1001180 <mm_malloc+0x16>
 1001176:	b8 00 00 00 00       	mov    $0x0,%eax
 100117b:	e9 9f 00 00 00       	jmp    100121f <mm_malloc+0xb5>
 1001180:	83 7d 08 08          	cmpl   $0x8,0x8(%ebp)
 1001184:	7f 09                	jg     100118f <mm_malloc+0x25>
 1001186:	c7 45 f4 10 00 00 00 	movl   $0x10,-0xc(%ebp)
 100118d:	eb 17                	jmp    10011a6 <mm_malloc+0x3c>
 100118f:	8b 45 08             	mov    0x8(%ebp),%eax
 1001192:	83 c0 0f             	add    $0xf,%eax
 1001195:	8d 50 07             	lea    0x7(%eax),%edx
 1001198:	85 c0                	test   %eax,%eax
 100119a:	0f 48 c2             	cmovs  %edx,%eax
 100119d:	c1 f8 03             	sar    $0x3,%eax
 10011a0:	c1 e0 03             	shl    $0x3,%eax
 10011a3:	89 45 f4             	mov    %eax,-0xc(%ebp)
 10011a6:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10011a9:	89 04 24             	mov    %eax,(%esp)
 10011ac:	e8 4b 03 00 00       	call   10014fc <find_fit>
 10011b1:	89 45 f0             	mov    %eax,-0x10(%ebp)
 10011b4:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 10011b8:	74 17                	je     10011d1 <mm_malloc+0x67>
 10011ba:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10011bd:	89 44 24 04          	mov    %eax,0x4(%esp)
 10011c1:	8b 45 f0             	mov    -0x10(%ebp),%eax
 10011c4:	89 04 24             	mov    %eax,(%esp)
 10011c7:	e8 73 02 00 00       	call   100143f <place>
 10011cc:	8b 45 f0             	mov    -0x10(%ebp),%eax
 10011cf:	eb 4e                	jmp    100121f <mm_malloc+0xb5>
 10011d1:	b8 00 10 00 00       	mov    $0x1000,%eax
 10011d6:	81 7d f4 00 10 00 00 	cmpl   $0x1000,-0xc(%ebp)
 10011dd:	0f 4d 45 f4          	cmovge -0xc(%ebp),%eax
 10011e1:	89 45 ec             	mov    %eax,-0x14(%ebp)
 10011e4:	8b 45 ec             	mov    -0x14(%ebp),%eax
 10011e7:	8d 50 03             	lea    0x3(%eax),%edx
 10011ea:	85 c0                	test   %eax,%eax
 10011ec:	0f 48 c2             	cmovs  %edx,%eax
 10011ef:	c1 f8 02             	sar    $0x2,%eax
 10011f2:	89 04 24             	mov    %eax,(%esp)
 10011f5:	e8 c1 01 00 00       	call   10013bb <extend_heap>
 10011fa:	89 45 f0             	mov    %eax,-0x10(%ebp)
 10011fd:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 1001201:	75 07                	jne    100120a <mm_malloc+0xa0>
 1001203:	b8 00 00 00 00       	mov    $0x0,%eax
 1001208:	eb 15                	jmp    100121f <mm_malloc+0xb5>
 100120a:	8b 45 f4             	mov    -0xc(%ebp),%eax
 100120d:	89 44 24 04          	mov    %eax,0x4(%esp)
 1001211:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1001214:	89 04 24             	mov    %eax,(%esp)
 1001217:	e8 23 02 00 00       	call   100143f <place>
 100121c:	8b 45 f0             	mov    -0x10(%ebp),%eax
 100121f:	c9                   	leave  
 1001220:	c3                   	ret    

01001221 <mm_free>:
 1001221:	55                   	push   %ebp
 1001222:	89 e5                	mov    %esp,%ebp
 1001224:	83 ec 28             	sub    $0x28,%esp
 1001227:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
 100122b:	74 3a                	je     1001267 <mm_free+0x46>
 100122d:	8b 45 08             	mov    0x8(%ebp),%eax
 1001230:	83 e8 04             	sub    $0x4,%eax
 1001233:	8b 00                	mov    (%eax),%eax
 1001235:	83 e0 f8             	and    $0xfffffff8,%eax
 1001238:	89 45 f4             	mov    %eax,-0xc(%ebp)
 100123b:	8b 45 08             	mov    0x8(%ebp),%eax
 100123e:	8d 50 fc             	lea    -0x4(%eax),%edx
 1001241:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1001244:	89 02                	mov    %eax,(%edx)
 1001246:	8b 45 08             	mov    0x8(%ebp),%eax
 1001249:	83 e8 04             	sub    $0x4,%eax
 100124c:	8b 00                	mov    (%eax),%eax
 100124e:	83 e0 f8             	and    $0xfffffff8,%eax
 1001251:	83 e8 08             	sub    $0x8,%eax
 1001254:	03 45 08             	add    0x8(%ebp),%eax
 1001257:	8b 55 f4             	mov    -0xc(%ebp),%edx
 100125a:	89 10                	mov    %edx,(%eax)
 100125c:	8b 45 08             	mov    0x8(%ebp),%eax
 100125f:	89 04 24             	mov    %eax,(%esp)
 1001262:	e8 ed 02 00 00       	call   1001554 <coalesce>
 1001267:	c9                   	leave  
 1001268:	c3                   	ret    

01001269 <mm_realloc>:
 1001269:	55                   	push   %ebp
 100126a:	89 e5                	mov    %esp,%ebp
 100126c:	83 ec 28             	sub    $0x28,%esp
 100126f:	8b 45 0c             	mov    0xc(%ebp),%eax
 1001272:	89 04 24             	mov    %eax,(%esp)
 1001275:	e8 f0 fe ff ff       	call   100116a <mm_malloc>
 100127a:	89 45 f4             	mov    %eax,-0xc(%ebp)
 100127d:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 1001281:	75 07                	jne    100128a <mm_realloc+0x21>
 1001283:	b8 00 00 00 00       	mov    $0x0,%eax
 1001288:	eb 4a                	jmp    10012d4 <mm_realloc+0x6b>
 100128a:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
 100128e:	74 41                	je     10012d1 <mm_realloc+0x68>
 1001290:	8b 45 08             	mov    0x8(%ebp),%eax
 1001293:	83 e8 04             	sub    $0x4,%eax
 1001296:	8b 00                	mov    (%eax),%eax
 1001298:	83 e0 f8             	and    $0xfffffff8,%eax
 100129b:	89 45 f0             	mov    %eax,-0x10(%ebp)
 100129e:	8b 45 0c             	mov    0xc(%ebp),%eax
 10012a1:	89 44 24 04          	mov    %eax,0x4(%esp)
 10012a5:	8b 45 f0             	mov    -0x10(%ebp),%eax
 10012a8:	89 04 24             	mov    %eax,(%esp)
 10012ab:	e8 18 fe ff ff       	call   10010c8 <min>
 10012b0:	89 44 24 08          	mov    %eax,0x8(%esp)
 10012b4:	8b 45 08             	mov    0x8(%ebp),%eax
 10012b7:	89 44 24 04          	mov    %eax,0x4(%esp)
 10012bb:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10012be:	89 04 24             	mov    %eax,(%esp)
 10012c1:	e8 96 16 00 00       	call   100295c <memcpy>
 10012c6:	8b 45 08             	mov    0x8(%ebp),%eax
 10012c9:	89 04 24             	mov    %eax,(%esp)
 10012cc:	e8 50 ff ff ff       	call   1001221 <mm_free>
 10012d1:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10012d4:	c9                   	leave  
 10012d5:	c3                   	ret    

010012d6 <mm_checkheap>:
 10012d6:	55                   	push   %ebp
 10012d7:	89 e5                	mov    %esp,%ebp
 10012d9:	83 ec 28             	sub    $0x28,%esp
 10012dc:	a1 48 40 00 01       	mov    0x1004048,%eax
 10012e1:	89 45 f4             	mov    %eax,-0xc(%ebp)
 10012e4:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
 10012e8:	74 15                	je     10012ff <mm_checkheap+0x29>
 10012ea:	a1 48 40 00 01       	mov    0x1004048,%eax
 10012ef:	89 44 24 04          	mov    %eax,0x4(%esp)
 10012f3:	c7 04 24 ac 2e 00 01 	movl   $0x1002eac,(%esp)
 10012fa:	e8 a6 09 00 00       	call   1001ca5 <lprintf>
 10012ff:	a1 48 40 00 01       	mov    0x1004048,%eax
 1001304:	83 e8 04             	sub    $0x4,%eax
 1001307:	8b 00                	mov    (%eax),%eax
 1001309:	83 e0 f8             	and    $0xfffffff8,%eax
 100130c:	83 f8 08             	cmp    $0x8,%eax
 100130f:	75 11                	jne    1001322 <mm_checkheap+0x4c>
 1001311:	a1 48 40 00 01       	mov    0x1004048,%eax
 1001316:	83 e8 04             	sub    $0x4,%eax
 1001319:	8b 00                	mov    (%eax),%eax
 100131b:	83 e0 01             	and    $0x1,%eax
 100131e:	85 c0                	test   %eax,%eax
 1001320:	75 0c                	jne    100132e <mm_checkheap+0x58>
 1001322:	c7 04 24 ba 2e 00 01 	movl   $0x1002eba,(%esp)
 1001329:	e8 77 09 00 00       	call   1001ca5 <lprintf>
 100132e:	a1 48 40 00 01       	mov    0x1004048,%eax
 1001333:	89 04 24             	mov    %eax,(%esp)
 1001336:	e8 c6 04 00 00       	call   1001801 <checkblock>
 100133b:	a1 48 40 00 01       	mov    0x1004048,%eax
 1001340:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1001343:	eb 2a                	jmp    100136f <mm_checkheap+0x99>
 1001345:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
 1001349:	74 0b                	je     1001356 <mm_checkheap+0x80>
 100134b:	8b 45 f4             	mov    -0xc(%ebp),%eax
 100134e:	89 04 24             	mov    %eax,(%esp)
 1001351:	e8 ed 03 00 00       	call   1001743 <printblock>
 1001356:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1001359:	89 04 24             	mov    %eax,(%esp)
 100135c:	e8 a0 04 00 00       	call   1001801 <checkblock>
 1001361:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1001364:	83 e8 04             	sub    $0x4,%eax
 1001367:	8b 00                	mov    (%eax),%eax
 1001369:	83 e0 f8             	and    $0xfffffff8,%eax
 100136c:	01 45 f4             	add    %eax,-0xc(%ebp)
 100136f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1001372:	83 e8 04             	sub    $0x4,%eax
 1001375:	8b 00                	mov    (%eax),%eax
 1001377:	83 e0 f8             	and    $0xfffffff8,%eax
 100137a:	85 c0                	test   %eax,%eax
 100137c:	7f c7                	jg     1001345 <mm_checkheap+0x6f>
 100137e:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
 1001382:	74 0b                	je     100138f <mm_checkheap+0xb9>
 1001384:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1001387:	89 04 24             	mov    %eax,(%esp)
 100138a:	e8 b4 03 00 00       	call   1001743 <printblock>
 100138f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1001392:	83 e8 04             	sub    $0x4,%eax
 1001395:	8b 00                	mov    (%eax),%eax
 1001397:	83 e0 f8             	and    $0xfffffff8,%eax
 100139a:	85 c0                	test   %eax,%eax
 100139c:	75 0f                	jne    10013ad <mm_checkheap+0xd7>
 100139e:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10013a1:	83 e8 04             	sub    $0x4,%eax
 10013a4:	8b 00                	mov    (%eax),%eax
 10013a6:	83 e0 01             	and    $0x1,%eax
 10013a9:	85 c0                	test   %eax,%eax
 10013ab:	75 0c                	jne    10013b9 <mm_checkheap+0xe3>
 10013ad:	c7 04 24 cf 2e 00 01 	movl   $0x1002ecf,(%esp)
 10013b4:	e8 ec 08 00 00       	call   1001ca5 <lprintf>
 10013b9:	c9                   	leave  
 10013ba:	c3                   	ret    

010013bb <extend_heap>:
 10013bb:	55                   	push   %ebp
 10013bc:	89 e5                	mov    %esp,%ebp
 10013be:	83 ec 28             	sub    $0x28,%esp
 10013c1:	8b 45 08             	mov    0x8(%ebp),%eax
 10013c4:	83 e0 01             	and    $0x1,%eax
 10013c7:	84 c0                	test   %al,%al
 10013c9:	74 0b                	je     10013d6 <extend_heap+0x1b>
 10013cb:	8b 45 08             	mov    0x8(%ebp),%eax
 10013ce:	83 c0 01             	add    $0x1,%eax
 10013d1:	c1 e0 02             	shl    $0x2,%eax
 10013d4:	eb 06                	jmp    10013dc <extend_heap+0x21>
 10013d6:	8b 45 08             	mov    0x8(%ebp),%eax
 10013d9:	c1 e0 02             	shl    $0x2,%eax
 10013dc:	89 45 f4             	mov    %eax,-0xc(%ebp)
 10013df:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10013e2:	89 04 24             	mov    %eax,(%esp)
 10013e5:	e8 cc 04 00 00       	call   10018b6 <mem_sbrk>
 10013ea:	89 45 f0             	mov    %eax,-0x10(%ebp)
 10013ed:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 10013f1:	75 07                	jne    10013fa <extend_heap+0x3f>
 10013f3:	b8 00 00 00 00       	mov    $0x0,%eax
 10013f8:	eb 43                	jmp    100143d <extend_heap+0x82>
 10013fa:	8b 45 f0             	mov    -0x10(%ebp),%eax
 10013fd:	8d 50 fc             	lea    -0x4(%eax),%edx
 1001400:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1001403:	89 02                	mov    %eax,(%edx)
 1001405:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1001408:	83 e8 04             	sub    $0x4,%eax
 100140b:	8b 00                	mov    (%eax),%eax
 100140d:	83 e0 f8             	and    $0xfffffff8,%eax
 1001410:	83 e8 08             	sub    $0x8,%eax
 1001413:	03 45 f0             	add    -0x10(%ebp),%eax
 1001416:	8b 55 f4             	mov    -0xc(%ebp),%edx
 1001419:	89 10                	mov    %edx,(%eax)
 100141b:	8b 45 f0             	mov    -0x10(%ebp),%eax
 100141e:	83 e8 04             	sub    $0x4,%eax
 1001421:	8b 00                	mov    (%eax),%eax
 1001423:	83 e0 f8             	and    $0xfffffff8,%eax
 1001426:	83 e8 04             	sub    $0x4,%eax
 1001429:	03 45 f0             	add    -0x10(%ebp),%eax
 100142c:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
 1001432:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1001435:	89 04 24             	mov    %eax,(%esp)
 1001438:	e8 17 01 00 00       	call   1001554 <coalesce>
 100143d:	c9                   	leave  
 100143e:	c3                   	ret    

0100143f <place>:
 100143f:	55                   	push   %ebp
 1001440:	89 e5                	mov    %esp,%ebp
 1001442:	53                   	push   %ebx
 1001443:	83 ec 10             	sub    $0x10,%esp
 1001446:	8b 45 08             	mov    0x8(%ebp),%eax
 1001449:	83 e8 04             	sub    $0x4,%eax
 100144c:	8b 00                	mov    (%eax),%eax
 100144e:	83 e0 f8             	and    $0xfffffff8,%eax
 1001451:	89 45 f8             	mov    %eax,-0x8(%ebp)
 1001454:	8b 45 0c             	mov    0xc(%ebp),%eax
 1001457:	8b 55 f8             	mov    -0x8(%ebp),%edx
 100145a:	89 d1                	mov    %edx,%ecx
 100145c:	29 c1                	sub    %eax,%ecx
 100145e:	89 c8                	mov    %ecx,%eax
 1001460:	83 f8 0f             	cmp    $0xf,%eax
 1001463:	7e 6a                	jle    10014cf <place+0x90>
 1001465:	8b 45 08             	mov    0x8(%ebp),%eax
 1001468:	83 e8 04             	sub    $0x4,%eax
 100146b:	8b 55 0c             	mov    0xc(%ebp),%edx
 100146e:	83 ca 01             	or     $0x1,%edx
 1001471:	89 10                	mov    %edx,(%eax)
 1001473:	8b 45 08             	mov    0x8(%ebp),%eax
 1001476:	83 e8 04             	sub    $0x4,%eax
 1001479:	8b 00                	mov    (%eax),%eax
 100147b:	83 e0 f8             	and    $0xfffffff8,%eax
 100147e:	83 e8 08             	sub    $0x8,%eax
 1001481:	03 45 08             	add    0x8(%ebp),%eax
 1001484:	8b 55 0c             	mov    0xc(%ebp),%edx
 1001487:	83 ca 01             	or     $0x1,%edx
 100148a:	89 10                	mov    %edx,(%eax)
 100148c:	8b 45 08             	mov    0x8(%ebp),%eax
 100148f:	83 e8 04             	sub    $0x4,%eax
 1001492:	8b 00                	mov    (%eax),%eax
 1001494:	83 e0 f8             	and    $0xfffffff8,%eax
 1001497:	01 45 08             	add    %eax,0x8(%ebp)
 100149a:	8b 45 08             	mov    0x8(%ebp),%eax
 100149d:	83 e8 04             	sub    $0x4,%eax
 10014a0:	8b 55 0c             	mov    0xc(%ebp),%edx
 10014a3:	8b 4d f8             	mov    -0x8(%ebp),%ecx
 10014a6:	89 cb                	mov    %ecx,%ebx
 10014a8:	29 d3                	sub    %edx,%ebx
 10014aa:	89 da                	mov    %ebx,%edx
 10014ac:	89 10                	mov    %edx,(%eax)
 10014ae:	8b 45 08             	mov    0x8(%ebp),%eax
 10014b1:	83 e8 04             	sub    $0x4,%eax
 10014b4:	8b 00                	mov    (%eax),%eax
 10014b6:	83 e0 f8             	and    $0xfffffff8,%eax
 10014b9:	83 e8 08             	sub    $0x8,%eax
 10014bc:	03 45 08             	add    0x8(%ebp),%eax
 10014bf:	8b 55 0c             	mov    0xc(%ebp),%edx
 10014c2:	8b 4d f8             	mov    -0x8(%ebp),%ecx
 10014c5:	89 cb                	mov    %ecx,%ebx
 10014c7:	29 d3                	sub    %edx,%ebx
 10014c9:	89 da                	mov    %ebx,%edx
 10014cb:	89 10                	mov    %edx,(%eax)
 10014cd:	eb 27                	jmp    10014f6 <place+0xb7>
 10014cf:	8b 45 08             	mov    0x8(%ebp),%eax
 10014d2:	83 e8 04             	sub    $0x4,%eax
 10014d5:	8b 55 f8             	mov    -0x8(%ebp),%edx
 10014d8:	83 ca 01             	or     $0x1,%edx
 10014db:	89 10                	mov    %edx,(%eax)
 10014dd:	8b 45 08             	mov    0x8(%ebp),%eax
 10014e0:	83 e8 04             	sub    $0x4,%eax
 10014e3:	8b 00                	mov    (%eax),%eax
 10014e5:	83 e0 f8             	and    $0xfffffff8,%eax
 10014e8:	83 e8 08             	sub    $0x8,%eax
 10014eb:	03 45 08             	add    0x8(%ebp),%eax
 10014ee:	8b 55 f8             	mov    -0x8(%ebp),%edx
 10014f1:	83 ca 01             	or     $0x1,%edx
 10014f4:	89 10                	mov    %edx,(%eax)
 10014f6:	83 c4 10             	add    $0x10,%esp
 10014f9:	5b                   	pop    %ebx
 10014fa:	5d                   	pop    %ebp
 10014fb:	c3                   	ret    

010014fc <find_fit>:
 10014fc:	55                   	push   %ebp
 10014fd:	89 e5                	mov    %esp,%ebp
 10014ff:	83 ec 10             	sub    $0x10,%esp
 1001502:	a1 48 40 00 01       	mov    0x1004048,%eax
 1001507:	89 45 fc             	mov    %eax,-0x4(%ebp)
 100150a:	eb 32                	jmp    100153e <find_fit+0x42>
 100150c:	8b 45 fc             	mov    -0x4(%ebp),%eax
 100150f:	83 e8 04             	sub    $0x4,%eax
 1001512:	8b 00                	mov    (%eax),%eax
 1001514:	83 e0 01             	and    $0x1,%eax
 1001517:	85 c0                	test   %eax,%eax
 1001519:	75 15                	jne    1001530 <find_fit+0x34>
 100151b:	8b 45 fc             	mov    -0x4(%ebp),%eax
 100151e:	83 e8 04             	sub    $0x4,%eax
 1001521:	8b 00                	mov    (%eax),%eax
 1001523:	83 e0 f8             	and    $0xfffffff8,%eax
 1001526:	3b 45 08             	cmp    0x8(%ebp),%eax
 1001529:	7c 05                	jl     1001530 <find_fit+0x34>
 100152b:	8b 45 fc             	mov    -0x4(%ebp),%eax
 100152e:	eb 22                	jmp    1001552 <find_fit+0x56>
 1001530:	8b 45 fc             	mov    -0x4(%ebp),%eax
 1001533:	83 e8 04             	sub    $0x4,%eax
 1001536:	8b 00                	mov    (%eax),%eax
 1001538:	83 e0 f8             	and    $0xfffffff8,%eax
 100153b:	01 45 fc             	add    %eax,-0x4(%ebp)
 100153e:	8b 45 fc             	mov    -0x4(%ebp),%eax
 1001541:	83 e8 04             	sub    $0x4,%eax
 1001544:	8b 00                	mov    (%eax),%eax
 1001546:	83 e0 f8             	and    $0xfffffff8,%eax
 1001549:	85 c0                	test   %eax,%eax
 100154b:	7f bf                	jg     100150c <find_fit+0x10>
 100154d:	b8 00 00 00 00       	mov    $0x0,%eax
 1001552:	c9                   	leave  
 1001553:	c3                   	ret    

01001554 <coalesce>:
 1001554:	55                   	push   %ebp
 1001555:	89 e5                	mov    %esp,%ebp
 1001557:	83 ec 10             	sub    $0x10,%esp
 100155a:	8b 45 08             	mov    0x8(%ebp),%eax
 100155d:	83 e8 08             	sub    $0x8,%eax
 1001560:	8b 00                	mov    (%eax),%eax
 1001562:	89 c2                	mov    %eax,%edx
 1001564:	83 e2 f8             	and    $0xfffffff8,%edx
 1001567:	b8 fc ff ff ff       	mov    $0xfffffffc,%eax
 100156c:	29 d0                	sub    %edx,%eax
 100156e:	03 45 08             	add    0x8(%ebp),%eax
 1001571:	8b 00                	mov    (%eax),%eax
 1001573:	89 c2                	mov    %eax,%edx
 1001575:	83 e2 f8             	and    $0xfffffff8,%edx
 1001578:	8b 45 08             	mov    0x8(%ebp),%eax
 100157b:	83 e8 08             	sub    $0x8,%eax
 100157e:	8b 00                	mov    (%eax),%eax
 1001580:	83 e0 f8             	and    $0xfffffff8,%eax
 1001583:	89 d1                	mov    %edx,%ecx
 1001585:	29 c1                	sub    %eax,%ecx
 1001587:	89 c8                	mov    %ecx,%eax
 1001589:	83 e8 08             	sub    $0x8,%eax
 100158c:	03 45 08             	add    0x8(%ebp),%eax
 100158f:	8b 00                	mov    (%eax),%eax
 1001591:	83 e0 01             	and    $0x1,%eax
 1001594:	89 45 fc             	mov    %eax,-0x4(%ebp)
 1001597:	8b 45 08             	mov    0x8(%ebp),%eax
 100159a:	83 e8 04             	sub    $0x4,%eax
 100159d:	8b 00                	mov    (%eax),%eax
 100159f:	83 e0 f8             	and    $0xfffffff8,%eax
 10015a2:	83 e8 04             	sub    $0x4,%eax
 10015a5:	03 45 08             	add    0x8(%ebp),%eax
 10015a8:	8b 00                	mov    (%eax),%eax
 10015aa:	83 e0 01             	and    $0x1,%eax
 10015ad:	89 45 f8             	mov    %eax,-0x8(%ebp)
 10015b0:	8b 45 08             	mov    0x8(%ebp),%eax
 10015b3:	83 e8 04             	sub    $0x4,%eax
 10015b6:	8b 00                	mov    (%eax),%eax
 10015b8:	83 e0 f8             	and    $0xfffffff8,%eax
 10015bb:	89 45 f4             	mov    %eax,-0xc(%ebp)
 10015be:	83 7d fc 00          	cmpl   $0x0,-0x4(%ebp)
 10015c2:	74 0e                	je     10015d2 <coalesce+0x7e>
 10015c4:	83 7d f8 00          	cmpl   $0x0,-0x8(%ebp)
 10015c8:	74 08                	je     10015d2 <coalesce+0x7e>
 10015ca:	8b 45 08             	mov    0x8(%ebp),%eax
 10015cd:	e9 6f 01 00 00       	jmp    1001741 <coalesce+0x1ed>
 10015d2:	83 7d fc 00          	cmpl   $0x0,-0x4(%ebp)
 10015d6:	74 48                	je     1001620 <coalesce+0xcc>
 10015d8:	83 7d f8 00          	cmpl   $0x0,-0x8(%ebp)
 10015dc:	75 42                	jne    1001620 <coalesce+0xcc>
 10015de:	8b 45 08             	mov    0x8(%ebp),%eax
 10015e1:	83 e8 04             	sub    $0x4,%eax
 10015e4:	8b 00                	mov    (%eax),%eax
 10015e6:	83 e0 f8             	and    $0xfffffff8,%eax
 10015e9:	83 e8 04             	sub    $0x4,%eax
 10015ec:	03 45 08             	add    0x8(%ebp),%eax
 10015ef:	8b 00                	mov    (%eax),%eax
 10015f1:	83 e0 f8             	and    $0xfffffff8,%eax
 10015f4:	01 45 f4             	add    %eax,-0xc(%ebp)
 10015f7:	8b 45 08             	mov    0x8(%ebp),%eax
 10015fa:	8d 50 fc             	lea    -0x4(%eax),%edx
 10015fd:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1001600:	89 02                	mov    %eax,(%edx)
 1001602:	8b 45 08             	mov    0x8(%ebp),%eax
 1001605:	83 e8 04             	sub    $0x4,%eax
 1001608:	8b 00                	mov    (%eax),%eax
 100160a:	83 e0 f8             	and    $0xfffffff8,%eax
 100160d:	83 e8 08             	sub    $0x8,%eax
 1001610:	03 45 08             	add    0x8(%ebp),%eax
 1001613:	8b 55 f4             	mov    -0xc(%ebp),%edx
 1001616:	89 10                	mov    %edx,(%eax)
 1001618:	8b 45 08             	mov    0x8(%ebp),%eax
 100161b:	e9 21 01 00 00       	jmp    1001741 <coalesce+0x1ed>
 1001620:	83 7d fc 00          	cmpl   $0x0,-0x4(%ebp)
 1001624:	75 6c                	jne    1001692 <coalesce+0x13e>
 1001626:	83 7d f8 00          	cmpl   $0x0,-0x8(%ebp)
 100162a:	74 66                	je     1001692 <coalesce+0x13e>
 100162c:	8b 45 08             	mov    0x8(%ebp),%eax
 100162f:	83 e8 08             	sub    $0x8,%eax
 1001632:	8b 00                	mov    (%eax),%eax
 1001634:	89 c2                	mov    %eax,%edx
 1001636:	83 e2 f8             	and    $0xfffffff8,%edx
 1001639:	b8 fc ff ff ff       	mov    $0xfffffffc,%eax
 100163e:	29 d0                	sub    %edx,%eax
 1001640:	03 45 08             	add    0x8(%ebp),%eax
 1001643:	8b 00                	mov    (%eax),%eax
 1001645:	83 e0 f8             	and    $0xfffffff8,%eax
 1001648:	01 45 f4             	add    %eax,-0xc(%ebp)
 100164b:	8b 45 08             	mov    0x8(%ebp),%eax
 100164e:	83 e8 04             	sub    $0x4,%eax
 1001651:	8b 00                	mov    (%eax),%eax
 1001653:	83 e0 f8             	and    $0xfffffff8,%eax
 1001656:	83 e8 08             	sub    $0x8,%eax
 1001659:	03 45 08             	add    0x8(%ebp),%eax
 100165c:	8b 55 f4             	mov    -0xc(%ebp),%edx
 100165f:	89 10                	mov    %edx,(%eax)
 1001661:	8b 45 08             	mov    0x8(%ebp),%eax
 1001664:	83 e8 08             	sub    $0x8,%eax
 1001667:	8b 00                	mov    (%eax),%eax
 1001669:	89 c2                	mov    %eax,%edx
 100166b:	83 e2 f8             	and    $0xfffffff8,%edx
 100166e:	b8 fc ff ff ff       	mov    $0xfffffffc,%eax
 1001673:	29 d0                	sub    %edx,%eax
 1001675:	03 45 08             	add    0x8(%ebp),%eax
 1001678:	8b 55 f4             	mov    -0xc(%ebp),%edx
 100167b:	89 10                	mov    %edx,(%eax)
 100167d:	8b 45 08             	mov    0x8(%ebp),%eax
 1001680:	83 e8 08             	sub    $0x8,%eax
 1001683:	8b 00                	mov    (%eax),%eax
 1001685:	83 e0 f8             	and    $0xfffffff8,%eax
 1001688:	f7 d8                	neg    %eax
 100168a:	03 45 08             	add    0x8(%ebp),%eax
 100168d:	e9 af 00 00 00       	jmp    1001741 <coalesce+0x1ed>
 1001692:	8b 45 08             	mov    0x8(%ebp),%eax
 1001695:	83 e8 08             	sub    $0x8,%eax
 1001698:	8b 00                	mov    (%eax),%eax
 100169a:	89 c2                	mov    %eax,%edx
 100169c:	83 e2 f8             	and    $0xfffffff8,%edx
 100169f:	b8 fc ff ff ff       	mov    $0xfffffffc,%eax
 10016a4:	29 d0                	sub    %edx,%eax
 10016a6:	03 45 08             	add    0x8(%ebp),%eax
 10016a9:	8b 00                	mov    (%eax),%eax
 10016ab:	89 c2                	mov    %eax,%edx
 10016ad:	83 e2 f8             	and    $0xfffffff8,%edx
 10016b0:	8b 45 08             	mov    0x8(%ebp),%eax
 10016b3:	83 e8 04             	sub    $0x4,%eax
 10016b6:	8b 00                	mov    (%eax),%eax
 10016b8:	89 c1                	mov    %eax,%ecx
 10016ba:	83 e1 f8             	and    $0xfffffff8,%ecx
 10016bd:	8b 45 08             	mov    0x8(%ebp),%eax
 10016c0:	83 e8 04             	sub    $0x4,%eax
 10016c3:	8b 00                	mov    (%eax),%eax
 10016c5:	83 e0 f8             	and    $0xfffffff8,%eax
 10016c8:	83 e8 04             	sub    $0x4,%eax
 10016cb:	03 45 08             	add    0x8(%ebp),%eax
 10016ce:	8b 00                	mov    (%eax),%eax
 10016d0:	83 e0 f8             	and    $0xfffffff8,%eax
 10016d3:	01 c8                	add    %ecx,%eax
 10016d5:	83 e8 08             	sub    $0x8,%eax
 10016d8:	03 45 08             	add    0x8(%ebp),%eax
 10016db:	8b 00                	mov    (%eax),%eax
 10016dd:	83 e0 f8             	and    $0xfffffff8,%eax
 10016e0:	01 d0                	add    %edx,%eax
 10016e2:	01 45 f4             	add    %eax,-0xc(%ebp)
 10016e5:	8b 45 08             	mov    0x8(%ebp),%eax
 10016e8:	83 e8 08             	sub    $0x8,%eax
 10016eb:	8b 00                	mov    (%eax),%eax
 10016ed:	89 c2                	mov    %eax,%edx
 10016ef:	83 e2 f8             	and    $0xfffffff8,%edx
 10016f2:	b8 fc ff ff ff       	mov    $0xfffffffc,%eax
 10016f7:	29 d0                	sub    %edx,%eax
 10016f9:	03 45 08             	add    0x8(%ebp),%eax
 10016fc:	8b 55 f4             	mov    -0xc(%ebp),%edx
 10016ff:	89 10                	mov    %edx,(%eax)
 1001701:	8b 45 08             	mov    0x8(%ebp),%eax
 1001704:	83 e8 04             	sub    $0x4,%eax
 1001707:	8b 00                	mov    (%eax),%eax
 1001709:	89 c2                	mov    %eax,%edx
 100170b:	83 e2 f8             	and    $0xfffffff8,%edx
 100170e:	8b 45 08             	mov    0x8(%ebp),%eax
 1001711:	83 e8 04             	sub    $0x4,%eax
 1001714:	8b 00                	mov    (%eax),%eax
 1001716:	83 e0 f8             	and    $0xfffffff8,%eax
 1001719:	83 e8 04             	sub    $0x4,%eax
 100171c:	03 45 08             	add    0x8(%ebp),%eax
 100171f:	8b 00                	mov    (%eax),%eax
 1001721:	83 e0 f8             	and    $0xfffffff8,%eax
 1001724:	01 d0                	add    %edx,%eax
 1001726:	83 e8 08             	sub    $0x8,%eax
 1001729:	03 45 08             	add    0x8(%ebp),%eax
 100172c:	8b 55 f4             	mov    -0xc(%ebp),%edx
 100172f:	89 10                	mov    %edx,(%eax)
 1001731:	8b 45 08             	mov    0x8(%ebp),%eax
 1001734:	83 e8 08             	sub    $0x8,%eax
 1001737:	8b 00                	mov    (%eax),%eax
 1001739:	83 e0 f8             	and    $0xfffffff8,%eax
 100173c:	f7 d8                	neg    %eax
 100173e:	03 45 08             	add    0x8(%ebp),%eax
 1001741:	c9                   	leave  
 1001742:	c3                   	ret    

01001743 <printblock>:
 1001743:	55                   	push   %ebp
 1001744:	89 e5                	mov    %esp,%ebp
 1001746:	83 ec 38             	sub    $0x38,%esp
 1001749:	8b 45 08             	mov    0x8(%ebp),%eax
 100174c:	83 e8 04             	sub    $0x4,%eax
 100174f:	8b 00                	mov    (%eax),%eax
 1001751:	83 e0 f8             	and    $0xfffffff8,%eax
 1001754:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1001757:	8b 45 08             	mov    0x8(%ebp),%eax
 100175a:	83 e8 04             	sub    $0x4,%eax
 100175d:	8b 00                	mov    (%eax),%eax
 100175f:	83 e0 01             	and    $0x1,%eax
 1001762:	89 45 f0             	mov    %eax,-0x10(%ebp)
 1001765:	8b 45 08             	mov    0x8(%ebp),%eax
 1001768:	83 e8 04             	sub    $0x4,%eax
 100176b:	8b 00                	mov    (%eax),%eax
 100176d:	83 e0 f8             	and    $0xfffffff8,%eax
 1001770:	83 e8 08             	sub    $0x8,%eax
 1001773:	03 45 08             	add    0x8(%ebp),%eax
 1001776:	8b 00                	mov    (%eax),%eax
 1001778:	83 e0 f8             	and    $0xfffffff8,%eax
 100177b:	89 45 ec             	mov    %eax,-0x14(%ebp)
 100177e:	8b 45 08             	mov    0x8(%ebp),%eax
 1001781:	83 e8 04             	sub    $0x4,%eax
 1001784:	8b 00                	mov    (%eax),%eax
 1001786:	83 e0 f8             	and    $0xfffffff8,%eax
 1001789:	83 e8 08             	sub    $0x8,%eax
 100178c:	03 45 08             	add    0x8(%ebp),%eax
 100178f:	8b 00                	mov    (%eax),%eax
 1001791:	83 e0 01             	and    $0x1,%eax
 1001794:	89 45 e8             	mov    %eax,-0x18(%ebp)
 1001797:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 100179b:	75 15                	jne    10017b2 <printblock+0x6f>
 100179d:	8b 45 08             	mov    0x8(%ebp),%eax
 10017a0:	89 44 24 04          	mov    %eax,0x4(%esp)
 10017a4:	c7 04 24 e4 2e 00 01 	movl   $0x1002ee4,(%esp)
 10017ab:	e8 f5 04 00 00       	call   1001ca5 <lprintf>
 10017b0:	eb 4d                	jmp    10017ff <printblock+0xbc>
 10017b2:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
 10017b6:	74 07                	je     10017bf <printblock+0x7c>
 10017b8:	ba 61 00 00 00       	mov    $0x61,%edx
 10017bd:	eb 05                	jmp    10017c4 <printblock+0x81>
 10017bf:	ba 66 00 00 00       	mov    $0x66,%edx
 10017c4:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 10017c8:	74 07                	je     10017d1 <printblock+0x8e>
 10017ca:	b8 61 00 00 00       	mov    $0x61,%eax
 10017cf:	eb 05                	jmp    10017d6 <printblock+0x93>
 10017d1:	b8 66 00 00 00       	mov    $0x66,%eax
 10017d6:	89 54 24 14          	mov    %edx,0x14(%esp)
 10017da:	8b 55 ec             	mov    -0x14(%ebp),%edx
 10017dd:	89 54 24 10          	mov    %edx,0x10(%esp)
 10017e1:	89 44 24 0c          	mov    %eax,0xc(%esp)
 10017e5:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10017e8:	89 44 24 08          	mov    %eax,0x8(%esp)
 10017ec:	8b 45 08             	mov    0x8(%ebp),%eax
 10017ef:	89 44 24 04          	mov    %eax,0x4(%esp)
 10017f3:	c7 04 24 f0 2e 00 01 	movl   $0x1002ef0,(%esp)
 10017fa:	e8 a6 04 00 00       	call   1001ca5 <lprintf>
 10017ff:	c9                   	leave  
 1001800:	c3                   	ret    

01001801 <checkblock>:
 1001801:	55                   	push   %ebp
 1001802:	89 e5                	mov    %esp,%ebp
 1001804:	83 ec 18             	sub    $0x18,%esp
 1001807:	8b 45 08             	mov    0x8(%ebp),%eax
 100180a:	83 e0 07             	and    $0x7,%eax
 100180d:	85 c0                	test   %eax,%eax
 100180f:	74 13                	je     1001824 <checkblock+0x23>
 1001811:	8b 45 08             	mov    0x8(%ebp),%eax
 1001814:	89 44 24 04          	mov    %eax,0x4(%esp)
 1001818:	c7 04 24 18 2f 00 01 	movl   $0x1002f18,(%esp)
 100181f:	e8 81 04 00 00       	call   1001ca5 <lprintf>
 1001824:	8b 45 08             	mov    0x8(%ebp),%eax
 1001827:	83 e8 04             	sub    $0x4,%eax
 100182a:	8b 10                	mov    (%eax),%edx
 100182c:	8b 45 08             	mov    0x8(%ebp),%eax
 100182f:	83 e8 04             	sub    $0x4,%eax
 1001832:	8b 00                	mov    (%eax),%eax
 1001834:	83 e0 f8             	and    $0xfffffff8,%eax
 1001837:	83 e8 08             	sub    $0x8,%eax
 100183a:	03 45 08             	add    0x8(%ebp),%eax
 100183d:	8b 00                	mov    (%eax),%eax
 100183f:	39 c2                	cmp    %eax,%edx
 1001841:	74 0c                	je     100184f <checkblock+0x4e>
 1001843:	c7 04 24 40 2f 00 01 	movl   $0x1002f40,(%esp)
 100184a:	e8 56 04 00 00       	call   1001ca5 <lprintf>
 100184f:	c9                   	leave  
 1001850:	c3                   	ret    
 1001851:	90                   	nop
 1001852:	90                   	nop
 1001853:	90                   	nop

01001854 <mem_init>:
 1001854:	55                   	push   %ebp
 1001855:	89 e5                	mov    %esp,%ebp
 1001857:	83 ec 18             	sub    $0x18,%esp
 100185a:	8b 45 08             	mov    0x8(%ebp),%eax
 100185d:	a3 4c 40 00 01       	mov    %eax,0x100404c
 1001862:	b8 94 50 00 01       	mov    $0x1005094,%eax
 1001867:	a3 50 40 00 01       	mov    %eax,0x1004050
 100186c:	a1 50 40 00 01       	mov    0x1004050,%eax
 1001871:	25 00 f0 ff ff       	and    $0xfffff000,%eax
 1001876:	a3 50 40 00 01       	mov    %eax,0x1004050
 100187b:	eb 0f                	jmp    100188c <mem_init+0x38>
 100187d:	a1 50 40 00 01       	mov    0x1004050,%eax
 1001882:	05 00 10 00 00       	add    $0x1000,%eax
 1001887:	a3 50 40 00 01       	mov    %eax,0x1004050
 100188c:	a1 50 40 00 01       	mov    0x1004050,%eax
 1001891:	c7 44 24 04 00 10 00 	movl   $0x1000,0x4(%esp)
 1001898:	00 
 1001899:	89 04 24             	mov    %eax,(%esp)
 100189c:	e8 13 12 00 00       	call   1002ab4 <new_pages>
 10018a1:	85 c0                	test   %eax,%eax
 10018a3:	75 d8                	jne    100187d <mem_init+0x29>
 10018a5:	a1 50 40 00 01       	mov    0x1004050,%eax
 10018aa:	05 00 10 00 00       	add    $0x1000,%eax
 10018af:	a3 54 40 00 01       	mov    %eax,0x1004054
 10018b4:	c9                   	leave  
 10018b5:	c3                   	ret    

010018b6 <mem_sbrk>:
 10018b6:	55                   	push   %ebp
 10018b7:	89 e5                	mov    %esp,%ebp
 10018b9:	83 ec 28             	sub    $0x28,%esp
 10018bc:	a1 50 40 00 01       	mov    0x1004050,%eax
 10018c1:	89 45 f4             	mov    %eax,-0xc(%ebp)
 10018c4:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
 10018c8:	78 11                	js     10018db <mem_sbrk+0x25>
 10018ca:	8b 45 08             	mov    0x8(%ebp),%eax
 10018cd:	89 c2                	mov    %eax,%edx
 10018cf:	03 55 f4             	add    -0xc(%ebp),%edx
 10018d2:	a1 4c 40 00 01       	mov    0x100404c,%eax
 10018d7:	39 c2                	cmp    %eax,%edx
 10018d9:	76 07                	jbe    10018e2 <mem_sbrk+0x2c>
 10018db:	b8 00 00 00 00       	mov    $0x0,%eax
 10018e0:	eb 77                	jmp    1001959 <mem_sbrk+0xa3>
 10018e2:	8b 45 08             	mov    0x8(%ebp),%eax
 10018e5:	89 c2                	mov    %eax,%edx
 10018e7:	03 55 f4             	add    -0xc(%ebp),%edx
 10018ea:	a1 54 40 00 01       	mov    0x1004054,%eax
 10018ef:	39 c2                	cmp    %eax,%edx
 10018f1:	76 53                	jbe    1001946 <mem_sbrk+0x90>
 10018f3:	8b 45 08             	mov    0x8(%ebp),%eax
 10018f6:	03 45 f4             	add    -0xc(%ebp),%eax
 10018f9:	89 c2                	mov    %eax,%edx
 10018fb:	a1 54 40 00 01       	mov    0x1004054,%eax
 1001900:	89 d1                	mov    %edx,%ecx
 1001902:	29 c1                	sub    %eax,%ecx
 1001904:	89 c8                	mov    %ecx,%eax
 1001906:	89 45 f0             	mov    %eax,-0x10(%ebp)
 1001909:	81 45 f0 ff 0f 00 00 	addl   $0xfff,-0x10(%ebp)
 1001910:	81 65 f0 00 f0 ff ff 	andl   $0xfffff000,-0x10(%ebp)
 1001917:	a1 54 40 00 01       	mov    0x1004054,%eax
 100191c:	8b 55 f0             	mov    -0x10(%ebp),%edx
 100191f:	89 54 24 04          	mov    %edx,0x4(%esp)
 1001923:	89 04 24             	mov    %eax,(%esp)
 1001926:	e8 89 11 00 00       	call   1002ab4 <new_pages>
 100192b:	85 c0                	test   %eax,%eax
 100192d:	74 07                	je     1001936 <mem_sbrk+0x80>
 100192f:	b8 00 00 00 00       	mov    $0x0,%eax
 1001934:	eb 23                	jmp    1001959 <mem_sbrk+0xa3>
 1001936:	8b 15 54 40 00 01    	mov    0x1004054,%edx
 100193c:	8b 45 f0             	mov    -0x10(%ebp),%eax
 100193f:	01 d0                	add    %edx,%eax
 1001941:	a3 54 40 00 01       	mov    %eax,0x1004054
 1001946:	8b 15 50 40 00 01    	mov    0x1004050,%edx
 100194c:	8b 45 08             	mov    0x8(%ebp),%eax
 100194f:	01 d0                	add    %edx,%eax
 1001951:	a3 50 40 00 01       	mov    %eax,0x1004050
 1001956:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1001959:	c9                   	leave  
 100195a:	c3                   	ret    
 100195b:	90                   	nop

0100195c <flush>:
 100195c:	55                   	push   %ebp
 100195d:	89 e5                	mov    %esp,%ebp
 100195f:	83 ec 18             	sub    $0x18,%esp
 1001962:	8b 55 08             	mov    0x8(%ebp),%edx
 1001965:	8b 45 08             	mov    0x8(%ebp),%eax
 1001968:	8b 80 80 00 00 00    	mov    0x80(%eax),%eax
 100196e:	89 54 24 04          	mov    %edx,0x4(%esp)
 1001972:	89 04 24             	mov    %eax,(%esp)
 1001975:	e8 52 10 00 00       	call   10029cc <print>
 100197a:	8b 45 08             	mov    0x8(%ebp),%eax
 100197d:	c7 80 80 00 00 00 00 	movl   $0x0,0x80(%eax)
 1001984:	00 00 00 
 1001987:	c9                   	leave  
 1001988:	c3                   	ret    

01001989 <printf_char>:
 1001989:	55                   	push   %ebp
 100198a:	89 e5                	mov    %esp,%ebp
 100198c:	83 ec 28             	sub    $0x28,%esp
 100198f:	8b 45 08             	mov    0x8(%ebp),%eax
 1001992:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1001995:	83 7d 0c 0a          	cmpl   $0xa,0xc(%ebp)
 1001999:	75 2a                	jne    10019c5 <printf_char+0x3c>
 100199b:	8b 45 f4             	mov    -0xc(%ebp),%eax
 100199e:	8b 80 80 00 00 00    	mov    0x80(%eax),%eax
 10019a4:	8b 55 f4             	mov    -0xc(%ebp),%edx
 10019a7:	c6 04 02 00          	movb   $0x0,(%edx,%eax,1)
 10019ab:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10019ae:	89 04 24             	mov    %eax,(%esp)
 10019b1:	e8 06 01 00 00       	call   1001abc <puts>
 10019b6:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10019b9:	c7 80 80 00 00 00 00 	movl   $0x0,0x80(%eax)
 10019c0:	00 00 00 
 10019c3:	eb 55                	jmp    1001a1a <printf_char+0x91>
 10019c5:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 10019c9:	74 0e                	je     10019d9 <printf_char+0x50>
 10019cb:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10019ce:	8b 80 80 00 00 00    	mov    0x80(%eax),%eax
 10019d4:	83 f8 7f             	cmp    $0x7f,%eax
 10019d7:	76 18                	jbe    10019f1 <printf_char+0x68>
 10019d9:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10019dc:	89 04 24             	mov    %eax,(%esp)
 10019df:	e8 78 ff ff ff       	call   100195c <flush>
 10019e4:	8b 45 0c             	mov    0xc(%ebp),%eax
 10019e7:	89 04 24             	mov    %eax,(%esp)
 10019ea:	e8 ad 00 00 00       	call   1001a9c <putchar>
 10019ef:	eb 29                	jmp    1001a1a <printf_char+0x91>
 10019f1:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10019f4:	8b 80 80 00 00 00    	mov    0x80(%eax),%eax
 10019fa:	8b 55 0c             	mov    0xc(%ebp),%edx
 10019fd:	89 d1                	mov    %edx,%ecx
 10019ff:	8b 55 f4             	mov    -0xc(%ebp),%edx
 1001a02:	88 0c 02             	mov    %cl,(%edx,%eax,1)
 1001a05:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1001a08:	8b 80 80 00 00 00    	mov    0x80(%eax),%eax
 1001a0e:	8d 50 01             	lea    0x1(%eax),%edx
 1001a11:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1001a14:	89 90 80 00 00 00    	mov    %edx,0x80(%eax)
 1001a1a:	c9                   	leave  
 1001a1b:	c3                   	ret    

01001a1c <vprintf>:
 1001a1c:	55                   	push   %ebp
 1001a1d:	89 e5                	mov    %esp,%ebp
 1001a1f:	81 ec b8 00 00 00    	sub    $0xb8,%esp
 1001a25:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 1001a2c:	8d 85 74 ff ff ff    	lea    -0x8c(%ebp),%eax
 1001a32:	89 44 24 10          	mov    %eax,0x10(%esp)
 1001a36:	c7 44 24 0c 89 19 00 	movl   $0x1001989,0xc(%esp)
 1001a3d:	01 
 1001a3e:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
 1001a45:	00 
 1001a46:	8b 45 0c             	mov    0xc(%ebp),%eax
 1001a49:	89 44 24 04          	mov    %eax,0x4(%esp)
 1001a4d:	8b 45 08             	mov    0x8(%ebp),%eax
 1001a50:	89 04 24             	mov    %eax,(%esp)
 1001a53:	e8 fd 02 00 00       	call   1001d55 <_doprnt>
 1001a58:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1001a5b:	85 c0                	test   %eax,%eax
 1001a5d:	74 0e                	je     1001a6d <vprintf+0x51>
 1001a5f:	8d 85 74 ff ff ff    	lea    -0x8c(%ebp),%eax
 1001a65:	89 04 24             	mov    %eax,(%esp)
 1001a68:	e8 ef fe ff ff       	call   100195c <flush>
 1001a6d:	b8 00 00 00 00       	mov    $0x0,%eax
 1001a72:	c9                   	leave  
 1001a73:	c3                   	ret    

01001a74 <printf>:
 1001a74:	55                   	push   %ebp
 1001a75:	89 e5                	mov    %esp,%ebp
 1001a77:	83 ec 28             	sub    $0x28,%esp
 1001a7a:	8d 45 0c             	lea    0xc(%ebp),%eax
 1001a7d:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1001a80:	8b 45 08             	mov    0x8(%ebp),%eax
 1001a83:	8b 55 f4             	mov    -0xc(%ebp),%edx
 1001a86:	89 54 24 04          	mov    %edx,0x4(%esp)
 1001a8a:	89 04 24             	mov    %eax,(%esp)
 1001a8d:	e8 8a ff ff ff       	call   1001a1c <vprintf>
 1001a92:	89 45 f0             	mov    %eax,-0x10(%ebp)
 1001a95:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1001a98:	c9                   	leave  
 1001a99:	c3                   	ret    
 1001a9a:	90                   	nop
 1001a9b:	90                   	nop

01001a9c <putchar>:
 1001a9c:	55                   	push   %ebp
 1001a9d:	89 e5                	mov    %esp,%ebp
 1001a9f:	83 ec 18             	sub    $0x18,%esp
 1001aa2:	8d 45 08             	lea    0x8(%ebp),%eax
 1001aa5:	89 44 24 04          	mov    %eax,0x4(%esp)
 1001aa9:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
 1001ab0:	e8 17 0f 00 00       	call   10029cc <print>
 1001ab5:	8b 45 08             	mov    0x8(%ebp),%eax
 1001ab8:	c9                   	leave  
 1001ab9:	c3                   	ret    
 1001aba:	90                   	nop
 1001abb:	90                   	nop

01001abc <puts>:
 1001abc:	55                   	push   %ebp
 1001abd:	89 e5                	mov    %esp,%ebp
 1001abf:	83 ec 18             	sub    $0x18,%esp
 1001ac2:	eb 15                	jmp    1001ad9 <puts+0x1d>
 1001ac4:	8b 45 08             	mov    0x8(%ebp),%eax
 1001ac7:	0f b6 00             	movzbl (%eax),%eax
 1001aca:	0f be c0             	movsbl %al,%eax
 1001acd:	89 04 24             	mov    %eax,(%esp)
 1001ad0:	e8 c7 ff ff ff       	call   1001a9c <putchar>
 1001ad5:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 1001ad9:	8b 45 08             	mov    0x8(%ebp),%eax
 1001adc:	0f b6 00             	movzbl (%eax),%eax
 1001adf:	84 c0                	test   %al,%al
 1001ae1:	75 e1                	jne    1001ac4 <puts+0x8>
 1001ae3:	c7 04 24 0a 00 00 00 	movl   $0xa,(%esp)
 1001aea:	e8 ad ff ff ff       	call   1001a9c <putchar>
 1001aef:	b8 00 00 00 00       	mov    $0x0,%eax
 1001af4:	c9                   	leave  
 1001af5:	c3                   	ret    
 1001af6:	90                   	nop
 1001af7:	90                   	nop

01001af8 <savechar>:
 1001af8:	55                   	push   %ebp
 1001af9:	89 e5                	mov    %esp,%ebp
 1001afb:	83 ec 10             	sub    $0x10,%esp
 1001afe:	8b 45 08             	mov    0x8(%ebp),%eax
 1001b01:	89 45 fc             	mov    %eax,-0x4(%ebp)
 1001b04:	8b 45 fc             	mov    -0x4(%ebp),%eax
 1001b07:	8b 40 08             	mov    0x8(%eax),%eax
 1001b0a:	83 f8 ff             	cmp    $0xffffffff,%eax
 1001b0d:	74 10                	je     1001b1f <savechar+0x27>
 1001b0f:	8b 45 fc             	mov    -0x4(%ebp),%eax
 1001b12:	8b 50 04             	mov    0x4(%eax),%edx
 1001b15:	8b 45 fc             	mov    -0x4(%ebp),%eax
 1001b18:	8b 40 08             	mov    0x8(%eax),%eax
 1001b1b:	39 c2                	cmp    %eax,%edx
 1001b1d:	74 28                	je     1001b47 <savechar+0x4f>
 1001b1f:	8b 45 fc             	mov    -0x4(%ebp),%eax
 1001b22:	8b 40 04             	mov    0x4(%eax),%eax
 1001b25:	8d 50 01             	lea    0x1(%eax),%edx
 1001b28:	8b 45 fc             	mov    -0x4(%ebp),%eax
 1001b2b:	89 50 04             	mov    %edx,0x4(%eax)
 1001b2e:	8b 45 fc             	mov    -0x4(%ebp),%eax
 1001b31:	8b 00                	mov    (%eax),%eax
 1001b33:	8b 55 0c             	mov    0xc(%ebp),%edx
 1001b36:	88 10                	mov    %dl,(%eax)
 1001b38:	8b 45 fc             	mov    -0x4(%ebp),%eax
 1001b3b:	8b 00                	mov    (%eax),%eax
 1001b3d:	8d 50 01             	lea    0x1(%eax),%edx
 1001b40:	8b 45 fc             	mov    -0x4(%ebp),%eax
 1001b43:	89 10                	mov    %edx,(%eax)
 1001b45:	eb 01                	jmp    1001b48 <savechar+0x50>
 1001b47:	90                   	nop
 1001b48:	c9                   	leave  
 1001b49:	c3                   	ret    

01001b4a <vsprintf>:
 1001b4a:	55                   	push   %ebp
 1001b4b:	89 e5                	mov    %esp,%ebp
 1001b4d:	83 ec 38             	sub    $0x38,%esp
 1001b50:	c7 45 f4 ff ff ff ff 	movl   $0xffffffff,-0xc(%ebp)
 1001b57:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 1001b5e:	8b 45 08             	mov    0x8(%ebp),%eax
 1001b61:	89 45 ec             	mov    %eax,-0x14(%ebp)
 1001b64:	8d 45 ec             	lea    -0x14(%ebp),%eax
 1001b67:	89 44 24 10          	mov    %eax,0x10(%esp)
 1001b6b:	c7 44 24 0c f8 1a 00 	movl   $0x1001af8,0xc(%esp)
 1001b72:	01 
 1001b73:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
 1001b7a:	00 
 1001b7b:	8b 45 10             	mov    0x10(%ebp),%eax
 1001b7e:	89 44 24 04          	mov    %eax,0x4(%esp)
 1001b82:	8b 45 0c             	mov    0xc(%ebp),%eax
 1001b85:	89 04 24             	mov    %eax,(%esp)
 1001b88:	e8 c8 01 00 00       	call   1001d55 <_doprnt>
 1001b8d:	8b 45 ec             	mov    -0x14(%ebp),%eax
 1001b90:	c6 00 00             	movb   $0x0,(%eax)
 1001b93:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1001b96:	c9                   	leave  
 1001b97:	c3                   	ret    

01001b98 <vsnprintf>:
 1001b98:	55                   	push   %ebp
 1001b99:	89 e5                	mov    %esp,%ebp
 1001b9b:	83 ec 38             	sub    $0x38,%esp
 1001b9e:	8b 45 0c             	mov    0xc(%ebp),%eax
 1001ba1:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1001ba4:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 1001bab:	8b 45 08             	mov    0x8(%ebp),%eax
 1001bae:	89 45 ec             	mov    %eax,-0x14(%ebp)
 1001bb1:	8d 45 ec             	lea    -0x14(%ebp),%eax
 1001bb4:	89 44 24 10          	mov    %eax,0x10(%esp)
 1001bb8:	c7 44 24 0c f8 1a 00 	movl   $0x1001af8,0xc(%esp)
 1001bbf:	01 
 1001bc0:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
 1001bc7:	00 
 1001bc8:	8b 45 14             	mov    0x14(%ebp),%eax
 1001bcb:	89 44 24 04          	mov    %eax,0x4(%esp)
 1001bcf:	8b 45 10             	mov    0x10(%ebp),%eax
 1001bd2:	89 04 24             	mov    %eax,(%esp)
 1001bd5:	e8 7b 01 00 00       	call   1001d55 <_doprnt>
 1001bda:	8b 45 ec             	mov    -0x14(%ebp),%eax
 1001bdd:	c6 00 00             	movb   $0x0,(%eax)
 1001be0:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1001be3:	c9                   	leave  
 1001be4:	c3                   	ret    

01001be5 <sprintf>:
 1001be5:	55                   	push   %ebp
 1001be6:	89 e5                	mov    %esp,%ebp
 1001be8:	83 ec 28             	sub    $0x28,%esp
 1001beb:	8d 45 0c             	lea    0xc(%ebp),%eax
 1001bee:	83 c0 04             	add    $0x4,%eax
 1001bf1:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1001bf4:	8b 45 0c             	mov    0xc(%ebp),%eax
 1001bf7:	8b 55 f4             	mov    -0xc(%ebp),%edx
 1001bfa:	89 54 24 08          	mov    %edx,0x8(%esp)
 1001bfe:	89 44 24 04          	mov    %eax,0x4(%esp)
 1001c02:	8b 45 08             	mov    0x8(%ebp),%eax
 1001c05:	89 04 24             	mov    %eax,(%esp)
 1001c08:	e8 3d ff ff ff       	call   1001b4a <vsprintf>
 1001c0d:	89 45 f0             	mov    %eax,-0x10(%ebp)
 1001c10:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1001c13:	c9                   	leave  
 1001c14:	c3                   	ret    

01001c15 <snprintf>:
 1001c15:	55                   	push   %ebp
 1001c16:	89 e5                	mov    %esp,%ebp
 1001c18:	83 ec 28             	sub    $0x28,%esp
 1001c1b:	8d 45 10             	lea    0x10(%ebp),%eax
 1001c1e:	83 c0 04             	add    $0x4,%eax
 1001c21:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1001c24:	8b 45 10             	mov    0x10(%ebp),%eax
 1001c27:	8b 55 f4             	mov    -0xc(%ebp),%edx
 1001c2a:	89 54 24 0c          	mov    %edx,0xc(%esp)
 1001c2e:	89 44 24 08          	mov    %eax,0x8(%esp)
 1001c32:	8b 45 0c             	mov    0xc(%ebp),%eax
 1001c35:	89 44 24 04          	mov    %eax,0x4(%esp)
 1001c39:	8b 45 08             	mov    0x8(%ebp),%eax
 1001c3c:	89 04 24             	mov    %eax,(%esp)
 1001c3f:	e8 54 ff ff ff       	call   1001b98 <vsnprintf>
 1001c44:	89 45 f0             	mov    %eax,-0x10(%ebp)
 1001c47:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1001c4a:	c9                   	leave  
 1001c4b:	c3                   	ret    

01001c4c <simicsPrint>:
 1001c4c:	55                   	push   %ebp
 1001c4d:	89 e5                	mov    %esp,%ebp
 1001c4f:	53                   	push   %ebx
 1001c50:	50                   	push   %eax
 1001c51:	53                   	push   %ebx
 1001c52:	8b 45 08             	mov    0x8(%ebp),%eax
 1001c55:	bb 0f d0 ad 1b       	mov    $0x1badd00f,%ebx
 1001c5a:	66 87 db             	xchg   %bx,%bx
 1001c5d:	5b                   	pop    %ebx
 1001c5e:	58                   	pop    %eax
 1001c5f:	5b                   	pop    %ebx
 1001c60:	5d                   	pop    %ebp
 1001c61:	c3                   	ret    

01001c62 <lprintf_kern>:
 1001c62:	55                   	push   %ebp
 1001c63:	89 e5                	mov    %esp,%ebp
 1001c65:	81 ec 28 01 00 00    	sub    $0x128,%esp
 1001c6b:	8d 45 0c             	lea    0xc(%ebp),%eax
 1001c6e:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1001c71:	8b 45 08             	mov    0x8(%ebp),%eax
 1001c74:	8b 55 f4             	mov    -0xc(%ebp),%edx
 1001c77:	89 54 24 0c          	mov    %edx,0xc(%esp)
 1001c7b:	89 44 24 08          	mov    %eax,0x8(%esp)
 1001c7f:	c7 44 24 04 ff 00 00 	movl   $0xff,0x4(%esp)
 1001c86:	00 
 1001c87:	8d 85 f4 fe ff ff    	lea    -0x10c(%ebp),%eax
 1001c8d:	89 04 24             	mov    %eax,(%esp)
 1001c90:	e8 03 ff ff ff       	call   1001b98 <vsnprintf>
 1001c95:	8d 85 f4 fe ff ff    	lea    -0x10c(%ebp),%eax
 1001c9b:	89 04 24             	mov    %eax,(%esp)
 1001c9e:	e8 a9 ff ff ff       	call   1001c4c <simicsPrint>
 1001ca3:	c9                   	leave  
 1001ca4:	c3                   	ret    

01001ca5 <lprintf>:
 1001ca5:	55                   	push   %ebp
 1001ca6:	89 e5                	mov    %esp,%ebp
 1001ca8:	81 ec 28 01 00 00    	sub    $0x128,%esp
 1001cae:	8d 45 0c             	lea    0xc(%ebp),%eax
 1001cb1:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1001cb4:	8b 45 08             	mov    0x8(%ebp),%eax
 1001cb7:	8b 55 f4             	mov    -0xc(%ebp),%edx
 1001cba:	89 54 24 0c          	mov    %edx,0xc(%esp)
 1001cbe:	89 44 24 08          	mov    %eax,0x8(%esp)
 1001cc2:	c7 44 24 04 ff 00 00 	movl   $0xff,0x4(%esp)
 1001cc9:	00 
 1001cca:	8d 85 f4 fe ff ff    	lea    -0x10c(%ebp),%eax
 1001cd0:	89 04 24             	mov    %eax,(%esp)
 1001cd3:	e8 c0 fe ff ff       	call   1001b98 <vsnprintf>
 1001cd8:	8d 85 f4 fe ff ff    	lea    -0x10c(%ebp),%eax
 1001cde:	89 04 24             	mov    %eax,(%esp)
 1001ce1:	e8 66 ff ff ff       	call   1001c4c <simicsPrint>
 1001ce6:	c9                   	leave  
 1001ce7:	c3                   	ret    

01001ce8 <printnum>:
 1001ce8:	55                   	push   %ebp
 1001ce9:	89 e5                	mov    %esp,%ebp
 1001ceb:	57                   	push   %edi
 1001cec:	56                   	push   %esi
 1001ced:	53                   	push   %ebx
 1001cee:	83 ec 4c             	sub    $0x4c,%esp
 1001cf1:	8b 4d 08             	mov    0x8(%ebp),%ecx
 1001cf4:	8b 7d 0c             	mov    0xc(%ebp),%edi
 1001cf7:	8d 45 c8             	lea    -0x38(%ebp),%eax
 1001cfa:	8d 58 1f             	lea    0x1f(%eax),%ebx
 1001cfd:	89 fe                	mov    %edi,%esi
 1001cff:	89 c8                	mov    %ecx,%eax
 1001d01:	ba 00 00 00 00       	mov    $0x0,%edx
 1001d06:	f7 f6                	div    %esi
 1001d08:	89 d0                	mov    %edx,%eax
 1001d0a:	0f b6 80 25 40 00 01 	movzbl 0x1004025(%eax),%eax
 1001d11:	88 03                	mov    %al,(%ebx)
 1001d13:	83 eb 01             	sub    $0x1,%ebx
 1001d16:	89 7d c4             	mov    %edi,-0x3c(%ebp)
 1001d19:	89 c8                	mov    %ecx,%eax
 1001d1b:	ba 00 00 00 00       	mov    $0x0,%edx
 1001d20:	f7 75 c4             	divl   -0x3c(%ebp)
 1001d23:	89 c1                	mov    %eax,%ecx
 1001d25:	85 c9                	test   %ecx,%ecx
 1001d27:	75 d4                	jne    1001cfd <printnum+0x15>
 1001d29:	eb 15                	jmp    1001d40 <printnum+0x58>
 1001d2b:	0f b6 03             	movzbl (%ebx),%eax
 1001d2e:	0f be c0             	movsbl %al,%eax
 1001d31:	89 44 24 04          	mov    %eax,0x4(%esp)
 1001d35:	8b 45 14             	mov    0x14(%ebp),%eax
 1001d38:	89 04 24             	mov    %eax,(%esp)
 1001d3b:	8b 45 10             	mov    0x10(%ebp),%eax
 1001d3e:	ff d0                	call   *%eax
 1001d40:	83 c3 01             	add    $0x1,%ebx
 1001d43:	8d 45 c8             	lea    -0x38(%ebp),%eax
 1001d46:	83 c0 20             	add    $0x20,%eax
 1001d49:	39 c3                	cmp    %eax,%ebx
 1001d4b:	75 de                	jne    1001d2b <printnum+0x43>
 1001d4d:	83 c4 4c             	add    $0x4c,%esp
 1001d50:	5b                   	pop    %ebx
 1001d51:	5e                   	pop    %esi
 1001d52:	5f                   	pop    %edi
 1001d53:	5d                   	pop    %ebp
 1001d54:	c3                   	ret    

01001d55 <_doprnt>:
 1001d55:	55                   	push   %ebp
 1001d56:	89 e5                	mov    %esp,%ebp
 1001d58:	57                   	push   %edi
 1001d59:	56                   	push   %esi
 1001d5a:	53                   	push   %ebx
 1001d5b:	81 ec 8c 00 00 00    	sub    $0x8c,%esp
 1001d61:	8b 45 08             	mov    0x8(%ebp),%eax
 1001d64:	89 45 b8             	mov    %eax,-0x48(%ebp)
 1001d67:	e9 9f 07 00 00       	jmp    100250b <_doprnt+0x7b6>
 1001d6c:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001d6f:	0f b6 00             	movzbl (%eax),%eax
 1001d72:	3c 25                	cmp    $0x25,%al
 1001d74:	74 21                	je     1001d97 <_doprnt+0x42>
 1001d76:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001d79:	0f b6 00             	movzbl (%eax),%eax
 1001d7c:	0f be c0             	movsbl %al,%eax
 1001d7f:	83 45 b8 01          	addl   $0x1,-0x48(%ebp)
 1001d83:	89 44 24 04          	mov    %eax,0x4(%esp)
 1001d87:	8b 45 18             	mov    0x18(%ebp),%eax
 1001d8a:	89 04 24             	mov    %eax,(%esp)
 1001d8d:	8b 45 14             	mov    0x14(%ebp),%eax
 1001d90:	ff d0                	call   *%eax
 1001d92:	e9 74 07 00 00       	jmp    100250b <_doprnt+0x7b6>
 1001d97:	83 45 b8 01          	addl   $0x1,-0x48(%ebp)
 1001d9b:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
 1001da2:	c7 45 e0 ff ff ff ff 	movl   $0xffffffff,-0x20(%ebp)
 1001da9:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
 1001db0:	c6 45 db 20          	movb   $0x20,-0x25(%ebp)
 1001db4:	c7 45 cc 00 00 00 00 	movl   $0x0,-0x34(%ebp)
 1001dbb:	c7 45 c8 00 00 00 00 	movl   $0x0,-0x38(%ebp)
 1001dc2:	c7 45 c4 00 00 00 00 	movl   $0x0,-0x3c(%ebp)
 1001dc9:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001dcc:	0f b6 00             	movzbl (%eax),%eax
 1001dcf:	3c 23                	cmp    $0x23,%al
 1001dd1:	75 0d                	jne    1001de0 <_doprnt+0x8b>
 1001dd3:	c7 45 c4 01 00 00 00 	movl   $0x1,-0x3c(%ebp)
 1001dda:	83 45 b8 01          	addl   $0x1,-0x48(%ebp)
 1001dde:	eb e9                	jmp    1001dc9 <_doprnt+0x74>
 1001de0:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001de3:	0f b6 00             	movzbl (%eax),%eax
 1001de6:	3c 2d                	cmp    $0x2d,%al
 1001de8:	75 0d                	jne    1001df7 <_doprnt+0xa2>
 1001dea:	c7 45 dc 01 00 00 00 	movl   $0x1,-0x24(%ebp)
 1001df1:	83 45 b8 01          	addl   $0x1,-0x48(%ebp)
 1001df5:	eb d2                	jmp    1001dc9 <_doprnt+0x74>
 1001df7:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001dfa:	0f b6 00             	movzbl (%eax),%eax
 1001dfd:	3c 2b                	cmp    $0x2b,%al
 1001dff:	75 0d                	jne    1001e0e <_doprnt+0xb9>
 1001e01:	c7 45 cc 2b 00 00 00 	movl   $0x2b,-0x34(%ebp)
 1001e08:	83 45 b8 01          	addl   $0x1,-0x48(%ebp)
 1001e0c:	eb bb                	jmp    1001dc9 <_doprnt+0x74>
 1001e0e:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001e11:	0f b6 00             	movzbl (%eax),%eax
 1001e14:	3c 20                	cmp    $0x20,%al
 1001e16:	75 13                	jne    1001e2b <_doprnt+0xd6>
 1001e18:	83 7d cc 00          	cmpl   $0x0,-0x34(%ebp)
 1001e1c:	75 07                	jne    1001e25 <_doprnt+0xd0>
 1001e1e:	c7 45 cc 20 00 00 00 	movl   $0x20,-0x34(%ebp)
 1001e25:	83 45 b8 01          	addl   $0x1,-0x48(%ebp)
 1001e29:	eb 9e                	jmp    1001dc9 <_doprnt+0x74>
 1001e2b:	90                   	nop
 1001e2c:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001e2f:	0f b6 00             	movzbl (%eax),%eax
 1001e32:	3c 30                	cmp    $0x30,%al
 1001e34:	75 08                	jne    1001e3e <_doprnt+0xe9>
 1001e36:	c6 45 db 30          	movb   $0x30,-0x25(%ebp)
 1001e3a:	83 45 b8 01          	addl   $0x1,-0x48(%ebp)
 1001e3e:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001e41:	0f b6 00             	movzbl (%eax),%eax
 1001e44:	3c 2f                	cmp    $0x2f,%al
 1001e46:	7e 45                	jle    1001e8d <_doprnt+0x138>
 1001e48:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001e4b:	0f b6 00             	movzbl (%eax),%eax
 1001e4e:	3c 39                	cmp    $0x39,%al
 1001e50:	7f 3b                	jg     1001e8d <_doprnt+0x138>
 1001e52:	eb 23                	jmp    1001e77 <_doprnt+0x122>
 1001e54:	8b 55 e4             	mov    -0x1c(%ebp),%edx
 1001e57:	89 d0                	mov    %edx,%eax
 1001e59:	c1 e0 02             	shl    $0x2,%eax
 1001e5c:	01 d0                	add    %edx,%eax
 1001e5e:	01 c0                	add    %eax,%eax
 1001e60:	89 c2                	mov    %eax,%edx
 1001e62:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001e65:	0f b6 00             	movzbl (%eax),%eax
 1001e68:	0f be c0             	movsbl %al,%eax
 1001e6b:	83 e8 30             	sub    $0x30,%eax
 1001e6e:	01 d0                	add    %edx,%eax
 1001e70:	89 45 e4             	mov    %eax,-0x1c(%ebp)
 1001e73:	83 45 b8 01          	addl   $0x1,-0x48(%ebp)
 1001e77:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001e7a:	0f b6 00             	movzbl (%eax),%eax
 1001e7d:	3c 2f                	cmp    $0x2f,%al
 1001e7f:	7e 3f                	jle    1001ec0 <_doprnt+0x16b>
 1001e81:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001e84:	0f b6 00             	movzbl (%eax),%eax
 1001e87:	3c 39                	cmp    $0x39,%al
 1001e89:	7e c9                	jle    1001e54 <_doprnt+0xff>
 1001e8b:	eb 33                	jmp    1001ec0 <_doprnt+0x16b>
 1001e8d:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001e90:	0f b6 00             	movzbl (%eax),%eax
 1001e93:	3c 2a                	cmp    $0x2a,%al
 1001e95:	75 2a                	jne    1001ec1 <_doprnt+0x16c>
 1001e97:	83 45 0c 04          	addl   $0x4,0xc(%ebp)
 1001e9b:	8b 45 0c             	mov    0xc(%ebp),%eax
 1001e9e:	8b 40 fc             	mov    -0x4(%eax),%eax
 1001ea1:	89 45 e4             	mov    %eax,-0x1c(%ebp)
 1001ea4:	83 45 b8 01          	addl   $0x1,-0x48(%ebp)
 1001ea8:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
 1001eac:	79 13                	jns    1001ec1 <_doprnt+0x16c>
 1001eae:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
 1001eb2:	0f 94 c0             	sete   %al
 1001eb5:	0f b6 c0             	movzbl %al,%eax
 1001eb8:	89 45 dc             	mov    %eax,-0x24(%ebp)
 1001ebb:	f7 5d e4             	negl   -0x1c(%ebp)
 1001ebe:	eb 01                	jmp    1001ec1 <_doprnt+0x16c>
 1001ec0:	90                   	nop
 1001ec1:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001ec4:	0f b6 00             	movzbl (%eax),%eax
 1001ec7:	3c 2e                	cmp    $0x2e,%al
 1001ec9:	75 78                	jne    1001f43 <_doprnt+0x1ee>
 1001ecb:	83 45 b8 01          	addl   $0x1,-0x48(%ebp)
 1001ecf:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001ed2:	0f b6 00             	movzbl (%eax),%eax
 1001ed5:	3c 2f                	cmp    $0x2f,%al
 1001ed7:	7e 4c                	jle    1001f25 <_doprnt+0x1d0>
 1001ed9:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001edc:	0f b6 00             	movzbl (%eax),%eax
 1001edf:	3c 39                	cmp    $0x39,%al
 1001ee1:	7f 42                	jg     1001f25 <_doprnt+0x1d0>
 1001ee3:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
 1001eea:	eb 23                	jmp    1001f0f <_doprnt+0x1ba>
 1001eec:	8b 55 e0             	mov    -0x20(%ebp),%edx
 1001eef:	89 d0                	mov    %edx,%eax
 1001ef1:	c1 e0 02             	shl    $0x2,%eax
 1001ef4:	01 d0                	add    %edx,%eax
 1001ef6:	01 c0                	add    %eax,%eax
 1001ef8:	89 c2                	mov    %eax,%edx
 1001efa:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001efd:	0f b6 00             	movzbl (%eax),%eax
 1001f00:	0f be c0             	movsbl %al,%eax
 1001f03:	83 e8 30             	sub    $0x30,%eax
 1001f06:	01 d0                	add    %edx,%eax
 1001f08:	89 45 e0             	mov    %eax,-0x20(%ebp)
 1001f0b:	83 45 b8 01          	addl   $0x1,-0x48(%ebp)
 1001f0f:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001f12:	0f b6 00             	movzbl (%eax),%eax
 1001f15:	3c 2f                	cmp    $0x2f,%al
 1001f17:	7e 29                	jle    1001f42 <_doprnt+0x1ed>
 1001f19:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001f1c:	0f b6 00             	movzbl (%eax),%eax
 1001f1f:	3c 39                	cmp    $0x39,%al
 1001f21:	7e c9                	jle    1001eec <_doprnt+0x197>
 1001f23:	eb 1d                	jmp    1001f42 <_doprnt+0x1ed>
 1001f25:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001f28:	0f b6 00             	movzbl (%eax),%eax
 1001f2b:	3c 2a                	cmp    $0x2a,%al
 1001f2d:	75 14                	jne    1001f43 <_doprnt+0x1ee>
 1001f2f:	83 45 0c 04          	addl   $0x4,0xc(%ebp)
 1001f33:	8b 45 0c             	mov    0xc(%ebp),%eax
 1001f36:	8b 40 fc             	mov    -0x4(%eax),%eax
 1001f39:	89 45 e0             	mov    %eax,-0x20(%ebp)
 1001f3c:	83 45 b8 01          	addl   $0x1,-0x48(%ebp)
 1001f40:	eb 01                	jmp    1001f43 <_doprnt+0x1ee>
 1001f42:	90                   	nop
 1001f43:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001f46:	0f b6 00             	movzbl (%eax),%eax
 1001f49:	3c 6c                	cmp    $0x6c,%al
 1001f4b:	75 04                	jne    1001f51 <_doprnt+0x1fc>
 1001f4d:	83 45 b8 01          	addl   $0x1,-0x48(%ebp)
 1001f51:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%ebp)
 1001f58:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001f5b:	0f b6 00             	movzbl (%eax),%eax
 1001f5e:	0f be c0             	movsbl %al,%eax
 1001f61:	83 f8 7a             	cmp    $0x7a,%eax
 1001f64:	0f 87 79 05 00 00    	ja     10024e3 <_doprnt+0x78e>
 1001f6a:	8b 04 85 70 2f 00 01 	mov    0x1002f70(,%eax,4),%eax
 1001f71:	ff e0                	jmp    *%eax
 1001f73:	83 45 0c 04          	addl   $0x4,0xc(%ebp)
 1001f77:	8b 45 0c             	mov    0xc(%ebp),%eax
 1001f7a:	8b 40 fc             	mov    -0x4(%eax),%eax
 1001f7d:	89 45 d0             	mov    %eax,-0x30(%ebp)
 1001f80:	83 45 0c 04          	addl   $0x4,0xc(%ebp)
 1001f84:	8b 45 0c             	mov    0xc(%ebp),%eax
 1001f87:	8b 58 fc             	mov    -0x4(%eax),%ebx
 1001f8a:	0f b6 03             	movzbl (%ebx),%eax
 1001f8d:	0f be c0             	movsbl %al,%eax
 1001f90:	89 45 bc             	mov    %eax,-0x44(%ebp)
 1001f93:	83 c3 01             	add    $0x1,%ebx
 1001f96:	8b 45 18             	mov    0x18(%ebp),%eax
 1001f99:	89 44 24 0c          	mov    %eax,0xc(%esp)
 1001f9d:	8b 45 14             	mov    0x14(%ebp),%eax
 1001fa0:	89 44 24 08          	mov    %eax,0x8(%esp)
 1001fa4:	8b 45 bc             	mov    -0x44(%ebp),%eax
 1001fa7:	89 44 24 04          	mov    %eax,0x4(%esp)
 1001fab:	8b 45 d0             	mov    -0x30(%ebp),%eax
 1001fae:	89 04 24             	mov    %eax,(%esp)
 1001fb1:	e8 32 fd ff ff       	call   1001ce8 <printnum>
 1001fb6:	83 7d d0 00          	cmpl   $0x0,-0x30(%ebp)
 1001fba:	0f 84 3d 05 00 00    	je     10024fd <_doprnt+0x7a8>
 1001fc0:	c7 45 b4 00 00 00 00 	movl   $0x0,-0x4c(%ebp)
 1001fc7:	e9 37 01 00 00       	jmp    1002103 <_doprnt+0x3ae>
 1001fcc:	0f b6 03             	movzbl (%ebx),%eax
 1001fcf:	3c 20                	cmp    $0x20,%al
 1001fd1:	0f 8f ad 00 00 00    	jg     1002084 <_doprnt+0x32f>
 1001fd7:	83 7d b4 00          	cmpl   $0x0,-0x4c(%ebp)
 1001fdb:	74 15                	je     1001ff2 <_doprnt+0x29d>
 1001fdd:	c7 44 24 04 2c 00 00 	movl   $0x2c,0x4(%esp)
 1001fe4:	00 
 1001fe5:	8b 45 18             	mov    0x18(%ebp),%eax
 1001fe8:	89 04 24             	mov    %eax,(%esp)
 1001feb:	8b 45 14             	mov    0x14(%ebp),%eax
 1001fee:	ff d0                	call   *%eax
 1001ff0:	eb 1a                	jmp    100200c <_doprnt+0x2b7>
 1001ff2:	c7 44 24 04 3c 00 00 	movl   $0x3c,0x4(%esp)
 1001ff9:	00 
 1001ffa:	8b 45 18             	mov    0x18(%ebp),%eax
 1001ffd:	89 04 24             	mov    %eax,(%esp)
 1002000:	8b 45 14             	mov    0x14(%ebp),%eax
 1002003:	ff d0                	call   *%eax
 1002005:	c7 45 b4 01 00 00 00 	movl   $0x1,-0x4c(%ebp)
 100200c:	0f b6 03             	movzbl (%ebx),%eax
 100200f:	0f be f8             	movsbl %al,%edi
 1002012:	83 c3 01             	add    $0x1,%ebx
 1002015:	eb 16                	jmp    100202d <_doprnt+0x2d8>
 1002017:	0f be 45 af          	movsbl -0x51(%ebp),%eax
 100201b:	89 44 24 04          	mov    %eax,0x4(%esp)
 100201f:	8b 45 18             	mov    0x18(%ebp),%eax
 1002022:	89 04 24             	mov    %eax,(%esp)
 1002025:	8b 45 14             	mov    0x14(%ebp),%eax
 1002028:	ff d0                	call   *%eax
 100202a:	83 c3 01             	add    $0x1,%ebx
 100202d:	0f b6 03             	movzbl (%ebx),%eax
 1002030:	88 45 af             	mov    %al,-0x51(%ebp)
 1002033:	80 7d af 20          	cmpb   $0x20,-0x51(%ebp)
 1002037:	7f de                	jg     1002017 <_doprnt+0x2c2>
 1002039:	8d 47 ff             	lea    -0x1(%edi),%eax
 100203c:	8b 55 d0             	mov    -0x30(%ebp),%edx
 100203f:	89 55 80             	mov    %edx,-0x80(%ebp)
 1002042:	8b 55 80             	mov    -0x80(%ebp),%edx
 1002045:	89 c1                	mov    %eax,%ecx
 1002047:	d3 ea                	shr    %cl,%edx
 1002049:	89 55 84             	mov    %edx,-0x7c(%ebp)
 100204c:	89 f0                	mov    %esi,%eax
 100204e:	29 f8                	sub    %edi,%eax
 1002050:	ba 02 00 00 00       	mov    $0x2,%edx
 1002055:	89 d6                	mov    %edx,%esi
 1002057:	89 c1                	mov    %eax,%ecx
 1002059:	d3 e6                	shl    %cl,%esi
 100205b:	89 f0                	mov    %esi,%eax
 100205d:	83 e8 01             	sub    $0x1,%eax
 1002060:	8b 55 84             	mov    -0x7c(%ebp),%edx
 1002063:	21 c2                	and    %eax,%edx
 1002065:	8b 45 18             	mov    0x18(%ebp),%eax
 1002068:	89 44 24 0c          	mov    %eax,0xc(%esp)
 100206c:	8b 45 14             	mov    0x14(%ebp),%eax
 100206f:	89 44 24 08          	mov    %eax,0x8(%esp)
 1002073:	8b 45 bc             	mov    -0x44(%ebp),%eax
 1002076:	89 44 24 04          	mov    %eax,0x4(%esp)
 100207a:	89 14 24             	mov    %edx,(%esp)
 100207d:	e8 66 fc ff ff       	call   1001ce8 <printnum>
 1002082:	eb 7f                	jmp    1002103 <_doprnt+0x3ae>
 1002084:	8d 46 ff             	lea    -0x1(%esi),%eax
 1002087:	ba 01 00 00 00       	mov    $0x1,%edx
 100208c:	89 d6                	mov    %edx,%esi
 100208e:	89 c1                	mov    %eax,%ecx
 1002090:	d3 e6                	shl    %cl,%esi
 1002092:	89 f0                	mov    %esi,%eax
 1002094:	23 45 d0             	and    -0x30(%ebp),%eax
 1002097:	85 c0                	test   %eax,%eax
 1002099:	74 60                	je     10020fb <_doprnt+0x3a6>
 100209b:	83 7d b4 00          	cmpl   $0x0,-0x4c(%ebp)
 100209f:	74 15                	je     10020b6 <_doprnt+0x361>
 10020a1:	c7 44 24 04 2c 00 00 	movl   $0x2c,0x4(%esp)
 10020a8:	00 
 10020a9:	8b 45 18             	mov    0x18(%ebp),%eax
 10020ac:	89 04 24             	mov    %eax,(%esp)
 10020af:	8b 45 14             	mov    0x14(%ebp),%eax
 10020b2:	ff d0                	call   *%eax
 10020b4:	eb 32                	jmp    10020e8 <_doprnt+0x393>
 10020b6:	c7 44 24 04 3c 00 00 	movl   $0x3c,0x4(%esp)
 10020bd:	00 
 10020be:	8b 45 18             	mov    0x18(%ebp),%eax
 10020c1:	89 04 24             	mov    %eax,(%esp)
 10020c4:	8b 45 14             	mov    0x14(%ebp),%eax
 10020c7:	ff d0                	call   *%eax
 10020c9:	c7 45 b4 01 00 00 00 	movl   $0x1,-0x4c(%ebp)
 10020d0:	eb 16                	jmp    10020e8 <_doprnt+0x393>
 10020d2:	0f be 45 af          	movsbl -0x51(%ebp),%eax
 10020d6:	89 44 24 04          	mov    %eax,0x4(%esp)
 10020da:	8b 45 18             	mov    0x18(%ebp),%eax
 10020dd:	89 04 24             	mov    %eax,(%esp)
 10020e0:	8b 45 14             	mov    0x14(%ebp),%eax
 10020e3:	ff d0                	call   *%eax
 10020e5:	83 c3 01             	add    $0x1,%ebx
 10020e8:	0f b6 03             	movzbl (%ebx),%eax
 10020eb:	88 45 af             	mov    %al,-0x51(%ebp)
 10020ee:	80 7d af 20          	cmpb   $0x20,-0x51(%ebp)
 10020f2:	7f de                	jg     10020d2 <_doprnt+0x37d>
 10020f4:	eb 0d                	jmp    1002103 <_doprnt+0x3ae>
 10020f6:	83 c3 01             	add    $0x1,%ebx
 10020f9:	eb 01                	jmp    10020fc <_doprnt+0x3a7>
 10020fb:	90                   	nop
 10020fc:	0f b6 03             	movzbl (%ebx),%eax
 10020ff:	3c 20                	cmp    $0x20,%al
 1002101:	7f f3                	jg     10020f6 <_doprnt+0x3a1>
 1002103:	0f b6 03             	movzbl (%ebx),%eax
 1002106:	0f be f0             	movsbl %al,%esi
 1002109:	85 f6                	test   %esi,%esi
 100210b:	0f 95 c0             	setne  %al
 100210e:	83 c3 01             	add    $0x1,%ebx
 1002111:	84 c0                	test   %al,%al
 1002113:	0f 85 b3 fe ff ff    	jne    1001fcc <_doprnt+0x277>
 1002119:	83 7d b4 00          	cmpl   $0x0,-0x4c(%ebp)
 100211d:	0f 84 dd 03 00 00    	je     1002500 <_doprnt+0x7ab>
 1002123:	c7 44 24 04 3e 00 00 	movl   $0x3e,0x4(%esp)
 100212a:	00 
 100212b:	8b 45 18             	mov    0x18(%ebp),%eax
 100212e:	89 04 24             	mov    %eax,(%esp)
 1002131:	8b 45 14             	mov    0x14(%ebp),%eax
 1002134:	ff d0                	call   *%eax
 1002136:	e9 c5 03 00 00       	jmp    1002500 <_doprnt+0x7ab>
 100213b:	83 45 0c 04          	addl   $0x4,0xc(%ebp)
 100213f:	8b 45 0c             	mov    0xc(%ebp),%eax
 1002142:	83 e8 04             	sub    $0x4,%eax
 1002145:	8b 00                	mov    (%eax),%eax
 1002147:	88 45 af             	mov    %al,-0x51(%ebp)
 100214a:	0f be 45 af          	movsbl -0x51(%ebp),%eax
 100214e:	89 44 24 04          	mov    %eax,0x4(%esp)
 1002152:	8b 45 18             	mov    0x18(%ebp),%eax
 1002155:	89 04 24             	mov    %eax,(%esp)
 1002158:	8b 45 14             	mov    0x14(%ebp),%eax
 100215b:	ff d0                	call   *%eax
 100215d:	e9 a5 03 00 00       	jmp    1002507 <_doprnt+0x7b2>
 1002162:	83 7d e0 ff          	cmpl   $0xffffffff,-0x20(%ebp)
 1002166:	75 07                	jne    100216f <_doprnt+0x41a>
 1002168:	c7 45 e0 ff ff ff 7f 	movl   $0x7fffffff,-0x20(%ebp)
 100216f:	83 45 0c 04          	addl   $0x4,0xc(%ebp)
 1002173:	8b 45 0c             	mov    0xc(%ebp),%eax
 1002176:	8b 58 fc             	mov    -0x4(%eax),%ebx
 1002179:	85 db                	test   %ebx,%ebx
 100217b:	75 05                	jne    1002182 <_doprnt+0x42d>
 100217d:	bb 68 2f 00 01       	mov    $0x1002f68,%ebx
 1002182:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
 1002186:	7e 4a                	jle    10021d2 <_doprnt+0x47d>
 1002188:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
 100218c:	75 44                	jne    10021d2 <_doprnt+0x47d>
 100218e:	c7 45 d4 00 00 00 00 	movl   $0x0,-0x2c(%ebp)
 1002195:	89 de                	mov    %ebx,%esi
 1002197:	eb 07                	jmp    10021a0 <_doprnt+0x44b>
 1002199:	83 45 d4 01          	addl   $0x1,-0x2c(%ebp)
 100219d:	83 c3 01             	add    $0x1,%ebx
 10021a0:	0f b6 03             	movzbl (%ebx),%eax
 10021a3:	84 c0                	test   %al,%al
 10021a5:	74 08                	je     10021af <_doprnt+0x45a>
 10021a7:	8b 45 d4             	mov    -0x2c(%ebp),%eax
 10021aa:	3b 45 e0             	cmp    -0x20(%ebp),%eax
 10021ad:	7c ea                	jl     1002199 <_doprnt+0x444>
 10021af:	89 f3                	mov    %esi,%ebx
 10021b1:	eb 17                	jmp    10021ca <_doprnt+0x475>
 10021b3:	c7 44 24 04 20 00 00 	movl   $0x20,0x4(%esp)
 10021ba:	00 
 10021bb:	8b 45 18             	mov    0x18(%ebp),%eax
 10021be:	89 04 24             	mov    %eax,(%esp)
 10021c1:	8b 45 14             	mov    0x14(%ebp),%eax
 10021c4:	ff d0                	call   *%eax
 10021c6:	83 45 d4 01          	addl   $0x1,-0x2c(%ebp)
 10021ca:	8b 45 d4             	mov    -0x2c(%ebp),%eax
 10021cd:	3b 45 e4             	cmp    -0x1c(%ebp),%eax
 10021d0:	7c e1                	jl     10021b3 <_doprnt+0x45e>
 10021d2:	c7 45 d4 00 00 00 00 	movl   $0x0,-0x2c(%ebp)
 10021d9:	eb 24                	jmp    10021ff <_doprnt+0x4aa>
 10021db:	83 45 d4 01          	addl   $0x1,-0x2c(%ebp)
 10021df:	8b 45 d4             	mov    -0x2c(%ebp),%eax
 10021e2:	3b 45 e0             	cmp    -0x20(%ebp),%eax
 10021e5:	7f 21                	jg     1002208 <_doprnt+0x4b3>
 10021e7:	0f b6 03             	movzbl (%ebx),%eax
 10021ea:	0f be c0             	movsbl %al,%eax
 10021ed:	83 c3 01             	add    $0x1,%ebx
 10021f0:	89 44 24 04          	mov    %eax,0x4(%esp)
 10021f4:	8b 45 18             	mov    0x18(%ebp),%eax
 10021f7:	89 04 24             	mov    %eax,(%esp)
 10021fa:	8b 45 14             	mov    0x14(%ebp),%eax
 10021fd:	ff d0                	call   *%eax
 10021ff:	0f b6 03             	movzbl (%ebx),%eax
 1002202:	84 c0                	test   %al,%al
 1002204:	75 d5                	jne    10021db <_doprnt+0x486>
 1002206:	eb 01                	jmp    1002209 <_doprnt+0x4b4>
 1002208:	90                   	nop
 1002209:	8b 45 d4             	mov    -0x2c(%ebp),%eax
 100220c:	3b 45 e4             	cmp    -0x1c(%ebp),%eax
 100220f:	0f 8d ee 02 00 00    	jge    1002503 <_doprnt+0x7ae>
 1002215:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
 1002219:	0f 84 e4 02 00 00    	je     1002503 <_doprnt+0x7ae>
 100221f:	eb 17                	jmp    1002238 <_doprnt+0x4e3>
 1002221:	c7 44 24 04 20 00 00 	movl   $0x20,0x4(%esp)
 1002228:	00 
 1002229:	8b 45 18             	mov    0x18(%ebp),%eax
 100222c:	89 04 24             	mov    %eax,(%esp)
 100222f:	8b 45 14             	mov    0x14(%ebp),%eax
 1002232:	ff d0                	call   *%eax
 1002234:	83 45 d4 01          	addl   $0x1,-0x2c(%ebp)
 1002238:	8b 45 d4             	mov    -0x2c(%ebp),%eax
 100223b:	3b 45 e4             	cmp    -0x1c(%ebp),%eax
 100223e:	7c e1                	jl     1002221 <_doprnt+0x4cc>
 1002240:	e9 be 02 00 00       	jmp    1002503 <_doprnt+0x7ae>
 1002245:	a1 58 40 00 01       	mov    0x1004058,%eax
 100224a:	89 45 c0             	mov    %eax,-0x40(%ebp)
 100224d:	c7 45 bc 08 00 00 00 	movl   $0x8,-0x44(%ebp)
 1002254:	e9 cd 00 00 00       	jmp    1002326 <_doprnt+0x5d1>
 1002259:	a1 58 40 00 01       	mov    0x1004058,%eax
 100225e:	89 45 c0             	mov    %eax,-0x40(%ebp)
 1002261:	c7 45 bc 0a 00 00 00 	movl   $0xa,-0x44(%ebp)
 1002268:	e9 87 00 00 00       	jmp    10022f4 <_doprnt+0x59f>
 100226d:	a1 58 40 00 01       	mov    0x1004058,%eax
 1002272:	89 45 c0             	mov    %eax,-0x40(%ebp)
 1002275:	c7 45 bc 0a 00 00 00 	movl   $0xa,-0x44(%ebp)
 100227c:	e9 a5 00 00 00       	jmp    1002326 <_doprnt+0x5d1>
 1002281:	c6 45 db 30          	movb   $0x30,-0x25(%ebp)
 1002285:	c7 45 e4 08 00 00 00 	movl   $0x8,-0x1c(%ebp)
 100228c:	c7 44 24 04 30 00 00 	movl   $0x30,0x4(%esp)
 1002293:	00 
 1002294:	8b 45 18             	mov    0x18(%ebp),%eax
 1002297:	89 04 24             	mov    %eax,(%esp)
 100229a:	8b 45 14             	mov    0x14(%ebp),%eax
 100229d:	ff d0                	call   *%eax
 100229f:	c7 44 24 04 78 00 00 	movl   $0x78,0x4(%esp)
 10022a6:	00 
 10022a7:	8b 45 18             	mov    0x18(%ebp),%eax
 10022aa:	89 04 24             	mov    %eax,(%esp)
 10022ad:	8b 45 14             	mov    0x14(%ebp),%eax
 10022b0:	ff d0                	call   *%eax
 10022b2:	a1 58 40 00 01       	mov    0x1004058,%eax
 10022b7:	89 45 c0             	mov    %eax,-0x40(%ebp)
 10022ba:	c7 45 bc 10 00 00 00 	movl   $0x10,-0x44(%ebp)
 10022c1:	eb 63                	jmp    1002326 <_doprnt+0x5d1>
 10022c3:	a1 58 40 00 01       	mov    0x1004058,%eax
 10022c8:	89 45 c0             	mov    %eax,-0x40(%ebp)
 10022cb:	c7 45 bc 10 00 00 00 	movl   $0x10,-0x44(%ebp)
 10022d2:	eb 20                	jmp    10022f4 <_doprnt+0x59f>
 10022d4:	a1 58 40 00 01       	mov    0x1004058,%eax
 10022d9:	89 45 c0             	mov    %eax,-0x40(%ebp)
 10022dc:	8b 45 10             	mov    0x10(%ebp),%eax
 10022df:	89 45 bc             	mov    %eax,-0x44(%ebp)
 10022e2:	eb 10                	jmp    10022f4 <_doprnt+0x59f>
 10022e4:	a1 58 40 00 01       	mov    0x1004058,%eax
 10022e9:	89 45 c0             	mov    %eax,-0x40(%ebp)
 10022ec:	8b 45 10             	mov    0x10(%ebp),%eax
 10022ef:	89 45 bc             	mov    %eax,-0x44(%ebp)
 10022f2:	eb 32                	jmp    1002326 <_doprnt+0x5d1>
 10022f4:	83 45 0c 04          	addl   $0x4,0xc(%ebp)
 10022f8:	8b 45 0c             	mov    0xc(%ebp),%eax
 10022fb:	8b 40 fc             	mov    -0x4(%eax),%eax
 10022fe:	89 45 d4             	mov    %eax,-0x2c(%ebp)
 1002301:	83 7d d4 00          	cmpl   $0x0,-0x2c(%ebp)
 1002305:	78 0e                	js     1002315 <_doprnt+0x5c0>
 1002307:	8b 45 d4             	mov    -0x2c(%ebp),%eax
 100230a:	89 45 d0             	mov    %eax,-0x30(%ebp)
 100230d:	8b 45 cc             	mov    -0x34(%ebp),%eax
 1002310:	89 45 c8             	mov    %eax,-0x38(%ebp)
 1002313:	eb 1f                	jmp    1002334 <_doprnt+0x5df>
 1002315:	8b 45 d4             	mov    -0x2c(%ebp),%eax
 1002318:	f7 d8                	neg    %eax
 100231a:	89 45 d0             	mov    %eax,-0x30(%ebp)
 100231d:	c7 45 c8 2d 00 00 00 	movl   $0x2d,-0x38(%ebp)
 1002324:	eb 0e                	jmp    1002334 <_doprnt+0x5df>
 1002326:	83 45 0c 04          	addl   $0x4,0xc(%ebp)
 100232a:	8b 45 0c             	mov    0xc(%ebp),%eax
 100232d:	8b 40 fc             	mov    -0x4(%eax),%eax
 1002330:	89 45 d0             	mov    %eax,-0x30(%ebp)
 1002333:	90                   	nop
 1002334:	8d 45 8f             	lea    -0x71(%ebp),%eax
 1002337:	8d 58 1f             	lea    0x1f(%eax),%ebx
 100233a:	c7 45 b0 00 00 00 00 	movl   $0x0,-0x50(%ebp)
 1002341:	83 7d c0 00          	cmpl   $0x0,-0x40(%ebp)
 1002345:	83 7d d0 00          	cmpl   $0x0,-0x30(%ebp)
 1002349:	74 22                	je     100236d <_doprnt+0x618>
 100234b:	83 7d c4 00          	cmpl   $0x0,-0x3c(%ebp)
 100234f:	74 1c                	je     100236d <_doprnt+0x618>
 1002351:	83 7d bc 08          	cmpl   $0x8,-0x44(%ebp)
 1002355:	75 09                	jne    1002360 <_doprnt+0x60b>
 1002357:	c7 45 b0 69 2f 00 01 	movl   $0x1002f69,-0x50(%ebp)
 100235e:	eb 0d                	jmp    100236d <_doprnt+0x618>
 1002360:	83 7d bc 10          	cmpl   $0x10,-0x44(%ebp)
 1002364:	75 07                	jne    100236d <_doprnt+0x618>
 1002366:	c7 45 b0 6b 2f 00 01 	movl   $0x1002f6b,-0x50(%ebp)
 100236d:	8b 4d bc             	mov    -0x44(%ebp),%ecx
 1002370:	8b 45 d0             	mov    -0x30(%ebp),%eax
 1002373:	ba 00 00 00 00       	mov    $0x0,%edx
 1002378:	f7 f1                	div    %ecx
 100237a:	89 d0                	mov    %edx,%eax
 100237c:	0f b6 80 14 40 00 01 	movzbl 0x1004014(%eax),%eax
 1002383:	88 03                	mov    %al,(%ebx)
 1002385:	83 eb 01             	sub    $0x1,%ebx
 1002388:	8b 55 bc             	mov    -0x44(%ebp),%edx
 100238b:	89 55 80             	mov    %edx,-0x80(%ebp)
 100238e:	8b 45 d0             	mov    -0x30(%ebp),%eax
 1002391:	ba 00 00 00 00       	mov    $0x0,%edx
 1002396:	f7 75 80             	divl   -0x80(%ebp)
 1002399:	89 45 d0             	mov    %eax,-0x30(%ebp)
 100239c:	83 6d e0 01          	subl   $0x1,-0x20(%ebp)
 10023a0:	83 7d d0 00          	cmpl   $0x0,-0x30(%ebp)
 10023a4:	75 c7                	jne    100236d <_doprnt+0x618>
 10023a6:	89 d8                	mov    %ebx,%eax
 10023a8:	8d 55 8f             	lea    -0x71(%ebp),%edx
 10023ab:	83 c2 1f             	add    $0x1f,%edx
 10023ae:	29 d0                	sub    %edx,%eax
 10023b0:	01 45 e4             	add    %eax,-0x1c(%ebp)
 10023b3:	83 7d c8 00          	cmpl   $0x0,-0x38(%ebp)
 10023b7:	74 04                	je     10023bd <_doprnt+0x668>
 10023b9:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
 10023bd:	83 7d b0 00          	cmpl   $0x0,-0x50(%ebp)
 10023c1:	74 0e                	je     10023d1 <_doprnt+0x67c>
 10023c3:	8b 45 b0             	mov    -0x50(%ebp),%eax
 10023c6:	89 04 24             	mov    %eax,(%esp)
 10023c9:	e8 ca 05 00 00       	call   1002998 <strlen>
 10023ce:	29 45 e4             	sub    %eax,-0x1c(%ebp)
 10023d1:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
 10023d5:	7e 06                	jle    10023dd <_doprnt+0x688>
 10023d7:	8b 45 e0             	mov    -0x20(%ebp),%eax
 10023da:	29 45 e4             	sub    %eax,-0x1c(%ebp)
 10023dd:	80 7d db 20          	cmpb   $0x20,-0x25(%ebp)
 10023e1:	75 25                	jne    1002408 <_doprnt+0x6b3>
 10023e3:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
 10023e7:	75 1f                	jne    1002408 <_doprnt+0x6b3>
 10023e9:	eb 13                	jmp    10023fe <_doprnt+0x6a9>
 10023eb:	c7 44 24 04 20 00 00 	movl   $0x20,0x4(%esp)
 10023f2:	00 
 10023f3:	8b 45 18             	mov    0x18(%ebp),%eax
 10023f6:	89 04 24             	mov    %eax,(%esp)
 10023f9:	8b 45 14             	mov    0x14(%ebp),%eax
 10023fc:	ff d0                	call   *%eax
 10023fe:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
 1002402:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
 1002406:	79 e3                	jns    10023eb <_doprnt+0x696>
 1002408:	83 7d c8 00          	cmpl   $0x0,-0x38(%ebp)
 100240c:	74 12                	je     1002420 <_doprnt+0x6cb>
 100240e:	8b 45 c8             	mov    -0x38(%ebp),%eax
 1002411:	89 44 24 04          	mov    %eax,0x4(%esp)
 1002415:	8b 45 18             	mov    0x18(%ebp),%eax
 1002418:	89 04 24             	mov    %eax,(%esp)
 100241b:	8b 45 14             	mov    0x14(%ebp),%eax
 100241e:	ff d0                	call   *%eax
 1002420:	83 7d b0 00          	cmpl   $0x0,-0x50(%ebp)
 1002424:	74 3d                	je     1002463 <_doprnt+0x70e>
 1002426:	eb 1c                	jmp    1002444 <_doprnt+0x6ef>
 1002428:	8b 45 b0             	mov    -0x50(%ebp),%eax
 100242b:	0f b6 00             	movzbl (%eax),%eax
 100242e:	0f be c0             	movsbl %al,%eax
 1002431:	83 45 b0 01          	addl   $0x1,-0x50(%ebp)
 1002435:	89 44 24 04          	mov    %eax,0x4(%esp)
 1002439:	8b 45 18             	mov    0x18(%ebp),%eax
 100243c:	89 04 24             	mov    %eax,(%esp)
 100243f:	8b 45 14             	mov    0x14(%ebp),%eax
 1002442:	ff d0                	call   *%eax
 1002444:	8b 45 b0             	mov    -0x50(%ebp),%eax
 1002447:	0f b6 00             	movzbl (%eax),%eax
 100244a:	84 c0                	test   %al,%al
 100244c:	75 da                	jne    1002428 <_doprnt+0x6d3>
 100244e:	eb 13                	jmp    1002463 <_doprnt+0x70e>
 1002450:	c7 44 24 04 30 00 00 	movl   $0x30,0x4(%esp)
 1002457:	00 
 1002458:	8b 45 18             	mov    0x18(%ebp),%eax
 100245b:	89 04 24             	mov    %eax,(%esp)
 100245e:	8b 45 14             	mov    0x14(%ebp),%eax
 1002461:	ff d0                	call   *%eax
 1002463:	83 6d e0 01          	subl   $0x1,-0x20(%ebp)
 1002467:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
 100246b:	79 e3                	jns    1002450 <_doprnt+0x6fb>
 100246d:	80 7d db 30          	cmpb   $0x30,-0x25(%ebp)
 1002471:	75 36                	jne    10024a9 <_doprnt+0x754>
 1002473:	eb 13                	jmp    1002488 <_doprnt+0x733>
 1002475:	c7 44 24 04 30 00 00 	movl   $0x30,0x4(%esp)
 100247c:	00 
 100247d:	8b 45 18             	mov    0x18(%ebp),%eax
 1002480:	89 04 24             	mov    %eax,(%esp)
 1002483:	8b 45 14             	mov    0x14(%ebp),%eax
 1002486:	ff d0                	call   *%eax
 1002488:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
 100248c:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
 1002490:	79 e3                	jns    1002475 <_doprnt+0x720>
 1002492:	eb 15                	jmp    10024a9 <_doprnt+0x754>
 1002494:	0f b6 03             	movzbl (%ebx),%eax
 1002497:	0f be c0             	movsbl %al,%eax
 100249a:	89 44 24 04          	mov    %eax,0x4(%esp)
 100249e:	8b 45 18             	mov    0x18(%ebp),%eax
 10024a1:	89 04 24             	mov    %eax,(%esp)
 10024a4:	8b 45 14             	mov    0x14(%ebp),%eax
 10024a7:	ff d0                	call   *%eax
 10024a9:	83 c3 01             	add    $0x1,%ebx
 10024ac:	8d 45 8f             	lea    -0x71(%ebp),%eax
 10024af:	83 c0 20             	add    $0x20,%eax
 10024b2:	39 c3                	cmp    %eax,%ebx
 10024b4:	75 de                	jne    1002494 <_doprnt+0x73f>
 10024b6:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
 10024ba:	74 4a                	je     1002506 <_doprnt+0x7b1>
 10024bc:	eb 13                	jmp    10024d1 <_doprnt+0x77c>
 10024be:	c7 44 24 04 20 00 00 	movl   $0x20,0x4(%esp)
 10024c5:	00 
 10024c6:	8b 45 18             	mov    0x18(%ebp),%eax
 10024c9:	89 04 24             	mov    %eax,(%esp)
 10024cc:	8b 45 14             	mov    0x14(%ebp),%eax
 10024cf:	ff d0                	call   *%eax
 10024d1:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
 10024d5:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
 10024d9:	79 e3                	jns    10024be <_doprnt+0x769>
 10024db:	eb 29                	jmp    1002506 <_doprnt+0x7b1>
 10024dd:	83 6d b8 01          	subl   $0x1,-0x48(%ebp)
 10024e1:	eb 24                	jmp    1002507 <_doprnt+0x7b2>
 10024e3:	8b 45 b8             	mov    -0x48(%ebp),%eax
 10024e6:	0f b6 00             	movzbl (%eax),%eax
 10024e9:	0f be c0             	movsbl %al,%eax
 10024ec:	89 44 24 04          	mov    %eax,0x4(%esp)
 10024f0:	8b 45 18             	mov    0x18(%ebp),%eax
 10024f3:	89 04 24             	mov    %eax,(%esp)
 10024f6:	8b 45 14             	mov    0x14(%ebp),%eax
 10024f9:	ff d0                	call   *%eax
 10024fb:	eb 0a                	jmp    1002507 <_doprnt+0x7b2>
 10024fd:	90                   	nop
 10024fe:	eb 07                	jmp    1002507 <_doprnt+0x7b2>
 1002500:	90                   	nop
 1002501:	eb 04                	jmp    1002507 <_doprnt+0x7b2>
 1002503:	90                   	nop
 1002504:	eb 01                	jmp    1002507 <_doprnt+0x7b2>
 1002506:	90                   	nop
 1002507:	83 45 b8 01          	addl   $0x1,-0x48(%ebp)
 100250b:	8b 45 b8             	mov    -0x48(%ebp),%eax
 100250e:	0f b6 00             	movzbl (%eax),%eax
 1002511:	84 c0                	test   %al,%al
 1002513:	0f 85 53 f8 ff ff    	jne    1001d6c <_doprnt+0x17>
 1002519:	81 c4 8c 00 00 00    	add    $0x8c,%esp
 100251f:	5b                   	pop    %ebx
 1002520:	5e                   	pop    %esi
 1002521:	5f                   	pop    %edi
 1002522:	5d                   	pop    %ebp
 1002523:	c3                   	ret    

01002524 <atol>:
 1002524:	55                   	push   %ebp
 1002525:	89 e5                	mov    %esp,%ebp
 1002527:	83 ec 18             	sub    $0x18,%esp
 100252a:	c7 44 24 08 0a 00 00 	movl   $0xa,0x8(%esp)
 1002531:	00 
 1002532:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
 1002539:	00 
 100253a:	8b 45 08             	mov    0x8(%ebp),%eax
 100253d:	89 04 24             	mov    %eax,(%esp)
 1002540:	e8 53 00 00 00       	call   1002598 <strtol>
 1002545:	c9                   	leave  
 1002546:	c3                   	ret    
 1002547:	90                   	nop

01002548 <exit>:
 1002548:	55                   	push   %ebp
 1002549:	89 e5                	mov    %esp,%ebp
 100254b:	83 ec 18             	sub    $0x18,%esp
 100254e:	8b 45 08             	mov    0x8(%ebp),%eax
 1002551:	89 04 24             	mov    %eax,(%esp)
 1002554:	e8 b7 04 00 00       	call   1002a10 <set_status>
 1002559:	e8 c2 04 00 00       	call   1002a20 <vanish>
 100255e:	90                   	nop
 100255f:	90                   	nop

01002560 <panic>:
 1002560:	55                   	push   %ebp
 1002561:	89 e5                	mov    %esp,%ebp
 1002563:	83 ec 28             	sub    $0x28,%esp
 1002566:	8d 45 0c             	lea    0xc(%ebp),%eax
 1002569:	89 45 f4             	mov    %eax,-0xc(%ebp)
 100256c:	8b 45 08             	mov    0x8(%ebp),%eax
 100256f:	8b 55 f4             	mov    -0xc(%ebp),%edx
 1002572:	89 54 24 04          	mov    %edx,0x4(%esp)
 1002576:	89 04 24             	mov    %eax,(%esp)
 1002579:	e8 9e f4 ff ff       	call   1001a1c <vprintf>
 100257e:	c7 04 24 5c 31 00 01 	movl   $0x100315c,(%esp)
 1002585:	e8 ea f4 ff ff       	call   1001a74 <printf>
 100258a:	c7 04 24 ff ff ff ff 	movl   $0xffffffff,(%esp)
 1002591:	e8 b2 ff ff ff       	call   1002548 <exit>
 1002596:	90                   	nop
 1002597:	90                   	nop

01002598 <strtol>:
 1002598:	55                   	push   %ebp
 1002599:	89 e5                	mov    %esp,%ebp
 100259b:	83 ec 28             	sub    $0x28,%esp
 100259e:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 10025a5:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 10025ac:	eb 04                	jmp    10025b2 <strtol+0x1a>
 10025ae:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 10025b2:	8b 45 08             	mov    0x8(%ebp),%eax
 10025b5:	0f b6 00             	movzbl (%eax),%eax
 10025b8:	0f be c0             	movsbl %al,%eax
 10025bb:	89 04 24             	mov    %eax,(%esp)
 10025be:	e8 e1 01 00 00       	call   10027a4 <isspace>
 10025c3:	85 c0                	test   %eax,%eax
 10025c5:	75 e7                	jne    10025ae <strtol+0x16>
 10025c7:	8b 45 08             	mov    0x8(%ebp),%eax
 10025ca:	0f b6 00             	movzbl (%eax),%eax
 10025cd:	3c 2d                	cmp    $0x2d,%al
 10025cf:	75 0d                	jne    10025de <strtol+0x46>
 10025d1:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
 10025d8:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 10025dc:	eb 11                	jmp    10025ef <strtol+0x57>
 10025de:	8b 45 08             	mov    0x8(%ebp),%eax
 10025e1:	0f b6 00             	movzbl (%eax),%eax
 10025e4:	3c 2b                	cmp    $0x2b,%al
 10025e6:	75 07                	jne    10025ef <strtol+0x57>
 10025e8:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 10025ef:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
 10025f3:	74 06                	je     10025fb <strtol+0x63>
 10025f5:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
 10025f9:	75 2f                	jne    100262a <strtol+0x92>
 10025fb:	8b 45 08             	mov    0x8(%ebp),%eax
 10025fe:	0f b6 00             	movzbl (%eax),%eax
 1002601:	3c 30                	cmp    $0x30,%al
 1002603:	75 25                	jne    100262a <strtol+0x92>
 1002605:	8b 45 08             	mov    0x8(%ebp),%eax
 1002608:	83 c0 01             	add    $0x1,%eax
 100260b:	0f b6 00             	movzbl (%eax),%eax
 100260e:	3c 78                	cmp    $0x78,%al
 1002610:	74 0d                	je     100261f <strtol+0x87>
 1002612:	8b 45 08             	mov    0x8(%ebp),%eax
 1002615:	83 c0 01             	add    $0x1,%eax
 1002618:	0f b6 00             	movzbl (%eax),%eax
 100261b:	3c 58                	cmp    $0x58,%al
 100261d:	75 0b                	jne    100262a <strtol+0x92>
 100261f:	83 45 08 02          	addl   $0x2,0x8(%ebp)
 1002623:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
 100262a:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
 100262e:	75 1a                	jne    100264a <strtol+0xb2>
 1002630:	8b 45 08             	mov    0x8(%ebp),%eax
 1002633:	0f b6 00             	movzbl (%eax),%eax
 1002636:	3c 30                	cmp    $0x30,%al
 1002638:	75 09                	jne    1002643 <strtol+0xab>
 100263a:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
 1002641:	eb 07                	jmp    100264a <strtol+0xb2>
 1002643:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
 100264a:	8b 45 08             	mov    0x8(%ebp),%eax
 100264d:	0f b6 00             	movzbl (%eax),%eax
 1002650:	88 45 ef             	mov    %al,-0x11(%ebp)
 1002653:	80 7d ef 2f          	cmpb   $0x2f,-0x11(%ebp)
 1002657:	7e 27                	jle    1002680 <strtol+0xe8>
 1002659:	80 7d ef 39          	cmpb   $0x39,-0x11(%ebp)
 100265d:	7f 21                	jg     1002680 <strtol+0xe8>
 100265f:	0f be 45 ef          	movsbl -0x11(%ebp),%eax
 1002663:	83 e8 30             	sub    $0x30,%eax
 1002666:	3b 45 10             	cmp    0x10(%ebp),%eax
 1002669:	7d 15                	jge    1002680 <strtol+0xe8>
 100266b:	8b 45 f4             	mov    -0xc(%ebp),%eax
 100266e:	0f af 45 10          	imul   0x10(%ebp),%eax
 1002672:	0f be 55 ef          	movsbl -0x11(%ebp),%edx
 1002676:	83 ea 30             	sub    $0x30,%edx
 1002679:	01 d0                	add    %edx,%eax
 100267b:	89 45 f4             	mov    %eax,-0xc(%ebp)
 100267e:	eb 58                	jmp    10026d8 <strtol+0x140>
 1002680:	80 7d ef 60          	cmpb   $0x60,-0x11(%ebp)
 1002684:	7e 27                	jle    10026ad <strtol+0x115>
 1002686:	80 7d ef 7a          	cmpb   $0x7a,-0x11(%ebp)
 100268a:	7f 21                	jg     10026ad <strtol+0x115>
 100268c:	0f be 45 ef          	movsbl -0x11(%ebp),%eax
 1002690:	83 e8 57             	sub    $0x57,%eax
 1002693:	3b 45 10             	cmp    0x10(%ebp),%eax
 1002696:	7d 15                	jge    10026ad <strtol+0x115>
 1002698:	8b 45 f4             	mov    -0xc(%ebp),%eax
 100269b:	0f af 45 10          	imul   0x10(%ebp),%eax
 100269f:	0f be 55 ef          	movsbl -0x11(%ebp),%edx
 10026a3:	83 ea 57             	sub    $0x57,%edx
 10026a6:	01 d0                	add    %edx,%eax
 10026a8:	89 45 f4             	mov    %eax,-0xc(%ebp)
 10026ab:	eb 2b                	jmp    10026d8 <strtol+0x140>
 10026ad:	80 7d ef 40          	cmpb   $0x40,-0x11(%ebp)
 10026b1:	7e 2e                	jle    10026e1 <strtol+0x149>
 10026b3:	80 7d ef 5a          	cmpb   $0x5a,-0x11(%ebp)
 10026b7:	7f 28                	jg     10026e1 <strtol+0x149>
 10026b9:	0f be 45 ef          	movsbl -0x11(%ebp),%eax
 10026bd:	83 e8 37             	sub    $0x37,%eax
 10026c0:	3b 45 10             	cmp    0x10(%ebp),%eax
 10026c3:	7d 1c                	jge    10026e1 <strtol+0x149>
 10026c5:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10026c8:	0f af 45 10          	imul   0x10(%ebp),%eax
 10026cc:	0f be 55 ef          	movsbl -0x11(%ebp),%edx
 10026d0:	83 ea 37             	sub    $0x37,%edx
 10026d3:	01 d0                	add    %edx,%eax
 10026d5:	89 45 f4             	mov    %eax,-0xc(%ebp)
 10026d8:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 10026dc:	e9 69 ff ff ff       	jmp    100264a <strtol+0xb2>
 10026e1:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 10026e5:	74 03                	je     10026ea <strtol+0x152>
 10026e7:	f7 5d f4             	negl   -0xc(%ebp)
 10026ea:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 10026ee:	74 08                	je     10026f8 <strtol+0x160>
 10026f0:	8b 45 0c             	mov    0xc(%ebp),%eax
 10026f3:	8b 55 08             	mov    0x8(%ebp),%edx
 10026f6:	89 10                	mov    %edx,(%eax)
 10026f8:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10026fb:	c9                   	leave  
 10026fc:	c3                   	ret    
 10026fd:	90                   	nop
 10026fe:	90                   	nop
 10026ff:	90                   	nop

01002700 <isascii>:
 1002700:	55                   	push   %ebp
 1002701:	89 e5                	mov    %esp,%ebp
 1002703:	8b 45 08             	mov    0x8(%ebp),%eax
 1002706:	83 e0 80             	and    $0xffffff80,%eax
 1002709:	85 c0                	test   %eax,%eax
 100270b:	0f 94 c0             	sete   %al
 100270e:	0f b6 c0             	movzbl %al,%eax
 1002711:	5d                   	pop    %ebp
 1002712:	c3                   	ret    

01002713 <iscntrl>:
 1002713:	55                   	push   %ebp
 1002714:	89 e5                	mov    %esp,%ebp
 1002716:	83 7d 08 1f          	cmpl   $0x1f,0x8(%ebp)
 100271a:	7e 06                	jle    1002722 <iscntrl+0xf>
 100271c:	83 7d 08 7e          	cmpl   $0x7e,0x8(%ebp)
 1002720:	7e 07                	jle    1002729 <iscntrl+0x16>
 1002722:	b8 01 00 00 00       	mov    $0x1,%eax
 1002727:	eb 05                	jmp    100272e <iscntrl+0x1b>
 1002729:	b8 00 00 00 00       	mov    $0x0,%eax
 100272e:	5d                   	pop    %ebp
 100272f:	c3                   	ret    

01002730 <isdigit>:
 1002730:	55                   	push   %ebp
 1002731:	89 e5                	mov    %esp,%ebp
 1002733:	83 7d 08 2f          	cmpl   $0x2f,0x8(%ebp)
 1002737:	7e 0d                	jle    1002746 <isdigit+0x16>
 1002739:	83 7d 08 39          	cmpl   $0x39,0x8(%ebp)
 100273d:	7f 07                	jg     1002746 <isdigit+0x16>
 100273f:	b8 01 00 00 00       	mov    $0x1,%eax
 1002744:	eb 05                	jmp    100274b <isdigit+0x1b>
 1002746:	b8 00 00 00 00       	mov    $0x0,%eax
 100274b:	5d                   	pop    %ebp
 100274c:	c3                   	ret    

0100274d <isgraph>:
 100274d:	55                   	push   %ebp
 100274e:	89 e5                	mov    %esp,%ebp
 1002750:	83 7d 08 20          	cmpl   $0x20,0x8(%ebp)
 1002754:	7e 0d                	jle    1002763 <isgraph+0x16>
 1002756:	83 7d 08 7e          	cmpl   $0x7e,0x8(%ebp)
 100275a:	7f 07                	jg     1002763 <isgraph+0x16>
 100275c:	b8 01 00 00 00       	mov    $0x1,%eax
 1002761:	eb 05                	jmp    1002768 <isgraph+0x1b>
 1002763:	b8 00 00 00 00       	mov    $0x0,%eax
 1002768:	5d                   	pop    %ebp
 1002769:	c3                   	ret    

0100276a <islower>:
 100276a:	55                   	push   %ebp
 100276b:	89 e5                	mov    %esp,%ebp
 100276d:	83 7d 08 60          	cmpl   $0x60,0x8(%ebp)
 1002771:	7e 0d                	jle    1002780 <islower+0x16>
 1002773:	83 7d 08 7a          	cmpl   $0x7a,0x8(%ebp)
 1002777:	7f 07                	jg     1002780 <islower+0x16>
 1002779:	b8 01 00 00 00       	mov    $0x1,%eax
 100277e:	eb 05                	jmp    1002785 <islower+0x1b>
 1002780:	b8 00 00 00 00       	mov    $0x0,%eax
 1002785:	5d                   	pop    %ebp
 1002786:	c3                   	ret    

01002787 <isprint>:
 1002787:	55                   	push   %ebp
 1002788:	89 e5                	mov    %esp,%ebp
 100278a:	83 7d 08 1f          	cmpl   $0x1f,0x8(%ebp)
 100278e:	7e 0d                	jle    100279d <isprint+0x16>
 1002790:	83 7d 08 7e          	cmpl   $0x7e,0x8(%ebp)
 1002794:	7f 07                	jg     100279d <isprint+0x16>
 1002796:	b8 01 00 00 00       	mov    $0x1,%eax
 100279b:	eb 05                	jmp    10027a2 <isprint+0x1b>
 100279d:	b8 00 00 00 00       	mov    $0x0,%eax
 10027a2:	5d                   	pop    %ebp
 10027a3:	c3                   	ret    

010027a4 <isspace>:
 10027a4:	55                   	push   %ebp
 10027a5:	89 e5                	mov    %esp,%ebp
 10027a7:	83 7d 08 20          	cmpl   $0x20,0x8(%ebp)
 10027ab:	74 1e                	je     10027cb <isspace+0x27>
 10027ad:	83 7d 08 0c          	cmpl   $0xc,0x8(%ebp)
 10027b1:	74 18                	je     10027cb <isspace+0x27>
 10027b3:	83 7d 08 0a          	cmpl   $0xa,0x8(%ebp)
 10027b7:	74 12                	je     10027cb <isspace+0x27>
 10027b9:	83 7d 08 0d          	cmpl   $0xd,0x8(%ebp)
 10027bd:	74 0c                	je     10027cb <isspace+0x27>
 10027bf:	83 7d 08 09          	cmpl   $0x9,0x8(%ebp)
 10027c3:	74 06                	je     10027cb <isspace+0x27>
 10027c5:	83 7d 08 0b          	cmpl   $0xb,0x8(%ebp)
 10027c9:	75 07                	jne    10027d2 <isspace+0x2e>
 10027cb:	b8 01 00 00 00       	mov    $0x1,%eax
 10027d0:	eb 05                	jmp    10027d7 <isspace+0x33>
 10027d2:	b8 00 00 00 00       	mov    $0x0,%eax
 10027d7:	5d                   	pop    %ebp
 10027d8:	c3                   	ret    

010027d9 <isupper>:
 10027d9:	55                   	push   %ebp
 10027da:	89 e5                	mov    %esp,%ebp
 10027dc:	83 7d 08 40          	cmpl   $0x40,0x8(%ebp)
 10027e0:	7e 0d                	jle    10027ef <isupper+0x16>
 10027e2:	83 7d 08 5a          	cmpl   $0x5a,0x8(%ebp)
 10027e6:	7f 07                	jg     10027ef <isupper+0x16>
 10027e8:	b8 01 00 00 00       	mov    $0x1,%eax
 10027ed:	eb 05                	jmp    10027f4 <isupper+0x1b>
 10027ef:	b8 00 00 00 00       	mov    $0x0,%eax
 10027f4:	5d                   	pop    %ebp
 10027f5:	c3                   	ret    

010027f6 <isxdigit>:
 10027f6:	55                   	push   %ebp
 10027f7:	89 e5                	mov    %esp,%ebp
 10027f9:	83 ec 04             	sub    $0x4,%esp
 10027fc:	8b 45 08             	mov    0x8(%ebp),%eax
 10027ff:	89 04 24             	mov    %eax,(%esp)
 1002802:	e8 29 ff ff ff       	call   1002730 <isdigit>
 1002807:	85 c0                	test   %eax,%eax
 1002809:	75 18                	jne    1002823 <isxdigit+0x2d>
 100280b:	83 7d 08 40          	cmpl   $0x40,0x8(%ebp)
 100280f:	7e 06                	jle    1002817 <isxdigit+0x21>
 1002811:	83 7d 08 46          	cmpl   $0x46,0x8(%ebp)
 1002815:	7e 0c                	jle    1002823 <isxdigit+0x2d>
 1002817:	83 7d 08 60          	cmpl   $0x60,0x8(%ebp)
 100281b:	7e 0d                	jle    100282a <isxdigit+0x34>
 100281d:	83 7d 08 66          	cmpl   $0x66,0x8(%ebp)
 1002821:	7f 07                	jg     100282a <isxdigit+0x34>
 1002823:	b8 01 00 00 00       	mov    $0x1,%eax
 1002828:	eb 05                	jmp    100282f <isxdigit+0x39>
 100282a:	b8 00 00 00 00       	mov    $0x0,%eax
 100282f:	c9                   	leave  
 1002830:	c3                   	ret    

01002831 <isalpha>:
 1002831:	55                   	push   %ebp
 1002832:	89 e5                	mov    %esp,%ebp
 1002834:	83 ec 04             	sub    $0x4,%esp
 1002837:	8b 45 08             	mov    0x8(%ebp),%eax
 100283a:	89 04 24             	mov    %eax,(%esp)
 100283d:	e8 28 ff ff ff       	call   100276a <islower>
 1002842:	85 c0                	test   %eax,%eax
 1002844:	75 0f                	jne    1002855 <isalpha+0x24>
 1002846:	8b 45 08             	mov    0x8(%ebp),%eax
 1002849:	89 04 24             	mov    %eax,(%esp)
 100284c:	e8 88 ff ff ff       	call   10027d9 <isupper>
 1002851:	85 c0                	test   %eax,%eax
 1002853:	74 07                	je     100285c <isalpha+0x2b>
 1002855:	b8 01 00 00 00       	mov    $0x1,%eax
 100285a:	eb 05                	jmp    1002861 <isalpha+0x30>
 100285c:	b8 00 00 00 00       	mov    $0x0,%eax
 1002861:	c9                   	leave  
 1002862:	c3                   	ret    

01002863 <isalnum>:
 1002863:	55                   	push   %ebp
 1002864:	89 e5                	mov    %esp,%ebp
 1002866:	83 ec 04             	sub    $0x4,%esp
 1002869:	8b 45 08             	mov    0x8(%ebp),%eax
 100286c:	89 04 24             	mov    %eax,(%esp)
 100286f:	e8 bd ff ff ff       	call   1002831 <isalpha>
 1002874:	85 c0                	test   %eax,%eax
 1002876:	75 0f                	jne    1002887 <isalnum+0x24>
 1002878:	8b 45 08             	mov    0x8(%ebp),%eax
 100287b:	89 04 24             	mov    %eax,(%esp)
 100287e:	e8 ad fe ff ff       	call   1002730 <isdigit>
 1002883:	85 c0                	test   %eax,%eax
 1002885:	74 07                	je     100288e <isalnum+0x2b>
 1002887:	b8 01 00 00 00       	mov    $0x1,%eax
 100288c:	eb 05                	jmp    1002893 <isalnum+0x30>
 100288e:	b8 00 00 00 00       	mov    $0x0,%eax
 1002893:	c9                   	leave  
 1002894:	c3                   	ret    

01002895 <ispunct>:
 1002895:	55                   	push   %ebp
 1002896:	89 e5                	mov    %esp,%ebp
 1002898:	83 ec 04             	sub    $0x4,%esp
 100289b:	8b 45 08             	mov    0x8(%ebp),%eax
 100289e:	89 04 24             	mov    %eax,(%esp)
 10028a1:	e8 a7 fe ff ff       	call   100274d <isgraph>
 10028a6:	85 c0                	test   %eax,%eax
 10028a8:	74 16                	je     10028c0 <ispunct+0x2b>
 10028aa:	8b 45 08             	mov    0x8(%ebp),%eax
 10028ad:	89 04 24             	mov    %eax,(%esp)
 10028b0:	e8 ae ff ff ff       	call   1002863 <isalnum>
 10028b5:	85 c0                	test   %eax,%eax
 10028b7:	75 07                	jne    10028c0 <ispunct+0x2b>
 10028b9:	b8 01 00 00 00       	mov    $0x1,%eax
 10028be:	eb 05                	jmp    10028c5 <ispunct+0x30>
 10028c0:	b8 00 00 00 00       	mov    $0x0,%eax
 10028c5:	c9                   	leave  
 10028c6:	c3                   	ret    

010028c7 <toascii>:
 10028c7:	55                   	push   %ebp
 10028c8:	89 e5                	mov    %esp,%ebp
 10028ca:	8b 45 08             	mov    0x8(%ebp),%eax
 10028cd:	83 e0 7f             	and    $0x7f,%eax
 10028d0:	5d                   	pop    %ebp
 10028d1:	c3                   	ret    

010028d2 <toupper>:
 10028d2:	55                   	push   %ebp
 10028d3:	89 e5                	mov    %esp,%ebp
 10028d5:	83 7d 08 60          	cmpl   $0x60,0x8(%ebp)
 10028d9:	7e 0e                	jle    10028e9 <toupper+0x17>
 10028db:	83 7d 08 7a          	cmpl   $0x7a,0x8(%ebp)
 10028df:	7f 08                	jg     10028e9 <toupper+0x17>
 10028e1:	8b 45 08             	mov    0x8(%ebp),%eax
 10028e4:	83 e8 20             	sub    $0x20,%eax
 10028e7:	eb 03                	jmp    10028ec <toupper+0x1a>
 10028e9:	8b 45 08             	mov    0x8(%ebp),%eax
 10028ec:	5d                   	pop    %ebp
 10028ed:	c3                   	ret    

010028ee <tolower>:
 10028ee:	55                   	push   %ebp
 10028ef:	89 e5                	mov    %esp,%ebp
 10028f1:	83 7d 08 40          	cmpl   $0x40,0x8(%ebp)
 10028f5:	7e 0e                	jle    1002905 <tolower+0x17>
 10028f7:	83 7d 08 5a          	cmpl   $0x5a,0x8(%ebp)
 10028fb:	7f 08                	jg     1002905 <tolower+0x17>
 10028fd:	8b 45 08             	mov    0x8(%ebp),%eax
 1002900:	83 c0 20             	add    $0x20,%eax
 1002903:	eb 03                	jmp    1002908 <tolower+0x1a>
 1002905:	8b 45 08             	mov    0x8(%ebp),%eax
 1002908:	5d                   	pop    %ebp
 1002909:	c3                   	ret    
 100290a:	90                   	nop
 100290b:	90                   	nop

0100290c <bcopy>:
 100290c:	55                   	push   %ebp
 100290d:	89 e5                	mov    %esp,%ebp
 100290f:	57                   	push   %edi
 1002910:	56                   	push   %esi
 1002911:	8b 75 08             	mov    0x8(%ebp),%esi
 1002914:	8b 7d 0c             	mov    0xc(%ebp),%edi

01002917 <bcopy_common>:
 1002917:	8b 55 10             	mov    0x10(%ebp),%edx
 100291a:	8d 04 16             	lea    (%esi,%edx,1),%eax
 100291d:	39 c7                	cmp    %eax,%edi
 100291f:	73 20                	jae    1002941 <bcopy_common+0x2a>
 1002921:	8d 04 17             	lea    (%edi,%edx,1),%eax
 1002924:	39 c6                	cmp    %eax,%esi
 1002926:	73 19                	jae    1002941 <bcopy_common+0x2a>
 1002928:	39 f7                	cmp    %esi,%edi
 100292a:	72 15                	jb     1002941 <bcopy_common+0x2a>
 100292c:	74 0c                	je     100293a <bcopy_common+0x23>
 100292e:	01 d6                	add    %edx,%esi
 1002930:	4e                   	dec    %esi
 1002931:	01 d7                	add    %edx,%edi
 1002933:	4f                   	dec    %edi
 1002934:	fd                   	std    
 1002935:	89 d1                	mov    %edx,%ecx
 1002937:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
 1002939:	fc                   	cld    
 100293a:	8b 45 08             	mov    0x8(%ebp),%eax
 100293d:	5e                   	pop    %esi
 100293e:	5f                   	pop    %edi
 100293f:	c9                   	leave  
 1002940:	c3                   	ret    
 1002941:	fc                   	cld    
 1002942:	89 d1                	mov    %edx,%ecx
 1002944:	c1 f9 02             	sar    $0x2,%ecx
 1002947:	78 09                	js     1002952 <bcopy_common+0x3b>
 1002949:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
 100294b:	89 d1                	mov    %edx,%ecx
 100294d:	83 e1 03             	and    $0x3,%ecx
 1002950:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
 1002952:	8b 45 08             	mov    0x8(%ebp),%eax
 1002955:	5e                   	pop    %esi
 1002956:	5f                   	pop    %edi
 1002957:	c9                   	leave  
 1002958:	c3                   	ret    
 1002959:	8d 76 00             	lea    0x0(%esi),%esi

0100295c <memcpy>:
 100295c:	55                   	push   %ebp
 100295d:	89 e5                	mov    %esp,%ebp
 100295f:	57                   	push   %edi
 1002960:	56                   	push   %esi
 1002961:	8b 7d 08             	mov    0x8(%ebp),%edi
 1002964:	8b 75 0c             	mov    0xc(%ebp),%esi
 1002967:	eb ae                	jmp    1002917 <bcopy_common>
 1002969:	90                   	nop
 100296a:	90                   	nop
 100296b:	90                   	nop

0100296c <bzero>:
 100296c:	57                   	push   %edi
 100296d:	8b 7c 24 08          	mov    0x8(%esp),%edi
 1002971:	8b 54 24 0c          	mov    0xc(%esp),%edx
 1002975:	fc                   	cld    
 1002976:	31 c0                	xor    %eax,%eax
 1002978:	83 fa 10             	cmp    $0x10,%edx
 100297b:	72 15                	jb     1002992 <L1>
 100297d:	89 f9                	mov    %edi,%ecx
 100297f:	f7 d9                	neg    %ecx
 1002981:	83 e1 03             	and    $0x3,%ecx
 1002984:	29 ca                	sub    %ecx,%edx
 1002986:	f3 aa                	rep stos %al,%es:(%edi)
 1002988:	89 d1                	mov    %edx,%ecx
 100298a:	c1 e9 02             	shr    $0x2,%ecx
 100298d:	83 e2 03             	and    $0x3,%edx
 1002990:	f3 ab                	rep stos %eax,%es:(%edi)

01002992 <L1>:
 1002992:	89 d1                	mov    %edx,%ecx
 1002994:	f3 aa                	rep stos %al,%es:(%edi)
 1002996:	5f                   	pop    %edi
 1002997:	c3                   	ret    

01002998 <strlen>:
 1002998:	55                   	push   %ebp
 1002999:	89 e5                	mov    %esp,%ebp
 100299b:	83 ec 10             	sub    $0x10,%esp
 100299e:	8b 45 08             	mov    0x8(%ebp),%eax
 10029a1:	89 45 fc             	mov    %eax,-0x4(%ebp)
 10029a4:	90                   	nop
 10029a5:	8b 45 08             	mov    0x8(%ebp),%eax
 10029a8:	0f b6 00             	movzbl (%eax),%eax
 10029ab:	84 c0                	test   %al,%al
 10029ad:	0f 95 c0             	setne  %al
 10029b0:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 10029b4:	84 c0                	test   %al,%al
 10029b6:	75 ed                	jne    10029a5 <strlen+0xd>
 10029b8:	8b 45 08             	mov    0x8(%ebp),%eax
 10029bb:	83 e8 01             	sub    $0x1,%eax
 10029be:	89 c2                	mov    %eax,%edx
 10029c0:	8b 45 fc             	mov    -0x4(%ebp),%eax
 10029c3:	89 d1                	mov    %edx,%ecx
 10029c5:	29 c1                	sub    %eax,%ecx
 10029c7:	89 c8                	mov    %ecx,%eax
 10029c9:	c9                   	leave  
 10029ca:	c3                   	ret    
 10029cb:	90                   	nop

010029cc <print>:
 10029cc:	55                   	push   %ebp
 10029cd:	89 e5                	mov    %esp,%ebp
 10029cf:	56                   	push   %esi
 10029d0:	53                   	push   %ebx
 10029d1:	83 ec 14             	sub    $0x14,%esp
 10029d4:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 10029db:	8b 45 08             	mov    0x8(%ebp),%eax
 10029de:	89 45 e8             	mov    %eax,-0x18(%ebp)
 10029e1:	8b 45 0c             	mov    0xc(%ebp),%eax
 10029e4:	89 45 ec             	mov    %eax,-0x14(%ebp)
 10029e7:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 10029ee:	8d 75 e8             	lea    -0x18(%ebp),%esi
 10029f1:	89 75 e4             	mov    %esi,-0x1c(%ebp)
 10029f4:	8b 75 e4             	mov    -0x1c(%ebp),%esi
 10029f7:	cd 4e                	int    $0x4e
 10029f9:	89 c3                	mov    %eax,%ebx
 10029fb:	89 5d f0             	mov    %ebx,-0x10(%ebp)
 10029fe:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1002a01:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1002a04:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1002a07:	83 c4 14             	add    $0x14,%esp
 1002a0a:	5b                   	pop    %ebx
 1002a0b:	5e                   	pop    %esi
 1002a0c:	5d                   	pop    %ebp
 1002a0d:	c3                   	ret    
 1002a0e:	90                   	nop
 1002a0f:	90                   	nop

01002a10 <set_status>:
 1002a10:	55                   	push   %ebp
 1002a11:	89 e5                	mov    %esp,%ebp
 1002a13:	56                   	push   %esi
 1002a14:	8b 45 08             	mov    0x8(%ebp),%eax
 1002a17:	89 c6                	mov    %eax,%esi
 1002a19:	cd 59                	int    $0x59
 1002a1b:	5e                   	pop    %esi
 1002a1c:	5d                   	pop    %ebp
 1002a1d:	c3                   	ret    
 1002a1e:	90                   	nop
 1002a1f:	90                   	nop

01002a20 <vanish>:
 1002a20:	55                   	push   %ebp
 1002a21:	89 e5                	mov    %esp,%ebp
 1002a23:	83 ec 10             	sub    $0x10,%esp
 1002a26:	c7 45 fc a6 ee ff ff 	movl   $0xffffeea6,-0x4(%ebp)
 1002a2d:	cd 60                	int    $0x60
 1002a2f:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 1002a36:	8b 45 fc             	mov    -0x4(%ebp),%eax
 1002a39:	89 c2                	mov    %eax,%edx
 1002a3b:	c1 fa 1f             	sar    $0x1f,%edx
 1002a3e:	f7 7d fc             	idivl  -0x4(%ebp)
 1002a41:	89 45 fc             	mov    %eax,-0x4(%ebp)
 1002a44:	8b 45 fc             	mov    -0x4(%ebp),%eax
 1002a47:	8b 55 fc             	mov    -0x4(%ebp),%edx
 1002a4a:	89 10                	mov    %edx,(%eax)
 1002a4c:	c9                   	leave  
 1002a4d:	c3                   	ret    
 1002a4e:	90                   	nop
 1002a4f:	90                   	nop

01002a50 <yield>:
 1002a50:	55                   	push   %ebp
 1002a51:	89 e5                	mov    %esp,%ebp
 1002a53:	56                   	push   %esi
 1002a54:	53                   	push   %ebx
 1002a55:	83 ec 14             	sub    $0x14,%esp
 1002a58:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 1002a5f:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 1002a66:	8b 75 08             	mov    0x8(%ebp),%esi
 1002a69:	89 75 e4             	mov    %esi,-0x1c(%ebp)
 1002a6c:	8b 75 e4             	mov    -0x1c(%ebp),%esi
 1002a6f:	cd 45                	int    $0x45
 1002a71:	89 c3                	mov    %eax,%ebx
 1002a73:	89 5d f0             	mov    %ebx,-0x10(%ebp)
 1002a76:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1002a79:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1002a7c:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1002a7f:	83 c4 14             	add    $0x14,%esp
 1002a82:	5b                   	pop    %ebx
 1002a83:	5e                   	pop    %esi
 1002a84:	5d                   	pop    %ebp
 1002a85:	c3                   	ret    
 1002a86:	90                   	nop
 1002a87:	90                   	nop

01002a88 <gettid>:
 1002a88:	55                   	push   %ebp
 1002a89:	89 e5                	mov    %esp,%ebp
 1002a8b:	53                   	push   %ebx
 1002a8c:	83 ec 10             	sub    $0x10,%esp
 1002a8f:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%ebp)
 1002a96:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 1002a9d:	cd 48                	int    $0x48
 1002a9f:	89 c3                	mov    %eax,%ebx
 1002aa1:	89 5d f4             	mov    %ebx,-0xc(%ebp)
 1002aa4:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1002aa7:	89 45 f8             	mov    %eax,-0x8(%ebp)
 1002aaa:	8b 45 f8             	mov    -0x8(%ebp),%eax
 1002aad:	83 c4 10             	add    $0x10,%esp
 1002ab0:	5b                   	pop    %ebx
 1002ab1:	5d                   	pop    %ebp
 1002ab2:	c3                   	ret    
 1002ab3:	90                   	nop

01002ab4 <new_pages>:
 1002ab4:	55                   	push   %ebp
 1002ab5:	89 e5                	mov    %esp,%ebp
 1002ab7:	56                   	push   %esi
 1002ab8:	53                   	push   %ebx
 1002ab9:	83 ec 14             	sub    $0x14,%esp
 1002abc:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 1002ac3:	8b 45 08             	mov    0x8(%ebp),%eax
 1002ac6:	89 45 e8             	mov    %eax,-0x18(%ebp)
 1002ac9:	8b 45 0c             	mov    0xc(%ebp),%eax
 1002acc:	89 45 ec             	mov    %eax,-0x14(%ebp)
 1002acf:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 1002ad6:	8d 75 e8             	lea    -0x18(%ebp),%esi
 1002ad9:	89 75 e4             	mov    %esi,-0x1c(%ebp)
 1002adc:	8b 75 e4             	mov    -0x1c(%ebp),%esi
 1002adf:	cd 49                	int    $0x49
 1002ae1:	89 c3                	mov    %eax,%ebx
 1002ae3:	89 5d f0             	mov    %ebx,-0x10(%ebp)
 1002ae6:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1002ae9:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1002aec:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1002aef:	83 c4 14             	add    $0x14,%esp
 1002af2:	5b                   	pop    %ebx
 1002af3:	5e                   	pop    %esi
 1002af4:	5d                   	pop    %ebp
 1002af5:	c3                   	ret    
