
juggle.o:     file format elf32-i386


Disassembly of section .text:

00000000 <inc_count>:
   0:	55                   	push   %ebp
   1:	89 e5                	mov    %esp,%ebp
   3:	83 ec 18             	sub    $0x18,%esp
   6:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
   d:	e8 fc ff ff ff       	call   e <inc_count+0xe>
  12:	85 c0                	test   %eax,%eax
  14:	74 0e                	je     24 <inc_count+0x24>
  16:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  1d:	e8 fc ff ff ff       	call   1e <inc_count+0x1e>
  22:	eb 2a                	jmp    4e <inc_count+0x4e>
  24:	a1 00 00 00 00       	mov    0x0,%eax
  29:	83 c0 01             	add    $0x1,%eax
  2c:	a3 00 00 00 00       	mov    %eax,0x0
  31:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  38:	e8 fc ff ff ff       	call   39 <inc_count+0x39>
  3d:	85 c0                	test   %eax,%eax
  3f:	74 0c                	je     4d <inc_count+0x4d>
  41:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
  48:	e8 fc ff ff ff       	call   49 <inc_count+0x49>
  4d:	90                   	nop
  4e:	c9                   	leave  
  4f:	c3                   	ret    

00000050 <print_count>:
  50:	55                   	push   %ebp
  51:	89 e5                	mov    %esp,%ebp
  53:	83 ec 28             	sub    $0x28,%esp
  56:	83 7d 08 04          	cmpl   $0x4,0x8(%ebp)
  5a:	7f 58                	jg     b4 <print_count+0x64>
  5c:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  63:	e8 fc ff ff ff       	call   64 <print_count+0x14>
  68:	85 c0                	test   %eax,%eax
  6a:	74 0e                	je     7a <print_count+0x2a>
  6c:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  73:	e8 fc ff ff ff       	call   74 <print_count+0x24>
  78:	eb 3b                	jmp    b5 <print_count+0x65>
  7a:	a1 00 00 00 00       	mov    0x0,%eax
  7f:	89 45 f4             	mov    %eax,-0xc(%ebp)
  82:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  89:	e8 fc ff ff ff       	call   8a <print_count+0x3a>
  8e:	85 c0                	test   %eax,%eax
  90:	74 0c                	je     9e <print_count+0x4e>
  92:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
  99:	e8 fc ff ff ff       	call   9a <print_count+0x4a>
  9e:	8b 45 f4             	mov    -0xc(%ebp),%eax
  a1:	89 44 24 04          	mov    %eax,0x4(%esp)
  a5:	c7 04 24 3f 00 00 00 	movl   $0x3f,(%esp)
  ac:	e8 fc ff ff ff       	call   ad <print_count+0x5d>
  b1:	90                   	nop
  b2:	eb 01                	jmp    b5 <print_count+0x65>
  b4:	90                   	nop
  b5:	c9                   	leave  
  b6:	c3                   	ret    

000000b7 <juggle>:
  b7:	55                   	push   %ebp
  b8:	89 e5                	mov    %esp,%ebp
  ba:	83 ec 48             	sub    $0x48,%esp
  bd:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  c4:	8b 45 08             	mov    0x8(%ebp),%eax
  c7:	89 45 ec             	mov    %eax,-0x14(%ebp)
  ca:	e8 fc ff ff ff       	call   cb <juggle+0x14>
  cf:	8b 45 ec             	mov    -0x14(%ebp),%eax
  d2:	89 04 24             	mov    %eax,(%esp)
  d5:	e8 fc ff ff ff       	call   d6 <juggle+0x1f>
  da:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
  de:	0f 8e 6a 01 00 00    	jle    24e <juggle+0x197>
  e4:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  eb:	e9 50 01 00 00       	jmp    240 <juggle+0x189>
  f0:	8b 45 ec             	mov    -0x14(%ebp),%eax
  f3:	83 e8 01             	sub    $0x1,%eax
  f6:	89 44 24 04          	mov    %eax,0x4(%esp)
  fa:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
 101:	e8 fc ff ff ff       	call   102 <juggle+0x4b>
 106:	89 45 e8             	mov    %eax,-0x18(%ebp)
 109:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
 10d:	79 1a                	jns    129 <juggle+0x72>
 10f:	8b 45 e8             	mov    -0x18(%ebp),%eax
 112:	89 44 24 08          	mov    %eax,0x8(%esp)
 116:	8b 45 ec             	mov    -0x14(%ebp),%eax
 119:	89 44 24 04          	mov    %eax,0x4(%esp)
 11d:	c7 04 24 54 00 00 00 	movl   $0x54,(%esp)
 124:	e8 fc ff ff ff       	call   125 <juggle+0x6e>
 129:	8b 45 ec             	mov    -0x14(%ebp),%eax
 12c:	83 e8 01             	sub    $0x1,%eax
 12f:	89 44 24 04          	mov    %eax,0x4(%esp)
 133:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
 13a:	e8 fc ff ff ff       	call   13b <juggle+0x84>
 13f:	89 45 e4             	mov    %eax,-0x1c(%ebp)
 142:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
 146:	79 1a                	jns    162 <juggle+0xab>
 148:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 14b:	89 44 24 08          	mov    %eax,0x8(%esp)
 14f:	8b 45 ec             	mov    -0x14(%ebp),%eax
 152:	89 44 24 04          	mov    %eax,0x4(%esp)
 156:	c7 04 24 84 00 00 00 	movl   $0x84,(%esp)
 15d:	e8 fc ff ff ff       	call   15e <juggle+0xa7>
 162:	8d 45 dc             	lea    -0x24(%ebp),%eax
 165:	89 44 24 04          	mov    %eax,0x4(%esp)
 169:	8b 45 e8             	mov    -0x18(%ebp),%eax
 16c:	89 04 24             	mov    %eax,(%esp)
 16f:	e8 fc ff ff ff       	call   170 <juggle+0xb9>
 174:	89 45 e0             	mov    %eax,-0x20(%ebp)
 177:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
 17b:	75 0d                	jne    18a <juggle+0xd3>
 17d:	8b 45 ec             	mov    -0x14(%ebp),%eax
 180:	8d 50 ff             	lea    -0x1(%eax),%edx
 183:	8b 45 dc             	mov    -0x24(%ebp),%eax
 186:	39 c2                	cmp    %eax,%edx
 188:	74 45                	je     1cf <juggle+0x118>
 18a:	8b 45 ec             	mov    -0x14(%ebp),%eax
 18d:	89 44 24 04          	mov    %eax,0x4(%esp)
 191:	c7 04 24 b8 00 00 00 	movl   $0xb8,(%esp)
 198:	e8 fc ff ff ff       	call   199 <juggle+0xe2>
 19d:	8b 45 dc             	mov    -0x24(%ebp),%eax
 1a0:	8b 55 ec             	mov    -0x14(%ebp),%edx
 1a3:	83 ea 01             	sub    $0x1,%edx
 1a6:	89 44 24 14          	mov    %eax,0x14(%esp)
 1aa:	89 54 24 10          	mov    %edx,0x10(%esp)
 1ae:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1b1:	89 44 24 0c          	mov    %eax,0xc(%esp)
 1b5:	8b 45 e8             	mov    -0x18(%ebp),%eax
 1b8:	89 44 24 08          	mov    %eax,0x8(%esp)
 1bc:	8b 45 e0             	mov    -0x20(%ebp),%eax
 1bf:	89 44 24 04          	mov    %eax,0x4(%esp)
 1c3:	c7 04 24 e8 00 00 00 	movl   $0xe8,(%esp)
 1ca:	e8 fc ff ff ff       	call   1cb <juggle+0x114>
 1cf:	8d 45 dc             	lea    -0x24(%ebp),%eax
 1d2:	89 44 24 04          	mov    %eax,0x4(%esp)
 1d6:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 1d9:	89 04 24             	mov    %eax,(%esp)
 1dc:	e8 fc ff ff ff       	call   1dd <juggle+0x126>
 1e1:	89 45 e0             	mov    %eax,-0x20(%ebp)
 1e4:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
 1e8:	75 0d                	jne    1f7 <juggle+0x140>
 1ea:	8b 45 ec             	mov    -0x14(%ebp),%eax
 1ed:	8d 50 ff             	lea    -0x1(%eax),%edx
 1f0:	8b 45 dc             	mov    -0x24(%ebp),%eax
 1f3:	39 c2                	cmp    %eax,%edx
 1f5:	74 45                	je     23c <juggle+0x185>
 1f7:	8b 45 ec             	mov    -0x14(%ebp),%eax
 1fa:	89 44 24 04          	mov    %eax,0x4(%esp)
 1fe:	c7 04 24 08 01 00 00 	movl   $0x108,(%esp)
 205:	e8 fc ff ff ff       	call   206 <juggle+0x14f>
 20a:	8b 45 dc             	mov    -0x24(%ebp),%eax
 20d:	8b 55 ec             	mov    -0x14(%ebp),%edx
 210:	83 ea 01             	sub    $0x1,%edx
 213:	89 44 24 14          	mov    %eax,0x14(%esp)
 217:	89 54 24 10          	mov    %edx,0x10(%esp)
 21b:	8b 45 f0             	mov    -0x10(%ebp),%eax
 21e:	89 44 24 0c          	mov    %eax,0xc(%esp)
 222:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 225:	89 44 24 08          	mov    %eax,0x8(%esp)
 229:	8b 45 e0             	mov    -0x20(%ebp),%eax
 22c:	89 44 24 04          	mov    %eax,0x4(%esp)
 230:	c7 04 24 e8 00 00 00 	movl   $0xe8,(%esp)
 237:	e8 fc ff ff ff       	call   238 <juggle+0x181>
 23c:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
 240:	a1 00 00 00 00       	mov    0x0,%eax
 245:	39 45 f4             	cmp    %eax,-0xc(%ebp)
 248:	0f 8c a2 fe ff ff    	jl     f0 <juggle+0x39>
 24e:	8b 45 ec             	mov    -0x14(%ebp),%eax
 251:	0f be c0             	movsbl %al,%eax
 254:	83 c0 30             	add    $0x30,%eax
 257:	89 04 24             	mov    %eax,(%esp)
 25a:	e8 fc ff ff ff       	call   25b <juggle+0x1a4>
 25f:	8b 45 ec             	mov    -0x14(%ebp),%eax
 262:	89 04 24             	mov    %eax,(%esp)
 265:	e8 fc ff ff ff       	call   266 <juggle+0x1af>
 26a:	e8 fc ff ff ff       	call   26b <juggle+0x1b4>
 26f:	89 c1                	mov    %eax,%ecx
 271:	ba cd cc cc cc       	mov    $0xcccccccd,%edx
 276:	89 c8                	mov    %ecx,%eax
 278:	f7 e2                	mul    %edx
 27a:	c1 ea 02             	shr    $0x2,%edx
 27d:	89 d0                	mov    %edx,%eax
 27f:	c1 e0 02             	shl    $0x2,%eax
 282:	01 d0                	add    %edx,%eax
 284:	89 ca                	mov    %ecx,%edx
 286:	29 c2                	sub    %eax,%edx
 288:	89 d0                	mov    %edx,%eax
 28a:	89 04 24             	mov    %eax,(%esp)
 28d:	e8 fc ff ff ff       	call   28e <juggle+0x1d7>
 292:	8b 45 ec             	mov    -0x14(%ebp),%eax
 295:	c9                   	leave  
 296:	c3                   	ret    

00000297 <print_usage>:
 297:	55                   	push   %ebp
 298:	89 e5                	mov    %esp,%ebp
 29a:	83 ec 18             	sub    $0x18,%esp
 29d:	8b 45 08             	mov    0x8(%ebp),%eax
 2a0:	89 44 24 04          	mov    %eax,0x4(%esp)
 2a4:	c7 04 24 3c 01 00 00 	movl   $0x13c,(%esp)
 2ab:	e8 fc ff ff ff       	call   2ac <print_usage+0x15>
 2b0:	c9                   	leave  
 2b1:	c3                   	ret    

000002b2 <main>:
 2b2:	55                   	push   %ebp
 2b3:	89 e5                	mov    %esp,%ebp
 2b5:	83 e4 f0             	and    $0xfffffff0,%esp
 2b8:	83 ec 30             	sub    $0x30,%esp
 2bb:	83 7d 08 05          	cmpl   $0x5,0x8(%ebp)
 2bf:	74 23                	je     2e4 <main+0x32>
 2c1:	c7 04 24 7b 01 00 00 	movl   $0x17b,(%esp)
 2c8:	e8 fc ff ff ff       	call   2c9 <main+0x17>
 2cd:	8b 45 0c             	mov    0xc(%ebp),%eax
 2d0:	8b 00                	mov    (%eax),%eax
 2d2:	89 04 24             	mov    %eax,(%esp)
 2d5:	e8 fc ff ff ff       	call   2d6 <main+0x24>
 2da:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 2df:	e9 b8 01 00 00       	jmp    49c <main+0x1ea>
 2e4:	8b 45 0c             	mov    0xc(%ebp),%eax
 2e7:	83 c0 04             	add    $0x4,%eax
 2ea:	8b 00                	mov    (%eax),%eax
 2ec:	89 04 24             	mov    %eax,(%esp)
 2ef:	e8 fc ff ff ff       	call   2f0 <main+0x3e>
 2f4:	89 44 24 28          	mov    %eax,0x28(%esp)
 2f8:	8b 45 0c             	mov    0xc(%ebp),%eax
 2fb:	83 c0 08             	add    $0x8,%eax
 2fe:	8b 00                	mov    (%eax),%eax
 300:	89 04 24             	mov    %eax,(%esp)
 303:	e8 fc ff ff ff       	call   304 <main+0x52>
 308:	a3 00 00 00 00       	mov    %eax,0x0
 30d:	8b 45 0c             	mov    0xc(%ebp),%eax
 310:	83 c0 0c             	add    $0xc,%eax
 313:	8b 00                	mov    (%eax),%eax
 315:	89 04 24             	mov    %eax,(%esp)
 318:	e8 fc ff ff ff       	call   319 <main+0x67>
 31d:	89 44 24 24          	mov    %eax,0x24(%esp)
 321:	8b 45 0c             	mov    0xc(%ebp),%eax
 324:	83 c0 10             	add    $0x10,%eax
 327:	8b 00                	mov    (%eax),%eax
 329:	89 04 24             	mov    %eax,(%esp)
 32c:	e8 fc ff ff ff       	call   32d <main+0x7b>
 331:	83 e8 01             	sub    $0x1,%eax
 334:	89 44 24 20          	mov    %eax,0x20(%esp)
 338:	83 7c 24 28 00       	cmpl   $0x0,0x28(%esp)
 33d:	79 23                	jns    362 <main+0xb0>
 33f:	c7 04 24 94 01 00 00 	movl   $0x194,(%esp)
 346:	e8 fc ff ff ff       	call   347 <main+0x95>
 34b:	8b 45 0c             	mov    0xc(%ebp),%eax
 34e:	8b 00                	mov    (%eax),%eax
 350:	89 04 24             	mov    %eax,(%esp)
 353:	e8 fc ff ff ff       	call   354 <main+0xa2>
 358:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 35d:	e9 3a 01 00 00       	jmp    49c <main+0x1ea>
 362:	a1 00 00 00 00       	mov    0x0,%eax
 367:	85 c0                	test   %eax,%eax
 369:	79 23                	jns    38e <main+0xdc>
 36b:	c7 04 24 b4 01 00 00 	movl   $0x1b4,(%esp)
 372:	e8 fc ff ff ff       	call   373 <main+0xc1>
 377:	8b 45 0c             	mov    0xc(%ebp),%eax
 37a:	8b 00                	mov    (%eax),%eax
 37c:	89 04 24             	mov    %eax,(%esp)
 37f:	e8 fc ff ff ff       	call   380 <main+0xce>
 384:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 389:	e9 0e 01 00 00       	jmp    49c <main+0x1ea>
 38e:	c7 04 24 00 10 00 00 	movl   $0x1000,(%esp)
 395:	e8 fc ff ff ff       	call   396 <main+0xe4>
 39a:	85 c0                	test   %eax,%eax
 39c:	74 16                	je     3b4 <main+0x102>
 39e:	c7 04 24 d4 01 00 00 	movl   $0x1d4,(%esp)
 3a5:	e8 fc ff ff ff       	call   3a6 <main+0xf4>
 3aa:	b8 f6 ff ff ff       	mov    $0xfffffff6,%eax
 3af:	e9 e8 00 00 00       	jmp    49c <main+0x1ea>
 3b4:	8b 44 24 20          	mov    0x20(%esp),%eax
 3b8:	89 44 24 04          	mov    %eax,0x4(%esp)
 3bc:	c7 04 24 f7 01 00 00 	movl   $0x1f7,(%esp)
 3c3:	e8 fc ff ff ff       	call   3c4 <main+0x112>
 3c8:	8b 44 24 20          	mov    0x20(%esp),%eax
 3cc:	89 04 24             	mov    %eax,(%esp)
 3cf:	e8 fc ff ff ff       	call   3d0 <main+0x11e>
 3d4:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
 3db:	e8 fc ff ff ff       	call   3dc <main+0x12a>
 3e0:	85 c0                	test   %eax,%eax
 3e2:	74 16                	je     3fa <main+0x148>
 3e4:	c7 04 24 08 02 00 00 	movl   $0x208,(%esp)
 3eb:	e8 fc ff ff ff       	call   3ec <main+0x13a>
 3f0:	b8 ec ff ff ff       	mov    $0xffffffec,%eax
 3f5:	e9 a2 00 00 00       	jmp    49c <main+0x1ea>
 3fa:	c7 44 24 2c 00 00 00 	movl   $0x0,0x2c(%esp)
 401:	00 
 402:	eb 7a                	jmp    47e <main+0x1cc>
 404:	8b 44 24 2c          	mov    0x2c(%esp),%eax
 408:	83 c0 01             	add    $0x1,%eax
 40b:	89 44 24 04          	mov    %eax,0x4(%esp)
 40f:	c7 04 24 41 02 00 00 	movl   $0x241,(%esp)
 416:	e8 fc ff ff ff       	call   417 <main+0x165>
 41b:	8b 44 24 28          	mov    0x28(%esp),%eax
 41f:	89 04 24             	mov    %eax,(%esp)
 422:	e8 fc ff ff ff       	call   423 <main+0x171>
 427:	89 44 24 1c          	mov    %eax,0x1c(%esp)
 42b:	8b 44 24 1c          	mov    0x1c(%esp),%eax
 42f:	3b 44 24 28          	cmp    0x28(%esp),%eax
 433:	74 23                	je     458 <main+0x1a6>
 435:	8b 44 24 28          	mov    0x28(%esp),%eax
 439:	89 44 24 08          	mov    %eax,0x8(%esp)
 43d:	8b 44 24 1c          	mov    0x1c(%esp),%eax
 441:	89 44 24 04          	mov    %eax,0x4(%esp)
 445:	c7 04 24 5c 02 00 00 	movl   $0x25c,(%esp)
 44c:	e8 fc ff ff ff       	call   44d <main+0x19b>
 451:	b8 fe ff ff ff       	mov    $0xfffffffe,%eax
 456:	eb 44                	jmp    49c <main+0x1ea>
 458:	c7 04 24 9c 02 00 00 	movl   $0x29c,(%esp)
 45f:	e8 fc ff ff ff       	call   460 <main+0x1ae>
 464:	a1 00 00 00 00       	mov    0x0,%eax
 469:	89 44 24 04          	mov    %eax,0x4(%esp)
 46d:	c7 04 24 c4 02 00 00 	movl   $0x2c4,(%esp)
 474:	e8 fc ff ff ff       	call   475 <main+0x1c3>
 479:	83 44 24 2c 01       	addl   $0x1,0x2c(%esp)
 47e:	83 7c 24 24 00       	cmpl   $0x0,0x24(%esp)
 483:	0f 84 7b ff ff ff    	je     404 <main+0x152>
 489:	8b 44 24 2c          	mov    0x2c(%esp),%eax
 48d:	3b 44 24 24          	cmp    0x24(%esp),%eax
 491:	0f 8c 6d ff ff ff    	jl     404 <main+0x152>
 497:	b8 00 00 00 00       	mov    $0x0,%eax
 49c:	c9                   	leave  
 49d:	c3                   	ret    
