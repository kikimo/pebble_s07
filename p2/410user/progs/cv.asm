
cvar_test:     file format elf32-i386


Disassembly of section .text:

01000000 <sleeper_thread>:
 1000000:	55                   	push   %ebp
 1000001:	89 e5                	mov    %esp,%ebp
 1000003:	83 ec 18             	sub    $0x18,%esp
 1000006:	c7 04 24 68 49 00 01 	movl   $0x1004968,(%esp)
 100000d:	e8 3b 0d 00 00       	call   1000d4d <mutex_lock>
 1000012:	85 c0                	test   %eax,%eax
 1000014:	74 18                	je     100002e <sleeper_thread+0x2e>
 1000016:	c7 04 24 40 28 00 01 	movl   $0x1002840,(%esp)
 100001d:	e8 8f 1b 00 00       	call   1001bb1 <lprintf>
 1000022:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
 1000029:	e8 02 24 00 00       	call   1002430 <exit>
 100002e:	c7 44 24 04 68 49 00 	movl   $0x1004968,0x4(%esp)
 1000035:	01 
 1000036:	c7 04 24 5c 49 00 01 	movl   $0x100495c,(%esp)
 100003d:	e8 f3 0e 00 00       	call   1000f35 <cond_wait>
 1000042:	85 c0                	test   %eax,%eax
 1000044:	74 18                	je     100005e <sleeper_thread+0x5e>
 1000046:	c7 04 24 58 28 00 01 	movl   $0x1002858,(%esp)
 100004d:	e8 5f 1b 00 00       	call   1001bb1 <lprintf>
 1000052:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
 1000059:	e8 d2 23 00 00       	call   1002430 <exit>
 100005e:	a1 2c 49 00 01       	mov    0x100492c,%eax
 1000063:	85 c0                	test   %eax,%eax
 1000065:	75 18                	jne    100007f <sleeper_thread+0x7f>
 1000067:	c7 04 24 7a 28 00 01 	movl   $0x100287a,(%esp)
 100006e:	e8 3e 1b 00 00       	call   1001bb1 <lprintf>
 1000073:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
 100007a:	e8 b1 23 00 00       	call   1002430 <exit>
 100007f:	c7 04 24 95 28 00 01 	movl   $0x1002895,(%esp)
 1000086:	e8 26 1b 00 00       	call   1001bb1 <lprintf>
 100008b:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
 1000092:	e8 d3 0b 00 00       	call   1000c6a <thr_exit>
 1000097:	b8 00 00 00 00       	mov    $0x0,%eax
 100009c:	c9                   	leave  
 100009d:	c3                   	ret    

0100009e <maniac>:
 100009e:	55                   	push   %ebp
 100009f:	89 e5                	mov    %esp,%ebp
 10000a1:	83 ec 28             	sub    $0x28,%esp
 10000a4:	e8 02 0c 00 00       	call   1000cab <thr_getid>
 10000a9:	89 45 f4             	mov    %eax,-0xc(%ebp)
 10000ac:	c7 04 24 68 49 00 01 	movl   $0x1004968,(%esp)
 10000b3:	e8 95 0c 00 00       	call   1000d4d <mutex_lock>
 10000b8:	85 c0                	test   %eax,%eax
 10000ba:	74 18                	je     10000d4 <maniac+0x36>
 10000bc:	c7 04 24 40 28 00 01 	movl   $0x1002840,(%esp)
 10000c3:	e8 e9 1a 00 00       	call   1001bb1 <lprintf>
 10000c8:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
 10000cf:	e8 5c 23 00 00       	call   1002430 <exit>
 10000d4:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10000d7:	89 44 24 04          	mov    %eax,0x4(%esp)
 10000db:	c7 04 24 a3 28 00 01 	movl   $0x10028a3,(%esp)
 10000e2:	e8 ca 1a 00 00       	call   1001bb1 <lprintf>
 10000e7:	c7 44 24 04 68 49 00 	movl   $0x1004968,0x4(%esp)
 10000ee:	01 
 10000ef:	c7 04 24 74 49 00 01 	movl   $0x1004974,(%esp)
 10000f6:	e8 3a 0e 00 00       	call   1000f35 <cond_wait>
 10000fb:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10000fe:	89 44 24 04          	mov    %eax,0x4(%esp)
 1000102:	c7 04 24 b5 28 00 01 	movl   $0x10028b5,(%esp)
 1000109:	e8 a3 1a 00 00       	call   1001bb1 <lprintf>
 100010e:	c7 04 24 50 49 00 01 	movl   $0x1004950,(%esp)
 1000115:	e8 a9 0e 00 00       	call   1000fc3 <cond_signal>
 100011a:	8b 45 f4             	mov    -0xc(%ebp),%eax
 100011d:	89 44 24 04          	mov    %eax,0x4(%esp)
 1000121:	c7 04 24 c7 28 00 01 	movl   $0x10028c7,(%esp)
 1000128:	e8 84 1a 00 00       	call   1001bb1 <lprintf>
 100012d:	c7 44 24 04 68 49 00 	movl   $0x1004968,0x4(%esp)
 1000134:	01 
 1000135:	c7 04 24 50 49 00 01 	movl   $0x1004950,(%esp)
 100013c:	e8 f4 0d 00 00       	call   1000f35 <cond_wait>
 1000141:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000144:	89 44 24 04          	mov    %eax,0x4(%esp)
 1000148:	c7 04 24 d9 28 00 01 	movl   $0x10028d9,(%esp)
 100014f:	e8 5d 1a 00 00       	call   1001bb1 <lprintf>
 1000154:	c7 04 24 74 49 00 01 	movl   $0x1004974,(%esp)
 100015b:	e8 ca 0e 00 00       	call   100102a <cond_broadcast>
 1000160:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000163:	89 44 24 04          	mov    %eax,0x4(%esp)
 1000167:	c7 04 24 a3 28 00 01 	movl   $0x10028a3,(%esp)
 100016e:	e8 3e 1a 00 00       	call   1001bb1 <lprintf>
 1000173:	c7 44 24 04 68 49 00 	movl   $0x1004968,0x4(%esp)
 100017a:	01 
 100017b:	c7 04 24 74 49 00 01 	movl   $0x1004974,(%esp)
 1000182:	e8 ae 0d 00 00       	call   1000f35 <cond_wait>
 1000187:	8b 45 f4             	mov    -0xc(%ebp),%eax
 100018a:	89 44 24 04          	mov    %eax,0x4(%esp)
 100018e:	c7 04 24 b5 28 00 01 	movl   $0x10028b5,(%esp)
 1000195:	e8 17 1a 00 00       	call   1001bb1 <lprintf>
 100019a:	c7 04 24 68 49 00 01 	movl   $0x1004968,(%esp)
 10001a1:	e8 d9 0b 00 00       	call   1000d7f <mutex_unlock>
 10001a6:	b8 00 00 00 00       	mov    $0x0,%eax
 10001ab:	c9                   	leave  
 10001ac:	c3                   	ret    

010001ad <instigator>:
 10001ad:	55                   	push   %ebp
 10001ae:	89 e5                	mov    %esp,%ebp
 10001b0:	83 ec 18             	sub    $0x18,%esp
 10001b3:	eb 3c                	jmp    10001f1 <instigator+0x44>
 10001b5:	c7 04 24 05 00 00 00 	movl   $0x5,(%esp)
 10001bc:	e8 03 26 00 00       	call   10027c4 <sleep>
 10001c1:	c7 04 24 eb 28 00 01 	movl   $0x10028eb,(%esp)
 10001c8:	e8 e4 19 00 00       	call   1001bb1 <lprintf>
 10001cd:	c7 04 24 74 49 00 01 	movl   $0x1004974,(%esp)
 10001d4:	e8 ea 0d 00 00       	call   1000fc3 <cond_signal>
 10001d9:	c7 04 24 08 29 00 01 	movl   $0x1002908,(%esp)
 10001e0:	e8 cc 19 00 00       	call   1001bb1 <lprintf>
 10001e5:	c7 04 24 50 49 00 01 	movl   $0x1004950,(%esp)
 10001ec:	e8 d2 0d 00 00       	call   1000fc3 <cond_signal>
 10001f1:	a1 30 49 00 01       	mov    0x1004930,%eax
 10001f6:	85 c0                	test   %eax,%eax
 10001f8:	74 bb                	je     10001b5 <instigator+0x8>
 10001fa:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
 1000201:	e8 64 0a 00 00       	call   1000c6a <thr_exit>
 1000206:	b8 00 00 00 00       	mov    $0x0,%eax
 100020b:	c9                   	leave  
 100020c:	c3                   	ret    

0100020d <main>:
 100020d:	55                   	push   %ebp
 100020e:	89 e5                	mov    %esp,%ebp
 1000210:	83 e4 f0             	and    $0xfffffff0,%esp
 1000213:	83 ec 40             	sub    $0x40,%esp
 1000216:	c7 04 24 00 10 00 00 	movl   $0x1000,(%esp)
 100021d:	e8 0a 07 00 00       	call   100092c <thr_init>
 1000222:	c7 04 24 68 49 00 01 	movl   $0x1004968,(%esp)
 1000229:	e8 e6 0a 00 00       	call   1000d14 <mutex_init>
 100022e:	c7 04 24 74 49 00 01 	movl   $0x1004974,(%esp)
 1000235:	e8 cb 0c 00 00       	call   1000f05 <cond_init>
 100023a:	c7 04 24 50 49 00 01 	movl   $0x1004950,(%esp)
 1000241:	e8 bf 0c 00 00       	call   1000f05 <cond_init>
 1000246:	c7 04 24 5c 49 00 01 	movl   $0x100495c,(%esp)
 100024d:	e8 b3 0c 00 00       	call   1000f05 <cond_init>
 1000252:	8d 44 24 10          	lea    0x10(%esp),%eax
 1000256:	89 04 24             	mov    %eax,(%esp)
 1000259:	e8 82 01 00 00       	call   10003e0 <thrgrp_init_group>
 100025e:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
 1000265:	00 
 1000266:	c7 04 24 00 00 00 01 	movl   $0x1000000,(%esp)
 100026d:	e8 65 07 00 00       	call   10009d7 <thr_create>
 1000272:	89 44 24 38          	mov    %eax,0x38(%esp)
 1000276:	83 7c 24 38 00       	cmpl   $0x0,0x38(%esp)
 100027b:	79 18                	jns    1000295 <main+0x88>
 100027d:	c7 04 24 25 29 00 01 	movl   $0x1002925,(%esp)
 1000284:	e8 28 19 00 00       	call   1001bb1 <lprintf>
 1000289:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
 1000290:	e8 9b 21 00 00       	call   1002430 <exit>
 1000295:	8b 44 24 38          	mov    0x38(%esp),%eax
 1000299:	89 04 24             	mov    %eax,(%esp)
 100029c:	e8 1f 0a 00 00       	call   1000cc0 <thr_yield>
 10002a1:	c7 44 24 3c 00 00 00 	movl   $0x0,0x3c(%esp)
 10002a8:	00 
 10002a9:	eb 50                	jmp    10002fb <main+0xee>
 10002ab:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
 10002b2:	00 
 10002b3:	c7 44 24 04 9e 00 00 	movl   $0x100009e,0x4(%esp)
 10002ba:	01 
 10002bb:	8d 44 24 10          	lea    0x10(%esp),%eax
 10002bf:	89 04 24             	mov    %eax,(%esp)
 10002c2:	e8 7c 02 00 00       	call   1000543 <thrgrp_create>
 10002c7:	89 44 24 34          	mov    %eax,0x34(%esp)
 10002cb:	83 7c 24 34 00       	cmpl   $0x0,0x34(%esp)
 10002d0:	79 18                	jns    10002ea <main+0xdd>
 10002d2:	c7 04 24 25 29 00 01 	movl   $0x1002925,(%esp)
 10002d9:	e8 d3 18 00 00       	call   1001bb1 <lprintf>
 10002de:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
 10002e5:	e8 46 21 00 00       	call   1002430 <exit>
 10002ea:	8b 44 24 34          	mov    0x34(%esp),%eax
 10002ee:	89 04 24             	mov    %eax,(%esp)
 10002f1:	e8 ca 09 00 00       	call   1000cc0 <thr_yield>
 10002f6:	83 44 24 3c 01       	addl   $0x1,0x3c(%esp)
 10002fb:	83 7c 24 3c 09       	cmpl   $0x9,0x3c(%esp)
 1000300:	7e a9                	jle    10002ab <main+0x9e>
 1000302:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
 1000309:	00 
 100030a:	c7 04 24 ad 01 00 01 	movl   $0x10001ad,(%esp)
 1000311:	e8 c1 06 00 00       	call   10009d7 <thr_create>
 1000316:	89 44 24 30          	mov    %eax,0x30(%esp)
 100031a:	83 7c 24 30 00       	cmpl   $0x0,0x30(%esp)
 100031f:	79 18                	jns    1000339 <main+0x12c>
 1000321:	c7 04 24 25 29 00 01 	movl   $0x1002925,(%esp)
 1000328:	e8 84 18 00 00       	call   1001bb1 <lprintf>
 100032d:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
 1000334:	e8 f7 20 00 00       	call   1002430 <exit>
 1000339:	8b 44 24 30          	mov    0x30(%esp),%eax
 100033d:	89 04 24             	mov    %eax,(%esp)
 1000340:	e8 7b 09 00 00       	call   1000cc0 <thr_yield>
 1000345:	c7 44 24 3c 00 00 00 	movl   $0x0,0x3c(%esp)
 100034c:	00 
 100034d:	eb 19                	jmp    1000368 <main+0x15b>
 100034f:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
 1000356:	00 
 1000357:	8d 44 24 10          	lea    0x10(%esp),%eax
 100035b:	89 04 24             	mov    %eax,(%esp)
 100035e:	e8 44 02 00 00       	call   10005a7 <thrgrp_join>
 1000363:	83 44 24 3c 01       	addl   $0x1,0x3c(%esp)
 1000368:	83 7c 24 3c 09       	cmpl   $0x9,0x3c(%esp)
 100036d:	7e e0                	jle    100034f <main+0x142>
 100036f:	c7 05 2c 49 00 01 01 	movl   $0x1,0x100492c
 1000376:	00 00 00 
 1000379:	c7 04 24 5c 49 00 01 	movl   $0x100495c,(%esp)
 1000380:	e8 3e 0c 00 00       	call   1000fc3 <cond_signal>
 1000385:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
 100038c:	00 
 100038d:	8b 44 24 38          	mov    0x38(%esp),%eax
 1000391:	89 04 24             	mov    %eax,(%esp)
 1000394:	e8 9e 07 00 00       	call   1000b37 <thr_join>
 1000399:	c7 05 30 49 00 01 01 	movl   $0x1,0x1004930
 10003a0:	00 00 00 
 10003a3:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
 10003aa:	00 
 10003ab:	8b 44 24 30          	mov    0x30(%esp),%eax
 10003af:	89 04 24             	mov    %eax,(%esp)
 10003b2:	e8 80 07 00 00       	call   1000b37 <thr_join>
 10003b7:	b8 00 00 00 00       	mov    $0x0,%eax
 10003bc:	c9                   	leave  
 10003bd:	c3                   	ret    
 10003be:	90                   	nop
 10003bf:	90                   	nop

010003c0 <_main>:
 10003c0:	55                   	push   %ebp
 10003c1:	89 e5                	mov    %esp,%ebp
 10003c3:	83 ec 18             	sub    $0x18,%esp
 10003c6:	8b 45 0c             	mov    0xc(%ebp),%eax
 10003c9:	89 44 24 04          	mov    %eax,0x4(%esp)
 10003cd:	8b 45 08             	mov    0x8(%ebp),%eax
 10003d0:	89 04 24             	mov    %eax,(%esp)
 10003d3:	e8 35 fe ff ff       	call   100020d <main>
 10003d8:	89 04 24             	mov    %eax,(%esp)
 10003db:	e8 50 20 00 00       	call   1002430 <exit>

010003e0 <thrgrp_init_group>:
 10003e0:	55                   	push   %ebp
 10003e1:	89 e5                	mov    %esp,%ebp
 10003e3:	83 ec 28             	sub    $0x28,%esp
 10003e6:	8b 45 08             	mov    0x8(%ebp),%eax
 10003e9:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
 10003f0:	8b 45 08             	mov    0x8(%ebp),%eax
 10003f3:	c7 40 10 00 00 00 00 	movl   $0x0,0x10(%eax)
 10003fa:	8b 45 08             	mov    0x8(%ebp),%eax
 10003fd:	83 c0 14             	add    $0x14,%eax
 1000400:	89 04 24             	mov    %eax,(%esp)
 1000403:	e8 0c 09 00 00       	call   1000d14 <mutex_init>
 1000408:	89 45 f4             	mov    %eax,-0xc(%ebp)
 100040b:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 100040f:	74 05                	je     1000416 <thrgrp_init_group+0x36>
 1000411:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000414:	eb 1e                	jmp    1000434 <thrgrp_init_group+0x54>
 1000416:	8b 45 08             	mov    0x8(%ebp),%eax
 1000419:	89 04 24             	mov    %eax,(%esp)
 100041c:	e8 e4 0a 00 00       	call   1000f05 <cond_init>
 1000421:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1000424:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 1000428:	74 05                	je     100042f <thrgrp_init_group+0x4f>
 100042a:	8b 45 f4             	mov    -0xc(%ebp),%eax
 100042d:	eb 05                	jmp    1000434 <thrgrp_init_group+0x54>
 100042f:	b8 00 00 00 00       	mov    $0x0,%eax
 1000434:	c9                   	leave  
 1000435:	c3                   	ret    

01000436 <thrgrp_destroy_group>:
 1000436:	55                   	push   %ebp
 1000437:	89 e5                	mov    %esp,%ebp
 1000439:	83 ec 28             	sub    $0x28,%esp
 100043c:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 1000443:	8b 45 08             	mov    0x8(%ebp),%eax
 1000446:	83 c0 14             	add    $0x14,%eax
 1000449:	89 04 24             	mov    %eax,(%esp)
 100044c:	e8 fc 08 00 00       	call   1000d4d <mutex_lock>
 1000451:	8b 45 08             	mov    0x8(%ebp),%eax
 1000454:	8b 40 0c             	mov    0xc(%eax),%eax
 1000457:	85 c0                	test   %eax,%eax
 1000459:	75 0a                	jne    1000465 <thrgrp_destroy_group+0x2f>
 100045b:	8b 45 08             	mov    0x8(%ebp),%eax
 100045e:	8b 40 10             	mov    0x10(%eax),%eax
 1000461:	85 c0                	test   %eax,%eax
 1000463:	74 07                	je     100046c <thrgrp_destroy_group+0x36>
 1000465:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)
 100046c:	8b 45 08             	mov    0x8(%ebp),%eax
 100046f:	83 c0 14             	add    $0x14,%eax
 1000472:	89 04 24             	mov    %eax,(%esp)
 1000475:	e8 05 09 00 00       	call   1000d7f <mutex_unlock>
 100047a:	8b 45 08             	mov    0x8(%ebp),%eax
 100047d:	83 c0 14             	add    $0x14,%eax
 1000480:	89 04 24             	mov    %eax,(%esp)
 1000483:	e8 bb 08 00 00       	call   1000d43 <mutex_destroy>
 1000488:	09 45 f4             	or     %eax,-0xc(%ebp)
 100048b:	8b 45 08             	mov    0x8(%ebp),%eax
 100048e:	89 04 24             	mov    %eax,(%esp)
 1000491:	e8 95 0a 00 00       	call   1000f2b <cond_destroy>
 1000496:	09 45 f4             	or     %eax,-0xc(%ebp)
 1000499:	8b 45 f4             	mov    -0xc(%ebp),%eax
 100049c:	c9                   	leave  
 100049d:	c3                   	ret    

0100049e <thrgrp_bottom>:
 100049e:	55                   	push   %ebp
 100049f:	89 e5                	mov    %esp,%ebp
 10004a1:	83 ec 38             	sub    $0x38,%esp
 10004a4:	8b 45 08             	mov    0x8(%ebp),%eax
 10004a7:	89 45 f4             	mov    %eax,-0xc(%ebp)
 10004aa:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10004ad:	8b 00                	mov    (%eax),%eax
 10004af:	89 45 f0             	mov    %eax,-0x10(%ebp)
 10004b2:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10004b5:	8b 40 04             	mov    0x4(%eax),%eax
 10004b8:	89 45 ec             	mov    %eax,-0x14(%ebp)
 10004bb:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10004be:	8b 40 08             	mov    0x8(%eax),%eax
 10004c1:	89 45 e8             	mov    %eax,-0x18(%ebp)
 10004c4:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10004c7:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
 10004cd:	e8 d9 07 00 00       	call   1000cab <thr_getid>
 10004d2:	8b 55 f4             	mov    -0xc(%ebp),%edx
 10004d5:	89 42 04             	mov    %eax,0x4(%edx)
 10004d8:	8b 45 ec             	mov    -0x14(%ebp),%eax
 10004db:	89 04 24             	mov    %eax,(%esp)
 10004de:	8b 45 f0             	mov    -0x10(%ebp),%eax
 10004e1:	ff d0                	call   *%eax
 10004e3:	89 45 e4             	mov    %eax,-0x1c(%ebp)
 10004e6:	8b 45 e8             	mov    -0x18(%ebp),%eax
 10004e9:	83 c0 14             	add    $0x14,%eax
 10004ec:	89 04 24             	mov    %eax,(%esp)
 10004ef:	e8 59 08 00 00       	call   1000d4d <mutex_lock>
 10004f4:	8b 45 e8             	mov    -0x18(%ebp),%eax
 10004f7:	8b 40 0c             	mov    0xc(%eax),%eax
 10004fa:	85 c0                	test   %eax,%eax
 10004fc:	74 0b                	je     1000509 <thrgrp_bottom+0x6b>
 10004fe:	8b 45 e8             	mov    -0x18(%ebp),%eax
 1000501:	8b 40 0c             	mov    0xc(%eax),%eax
 1000504:	8b 55 f4             	mov    -0xc(%ebp),%edx
 1000507:	89 10                	mov    %edx,(%eax)
 1000509:	8b 55 f4             	mov    -0xc(%ebp),%edx
 100050c:	8b 45 e8             	mov    -0x18(%ebp),%eax
 100050f:	89 50 0c             	mov    %edx,0xc(%eax)
 1000512:	8b 45 e8             	mov    -0x18(%ebp),%eax
 1000515:	8b 40 10             	mov    0x10(%eax),%eax
 1000518:	85 c0                	test   %eax,%eax
 100051a:	75 09                	jne    1000525 <thrgrp_bottom+0x87>
 100051c:	8b 55 f4             	mov    -0xc(%ebp),%edx
 100051f:	8b 45 e8             	mov    -0x18(%ebp),%eax
 1000522:	89 50 10             	mov    %edx,0x10(%eax)
 1000525:	8b 45 e8             	mov    -0x18(%ebp),%eax
 1000528:	89 04 24             	mov    %eax,(%esp)
 100052b:	e8 93 0a 00 00       	call   1000fc3 <cond_signal>
 1000530:	8b 45 e8             	mov    -0x18(%ebp),%eax
 1000533:	83 c0 14             	add    $0x14,%eax
 1000536:	89 04 24             	mov    %eax,(%esp)
 1000539:	e8 41 08 00 00       	call   1000d7f <mutex_unlock>
 100053e:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 1000541:	c9                   	leave  
 1000542:	c3                   	ret    

01000543 <thrgrp_create>:
 1000543:	55                   	push   %ebp
 1000544:	89 e5                	mov    %esp,%ebp
 1000546:	83 ec 28             	sub    $0x28,%esp
 1000549:	c7 04 24 0c 00 00 00 	movl   $0xc,(%esp)
 1000550:	e8 0b 01 00 00       	call   1000660 <malloc>
 1000555:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1000558:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 100055c:	75 07                	jne    1000565 <thrgrp_create+0x22>
 100055e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 1000563:	eb 40                	jmp    10005a5 <thrgrp_create+0x62>
 1000565:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000568:	8b 55 0c             	mov    0xc(%ebp),%edx
 100056b:	89 10                	mov    %edx,(%eax)
 100056d:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000570:	8b 55 10             	mov    0x10(%ebp),%edx
 1000573:	89 50 04             	mov    %edx,0x4(%eax)
 1000576:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000579:	8b 55 08             	mov    0x8(%ebp),%edx
 100057c:	89 50 08             	mov    %edx,0x8(%eax)
 100057f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000582:	89 44 24 04          	mov    %eax,0x4(%esp)
 1000586:	c7 04 24 9e 04 00 01 	movl   $0x100049e,(%esp)
 100058d:	e8 45 04 00 00       	call   10009d7 <thr_create>
 1000592:	89 45 f0             	mov    %eax,-0x10(%ebp)
 1000595:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 1000599:	79 05                	jns    10005a0 <thrgrp_create+0x5d>
 100059b:	8b 45 f0             	mov    -0x10(%ebp),%eax
 100059e:	eb 05                	jmp    10005a5 <thrgrp_create+0x62>
 10005a0:	b8 00 00 00 00       	mov    $0x0,%eax
 10005a5:	c9                   	leave  
 10005a6:	c3                   	ret    

010005a7 <thrgrp_join>:
 10005a7:	55                   	push   %ebp
 10005a8:	89 e5                	mov    %esp,%ebp
 10005aa:	83 ec 28             	sub    $0x28,%esp
 10005ad:	8b 45 08             	mov    0x8(%ebp),%eax
 10005b0:	83 c0 14             	add    $0x14,%eax
 10005b3:	89 04 24             	mov    %eax,(%esp)
 10005b6:	e8 92 07 00 00       	call   1000d4d <mutex_lock>
 10005bb:	eb 25                	jmp    10005e2 <thrgrp_join+0x3b>
 10005bd:	8b 45 08             	mov    0x8(%ebp),%eax
 10005c0:	8d 50 14             	lea    0x14(%eax),%edx
 10005c3:	8b 45 08             	mov    0x8(%ebp),%eax
 10005c6:	89 54 24 04          	mov    %edx,0x4(%esp)
 10005ca:	89 04 24             	mov    %eax,(%esp)
 10005cd:	e8 63 09 00 00       	call   1000f35 <cond_wait>
 10005d2:	85 c0                	test   %eax,%eax
 10005d4:	74 0c                	je     10005e2 <thrgrp_join+0x3b>
 10005d6:	c7 04 24 40 29 00 01 	movl   $0x1002940,(%esp)
 10005dd:	e8 66 1e 00 00       	call   1002448 <panic>
 10005e2:	8b 45 08             	mov    0x8(%ebp),%eax
 10005e5:	8b 40 10             	mov    0x10(%eax),%eax
 10005e8:	85 c0                	test   %eax,%eax
 10005ea:	74 d1                	je     10005bd <thrgrp_join+0x16>
 10005ec:	8b 45 08             	mov    0x8(%ebp),%eax
 10005ef:	8b 40 10             	mov    0x10(%eax),%eax
 10005f2:	89 45 f4             	mov    %eax,-0xc(%ebp)
 10005f5:	8b 45 08             	mov    0x8(%ebp),%eax
 10005f8:	8b 50 10             	mov    0x10(%eax),%edx
 10005fb:	8b 45 08             	mov    0x8(%ebp),%eax
 10005fe:	8b 40 0c             	mov    0xc(%eax),%eax
 1000601:	39 c2                	cmp    %eax,%edx
 1000603:	75 16                	jne    100061b <thrgrp_join+0x74>
 1000605:	8b 45 08             	mov    0x8(%ebp),%eax
 1000608:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
 100060f:	8b 45 08             	mov    0x8(%ebp),%eax
 1000612:	c7 40 10 00 00 00 00 	movl   $0x0,0x10(%eax)
 1000619:	eb 0e                	jmp    1000629 <thrgrp_join+0x82>
 100061b:	8b 45 08             	mov    0x8(%ebp),%eax
 100061e:	8b 40 10             	mov    0x10(%eax),%eax
 1000621:	8b 10                	mov    (%eax),%edx
 1000623:	8b 45 08             	mov    0x8(%ebp),%eax
 1000626:	89 50 10             	mov    %edx,0x10(%eax)
 1000629:	8b 45 08             	mov    0x8(%ebp),%eax
 100062c:	83 c0 14             	add    $0x14,%eax
 100062f:	89 04 24             	mov    %eax,(%esp)
 1000632:	e8 48 07 00 00       	call   1000d7f <mutex_unlock>
 1000637:	8b 45 f4             	mov    -0xc(%ebp),%eax
 100063a:	8b 40 04             	mov    0x4(%eax),%eax
 100063d:	89 45 f0             	mov    %eax,-0x10(%ebp)
 1000640:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000643:	89 04 24             	mov    %eax,(%esp)
 1000646:	e8 cb 00 00 00       	call   1000716 <free>
 100064b:	8b 45 0c             	mov    0xc(%ebp),%eax
 100064e:	89 44 24 04          	mov    %eax,0x4(%esp)
 1000652:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1000655:	89 04 24             	mov    %eax,(%esp)
 1000658:	e8 da 04 00 00       	call   1000b37 <thr_join>
 100065d:	c9                   	leave  
 100065e:	c3                   	ret    
 100065f:	90                   	nop

01000660 <malloc>:
 1000660:	55                   	push   %ebp
 1000661:	89 e5                	mov    %esp,%ebp
 1000663:	83 ec 28             	sub    $0x28,%esp
 1000666:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 100066d:	c7 04 24 80 49 00 01 	movl   $0x1004980,(%esp)
 1000674:	e8 d4 06 00 00       	call   1000d4d <mutex_lock>
 1000679:	8b 45 08             	mov    0x8(%ebp),%eax
 100067c:	89 04 24             	mov    %eax,(%esp)
 100067f:	e8 08 0a 00 00       	call   100108c <_malloc>
 1000684:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1000687:	c7 04 24 80 49 00 01 	movl   $0x1004980,(%esp)
 100068e:	e8 ec 06 00 00       	call   1000d7f <mutex_unlock>
 1000693:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000696:	c9                   	leave  
 1000697:	c3                   	ret    

01000698 <calloc>:
 1000698:	55                   	push   %ebp
 1000699:	89 e5                	mov    %esp,%ebp
 100069b:	83 ec 28             	sub    $0x28,%esp
 100069e:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 10006a5:	c7 04 24 80 49 00 01 	movl   $0x1004980,(%esp)
 10006ac:	e8 9c 06 00 00       	call   1000d4d <mutex_lock>
 10006b1:	8b 45 0c             	mov    0xc(%ebp),%eax
 10006b4:	89 44 24 04          	mov    %eax,0x4(%esp)
 10006b8:	8b 45 08             	mov    0x8(%ebp),%eax
 10006bb:	89 04 24             	mov    %eax,(%esp)
 10006be:	e8 ff 09 00 00       	call   10010c2 <_calloc>
 10006c3:	89 45 f4             	mov    %eax,-0xc(%ebp)
 10006c6:	c7 04 24 80 49 00 01 	movl   $0x1004980,(%esp)
 10006cd:	e8 ad 06 00 00       	call   1000d7f <mutex_unlock>
 10006d2:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10006d5:	c9                   	leave  
 10006d6:	c3                   	ret    

010006d7 <realloc>:
 10006d7:	55                   	push   %ebp
 10006d8:	89 e5                	mov    %esp,%ebp
 10006da:	83 ec 28             	sub    $0x28,%esp
 10006dd:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 10006e4:	c7 04 24 80 49 00 01 	movl   $0x1004980,(%esp)
 10006eb:	e8 5d 06 00 00       	call   1000d4d <mutex_lock>
 10006f0:	8b 45 0c             	mov    0xc(%ebp),%eax
 10006f3:	89 44 24 04          	mov    %eax,0x4(%esp)
 10006f7:	8b 45 08             	mov    0x8(%ebp),%eax
 10006fa:	89 04 24             	mov    %eax,(%esp)
 10006fd:	e8 23 0a 00 00       	call   1001125 <_realloc>
 1000702:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1000705:	c7 04 24 80 49 00 01 	movl   $0x1004980,(%esp)
 100070c:	e8 6e 06 00 00       	call   1000d7f <mutex_unlock>
 1000711:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000714:	c9                   	leave  
 1000715:	c3                   	ret    

01000716 <free>:
 1000716:	55                   	push   %ebp
 1000717:	89 e5                	mov    %esp,%ebp
 1000719:	83 ec 18             	sub    $0x18,%esp
 100071c:	c7 04 24 80 49 00 01 	movl   $0x1004980,(%esp)
 1000723:	e8 25 06 00 00       	call   1000d4d <mutex_lock>
 1000728:	8b 45 08             	mov    0x8(%ebp),%eax
 100072b:	89 04 24             	mov    %eax,(%esp)
 100072e:	e8 1c 0a 00 00       	call   100114f <_free>
 1000733:	c7 04 24 80 49 00 01 	movl   $0x1004980,(%esp)
 100073a:	e8 40 06 00 00       	call   1000d7f <mutex_unlock>
 100073f:	c9                   	leave  
 1000740:	c3                   	ret    
 1000741:	90                   	nop
 1000742:	90                   	nop
 1000743:	90                   	nop

01000744 <__list_add>:
 1000744:	55                   	push   %ebp
 1000745:	89 e5                	mov    %esp,%ebp
 1000747:	8b 45 10             	mov    0x10(%ebp),%eax
 100074a:	8b 55 08             	mov    0x8(%ebp),%edx
 100074d:	89 50 04             	mov    %edx,0x4(%eax)
 1000750:	8b 45 08             	mov    0x8(%ebp),%eax
 1000753:	8b 55 10             	mov    0x10(%ebp),%edx
 1000756:	89 10                	mov    %edx,(%eax)
 1000758:	8b 45 0c             	mov    0xc(%ebp),%eax
 100075b:	8b 55 08             	mov    0x8(%ebp),%edx
 100075e:	89 10                	mov    %edx,(%eax)
 1000760:	8b 45 08             	mov    0x8(%ebp),%eax
 1000763:	8b 55 0c             	mov    0xc(%ebp),%edx
 1000766:	89 50 04             	mov    %edx,0x4(%eax)
 1000769:	5d                   	pop    %ebp
 100076a:	c3                   	ret    

0100076b <__list_del>:
 100076b:	55                   	push   %ebp
 100076c:	89 e5                	mov    %esp,%ebp
 100076e:	8b 45 08             	mov    0x8(%ebp),%eax
 1000771:	8b 55 0c             	mov    0xc(%ebp),%edx
 1000774:	89 10                	mov    %edx,(%eax)
 1000776:	8b 45 0c             	mov    0xc(%ebp),%eax
 1000779:	8b 55 08             	mov    0x8(%ebp),%edx
 100077c:	89 50 04             	mov    %edx,0x4(%eax)
 100077f:	5d                   	pop    %ebp
 1000780:	c3                   	ret    

01000781 <list_add>:
 1000781:	55                   	push   %ebp
 1000782:	89 e5                	mov    %esp,%ebp
 1000784:	83 ec 0c             	sub    $0xc,%esp
 1000787:	8b 45 0c             	mov    0xc(%ebp),%eax
 100078a:	8b 00                	mov    (%eax),%eax
 100078c:	89 44 24 08          	mov    %eax,0x8(%esp)
 1000790:	8b 45 0c             	mov    0xc(%ebp),%eax
 1000793:	89 44 24 04          	mov    %eax,0x4(%esp)
 1000797:	8b 45 08             	mov    0x8(%ebp),%eax
 100079a:	89 04 24             	mov    %eax,(%esp)
 100079d:	e8 a2 ff ff ff       	call   1000744 <__list_add>
 10007a2:	c9                   	leave  
 10007a3:	c3                   	ret    

010007a4 <list_del>:
 10007a4:	55                   	push   %ebp
 10007a5:	89 e5                	mov    %esp,%ebp
 10007a7:	83 ec 08             	sub    $0x8,%esp
 10007aa:	8b 45 08             	mov    0x8(%ebp),%eax
 10007ad:	8b 10                	mov    (%eax),%edx
 10007af:	8b 45 08             	mov    0x8(%ebp),%eax
 10007b2:	8b 40 04             	mov    0x4(%eax),%eax
 10007b5:	89 54 24 04          	mov    %edx,0x4(%esp)
 10007b9:	89 04 24             	mov    %eax,(%esp)
 10007bc:	e8 aa ff ff ff       	call   100076b <__list_del>
 10007c1:	c9                   	leave  
 10007c2:	c3                   	ret    

010007c3 <xchg>:
 10007c3:	55                   	push   %ebp
 10007c4:	89 e5                	mov    %esp,%ebp
 10007c6:	83 ec 04             	sub    $0x4,%esp
 10007c9:	8b 45 0c             	mov    0xc(%ebp),%eax
 10007cc:	88 45 fc             	mov    %al,-0x4(%ebp)
 10007cf:	8b 55 08             	mov    0x8(%ebp),%edx
 10007d2:	0f b6 45 fc          	movzbl -0x4(%ebp),%eax
 10007d6:	f0 86 02             	lock xchg %al,(%edx)
 10007d9:	88 45 fc             	mov    %al,-0x4(%ebp)
 10007dc:	0f b6 45 fc          	movzbl -0x4(%ebp),%eax
 10007e0:	c9                   	leave  
 10007e1:	c3                   	ret    

010007e2 <spin_lock>:
 10007e2:	55                   	push   %ebp
 10007e3:	89 e5                	mov    %esp,%ebp
 10007e5:	83 ec 28             	sub    $0x28,%esp
 10007e8:	c6 45 f7 01          	movb   $0x1,-0x9(%ebp)
 10007ec:	0f be 45 f7          	movsbl -0x9(%ebp),%eax
 10007f0:	89 44 24 04          	mov    %eax,0x4(%esp)
 10007f4:	8b 45 08             	mov    0x8(%ebp),%eax
 10007f7:	89 04 24             	mov    %eax,(%esp)
 10007fa:	e8 c4 ff ff ff       	call   10007c3 <xchg>
 10007ff:	88 45 f7             	mov    %al,-0x9(%ebp)
 1000802:	80 7d f7 01          	cmpb   $0x1,-0x9(%ebp)
 1000806:	75 0e                	jne    1000816 <spin_lock+0x34>
 1000808:	c7 04 24 ff ff ff ff 	movl   $0xffffffff,(%esp)
 100080f:	e8 4c 1f 00 00       	call   1002760 <yield>
 1000814:	eb d6                	jmp    10007ec <spin_lock+0xa>
 1000816:	90                   	nop
 1000817:	c9                   	leave  
 1000818:	c3                   	ret    

01000819 <spin_try_lock>:
 1000819:	55                   	push   %ebp
 100081a:	89 e5                	mov    %esp,%ebp
 100081c:	83 ec 18             	sub    $0x18,%esp
 100081f:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
 1000826:	00 
 1000827:	8b 45 08             	mov    0x8(%ebp),%eax
 100082a:	89 04 24             	mov    %eax,(%esp)
 100082d:	e8 91 ff ff ff       	call   10007c3 <xchg>
 1000832:	88 45 ff             	mov    %al,-0x1(%ebp)
 1000835:	80 7d ff 00          	cmpb   $0x0,-0x1(%ebp)
 1000839:	0f 94 c0             	sete   %al
 100083c:	0f b6 c0             	movzbl %al,%eax
 100083f:	c9                   	leave  
 1000840:	c3                   	ret    

01000841 <spin_unlock>:
 1000841:	55                   	push   %ebp
 1000842:	89 e5                	mov    %esp,%ebp
 1000844:	8b 45 08             	mov    0x8(%ebp),%eax
 1000847:	c6 00 00             	movb   $0x0,(%eax)
 100084a:	5d                   	pop    %ebp
 100084b:	c3                   	ret    

0100084c <last_ebp>:
 100084c:	55                   	push   %ebp
 100084d:	89 e5                	mov    %esp,%ebp
 100084f:	83 ec 10             	sub    $0x10,%esp
 1000852:	89 6d f8             	mov    %ebp,-0x8(%ebp)
 1000855:	eb 06                	jmp    100085d <last_ebp+0x11>
 1000857:	8b 45 fc             	mov    -0x4(%ebp),%eax
 100085a:	89 45 f8             	mov    %eax,-0x8(%ebp)
 100085d:	8b 45 f8             	mov    -0x8(%ebp),%eax
 1000860:	8b 00                	mov    (%eax),%eax
 1000862:	89 45 fc             	mov    %eax,-0x4(%ebp)
 1000865:	83 7d fc 00          	cmpl   $0x0,-0x4(%ebp)
 1000869:	75 ec                	jne    1000857 <last_ebp+0xb>
 100086b:	8b 45 f8             	mov    -0x8(%ebp),%eax
 100086e:	c9                   	leave  
 100086f:	c3                   	ret    

01000870 <get_thread_local>:
 1000870:	55                   	push   %ebp
 1000871:	89 e5                	mov    %esp,%ebp
 1000873:	83 ec 10             	sub    $0x10,%esp
 1000876:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 100087d:	e8 ca ff ff ff       	call   100084c <last_ebp>
 1000882:	89 45 fc             	mov    %eax,-0x4(%ebp)
 1000885:	8b 45 fc             	mov    -0x4(%ebp),%eax
 1000888:	8b 40 04             	mov    0x4(%eax),%eax
 100088b:	3d af be ad de       	cmp    $0xdeadbeaf,%eax
 1000890:	74 07                	je     1000899 <get_thread_local+0x29>
 1000892:	b8 00 00 00 00       	mov    $0x0,%eax
 1000897:	eb 03                	jmp    100089c <get_thread_local+0x2c>
 1000899:	8b 45 fc             	mov    -0x4(%ebp),%eax
 100089c:	c9                   	leave  
 100089d:	c3                   	ret    

0100089e <current_thread>:
 100089e:	55                   	push   %ebp
 100089f:	89 e5                	mov    %esp,%ebp
 10008a1:	83 ec 10             	sub    $0x10,%esp
 10008a4:	e8 c7 ff ff ff       	call   1000870 <get_thread_local>
 10008a9:	89 45 fc             	mov    %eax,-0x4(%ebp)
 10008ac:	8b 45 fc             	mov    -0x4(%ebp),%eax
 10008af:	83 c0 08             	add    $0x8,%eax
 10008b2:	c9                   	leave  
 10008b3:	c3                   	ret    

010008b4 <thr_init_thread>:
 10008b4:	55                   	push   %ebp
 10008b5:	89 e5                	mov    %esp,%ebp
 10008b7:	83 ec 18             	sub    $0x18,%esp
 10008ba:	8b 45 08             	mov    0x8(%ebp),%eax
 10008bd:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
 10008c4:	8b 45 08             	mov    0x8(%ebp),%eax
 10008c7:	c6 40 0c 00          	movb   $0x0,0xc(%eax)
 10008cb:	8b 45 08             	mov    0x8(%ebp),%eax
 10008ce:	c6 40 0f 00          	movb   $0x0,0xf(%eax)
 10008d2:	8b 45 08             	mov    0x8(%ebp),%eax
 10008d5:	c6 40 0d 01          	movb   $0x1,0xd(%eax)
 10008d9:	8b 45 08             	mov    0x8(%ebp),%eax
 10008dc:	c6 40 0e 01          	movb   $0x1,0xe(%eax)
 10008e0:	8b 45 08             	mov    0x8(%ebp),%eax
 10008e3:	83 c0 10             	add    $0x10,%eax
 10008e6:	89 04 24             	mov    %eax,(%esp)
 10008e9:	e8 26 04 00 00       	call   1000d14 <mutex_init>
 10008ee:	8b 45 08             	mov    0x8(%ebp),%eax
 10008f1:	83 c0 10             	add    $0x10,%eax
 10008f4:	89 04 24             	mov    %eax,(%esp)
 10008f7:	e8 51 04 00 00       	call   1000d4d <mutex_lock>
 10008fc:	c7 04 24 34 49 00 01 	movl   $0x1004934,(%esp)
 1000903:	e8 da fe ff ff       	call   10007e2 <spin_lock>
 1000908:	8b 45 08             	mov    0x8(%ebp),%eax
 100090b:	83 c0 1c             	add    $0x1c,%eax
 100090e:	c7 44 24 04 00 49 00 	movl   $0x1004900,0x4(%esp)
 1000915:	01 
 1000916:	89 04 24             	mov    %eax,(%esp)
 1000919:	e8 63 fe ff ff       	call   1000781 <list_add>
 100091e:	c7 04 24 34 49 00 01 	movl   $0x1004934,(%esp)
 1000925:	e8 17 ff ff ff       	call   1000841 <spin_unlock>
 100092a:	c9                   	leave  
 100092b:	c3                   	ret    

0100092c <thr_init>:
 100092c:	55                   	push   %ebp
 100092d:	89 e5                	mov    %esp,%ebp
 100092f:	83 ec 28             	sub    $0x28,%esp
 1000932:	c7 04 24 80 49 00 01 	movl   $0x1004980,(%esp)
 1000939:	e8 d6 03 00 00       	call   1000d14 <mutex_init>
 100093e:	8b 45 08             	mov    0x8(%ebp),%eax
 1000941:	a3 8c 49 00 01       	mov    %eax,0x100498c
 1000946:	a1 8c 49 00 01       	mov    0x100498c,%eax
 100094b:	3d ff 0f 00 00       	cmp    $0xfff,%eax
 1000950:	77 0a                	ja     100095c <thr_init+0x30>
 1000952:	c7 05 8c 49 00 01 00 	movl   $0x1000,0x100498c
 1000959:	10 00 00 
 100095c:	a1 8c 49 00 01       	mov    0x100498c,%eax
 1000961:	83 e0 fc             	and    $0xfffffffc,%eax
 1000964:	a3 8c 49 00 01       	mov    %eax,0x100498c
 1000969:	c7 04 24 2c 00 00 00 	movl   $0x2c,(%esp)
 1000970:	e8 eb fc ff ff       	call   1000660 <malloc>
 1000975:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1000978:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 100097c:	75 07                	jne    1000985 <thr_init+0x59>
 100097e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 1000983:	eb 50                	jmp    10009d5 <thr_init+0xa9>
 1000985:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000988:	c7 40 04 af be ad de 	movl   $0xdeadbeaf,0x4(%eax)
 100098f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000992:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
 1000998:	e8 af fe ff ff       	call   100084c <last_ebp>
 100099d:	89 45 f0             	mov    %eax,-0x10(%ebp)
 10009a0:	8b 45 f0             	mov    -0x10(%ebp),%eax
 10009a3:	8b 55 f4             	mov    -0xc(%ebp),%edx
 10009a6:	89 10                	mov    %edx,(%eax)
 10009a8:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10009ab:	83 c0 08             	add    $0x8,%eax
 10009ae:	89 45 ec             	mov    %eax,-0x14(%ebp)
 10009b1:	e8 e2 1d 00 00       	call   1002798 <gettid>
 10009b6:	8b 55 ec             	mov    -0x14(%ebp),%edx
 10009b9:	89 02                	mov    %eax,(%edx)
 10009bb:	8b 45 ec             	mov    -0x14(%ebp),%eax
 10009be:	c7 40 04 00 00 00 00 	movl   $0x0,0x4(%eax)
 10009c5:	8b 45 ec             	mov    -0x14(%ebp),%eax
 10009c8:	89 04 24             	mov    %eax,(%esp)
 10009cb:	e8 e4 fe ff ff       	call   10008b4 <thr_init_thread>
 10009d0:	b8 00 00 00 00       	mov    $0x0,%eax
 10009d5:	c9                   	leave  
 10009d6:	c3                   	ret    

010009d7 <thr_create>:
 10009d7:	55                   	push   %ebp
 10009d8:	89 e5                	mov    %esp,%ebp
 10009da:	83 ec 38             	sub    $0x38,%esp
 10009dd:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 10009e4:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 10009eb:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
 10009f2:	a1 8c 49 00 01       	mov    0x100498c,%eax
 10009f7:	89 04 24             	mov    %eax,(%esp)
 10009fa:	e8 61 fc ff ff       	call   1000660 <malloc>
 10009ff:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1000a02:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 1000a06:	75 0a                	jne    1000a12 <thr_create+0x3b>
 1000a08:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 1000a0d:	e9 f4 00 00 00       	jmp    1000b06 <thr_create+0x12f>
 1000a12:	a1 8c 49 00 01       	mov    0x100498c,%eax
 1000a17:	83 e8 04             	sub    $0x4,%eax
 1000a1a:	03 45 f4             	add    -0xc(%ebp),%eax
 1000a1d:	89 45 e8             	mov    %eax,-0x18(%ebp)
 1000a20:	8b 45 e8             	mov    -0x18(%ebp),%eax
 1000a23:	83 c0 04             	add    $0x4,%eax
 1000a26:	c7 00 af be cd ab    	movl   $0xabcdbeaf,(%eax)
 1000a2c:	c7 04 24 2c 00 00 00 	movl   $0x2c,(%esp)
 1000a33:	e8 28 fc ff ff       	call   1000660 <malloc>
 1000a38:	89 45 f0             	mov    %eax,-0x10(%ebp)
 1000a3b:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 1000a3f:	75 0a                	jne    1000a4b <thr_create+0x74>
 1000a41:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 1000a46:	e9 bb 00 00 00       	jmp    1000b06 <thr_create+0x12f>
 1000a4b:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1000a4e:	c7 40 04 af be ad de 	movl   $0xdeadbeaf,0x4(%eax)
 1000a55:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1000a58:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
 1000a5e:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1000a61:	83 c0 08             	add    $0x8,%eax
 1000a64:	89 45 ec             	mov    %eax,-0x14(%ebp)
 1000a67:	8b 45 e8             	mov    -0x18(%ebp),%eax
 1000a6a:	8b 55 f0             	mov    -0x10(%ebp),%edx
 1000a6d:	89 10                	mov    %edx,(%eax)
 1000a6f:	8b 45 e8             	mov    -0x18(%ebp),%eax
 1000a72:	83 e8 0c             	sub    $0xc,%eax
 1000a75:	89 45 e4             	mov    %eax,-0x1c(%ebp)
 1000a78:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 1000a7b:	8b 55 08             	mov    0x8(%ebp),%edx
 1000a7e:	89 10                	mov    %edx,(%eax)
 1000a80:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 1000a83:	8d 50 04             	lea    0x4(%eax),%edx
 1000a86:	8b 45 0c             	mov    0xc(%ebp),%eax
 1000a89:	89 02                	mov    %eax,(%edx)
 1000a8b:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 1000a8e:	8d 50 08             	lea    0x8(%eax),%edx
 1000a91:	8b 45 ec             	mov    -0x14(%ebp),%eax
 1000a94:	89 02                	mov    %eax,(%edx)
 1000a96:	8b 45 e8             	mov    -0x18(%ebp),%eax
 1000a99:	89 44 24 04          	mov    %eax,0x4(%esp)
 1000a9d:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 1000aa0:	89 04 24             	mov    %eax,(%esp)
 1000aa3:	e8 e8 02 00 00       	call   1000d90 <thr_fork>
 1000aa8:	89 45 e0             	mov    %eax,-0x20(%ebp)
 1000aab:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
 1000aaf:	79 1b                	jns    1000acc <thr_create+0xf5>
 1000ab1:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000ab4:	89 04 24             	mov    %eax,(%esp)
 1000ab7:	e8 5a fc ff ff       	call   1000716 <free>
 1000abc:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1000abf:	89 04 24             	mov    %eax,(%esp)
 1000ac2:	e8 4f fc ff ff       	call   1000716 <free>
 1000ac7:	8b 45 e0             	mov    -0x20(%ebp),%eax
 1000aca:	eb 3a                	jmp    1000b06 <thr_create+0x12f>
 1000acc:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
 1000ad0:	75 07                	jne    1000ad9 <thr_create+0x102>
 1000ad2:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 1000ad7:	eb 2d                	jmp    1000b06 <thr_create+0x12f>
 1000ad9:	8b 45 ec             	mov    -0x14(%ebp),%eax
 1000adc:	8b 55 e0             	mov    -0x20(%ebp),%edx
 1000adf:	89 10                	mov    %edx,(%eax)
 1000ae1:	8b 45 ec             	mov    -0x14(%ebp),%eax
 1000ae4:	8b 55 f4             	mov    -0xc(%ebp),%edx
 1000ae7:	89 50 04             	mov    %edx,0x4(%eax)
 1000aea:	8b 45 ec             	mov    -0x14(%ebp),%eax
 1000aed:	89 04 24             	mov    %eax,(%esp)
 1000af0:	e8 bf fd ff ff       	call   10008b4 <thr_init_thread>
 1000af5:	8b 45 ec             	mov    -0x14(%ebp),%eax
 1000af8:	83 c0 0d             	add    $0xd,%eax
 1000afb:	89 04 24             	mov    %eax,(%esp)
 1000afe:	e8 3e fd ff ff       	call   1000841 <spin_unlock>
 1000b03:	8b 45 e0             	mov    -0x20(%ebp),%eax
 1000b06:	c9                   	leave  
 1000b07:	c3                   	ret    

01000b08 <thr_run_child_func>:
 1000b08:	55                   	push   %ebp
 1000b09:	89 e5                	mov    %esp,%ebp
 1000b0b:	83 ec 28             	sub    $0x28,%esp
 1000b0e:	8b 45 10             	mov    0x10(%ebp),%eax
 1000b11:	83 c0 0d             	add    $0xd,%eax
 1000b14:	89 04 24             	mov    %eax,(%esp)
 1000b17:	e8 c6 fc ff ff       	call   10007e2 <spin_lock>
 1000b1c:	8b 45 0c             	mov    0xc(%ebp),%eax
 1000b1f:	89 04 24             	mov    %eax,(%esp)
 1000b22:	8b 45 08             	mov    0x8(%ebp),%eax
 1000b25:	ff d0                	call   *%eax
 1000b27:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1000b2a:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000b2d:	89 04 24             	mov    %eax,(%esp)
 1000b30:	e8 35 01 00 00       	call   1000c6a <thr_exit>
 1000b35:	c9                   	leave  
 1000b36:	c3                   	ret    

01000b37 <thr_join>:
 1000b37:	55                   	push   %ebp
 1000b38:	89 e5                	mov    %esp,%ebp
 1000b3a:	83 ec 38             	sub    $0x38,%esp
 1000b3d:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 1000b44:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 1000b4b:	c7 04 24 34 49 00 01 	movl   $0x1004934,(%esp)
 1000b52:	e8 8b fc ff ff       	call   10007e2 <spin_lock>
 1000b57:	a1 00 49 00 01       	mov    0x1004900,%eax
 1000b5c:	89 45 ec             	mov    %eax,-0x14(%ebp)
 1000b5f:	8b 45 ec             	mov    -0x14(%ebp),%eax
 1000b62:	83 e8 1c             	sub    $0x1c,%eax
 1000b65:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1000b68:	eb 1c                	jmp    1000b86 <thr_join+0x4f>
 1000b6a:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000b6d:	8b 00                	mov    (%eax),%eax
 1000b6f:	3b 45 08             	cmp    0x8(%ebp),%eax
 1000b72:	74 21                	je     1000b95 <thr_join+0x5e>
 1000b74:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000b77:	8b 40 1c             	mov    0x1c(%eax),%eax
 1000b7a:	89 45 e8             	mov    %eax,-0x18(%ebp)
 1000b7d:	8b 45 e8             	mov    -0x18(%ebp),%eax
 1000b80:	83 e8 1c             	sub    $0x1c,%eax
 1000b83:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1000b86:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000b89:	83 c0 1c             	add    $0x1c,%eax
 1000b8c:	3d 00 49 00 01       	cmp    $0x1004900,%eax
 1000b91:	75 d7                	jne    1000b6a <thr_join+0x33>
 1000b93:	eb 01                	jmp    1000b96 <thr_join+0x5f>
 1000b95:	90                   	nop
 1000b96:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 1000b9a:	74 0a                	je     1000ba6 <thr_join+0x6f>
 1000b9c:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000b9f:	8b 00                	mov    (%eax),%eax
 1000ba1:	3b 45 08             	cmp    0x8(%ebp),%eax
 1000ba4:	74 16                	je     1000bbc <thr_join+0x85>
 1000ba6:	c7 04 24 34 49 00 01 	movl   $0x1004934,(%esp)
 1000bad:	e8 8f fc ff ff       	call   1000841 <spin_unlock>
 1000bb2:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 1000bb7:	e9 ac 00 00 00       	jmp    1000c68 <thr_join+0x131>
 1000bbc:	c7 04 24 34 49 00 01 	movl   $0x1004934,(%esp)
 1000bc3:	e8 79 fc ff ff       	call   1000841 <spin_unlock>
 1000bc8:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000bcb:	83 c0 0f             	add    $0xf,%eax
 1000bce:	89 04 24             	mov    %eax,(%esp)
 1000bd1:	e8 43 fc ff ff       	call   1000819 <spin_try_lock>
 1000bd6:	85 c0                	test   %eax,%eax
 1000bd8:	75 0a                	jne    1000be4 <thr_join+0xad>
 1000bda:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 1000bdf:	e9 84 00 00 00       	jmp    1000c68 <thr_join+0x131>
 1000be4:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000be7:	83 c0 10             	add    $0x10,%eax
 1000bea:	89 04 24             	mov    %eax,(%esp)
 1000bed:	e8 5b 01 00 00       	call   1000d4d <mutex_lock>
 1000bf2:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000bf5:	83 c0 0e             	add    $0xe,%eax
 1000bf8:	89 04 24             	mov    %eax,(%esp)
 1000bfb:	e8 e2 fb ff ff       	call   10007e2 <spin_lock>
 1000c00:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000c03:	8b 40 04             	mov    0x4(%eax),%eax
 1000c06:	85 c0                	test   %eax,%eax
 1000c08:	74 0e                	je     1000c18 <thr_join+0xe1>
 1000c0a:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000c0d:	8b 40 04             	mov    0x4(%eax),%eax
 1000c10:	89 04 24             	mov    %eax,(%esp)
 1000c13:	e8 fe fa ff ff       	call   1000716 <free>
 1000c18:	c7 04 24 34 49 00 01 	movl   $0x1004934,(%esp)
 1000c1f:	e8 be fb ff ff       	call   10007e2 <spin_lock>
 1000c24:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000c27:	83 c0 1c             	add    $0x1c,%eax
 1000c2a:	89 04 24             	mov    %eax,(%esp)
 1000c2d:	e8 72 fb ff ff       	call   10007a4 <list_del>
 1000c32:	c7 04 24 34 49 00 01 	movl   $0x1004934,(%esp)
 1000c39:	e8 03 fc ff ff       	call   1000841 <spin_unlock>
 1000c3e:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000c41:	8b 50 08             	mov    0x8(%eax),%edx
 1000c44:	8b 45 0c             	mov    0xc(%ebp),%eax
 1000c47:	89 10                	mov    %edx,(%eax)
 1000c49:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000c4c:	89 45 e4             	mov    %eax,-0x1c(%ebp)
 1000c4f:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 1000c52:	83 e8 08             	sub    $0x8,%eax
 1000c55:	89 45 f0             	mov    %eax,-0x10(%ebp)
 1000c58:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1000c5b:	89 04 24             	mov    %eax,(%esp)
 1000c5e:	e8 b3 fa ff ff       	call   1000716 <free>
 1000c63:	b8 00 00 00 00       	mov    $0x0,%eax
 1000c68:	c9                   	leave  
 1000c69:	c3                   	ret    

01000c6a <thr_exit>:
 1000c6a:	55                   	push   %ebp
 1000c6b:	89 e5                	mov    %esp,%ebp
 1000c6d:	83 ec 28             	sub    $0x28,%esp
 1000c70:	e8 29 fc ff ff       	call   100089e <current_thread>
 1000c75:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1000c78:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000c7b:	8b 55 08             	mov    0x8(%ebp),%edx
 1000c7e:	89 50 08             	mov    %edx,0x8(%eax)
 1000c81:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000c84:	8b 40 04             	mov    0x4(%eax),%eax
 1000c87:	85 c0                	test   %eax,%eax
 1000c89:	74 07                	je     1000c92 <thr_exit+0x28>
 1000c8b:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000c8e:	c6 40 0c ff          	movb   $0xff,0xc(%eax)
 1000c92:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000c95:	83 c0 10             	add    $0x10,%eax
 1000c98:	89 04 24             	mov    %eax,(%esp)
 1000c9b:	e8 df 00 00 00       	call   1000d7f <mutex_unlock>
 1000ca0:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000ca3:	c6 40 0e 00          	movb   $0x0,0xe(%eax)
 1000ca7:	cd 60                	int    $0x60
 1000ca9:	c9                   	leave  
 1000caa:	c3                   	ret    

01000cab <thr_getid>:
 1000cab:	55                   	push   %ebp
 1000cac:	89 e5                	mov    %esp,%ebp
 1000cae:	83 ec 10             	sub    $0x10,%esp
 1000cb1:	e8 e8 fb ff ff       	call   100089e <current_thread>
 1000cb6:	89 45 fc             	mov    %eax,-0x4(%ebp)
 1000cb9:	8b 45 fc             	mov    -0x4(%ebp),%eax
 1000cbc:	8b 00                	mov    (%eax),%eax
 1000cbe:	c9                   	leave  
 1000cbf:	c3                   	ret    

01000cc0 <thr_yield>:
 1000cc0:	55                   	push   %ebp
 1000cc1:	89 e5                	mov    %esp,%ebp
 1000cc3:	83 ec 18             	sub    $0x18,%esp
 1000cc6:	8b 45 08             	mov    0x8(%ebp),%eax
 1000cc9:	89 04 24             	mov    %eax,(%esp)
 1000ccc:	e8 8f 1a 00 00       	call   1002760 <yield>
 1000cd1:	c9                   	leave  
 1000cd2:	c3                   	ret    
 1000cd3:	90                   	nop

01000cd4 <list_init_head>:
 1000cd4:	55                   	push   %ebp
 1000cd5:	89 e5                	mov    %esp,%ebp
 1000cd7:	8b 45 08             	mov    0x8(%ebp),%eax
 1000cda:	8b 55 08             	mov    0x8(%ebp),%edx
 1000cdd:	89 10                	mov    %edx,(%eax)
 1000cdf:	8b 45 08             	mov    0x8(%ebp),%eax
 1000ce2:	8b 55 08             	mov    0x8(%ebp),%edx
 1000ce5:	89 50 04             	mov    %edx,0x4(%eax)
 1000ce8:	5d                   	pop    %ebp
 1000ce9:	c3                   	ret    

01000cea <xchg>:
 1000cea:	55                   	push   %ebp
 1000ceb:	89 e5                	mov    %esp,%ebp
 1000ced:	83 ec 04             	sub    $0x4,%esp
 1000cf0:	8b 45 0c             	mov    0xc(%ebp),%eax
 1000cf3:	88 45 fc             	mov    %al,-0x4(%ebp)
 1000cf6:	8b 55 08             	mov    0x8(%ebp),%edx
 1000cf9:	0f b6 45 fc          	movzbl -0x4(%ebp),%eax
 1000cfd:	f0 86 02             	lock xchg %al,(%edx)
 1000d00:	88 45 fc             	mov    %al,-0x4(%ebp)
 1000d03:	0f b6 45 fc          	movzbl -0x4(%ebp),%eax
 1000d07:	c9                   	leave  
 1000d08:	c3                   	ret    

01000d09 <spin_init>:
 1000d09:	55                   	push   %ebp
 1000d0a:	89 e5                	mov    %esp,%ebp
 1000d0c:	8b 45 08             	mov    0x8(%ebp),%eax
 1000d0f:	c6 00 00             	movb   $0x0,(%eax)
 1000d12:	5d                   	pop    %ebp
 1000d13:	c3                   	ret    

01000d14 <mutex_init>:
 1000d14:	55                   	push   %ebp
 1000d15:	89 e5                	mov    %esp,%ebp
 1000d17:	83 ec 04             	sub    $0x4,%esp
 1000d1a:	8b 45 08             	mov    0x8(%ebp),%eax
 1000d1d:	c6 00 00             	movb   $0x0,(%eax)
 1000d20:	8b 45 08             	mov    0x8(%ebp),%eax
 1000d23:	83 c0 01             	add    $0x1,%eax
 1000d26:	89 04 24             	mov    %eax,(%esp)
 1000d29:	e8 db ff ff ff       	call   1000d09 <spin_init>
 1000d2e:	8b 45 08             	mov    0x8(%ebp),%eax
 1000d31:	83 c0 04             	add    $0x4,%eax
 1000d34:	89 04 24             	mov    %eax,(%esp)
 1000d37:	e8 98 ff ff ff       	call   1000cd4 <list_init_head>
 1000d3c:	b8 00 00 00 00       	mov    $0x0,%eax
 1000d41:	c9                   	leave  
 1000d42:	c3                   	ret    

01000d43 <mutex_destroy>:
 1000d43:	55                   	push   %ebp
 1000d44:	89 e5                	mov    %esp,%ebp
 1000d46:	b8 00 00 00 00       	mov    $0x0,%eax
 1000d4b:	5d                   	pop    %ebp
 1000d4c:	c3                   	ret    

01000d4d <mutex_lock>:
 1000d4d:	55                   	push   %ebp
 1000d4e:	89 e5                	mov    %esp,%ebp
 1000d50:	83 ec 18             	sub    $0x18,%esp
 1000d53:	eb 0c                	jmp    1000d61 <mutex_lock+0x14>
 1000d55:	c7 04 24 ff ff ff ff 	movl   $0xffffffff,(%esp)
 1000d5c:	e8 5f ff ff ff       	call   1000cc0 <thr_yield>
 1000d61:	8b 45 08             	mov    0x8(%ebp),%eax
 1000d64:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
 1000d6b:	00 
 1000d6c:	89 04 24             	mov    %eax,(%esp)
 1000d6f:	e8 76 ff ff ff       	call   1000cea <xchg>
 1000d74:	3c 01                	cmp    $0x1,%al
 1000d76:	74 dd                	je     1000d55 <mutex_lock+0x8>
 1000d78:	b8 00 00 00 00       	mov    $0x0,%eax
 1000d7d:	c9                   	leave  
 1000d7e:	c3                   	ret    

01000d7f <mutex_unlock>:
 1000d7f:	55                   	push   %ebp
 1000d80:	89 e5                	mov    %esp,%ebp
 1000d82:	8b 45 08             	mov    0x8(%ebp),%eax
 1000d85:	c6 00 00             	movb   $0x0,(%eax)
 1000d88:	b8 00 00 00 00       	mov    $0x0,%eax
 1000d8d:	5d                   	pop    %ebp
 1000d8e:	c3                   	ret    
 1000d8f:	90                   	nop

01000d90 <thr_fork>:
 1000d90:	56                   	push   %esi
 1000d91:	57                   	push   %edi
 1000d92:	53                   	push   %ebx
 1000d93:	bb 27 09 48 88       	mov    $0x88480927,%ebx
 1000d98:	8b 44 24 10          	mov    0x10(%esp),%eax
 1000d9c:	66 87 db             	xchg   %bx,%bx
 1000d9f:	5b                   	pop    %ebx
 1000da0:	8b 74 24 0c          	mov    0xc(%esp),%esi
 1000da4:	8b 7c 24 10          	mov    0x10(%esp),%edi
 1000da8:	cd 52                	int    $0x52
 1000daa:	89 c2                	mov    %eax,%edx
 1000dac:	85 d2                	test   %edx,%edx
 1000dae:	78 0b                	js     1000dbb <end>
 1000db0:	75 09                	jne    1000dbb <end>
 1000db2:	89 f4                	mov    %esi,%esp
 1000db4:	89 fd                	mov    %edi,%ebp
 1000db6:	e8 4d fd ff ff       	call   1000b08 <thr_run_child_func>

01000dbb <end>:
 1000dbb:	53                   	push   %ebx
 1000dbc:	bb 28 09 48 88       	mov    $0x88480928,%ebx
 1000dc1:	66 87 db             	xchg   %bx,%bx
 1000dc4:	5b                   	pop    %ebx
 1000dc5:	5f                   	pop    %edi
 1000dc6:	5e                   	pop    %esi
 1000dc7:	c3                   	ret    

01000dc8 <__list_add>:
 1000dc8:	55                   	push   %ebp
 1000dc9:	89 e5                	mov    %esp,%ebp
 1000dcb:	8b 45 10             	mov    0x10(%ebp),%eax
 1000dce:	8b 55 08             	mov    0x8(%ebp),%edx
 1000dd1:	89 50 04             	mov    %edx,0x4(%eax)
 1000dd4:	8b 45 08             	mov    0x8(%ebp),%eax
 1000dd7:	8b 55 10             	mov    0x10(%ebp),%edx
 1000dda:	89 10                	mov    %edx,(%eax)
 1000ddc:	8b 45 0c             	mov    0xc(%ebp),%eax
 1000ddf:	8b 55 08             	mov    0x8(%ebp),%edx
 1000de2:	89 10                	mov    %edx,(%eax)
 1000de4:	8b 45 08             	mov    0x8(%ebp),%eax
 1000de7:	8b 55 0c             	mov    0xc(%ebp),%edx
 1000dea:	89 50 04             	mov    %edx,0x4(%eax)
 1000ded:	5d                   	pop    %ebp
 1000dee:	c3                   	ret    

01000def <__list_del>:
 1000def:	55                   	push   %ebp
 1000df0:	89 e5                	mov    %esp,%ebp
 1000df2:	8b 45 08             	mov    0x8(%ebp),%eax
 1000df5:	8b 55 0c             	mov    0xc(%ebp),%edx
 1000df8:	89 10                	mov    %edx,(%eax)
 1000dfa:	8b 45 0c             	mov    0xc(%ebp),%eax
 1000dfd:	8b 55 08             	mov    0x8(%ebp),%edx
 1000e00:	89 50 04             	mov    %edx,0x4(%eax)
 1000e03:	5d                   	pop    %ebp
 1000e04:	c3                   	ret    

01000e05 <list_init_head>:
 1000e05:	55                   	push   %ebp
 1000e06:	89 e5                	mov    %esp,%ebp
 1000e08:	8b 45 08             	mov    0x8(%ebp),%eax
 1000e0b:	8b 55 08             	mov    0x8(%ebp),%edx
 1000e0e:	89 10                	mov    %edx,(%eax)
 1000e10:	8b 45 08             	mov    0x8(%ebp),%eax
 1000e13:	8b 55 08             	mov    0x8(%ebp),%edx
 1000e16:	89 50 04             	mov    %edx,0x4(%eax)
 1000e19:	5d                   	pop    %ebp
 1000e1a:	c3                   	ret    

01000e1b <list_add_tail>:
 1000e1b:	55                   	push   %ebp
 1000e1c:	89 e5                	mov    %esp,%ebp
 1000e1e:	83 ec 0c             	sub    $0xc,%esp
 1000e21:	8b 45 0c             	mov    0xc(%ebp),%eax
 1000e24:	8b 40 04             	mov    0x4(%eax),%eax
 1000e27:	8b 55 0c             	mov    0xc(%ebp),%edx
 1000e2a:	89 54 24 08          	mov    %edx,0x8(%esp)
 1000e2e:	89 44 24 04          	mov    %eax,0x4(%esp)
 1000e32:	8b 45 08             	mov    0x8(%ebp),%eax
 1000e35:	89 04 24             	mov    %eax,(%esp)
 1000e38:	e8 8b ff ff ff       	call   1000dc8 <__list_add>
 1000e3d:	c9                   	leave  
 1000e3e:	c3                   	ret    

01000e3f <list_del>:
 1000e3f:	55                   	push   %ebp
 1000e40:	89 e5                	mov    %esp,%ebp
 1000e42:	83 ec 08             	sub    $0x8,%esp
 1000e45:	8b 45 08             	mov    0x8(%ebp),%eax
 1000e48:	8b 10                	mov    (%eax),%edx
 1000e4a:	8b 45 08             	mov    0x8(%ebp),%eax
 1000e4d:	8b 40 04             	mov    0x4(%eax),%eax
 1000e50:	89 54 24 04          	mov    %edx,0x4(%esp)
 1000e54:	89 04 24             	mov    %eax,(%esp)
 1000e57:	e8 93 ff ff ff       	call   1000def <__list_del>
 1000e5c:	c9                   	leave  
 1000e5d:	c3                   	ret    

01000e5e <list_is_empty>:
 1000e5e:	55                   	push   %ebp
 1000e5f:	89 e5                	mov    %esp,%ebp
 1000e61:	8b 45 08             	mov    0x8(%ebp),%eax
 1000e64:	8b 00                	mov    (%eax),%eax
 1000e66:	3b 45 08             	cmp    0x8(%ebp),%eax
 1000e69:	0f 94 c0             	sete   %al
 1000e6c:	0f b6 c0             	movzbl %al,%eax
 1000e6f:	5d                   	pop    %ebp
 1000e70:	c3                   	ret    

01000e71 <xchg>:
 1000e71:	55                   	push   %ebp
 1000e72:	89 e5                	mov    %esp,%ebp
 1000e74:	83 ec 04             	sub    $0x4,%esp
 1000e77:	8b 45 0c             	mov    0xc(%ebp),%eax
 1000e7a:	88 45 fc             	mov    %al,-0x4(%ebp)
 1000e7d:	8b 55 08             	mov    0x8(%ebp),%edx
 1000e80:	0f b6 45 fc          	movzbl -0x4(%ebp),%eax
 1000e84:	f0 86 02             	lock xchg %al,(%edx)
 1000e87:	88 45 fc             	mov    %al,-0x4(%ebp)
 1000e8a:	0f b6 45 fc          	movzbl -0x4(%ebp),%eax
 1000e8e:	c9                   	leave  
 1000e8f:	c3                   	ret    

01000e90 <spin_init>:
 1000e90:	55                   	push   %ebp
 1000e91:	89 e5                	mov    %esp,%ebp
 1000e93:	8b 45 08             	mov    0x8(%ebp),%eax
 1000e96:	c6 00 00             	movb   $0x0,(%eax)
 1000e99:	5d                   	pop    %ebp
 1000e9a:	c3                   	ret    

01000e9b <spin_lock>:
 1000e9b:	55                   	push   %ebp
 1000e9c:	89 e5                	mov    %esp,%ebp
 1000e9e:	83 ec 28             	sub    $0x28,%esp
 1000ea1:	c6 45 f7 01          	movb   $0x1,-0x9(%ebp)
 1000ea5:	0f be 45 f7          	movsbl -0x9(%ebp),%eax
 1000ea9:	89 44 24 04          	mov    %eax,0x4(%esp)
 1000ead:	8b 45 08             	mov    0x8(%ebp),%eax
 1000eb0:	89 04 24             	mov    %eax,(%esp)
 1000eb3:	e8 b9 ff ff ff       	call   1000e71 <xchg>
 1000eb8:	88 45 f7             	mov    %al,-0x9(%ebp)
 1000ebb:	80 7d f7 01          	cmpb   $0x1,-0x9(%ebp)
 1000ebf:	75 0e                	jne    1000ecf <spin_lock+0x34>
 1000ec1:	c7 04 24 ff ff ff ff 	movl   $0xffffffff,(%esp)
 1000ec8:	e8 93 18 00 00       	call   1002760 <yield>
 1000ecd:	eb d6                	jmp    1000ea5 <spin_lock+0xa>
 1000ecf:	90                   	nop
 1000ed0:	c9                   	leave  
 1000ed1:	c3                   	ret    

01000ed2 <spin_try_lock>:
 1000ed2:	55                   	push   %ebp
 1000ed3:	89 e5                	mov    %esp,%ebp
 1000ed5:	83 ec 18             	sub    $0x18,%esp
 1000ed8:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
 1000edf:	00 
 1000ee0:	8b 45 08             	mov    0x8(%ebp),%eax
 1000ee3:	89 04 24             	mov    %eax,(%esp)
 1000ee6:	e8 86 ff ff ff       	call   1000e71 <xchg>
 1000eeb:	88 45 ff             	mov    %al,-0x1(%ebp)
 1000eee:	80 7d ff 00          	cmpb   $0x0,-0x1(%ebp)
 1000ef2:	0f 94 c0             	sete   %al
 1000ef5:	0f b6 c0             	movzbl %al,%eax
 1000ef8:	c9                   	leave  
 1000ef9:	c3                   	ret    

01000efa <spin_unlock>:
 1000efa:	55                   	push   %ebp
 1000efb:	89 e5                	mov    %esp,%ebp
 1000efd:	8b 45 08             	mov    0x8(%ebp),%eax
 1000f00:	c6 00 00             	movb   $0x0,(%eax)
 1000f03:	5d                   	pop    %ebp
 1000f04:	c3                   	ret    

01000f05 <cond_init>:
 1000f05:	55                   	push   %ebp
 1000f06:	89 e5                	mov    %esp,%ebp
 1000f08:	83 ec 04             	sub    $0x4,%esp
 1000f0b:	8b 45 08             	mov    0x8(%ebp),%eax
 1000f0e:	89 04 24             	mov    %eax,(%esp)
 1000f11:	e8 7a ff ff ff       	call   1000e90 <spin_init>
 1000f16:	8b 45 08             	mov    0x8(%ebp),%eax
 1000f19:	83 c0 04             	add    $0x4,%eax
 1000f1c:	89 04 24             	mov    %eax,(%esp)
 1000f1f:	e8 e1 fe ff ff       	call   1000e05 <list_init_head>
 1000f24:	b8 00 00 00 00       	mov    $0x0,%eax
 1000f29:	c9                   	leave  
 1000f2a:	c3                   	ret    

01000f2b <cond_destroy>:
 1000f2b:	55                   	push   %ebp
 1000f2c:	89 e5                	mov    %esp,%ebp
 1000f2e:	b8 00 00 00 00       	mov    $0x0,%eax
 1000f33:	5d                   	pop    %ebp
 1000f34:	c3                   	ret    

01000f35 <cond_wait>:
 1000f35:	55                   	push   %ebp
 1000f36:	89 e5                	mov    %esp,%ebp
 1000f38:	83 ec 28             	sub    $0x28,%esp
 1000f3b:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
 1000f42:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
 1000f49:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 1000f50:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 1000f57:	c6 45 f4 01          	movb   $0x1,-0xc(%ebp)
 1000f5b:	8b 45 08             	mov    0x8(%ebp),%eax
 1000f5e:	89 04 24             	mov    %eax,(%esp)
 1000f61:	e8 35 ff ff ff       	call   1000e9b <spin_lock>
 1000f66:	8b 45 08             	mov    0x8(%ebp),%eax
 1000f69:	83 c0 04             	add    $0x4,%eax
 1000f6c:	89 44 24 04          	mov    %eax,0x4(%esp)
 1000f70:	8d 45 e8             	lea    -0x18(%ebp),%eax
 1000f73:	89 04 24             	mov    %eax,(%esp)
 1000f76:	e8 a0 fe ff ff       	call   1000e1b <list_add_tail>
 1000f7b:	8b 45 08             	mov    0x8(%ebp),%eax
 1000f7e:	89 04 24             	mov    %eax,(%esp)
 1000f81:	e8 74 ff ff ff       	call   1000efa <spin_unlock>
 1000f86:	8b 45 0c             	mov    0xc(%ebp),%eax
 1000f89:	89 04 24             	mov    %eax,(%esp)
 1000f8c:	e8 ee fd ff ff       	call   1000d7f <mutex_unlock>
 1000f91:	eb 0c                	jmp    1000f9f <cond_wait+0x6a>
 1000f93:	c7 04 24 ff ff ff ff 	movl   $0xffffffff,(%esp)
 1000f9a:	e8 21 fd ff ff       	call   1000cc0 <thr_yield>
 1000f9f:	8d 45 e8             	lea    -0x18(%ebp),%eax
 1000fa2:	83 c0 0c             	add    $0xc,%eax
 1000fa5:	89 04 24             	mov    %eax,(%esp)
 1000fa8:	e8 25 ff ff ff       	call   1000ed2 <spin_try_lock>
 1000fad:	85 c0                	test   %eax,%eax
 1000faf:	74 e2                	je     1000f93 <cond_wait+0x5e>
 1000fb1:	8b 45 0c             	mov    0xc(%ebp),%eax
 1000fb4:	89 04 24             	mov    %eax,(%esp)
 1000fb7:	e8 91 fd ff ff       	call   1000d4d <mutex_lock>
 1000fbc:	b8 00 00 00 00       	mov    $0x0,%eax
 1000fc1:	c9                   	leave  
 1000fc2:	c3                   	ret    

01000fc3 <cond_signal>:
 1000fc3:	55                   	push   %ebp
 1000fc4:	89 e5                	mov    %esp,%ebp
 1000fc6:	83 ec 28             	sub    $0x28,%esp
 1000fc9:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 1000fd0:	8b 45 08             	mov    0x8(%ebp),%eax
 1000fd3:	89 04 24             	mov    %eax,(%esp)
 1000fd6:	e8 c0 fe ff ff       	call   1000e9b <spin_lock>
 1000fdb:	8b 45 08             	mov    0x8(%ebp),%eax
 1000fde:	83 c0 04             	add    $0x4,%eax
 1000fe1:	89 04 24             	mov    %eax,(%esp)
 1000fe4:	e8 75 fe ff ff       	call   1000e5e <list_is_empty>
 1000fe9:	85 c0                	test   %eax,%eax
 1000feb:	75 2a                	jne    1001017 <cond_signal+0x54>
 1000fed:	8b 45 08             	mov    0x8(%ebp),%eax
 1000ff0:	8b 40 04             	mov    0x4(%eax),%eax
 1000ff3:	89 45 f0             	mov    %eax,-0x10(%ebp)
 1000ff6:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1000ff9:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1000ffc:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1000fff:	89 04 24             	mov    %eax,(%esp)
 1001002:	e8 38 fe ff ff       	call   1000e3f <list_del>
 1001007:	8b 45 f4             	mov    -0xc(%ebp),%eax
 100100a:	83 c0 0c             	add    $0xc,%eax
 100100d:	89 04 24             	mov    %eax,(%esp)
 1001010:	e8 e5 fe ff ff       	call   1000efa <spin_unlock>
 1001015:	eb 01                	jmp    1001018 <cond_signal+0x55>
 1001017:	90                   	nop
 1001018:	8b 45 08             	mov    0x8(%ebp),%eax
 100101b:	89 04 24             	mov    %eax,(%esp)
 100101e:	e8 d7 fe ff ff       	call   1000efa <spin_unlock>
 1001023:	b8 00 00 00 00       	mov    $0x0,%eax
 1001028:	c9                   	leave  
 1001029:	c3                   	ret    

0100102a <cond_broadcast>:
 100102a:	55                   	push   %ebp
 100102b:	89 e5                	mov    %esp,%ebp
 100102d:	83 ec 28             	sub    $0x28,%esp
 1001030:	8b 45 08             	mov    0x8(%ebp),%eax
 1001033:	89 04 24             	mov    %eax,(%esp)
 1001036:	e8 60 fe ff ff       	call   1000e9b <spin_lock>
 100103b:	eb 28                	jmp    1001065 <cond_broadcast+0x3b>
 100103d:	8b 45 08             	mov    0x8(%ebp),%eax
 1001040:	8b 40 04             	mov    0x4(%eax),%eax
 1001043:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1001046:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1001049:	89 45 f0             	mov    %eax,-0x10(%ebp)
 100104c:	8b 45 f0             	mov    -0x10(%ebp),%eax
 100104f:	89 04 24             	mov    %eax,(%esp)
 1001052:	e8 e8 fd ff ff       	call   1000e3f <list_del>
 1001057:	8b 45 f0             	mov    -0x10(%ebp),%eax
 100105a:	83 c0 0c             	add    $0xc,%eax
 100105d:	89 04 24             	mov    %eax,(%esp)
 1001060:	e8 95 fe ff ff       	call   1000efa <spin_unlock>
 1001065:	8b 45 08             	mov    0x8(%ebp),%eax
 1001068:	83 c0 04             	add    $0x4,%eax
 100106b:	89 04 24             	mov    %eax,(%esp)
 100106e:	e8 eb fd ff ff       	call   1000e5e <list_is_empty>
 1001073:	85 c0                	test   %eax,%eax
 1001075:	74 c6                	je     100103d <cond_broadcast+0x13>
 1001077:	8b 45 08             	mov    0x8(%ebp),%eax
 100107a:	89 04 24             	mov    %eax,(%esp)
 100107d:	e8 78 fe ff ff       	call   1000efa <spin_unlock>
 1001082:	b8 00 00 00 00       	mov    $0x0,%eax
 1001087:	c9                   	leave  
 1001088:	c3                   	ret    
 1001089:	90                   	nop
 100108a:	90                   	nop
 100108b:	90                   	nop

0100108c <_malloc>:
 100108c:	55                   	push   %ebp
 100108d:	89 e5                	mov    %esp,%ebp
 100108f:	83 ec 18             	sub    $0x18,%esp
 1001092:	a1 38 49 00 01       	mov    0x1004938,%eax
 1001097:	85 c0                	test   %eax,%eax
 1001099:	75 1a                	jne    10010b5 <_malloc+0x29>
 100109b:	e8 df 00 00 00       	call   100117f <mm_init>
 10010a0:	85 c0                	test   %eax,%eax
 10010a2:	79 07                	jns    10010ab <_malloc+0x1f>
 10010a4:	b8 00 00 00 00       	mov    $0x0,%eax
 10010a9:	eb 15                	jmp    10010c0 <_malloc+0x34>
 10010ab:	c7 05 38 49 00 01 01 	movl   $0x1,0x1004938
 10010b2:	00 00 00 
 10010b5:	8b 45 08             	mov    0x8(%ebp),%eax
 10010b8:	89 04 24             	mov    %eax,(%esp)
 10010bb:	e8 52 01 00 00       	call   1001212 <mm_malloc>
 10010c0:	c9                   	leave  
 10010c1:	c3                   	ret    

010010c2 <_calloc>:
 10010c2:	55                   	push   %ebp
 10010c3:	89 e5                	mov    %esp,%ebp
 10010c5:	83 ec 28             	sub    $0x28,%esp
 10010c8:	a1 38 49 00 01       	mov    0x1004938,%eax
 10010cd:	85 c0                	test   %eax,%eax
 10010cf:	75 1a                	jne    10010eb <_calloc+0x29>
 10010d1:	e8 a9 00 00 00       	call   100117f <mm_init>
 10010d6:	85 c0                	test   %eax,%eax
 10010d8:	79 07                	jns    10010e1 <_calloc+0x1f>
 10010da:	b8 00 00 00 00       	mov    $0x0,%eax
 10010df:	eb 42                	jmp    1001123 <_calloc+0x61>
 10010e1:	c7 05 38 49 00 01 01 	movl   $0x1,0x1004938
 10010e8:	00 00 00 
 10010eb:	8b 45 08             	mov    0x8(%ebp),%eax
 10010ee:	0f af 45 0c          	imul   0xc(%ebp),%eax
 10010f2:	89 04 24             	mov    %eax,(%esp)
 10010f5:	e8 18 01 00 00       	call   1001212 <mm_malloc>
 10010fa:	89 45 f4             	mov    %eax,-0xc(%ebp)
 10010fd:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 1001101:	75 07                	jne    100110a <_calloc+0x48>
 1001103:	b8 00 00 00 00       	mov    $0x0,%eax
 1001108:	eb 19                	jmp    1001123 <_calloc+0x61>
 100110a:	8b 45 08             	mov    0x8(%ebp),%eax
 100110d:	0f af 45 0c          	imul   0xc(%ebp),%eax
 1001111:	89 44 24 04          	mov    %eax,0x4(%esp)
 1001115:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1001118:	89 04 24             	mov    %eax,(%esp)
 100111b:	e8 c0 13 00 00       	call   10024e0 <bzero>
 1001120:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1001123:	c9                   	leave  
 1001124:	c3                   	ret    

01001125 <_realloc>:
 1001125:	55                   	push   %ebp
 1001126:	89 e5                	mov    %esp,%ebp
 1001128:	83 ec 18             	sub    $0x18,%esp
 100112b:	a1 38 49 00 01       	mov    0x1004938,%eax
 1001130:	85 c0                	test   %eax,%eax
 1001132:	75 07                	jne    100113b <_realloc+0x16>
 1001134:	b8 00 00 00 00       	mov    $0x0,%eax
 1001139:	eb 12                	jmp    100114d <_realloc+0x28>
 100113b:	8b 45 0c             	mov    0xc(%ebp),%eax
 100113e:	89 44 24 04          	mov    %eax,0x4(%esp)
 1001142:	8b 45 08             	mov    0x8(%ebp),%eax
 1001145:	89 04 24             	mov    %eax,(%esp)
 1001148:	e8 c4 01 00 00       	call   1001311 <mm_realloc>
 100114d:	c9                   	leave  
 100114e:	c3                   	ret    

0100114f <_free>:
 100114f:	55                   	push   %ebp
 1001150:	89 e5                	mov    %esp,%ebp
 1001152:	83 ec 18             	sub    $0x18,%esp
 1001155:	a1 38 49 00 01       	mov    0x1004938,%eax
 100115a:	85 c0                	test   %eax,%eax
 100115c:	74 0d                	je     100116b <_free+0x1c>
 100115e:	8b 45 08             	mov    0x8(%ebp),%eax
 1001161:	89 04 24             	mov    %eax,(%esp)
 1001164:	e8 60 01 00 00       	call   10012c9 <mm_free>
 1001169:	eb 01                	jmp    100116c <_free+0x1d>
 100116b:	90                   	nop
 100116c:	c9                   	leave  
 100116d:	c3                   	ret    
 100116e:	90                   	nop
 100116f:	90                   	nop

01001170 <min>:
 1001170:	55                   	push   %ebp
 1001171:	89 e5                	mov    %esp,%ebp
 1001173:	8b 45 08             	mov    0x8(%ebp),%eax
 1001176:	39 45 0c             	cmp    %eax,0xc(%ebp)
 1001179:	0f 46 45 0c          	cmovbe 0xc(%ebp),%eax
 100117d:	5d                   	pop    %ebp
 100117e:	c3                   	ret    

0100117f <mm_init>:
 100117f:	55                   	push   %ebp
 1001180:	89 e5                	mov    %esp,%ebp
 1001182:	83 ec 18             	sub    $0x18,%esp
 1001185:	c7 04 24 ff ff ff ff 	movl   $0xffffffff,(%esp)
 100118c:	e8 6b 07 00 00       	call   10018fc <mem_init>
 1001191:	c7 04 24 10 00 00 00 	movl   $0x10,(%esp)
 1001198:	e8 c1 07 00 00       	call   100195e <mem_sbrk>
 100119d:	a3 3c 49 00 01       	mov    %eax,0x100493c
 10011a2:	a1 3c 49 00 01       	mov    0x100493c,%eax
 10011a7:	85 c0                	test   %eax,%eax
 10011a9:	75 07                	jne    10011b2 <mm_init+0x33>
 10011ab:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 10011b0:	eb 5e                	jmp    1001210 <mm_init+0x91>
 10011b2:	a1 3c 49 00 01       	mov    0x100493c,%eax
 10011b7:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
 10011bd:	a1 3c 49 00 01       	mov    0x100493c,%eax
 10011c2:	83 c0 04             	add    $0x4,%eax
 10011c5:	c7 00 09 00 00 00    	movl   $0x9,(%eax)
 10011cb:	a1 3c 49 00 01       	mov    0x100493c,%eax
 10011d0:	83 c0 08             	add    $0x8,%eax
 10011d3:	c7 00 09 00 00 00    	movl   $0x9,(%eax)
 10011d9:	a1 3c 49 00 01       	mov    0x100493c,%eax
 10011de:	83 c0 0c             	add    $0xc,%eax
 10011e1:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
 10011e7:	a1 3c 49 00 01       	mov    0x100493c,%eax
 10011ec:	83 c0 08             	add    $0x8,%eax
 10011ef:	a3 3c 49 00 01       	mov    %eax,0x100493c
 10011f4:	c7 04 24 00 04 00 00 	movl   $0x400,(%esp)
 10011fb:	e8 63 02 00 00       	call   1001463 <extend_heap>
 1001200:	85 c0                	test   %eax,%eax
 1001202:	75 07                	jne    100120b <mm_init+0x8c>
 1001204:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 1001209:	eb 05                	jmp    1001210 <mm_init+0x91>
 100120b:	b8 00 00 00 00       	mov    $0x0,%eax
 1001210:	c9                   	leave  
 1001211:	c3                   	ret    

01001212 <mm_malloc>:
 1001212:	55                   	push   %ebp
 1001213:	89 e5                	mov    %esp,%ebp
 1001215:	83 ec 28             	sub    $0x28,%esp
 1001218:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
 100121c:	7f 0a                	jg     1001228 <mm_malloc+0x16>
 100121e:	b8 00 00 00 00       	mov    $0x0,%eax
 1001223:	e9 9f 00 00 00       	jmp    10012c7 <mm_malloc+0xb5>
 1001228:	83 7d 08 08          	cmpl   $0x8,0x8(%ebp)
 100122c:	7f 09                	jg     1001237 <mm_malloc+0x25>
 100122e:	c7 45 f4 10 00 00 00 	movl   $0x10,-0xc(%ebp)
 1001235:	eb 17                	jmp    100124e <mm_malloc+0x3c>
 1001237:	8b 45 08             	mov    0x8(%ebp),%eax
 100123a:	83 c0 0f             	add    $0xf,%eax
 100123d:	8d 50 07             	lea    0x7(%eax),%edx
 1001240:	85 c0                	test   %eax,%eax
 1001242:	0f 48 c2             	cmovs  %edx,%eax
 1001245:	c1 f8 03             	sar    $0x3,%eax
 1001248:	c1 e0 03             	shl    $0x3,%eax
 100124b:	89 45 f4             	mov    %eax,-0xc(%ebp)
 100124e:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1001251:	89 04 24             	mov    %eax,(%esp)
 1001254:	e8 4b 03 00 00       	call   10015a4 <find_fit>
 1001259:	89 45 f0             	mov    %eax,-0x10(%ebp)
 100125c:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 1001260:	74 17                	je     1001279 <mm_malloc+0x67>
 1001262:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1001265:	89 44 24 04          	mov    %eax,0x4(%esp)
 1001269:	8b 45 f0             	mov    -0x10(%ebp),%eax
 100126c:	89 04 24             	mov    %eax,(%esp)
 100126f:	e8 73 02 00 00       	call   10014e7 <place>
 1001274:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1001277:	eb 4e                	jmp    10012c7 <mm_malloc+0xb5>
 1001279:	b8 00 10 00 00       	mov    $0x1000,%eax
 100127e:	81 7d f4 00 10 00 00 	cmpl   $0x1000,-0xc(%ebp)
 1001285:	0f 4d 45 f4          	cmovge -0xc(%ebp),%eax
 1001289:	89 45 ec             	mov    %eax,-0x14(%ebp)
 100128c:	8b 45 ec             	mov    -0x14(%ebp),%eax
 100128f:	8d 50 03             	lea    0x3(%eax),%edx
 1001292:	85 c0                	test   %eax,%eax
 1001294:	0f 48 c2             	cmovs  %edx,%eax
 1001297:	c1 f8 02             	sar    $0x2,%eax
 100129a:	89 04 24             	mov    %eax,(%esp)
 100129d:	e8 c1 01 00 00       	call   1001463 <extend_heap>
 10012a2:	89 45 f0             	mov    %eax,-0x10(%ebp)
 10012a5:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 10012a9:	75 07                	jne    10012b2 <mm_malloc+0xa0>
 10012ab:	b8 00 00 00 00       	mov    $0x0,%eax
 10012b0:	eb 15                	jmp    10012c7 <mm_malloc+0xb5>
 10012b2:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10012b5:	89 44 24 04          	mov    %eax,0x4(%esp)
 10012b9:	8b 45 f0             	mov    -0x10(%ebp),%eax
 10012bc:	89 04 24             	mov    %eax,(%esp)
 10012bf:	e8 23 02 00 00       	call   10014e7 <place>
 10012c4:	8b 45 f0             	mov    -0x10(%ebp),%eax
 10012c7:	c9                   	leave  
 10012c8:	c3                   	ret    

010012c9 <mm_free>:
 10012c9:	55                   	push   %ebp
 10012ca:	89 e5                	mov    %esp,%ebp
 10012cc:	83 ec 28             	sub    $0x28,%esp
 10012cf:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
 10012d3:	74 3a                	je     100130f <mm_free+0x46>
 10012d5:	8b 45 08             	mov    0x8(%ebp),%eax
 10012d8:	83 e8 04             	sub    $0x4,%eax
 10012db:	8b 00                	mov    (%eax),%eax
 10012dd:	83 e0 f8             	and    $0xfffffff8,%eax
 10012e0:	89 45 f4             	mov    %eax,-0xc(%ebp)
 10012e3:	8b 45 08             	mov    0x8(%ebp),%eax
 10012e6:	8d 50 fc             	lea    -0x4(%eax),%edx
 10012e9:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10012ec:	89 02                	mov    %eax,(%edx)
 10012ee:	8b 45 08             	mov    0x8(%ebp),%eax
 10012f1:	83 e8 04             	sub    $0x4,%eax
 10012f4:	8b 00                	mov    (%eax),%eax
 10012f6:	83 e0 f8             	and    $0xfffffff8,%eax
 10012f9:	83 e8 08             	sub    $0x8,%eax
 10012fc:	03 45 08             	add    0x8(%ebp),%eax
 10012ff:	8b 55 f4             	mov    -0xc(%ebp),%edx
 1001302:	89 10                	mov    %edx,(%eax)
 1001304:	8b 45 08             	mov    0x8(%ebp),%eax
 1001307:	89 04 24             	mov    %eax,(%esp)
 100130a:	e8 ed 02 00 00       	call   10015fc <coalesce>
 100130f:	c9                   	leave  
 1001310:	c3                   	ret    

01001311 <mm_realloc>:
 1001311:	55                   	push   %ebp
 1001312:	89 e5                	mov    %esp,%ebp
 1001314:	83 ec 28             	sub    $0x28,%esp
 1001317:	8b 45 0c             	mov    0xc(%ebp),%eax
 100131a:	89 04 24             	mov    %eax,(%esp)
 100131d:	e8 f0 fe ff ff       	call   1001212 <mm_malloc>
 1001322:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1001325:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 1001329:	75 07                	jne    1001332 <mm_realloc+0x21>
 100132b:	b8 00 00 00 00       	mov    $0x0,%eax
 1001330:	eb 4a                	jmp    100137c <mm_realloc+0x6b>
 1001332:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
 1001336:	74 41                	je     1001379 <mm_realloc+0x68>
 1001338:	8b 45 08             	mov    0x8(%ebp),%eax
 100133b:	83 e8 04             	sub    $0x4,%eax
 100133e:	8b 00                	mov    (%eax),%eax
 1001340:	83 e0 f8             	and    $0xfffffff8,%eax
 1001343:	89 45 f0             	mov    %eax,-0x10(%ebp)
 1001346:	8b 45 0c             	mov    0xc(%ebp),%eax
 1001349:	89 44 24 04          	mov    %eax,0x4(%esp)
 100134d:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1001350:	89 04 24             	mov    %eax,(%esp)
 1001353:	e8 18 fe ff ff       	call   1001170 <min>
 1001358:	89 44 24 08          	mov    %eax,0x8(%esp)
 100135c:	8b 45 08             	mov    0x8(%ebp),%eax
 100135f:	89 44 24 04          	mov    %eax,0x4(%esp)
 1001363:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1001366:	89 04 24             	mov    %eax,(%esp)
 1001369:	e8 62 11 00 00       	call   10024d0 <memcpy>
 100136e:	8b 45 08             	mov    0x8(%ebp),%eax
 1001371:	89 04 24             	mov    %eax,(%esp)
 1001374:	e8 50 ff ff ff       	call   10012c9 <mm_free>
 1001379:	8b 45 f4             	mov    -0xc(%ebp),%eax
 100137c:	c9                   	leave  
 100137d:	c3                   	ret    

0100137e <mm_checkheap>:
 100137e:	55                   	push   %ebp
 100137f:	89 e5                	mov    %esp,%ebp
 1001381:	83 ec 28             	sub    $0x28,%esp
 1001384:	a1 3c 49 00 01       	mov    0x100493c,%eax
 1001389:	89 45 f4             	mov    %eax,-0xc(%ebp)
 100138c:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
 1001390:	74 15                	je     10013a7 <mm_checkheap+0x29>
 1001392:	a1 3c 49 00 01       	mov    0x100493c,%eax
 1001397:	89 44 24 04          	mov    %eax,0x4(%esp)
 100139b:	c7 04 24 74 29 00 01 	movl   $0x1002974,(%esp)
 10013a2:	e8 0a 08 00 00       	call   1001bb1 <lprintf>
 10013a7:	a1 3c 49 00 01       	mov    0x100493c,%eax
 10013ac:	83 e8 04             	sub    $0x4,%eax
 10013af:	8b 00                	mov    (%eax),%eax
 10013b1:	83 e0 f8             	and    $0xfffffff8,%eax
 10013b4:	83 f8 08             	cmp    $0x8,%eax
 10013b7:	75 11                	jne    10013ca <mm_checkheap+0x4c>
 10013b9:	a1 3c 49 00 01       	mov    0x100493c,%eax
 10013be:	83 e8 04             	sub    $0x4,%eax
 10013c1:	8b 00                	mov    (%eax),%eax
 10013c3:	83 e0 01             	and    $0x1,%eax
 10013c6:	85 c0                	test   %eax,%eax
 10013c8:	75 0c                	jne    10013d6 <mm_checkheap+0x58>
 10013ca:	c7 04 24 82 29 00 01 	movl   $0x1002982,(%esp)
 10013d1:	e8 db 07 00 00       	call   1001bb1 <lprintf>
 10013d6:	a1 3c 49 00 01       	mov    0x100493c,%eax
 10013db:	89 04 24             	mov    %eax,(%esp)
 10013de:	e8 c6 04 00 00       	call   10018a9 <checkblock>
 10013e3:	a1 3c 49 00 01       	mov    0x100493c,%eax
 10013e8:	89 45 f4             	mov    %eax,-0xc(%ebp)
 10013eb:	eb 2a                	jmp    1001417 <mm_checkheap+0x99>
 10013ed:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
 10013f1:	74 0b                	je     10013fe <mm_checkheap+0x80>
 10013f3:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10013f6:	89 04 24             	mov    %eax,(%esp)
 10013f9:	e8 ed 03 00 00       	call   10017eb <printblock>
 10013fe:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1001401:	89 04 24             	mov    %eax,(%esp)
 1001404:	e8 a0 04 00 00       	call   10018a9 <checkblock>
 1001409:	8b 45 f4             	mov    -0xc(%ebp),%eax
 100140c:	83 e8 04             	sub    $0x4,%eax
 100140f:	8b 00                	mov    (%eax),%eax
 1001411:	83 e0 f8             	and    $0xfffffff8,%eax
 1001414:	01 45 f4             	add    %eax,-0xc(%ebp)
 1001417:	8b 45 f4             	mov    -0xc(%ebp),%eax
 100141a:	83 e8 04             	sub    $0x4,%eax
 100141d:	8b 00                	mov    (%eax),%eax
 100141f:	83 e0 f8             	and    $0xfffffff8,%eax
 1001422:	85 c0                	test   %eax,%eax
 1001424:	7f c7                	jg     10013ed <mm_checkheap+0x6f>
 1001426:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
 100142a:	74 0b                	je     1001437 <mm_checkheap+0xb9>
 100142c:	8b 45 f4             	mov    -0xc(%ebp),%eax
 100142f:	89 04 24             	mov    %eax,(%esp)
 1001432:	e8 b4 03 00 00       	call   10017eb <printblock>
 1001437:	8b 45 f4             	mov    -0xc(%ebp),%eax
 100143a:	83 e8 04             	sub    $0x4,%eax
 100143d:	8b 00                	mov    (%eax),%eax
 100143f:	83 e0 f8             	and    $0xfffffff8,%eax
 1001442:	85 c0                	test   %eax,%eax
 1001444:	75 0f                	jne    1001455 <mm_checkheap+0xd7>
 1001446:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1001449:	83 e8 04             	sub    $0x4,%eax
 100144c:	8b 00                	mov    (%eax),%eax
 100144e:	83 e0 01             	and    $0x1,%eax
 1001451:	85 c0                	test   %eax,%eax
 1001453:	75 0c                	jne    1001461 <mm_checkheap+0xe3>
 1001455:	c7 04 24 97 29 00 01 	movl   $0x1002997,(%esp)
 100145c:	e8 50 07 00 00       	call   1001bb1 <lprintf>
 1001461:	c9                   	leave  
 1001462:	c3                   	ret    

01001463 <extend_heap>:
 1001463:	55                   	push   %ebp
 1001464:	89 e5                	mov    %esp,%ebp
 1001466:	83 ec 28             	sub    $0x28,%esp
 1001469:	8b 45 08             	mov    0x8(%ebp),%eax
 100146c:	83 e0 01             	and    $0x1,%eax
 100146f:	84 c0                	test   %al,%al
 1001471:	74 0b                	je     100147e <extend_heap+0x1b>
 1001473:	8b 45 08             	mov    0x8(%ebp),%eax
 1001476:	83 c0 01             	add    $0x1,%eax
 1001479:	c1 e0 02             	shl    $0x2,%eax
 100147c:	eb 06                	jmp    1001484 <extend_heap+0x21>
 100147e:	8b 45 08             	mov    0x8(%ebp),%eax
 1001481:	c1 e0 02             	shl    $0x2,%eax
 1001484:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1001487:	8b 45 f4             	mov    -0xc(%ebp),%eax
 100148a:	89 04 24             	mov    %eax,(%esp)
 100148d:	e8 cc 04 00 00       	call   100195e <mem_sbrk>
 1001492:	89 45 f0             	mov    %eax,-0x10(%ebp)
 1001495:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 1001499:	75 07                	jne    10014a2 <extend_heap+0x3f>
 100149b:	b8 00 00 00 00       	mov    $0x0,%eax
 10014a0:	eb 43                	jmp    10014e5 <extend_heap+0x82>
 10014a2:	8b 45 f0             	mov    -0x10(%ebp),%eax
 10014a5:	8d 50 fc             	lea    -0x4(%eax),%edx
 10014a8:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10014ab:	89 02                	mov    %eax,(%edx)
 10014ad:	8b 45 f0             	mov    -0x10(%ebp),%eax
 10014b0:	83 e8 04             	sub    $0x4,%eax
 10014b3:	8b 00                	mov    (%eax),%eax
 10014b5:	83 e0 f8             	and    $0xfffffff8,%eax
 10014b8:	83 e8 08             	sub    $0x8,%eax
 10014bb:	03 45 f0             	add    -0x10(%ebp),%eax
 10014be:	8b 55 f4             	mov    -0xc(%ebp),%edx
 10014c1:	89 10                	mov    %edx,(%eax)
 10014c3:	8b 45 f0             	mov    -0x10(%ebp),%eax
 10014c6:	83 e8 04             	sub    $0x4,%eax
 10014c9:	8b 00                	mov    (%eax),%eax
 10014cb:	83 e0 f8             	and    $0xfffffff8,%eax
 10014ce:	83 e8 04             	sub    $0x4,%eax
 10014d1:	03 45 f0             	add    -0x10(%ebp),%eax
 10014d4:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
 10014da:	8b 45 f0             	mov    -0x10(%ebp),%eax
 10014dd:	89 04 24             	mov    %eax,(%esp)
 10014e0:	e8 17 01 00 00       	call   10015fc <coalesce>
 10014e5:	c9                   	leave  
 10014e6:	c3                   	ret    

010014e7 <place>:
 10014e7:	55                   	push   %ebp
 10014e8:	89 e5                	mov    %esp,%ebp
 10014ea:	53                   	push   %ebx
 10014eb:	83 ec 10             	sub    $0x10,%esp
 10014ee:	8b 45 08             	mov    0x8(%ebp),%eax
 10014f1:	83 e8 04             	sub    $0x4,%eax
 10014f4:	8b 00                	mov    (%eax),%eax
 10014f6:	83 e0 f8             	and    $0xfffffff8,%eax
 10014f9:	89 45 f8             	mov    %eax,-0x8(%ebp)
 10014fc:	8b 45 0c             	mov    0xc(%ebp),%eax
 10014ff:	8b 55 f8             	mov    -0x8(%ebp),%edx
 1001502:	89 d1                	mov    %edx,%ecx
 1001504:	29 c1                	sub    %eax,%ecx
 1001506:	89 c8                	mov    %ecx,%eax
 1001508:	83 f8 0f             	cmp    $0xf,%eax
 100150b:	7e 6a                	jle    1001577 <place+0x90>
 100150d:	8b 45 08             	mov    0x8(%ebp),%eax
 1001510:	83 e8 04             	sub    $0x4,%eax
 1001513:	8b 55 0c             	mov    0xc(%ebp),%edx
 1001516:	83 ca 01             	or     $0x1,%edx
 1001519:	89 10                	mov    %edx,(%eax)
 100151b:	8b 45 08             	mov    0x8(%ebp),%eax
 100151e:	83 e8 04             	sub    $0x4,%eax
 1001521:	8b 00                	mov    (%eax),%eax
 1001523:	83 e0 f8             	and    $0xfffffff8,%eax
 1001526:	83 e8 08             	sub    $0x8,%eax
 1001529:	03 45 08             	add    0x8(%ebp),%eax
 100152c:	8b 55 0c             	mov    0xc(%ebp),%edx
 100152f:	83 ca 01             	or     $0x1,%edx
 1001532:	89 10                	mov    %edx,(%eax)
 1001534:	8b 45 08             	mov    0x8(%ebp),%eax
 1001537:	83 e8 04             	sub    $0x4,%eax
 100153a:	8b 00                	mov    (%eax),%eax
 100153c:	83 e0 f8             	and    $0xfffffff8,%eax
 100153f:	01 45 08             	add    %eax,0x8(%ebp)
 1001542:	8b 45 08             	mov    0x8(%ebp),%eax
 1001545:	83 e8 04             	sub    $0x4,%eax
 1001548:	8b 55 0c             	mov    0xc(%ebp),%edx
 100154b:	8b 4d f8             	mov    -0x8(%ebp),%ecx
 100154e:	89 cb                	mov    %ecx,%ebx
 1001550:	29 d3                	sub    %edx,%ebx
 1001552:	89 da                	mov    %ebx,%edx
 1001554:	89 10                	mov    %edx,(%eax)
 1001556:	8b 45 08             	mov    0x8(%ebp),%eax
 1001559:	83 e8 04             	sub    $0x4,%eax
 100155c:	8b 00                	mov    (%eax),%eax
 100155e:	83 e0 f8             	and    $0xfffffff8,%eax
 1001561:	83 e8 08             	sub    $0x8,%eax
 1001564:	03 45 08             	add    0x8(%ebp),%eax
 1001567:	8b 55 0c             	mov    0xc(%ebp),%edx
 100156a:	8b 4d f8             	mov    -0x8(%ebp),%ecx
 100156d:	89 cb                	mov    %ecx,%ebx
 100156f:	29 d3                	sub    %edx,%ebx
 1001571:	89 da                	mov    %ebx,%edx
 1001573:	89 10                	mov    %edx,(%eax)
 1001575:	eb 27                	jmp    100159e <place+0xb7>
 1001577:	8b 45 08             	mov    0x8(%ebp),%eax
 100157a:	83 e8 04             	sub    $0x4,%eax
 100157d:	8b 55 f8             	mov    -0x8(%ebp),%edx
 1001580:	83 ca 01             	or     $0x1,%edx
 1001583:	89 10                	mov    %edx,(%eax)
 1001585:	8b 45 08             	mov    0x8(%ebp),%eax
 1001588:	83 e8 04             	sub    $0x4,%eax
 100158b:	8b 00                	mov    (%eax),%eax
 100158d:	83 e0 f8             	and    $0xfffffff8,%eax
 1001590:	83 e8 08             	sub    $0x8,%eax
 1001593:	03 45 08             	add    0x8(%ebp),%eax
 1001596:	8b 55 f8             	mov    -0x8(%ebp),%edx
 1001599:	83 ca 01             	or     $0x1,%edx
 100159c:	89 10                	mov    %edx,(%eax)
 100159e:	83 c4 10             	add    $0x10,%esp
 10015a1:	5b                   	pop    %ebx
 10015a2:	5d                   	pop    %ebp
 10015a3:	c3                   	ret    

010015a4 <find_fit>:
 10015a4:	55                   	push   %ebp
 10015a5:	89 e5                	mov    %esp,%ebp
 10015a7:	83 ec 10             	sub    $0x10,%esp
 10015aa:	a1 3c 49 00 01       	mov    0x100493c,%eax
 10015af:	89 45 fc             	mov    %eax,-0x4(%ebp)
 10015b2:	eb 32                	jmp    10015e6 <find_fit+0x42>
 10015b4:	8b 45 fc             	mov    -0x4(%ebp),%eax
 10015b7:	83 e8 04             	sub    $0x4,%eax
 10015ba:	8b 00                	mov    (%eax),%eax
 10015bc:	83 e0 01             	and    $0x1,%eax
 10015bf:	85 c0                	test   %eax,%eax
 10015c1:	75 15                	jne    10015d8 <find_fit+0x34>
 10015c3:	8b 45 fc             	mov    -0x4(%ebp),%eax
 10015c6:	83 e8 04             	sub    $0x4,%eax
 10015c9:	8b 00                	mov    (%eax),%eax
 10015cb:	83 e0 f8             	and    $0xfffffff8,%eax
 10015ce:	3b 45 08             	cmp    0x8(%ebp),%eax
 10015d1:	7c 05                	jl     10015d8 <find_fit+0x34>
 10015d3:	8b 45 fc             	mov    -0x4(%ebp),%eax
 10015d6:	eb 22                	jmp    10015fa <find_fit+0x56>
 10015d8:	8b 45 fc             	mov    -0x4(%ebp),%eax
 10015db:	83 e8 04             	sub    $0x4,%eax
 10015de:	8b 00                	mov    (%eax),%eax
 10015e0:	83 e0 f8             	and    $0xfffffff8,%eax
 10015e3:	01 45 fc             	add    %eax,-0x4(%ebp)
 10015e6:	8b 45 fc             	mov    -0x4(%ebp),%eax
 10015e9:	83 e8 04             	sub    $0x4,%eax
 10015ec:	8b 00                	mov    (%eax),%eax
 10015ee:	83 e0 f8             	and    $0xfffffff8,%eax
 10015f1:	85 c0                	test   %eax,%eax
 10015f3:	7f bf                	jg     10015b4 <find_fit+0x10>
 10015f5:	b8 00 00 00 00       	mov    $0x0,%eax
 10015fa:	c9                   	leave  
 10015fb:	c3                   	ret    

010015fc <coalesce>:
 10015fc:	55                   	push   %ebp
 10015fd:	89 e5                	mov    %esp,%ebp
 10015ff:	83 ec 10             	sub    $0x10,%esp
 1001602:	8b 45 08             	mov    0x8(%ebp),%eax
 1001605:	83 e8 08             	sub    $0x8,%eax
 1001608:	8b 00                	mov    (%eax),%eax
 100160a:	89 c2                	mov    %eax,%edx
 100160c:	83 e2 f8             	and    $0xfffffff8,%edx
 100160f:	b8 fc ff ff ff       	mov    $0xfffffffc,%eax
 1001614:	29 d0                	sub    %edx,%eax
 1001616:	03 45 08             	add    0x8(%ebp),%eax
 1001619:	8b 00                	mov    (%eax),%eax
 100161b:	89 c2                	mov    %eax,%edx
 100161d:	83 e2 f8             	and    $0xfffffff8,%edx
 1001620:	8b 45 08             	mov    0x8(%ebp),%eax
 1001623:	83 e8 08             	sub    $0x8,%eax
 1001626:	8b 00                	mov    (%eax),%eax
 1001628:	83 e0 f8             	and    $0xfffffff8,%eax
 100162b:	89 d1                	mov    %edx,%ecx
 100162d:	29 c1                	sub    %eax,%ecx
 100162f:	89 c8                	mov    %ecx,%eax
 1001631:	83 e8 08             	sub    $0x8,%eax
 1001634:	03 45 08             	add    0x8(%ebp),%eax
 1001637:	8b 00                	mov    (%eax),%eax
 1001639:	83 e0 01             	and    $0x1,%eax
 100163c:	89 45 fc             	mov    %eax,-0x4(%ebp)
 100163f:	8b 45 08             	mov    0x8(%ebp),%eax
 1001642:	83 e8 04             	sub    $0x4,%eax
 1001645:	8b 00                	mov    (%eax),%eax
 1001647:	83 e0 f8             	and    $0xfffffff8,%eax
 100164a:	83 e8 04             	sub    $0x4,%eax
 100164d:	03 45 08             	add    0x8(%ebp),%eax
 1001650:	8b 00                	mov    (%eax),%eax
 1001652:	83 e0 01             	and    $0x1,%eax
 1001655:	89 45 f8             	mov    %eax,-0x8(%ebp)
 1001658:	8b 45 08             	mov    0x8(%ebp),%eax
 100165b:	83 e8 04             	sub    $0x4,%eax
 100165e:	8b 00                	mov    (%eax),%eax
 1001660:	83 e0 f8             	and    $0xfffffff8,%eax
 1001663:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1001666:	83 7d fc 00          	cmpl   $0x0,-0x4(%ebp)
 100166a:	74 0e                	je     100167a <coalesce+0x7e>
 100166c:	83 7d f8 00          	cmpl   $0x0,-0x8(%ebp)
 1001670:	74 08                	je     100167a <coalesce+0x7e>
 1001672:	8b 45 08             	mov    0x8(%ebp),%eax
 1001675:	e9 6f 01 00 00       	jmp    10017e9 <coalesce+0x1ed>
 100167a:	83 7d fc 00          	cmpl   $0x0,-0x4(%ebp)
 100167e:	74 48                	je     10016c8 <coalesce+0xcc>
 1001680:	83 7d f8 00          	cmpl   $0x0,-0x8(%ebp)
 1001684:	75 42                	jne    10016c8 <coalesce+0xcc>
 1001686:	8b 45 08             	mov    0x8(%ebp),%eax
 1001689:	83 e8 04             	sub    $0x4,%eax
 100168c:	8b 00                	mov    (%eax),%eax
 100168e:	83 e0 f8             	and    $0xfffffff8,%eax
 1001691:	83 e8 04             	sub    $0x4,%eax
 1001694:	03 45 08             	add    0x8(%ebp),%eax
 1001697:	8b 00                	mov    (%eax),%eax
 1001699:	83 e0 f8             	and    $0xfffffff8,%eax
 100169c:	01 45 f4             	add    %eax,-0xc(%ebp)
 100169f:	8b 45 08             	mov    0x8(%ebp),%eax
 10016a2:	8d 50 fc             	lea    -0x4(%eax),%edx
 10016a5:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10016a8:	89 02                	mov    %eax,(%edx)
 10016aa:	8b 45 08             	mov    0x8(%ebp),%eax
 10016ad:	83 e8 04             	sub    $0x4,%eax
 10016b0:	8b 00                	mov    (%eax),%eax
 10016b2:	83 e0 f8             	and    $0xfffffff8,%eax
 10016b5:	83 e8 08             	sub    $0x8,%eax
 10016b8:	03 45 08             	add    0x8(%ebp),%eax
 10016bb:	8b 55 f4             	mov    -0xc(%ebp),%edx
 10016be:	89 10                	mov    %edx,(%eax)
 10016c0:	8b 45 08             	mov    0x8(%ebp),%eax
 10016c3:	e9 21 01 00 00       	jmp    10017e9 <coalesce+0x1ed>
 10016c8:	83 7d fc 00          	cmpl   $0x0,-0x4(%ebp)
 10016cc:	75 6c                	jne    100173a <coalesce+0x13e>
 10016ce:	83 7d f8 00          	cmpl   $0x0,-0x8(%ebp)
 10016d2:	74 66                	je     100173a <coalesce+0x13e>
 10016d4:	8b 45 08             	mov    0x8(%ebp),%eax
 10016d7:	83 e8 08             	sub    $0x8,%eax
 10016da:	8b 00                	mov    (%eax),%eax
 10016dc:	89 c2                	mov    %eax,%edx
 10016de:	83 e2 f8             	and    $0xfffffff8,%edx
 10016e1:	b8 fc ff ff ff       	mov    $0xfffffffc,%eax
 10016e6:	29 d0                	sub    %edx,%eax
 10016e8:	03 45 08             	add    0x8(%ebp),%eax
 10016eb:	8b 00                	mov    (%eax),%eax
 10016ed:	83 e0 f8             	and    $0xfffffff8,%eax
 10016f0:	01 45 f4             	add    %eax,-0xc(%ebp)
 10016f3:	8b 45 08             	mov    0x8(%ebp),%eax
 10016f6:	83 e8 04             	sub    $0x4,%eax
 10016f9:	8b 00                	mov    (%eax),%eax
 10016fb:	83 e0 f8             	and    $0xfffffff8,%eax
 10016fe:	83 e8 08             	sub    $0x8,%eax
 1001701:	03 45 08             	add    0x8(%ebp),%eax
 1001704:	8b 55 f4             	mov    -0xc(%ebp),%edx
 1001707:	89 10                	mov    %edx,(%eax)
 1001709:	8b 45 08             	mov    0x8(%ebp),%eax
 100170c:	83 e8 08             	sub    $0x8,%eax
 100170f:	8b 00                	mov    (%eax),%eax
 1001711:	89 c2                	mov    %eax,%edx
 1001713:	83 e2 f8             	and    $0xfffffff8,%edx
 1001716:	b8 fc ff ff ff       	mov    $0xfffffffc,%eax
 100171b:	29 d0                	sub    %edx,%eax
 100171d:	03 45 08             	add    0x8(%ebp),%eax
 1001720:	8b 55 f4             	mov    -0xc(%ebp),%edx
 1001723:	89 10                	mov    %edx,(%eax)
 1001725:	8b 45 08             	mov    0x8(%ebp),%eax
 1001728:	83 e8 08             	sub    $0x8,%eax
 100172b:	8b 00                	mov    (%eax),%eax
 100172d:	83 e0 f8             	and    $0xfffffff8,%eax
 1001730:	f7 d8                	neg    %eax
 1001732:	03 45 08             	add    0x8(%ebp),%eax
 1001735:	e9 af 00 00 00       	jmp    10017e9 <coalesce+0x1ed>
 100173a:	8b 45 08             	mov    0x8(%ebp),%eax
 100173d:	83 e8 08             	sub    $0x8,%eax
 1001740:	8b 00                	mov    (%eax),%eax
 1001742:	89 c2                	mov    %eax,%edx
 1001744:	83 e2 f8             	and    $0xfffffff8,%edx
 1001747:	b8 fc ff ff ff       	mov    $0xfffffffc,%eax
 100174c:	29 d0                	sub    %edx,%eax
 100174e:	03 45 08             	add    0x8(%ebp),%eax
 1001751:	8b 00                	mov    (%eax),%eax
 1001753:	89 c2                	mov    %eax,%edx
 1001755:	83 e2 f8             	and    $0xfffffff8,%edx
 1001758:	8b 45 08             	mov    0x8(%ebp),%eax
 100175b:	83 e8 04             	sub    $0x4,%eax
 100175e:	8b 00                	mov    (%eax),%eax
 1001760:	89 c1                	mov    %eax,%ecx
 1001762:	83 e1 f8             	and    $0xfffffff8,%ecx
 1001765:	8b 45 08             	mov    0x8(%ebp),%eax
 1001768:	83 e8 04             	sub    $0x4,%eax
 100176b:	8b 00                	mov    (%eax),%eax
 100176d:	83 e0 f8             	and    $0xfffffff8,%eax
 1001770:	83 e8 04             	sub    $0x4,%eax
 1001773:	03 45 08             	add    0x8(%ebp),%eax
 1001776:	8b 00                	mov    (%eax),%eax
 1001778:	83 e0 f8             	and    $0xfffffff8,%eax
 100177b:	01 c8                	add    %ecx,%eax
 100177d:	83 e8 08             	sub    $0x8,%eax
 1001780:	03 45 08             	add    0x8(%ebp),%eax
 1001783:	8b 00                	mov    (%eax),%eax
 1001785:	83 e0 f8             	and    $0xfffffff8,%eax
 1001788:	01 d0                	add    %edx,%eax
 100178a:	01 45 f4             	add    %eax,-0xc(%ebp)
 100178d:	8b 45 08             	mov    0x8(%ebp),%eax
 1001790:	83 e8 08             	sub    $0x8,%eax
 1001793:	8b 00                	mov    (%eax),%eax
 1001795:	89 c2                	mov    %eax,%edx
 1001797:	83 e2 f8             	and    $0xfffffff8,%edx
 100179a:	b8 fc ff ff ff       	mov    $0xfffffffc,%eax
 100179f:	29 d0                	sub    %edx,%eax
 10017a1:	03 45 08             	add    0x8(%ebp),%eax
 10017a4:	8b 55 f4             	mov    -0xc(%ebp),%edx
 10017a7:	89 10                	mov    %edx,(%eax)
 10017a9:	8b 45 08             	mov    0x8(%ebp),%eax
 10017ac:	83 e8 04             	sub    $0x4,%eax
 10017af:	8b 00                	mov    (%eax),%eax
 10017b1:	89 c2                	mov    %eax,%edx
 10017b3:	83 e2 f8             	and    $0xfffffff8,%edx
 10017b6:	8b 45 08             	mov    0x8(%ebp),%eax
 10017b9:	83 e8 04             	sub    $0x4,%eax
 10017bc:	8b 00                	mov    (%eax),%eax
 10017be:	83 e0 f8             	and    $0xfffffff8,%eax
 10017c1:	83 e8 04             	sub    $0x4,%eax
 10017c4:	03 45 08             	add    0x8(%ebp),%eax
 10017c7:	8b 00                	mov    (%eax),%eax
 10017c9:	83 e0 f8             	and    $0xfffffff8,%eax
 10017cc:	01 d0                	add    %edx,%eax
 10017ce:	83 e8 08             	sub    $0x8,%eax
 10017d1:	03 45 08             	add    0x8(%ebp),%eax
 10017d4:	8b 55 f4             	mov    -0xc(%ebp),%edx
 10017d7:	89 10                	mov    %edx,(%eax)
 10017d9:	8b 45 08             	mov    0x8(%ebp),%eax
 10017dc:	83 e8 08             	sub    $0x8,%eax
 10017df:	8b 00                	mov    (%eax),%eax
 10017e1:	83 e0 f8             	and    $0xfffffff8,%eax
 10017e4:	f7 d8                	neg    %eax
 10017e6:	03 45 08             	add    0x8(%ebp),%eax
 10017e9:	c9                   	leave  
 10017ea:	c3                   	ret    

010017eb <printblock>:
 10017eb:	55                   	push   %ebp
 10017ec:	89 e5                	mov    %esp,%ebp
 10017ee:	83 ec 38             	sub    $0x38,%esp
 10017f1:	8b 45 08             	mov    0x8(%ebp),%eax
 10017f4:	83 e8 04             	sub    $0x4,%eax
 10017f7:	8b 00                	mov    (%eax),%eax
 10017f9:	83 e0 f8             	and    $0xfffffff8,%eax
 10017fc:	89 45 f4             	mov    %eax,-0xc(%ebp)
 10017ff:	8b 45 08             	mov    0x8(%ebp),%eax
 1001802:	83 e8 04             	sub    $0x4,%eax
 1001805:	8b 00                	mov    (%eax),%eax
 1001807:	83 e0 01             	and    $0x1,%eax
 100180a:	89 45 f0             	mov    %eax,-0x10(%ebp)
 100180d:	8b 45 08             	mov    0x8(%ebp),%eax
 1001810:	83 e8 04             	sub    $0x4,%eax
 1001813:	8b 00                	mov    (%eax),%eax
 1001815:	83 e0 f8             	and    $0xfffffff8,%eax
 1001818:	83 e8 08             	sub    $0x8,%eax
 100181b:	03 45 08             	add    0x8(%ebp),%eax
 100181e:	8b 00                	mov    (%eax),%eax
 1001820:	83 e0 f8             	and    $0xfffffff8,%eax
 1001823:	89 45 ec             	mov    %eax,-0x14(%ebp)
 1001826:	8b 45 08             	mov    0x8(%ebp),%eax
 1001829:	83 e8 04             	sub    $0x4,%eax
 100182c:	8b 00                	mov    (%eax),%eax
 100182e:	83 e0 f8             	and    $0xfffffff8,%eax
 1001831:	83 e8 08             	sub    $0x8,%eax
 1001834:	03 45 08             	add    0x8(%ebp),%eax
 1001837:	8b 00                	mov    (%eax),%eax
 1001839:	83 e0 01             	and    $0x1,%eax
 100183c:	89 45 e8             	mov    %eax,-0x18(%ebp)
 100183f:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 1001843:	75 15                	jne    100185a <printblock+0x6f>
 1001845:	8b 45 08             	mov    0x8(%ebp),%eax
 1001848:	89 44 24 04          	mov    %eax,0x4(%esp)
 100184c:	c7 04 24 ac 29 00 01 	movl   $0x10029ac,(%esp)
 1001853:	e8 59 03 00 00       	call   1001bb1 <lprintf>
 1001858:	eb 4d                	jmp    10018a7 <printblock+0xbc>
 100185a:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
 100185e:	74 07                	je     1001867 <printblock+0x7c>
 1001860:	ba 61 00 00 00       	mov    $0x61,%edx
 1001865:	eb 05                	jmp    100186c <printblock+0x81>
 1001867:	ba 66 00 00 00       	mov    $0x66,%edx
 100186c:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 1001870:	74 07                	je     1001879 <printblock+0x8e>
 1001872:	b8 61 00 00 00       	mov    $0x61,%eax
 1001877:	eb 05                	jmp    100187e <printblock+0x93>
 1001879:	b8 66 00 00 00       	mov    $0x66,%eax
 100187e:	89 54 24 14          	mov    %edx,0x14(%esp)
 1001882:	8b 55 ec             	mov    -0x14(%ebp),%edx
 1001885:	89 54 24 10          	mov    %edx,0x10(%esp)
 1001889:	89 44 24 0c          	mov    %eax,0xc(%esp)
 100188d:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1001890:	89 44 24 08          	mov    %eax,0x8(%esp)
 1001894:	8b 45 08             	mov    0x8(%ebp),%eax
 1001897:	89 44 24 04          	mov    %eax,0x4(%esp)
 100189b:	c7 04 24 b8 29 00 01 	movl   $0x10029b8,(%esp)
 10018a2:	e8 0a 03 00 00       	call   1001bb1 <lprintf>
 10018a7:	c9                   	leave  
 10018a8:	c3                   	ret    

010018a9 <checkblock>:
 10018a9:	55                   	push   %ebp
 10018aa:	89 e5                	mov    %esp,%ebp
 10018ac:	83 ec 18             	sub    $0x18,%esp
 10018af:	8b 45 08             	mov    0x8(%ebp),%eax
 10018b2:	83 e0 07             	and    $0x7,%eax
 10018b5:	85 c0                	test   %eax,%eax
 10018b7:	74 13                	je     10018cc <checkblock+0x23>
 10018b9:	8b 45 08             	mov    0x8(%ebp),%eax
 10018bc:	89 44 24 04          	mov    %eax,0x4(%esp)
 10018c0:	c7 04 24 e0 29 00 01 	movl   $0x10029e0,(%esp)
 10018c7:	e8 e5 02 00 00       	call   1001bb1 <lprintf>
 10018cc:	8b 45 08             	mov    0x8(%ebp),%eax
 10018cf:	83 e8 04             	sub    $0x4,%eax
 10018d2:	8b 10                	mov    (%eax),%edx
 10018d4:	8b 45 08             	mov    0x8(%ebp),%eax
 10018d7:	83 e8 04             	sub    $0x4,%eax
 10018da:	8b 00                	mov    (%eax),%eax
 10018dc:	83 e0 f8             	and    $0xfffffff8,%eax
 10018df:	83 e8 08             	sub    $0x8,%eax
 10018e2:	03 45 08             	add    0x8(%ebp),%eax
 10018e5:	8b 00                	mov    (%eax),%eax
 10018e7:	39 c2                	cmp    %eax,%edx
 10018e9:	74 0c                	je     10018f7 <checkblock+0x4e>
 10018eb:	c7 04 24 08 2a 00 01 	movl   $0x1002a08,(%esp)
 10018f2:	e8 ba 02 00 00       	call   1001bb1 <lprintf>
 10018f7:	c9                   	leave  
 10018f8:	c3                   	ret    
 10018f9:	90                   	nop
 10018fa:	90                   	nop
 10018fb:	90                   	nop

010018fc <mem_init>:
 10018fc:	55                   	push   %ebp
 10018fd:	89 e5                	mov    %esp,%ebp
 10018ff:	83 ec 18             	sub    $0x18,%esp
 1001902:	8b 45 08             	mov    0x8(%ebp),%eax
 1001905:	a3 40 49 00 01       	mov    %eax,0x1004940
 100190a:	b8 90 59 00 01       	mov    $0x1005990,%eax
 100190f:	a3 44 49 00 01       	mov    %eax,0x1004944
 1001914:	a1 44 49 00 01       	mov    0x1004944,%eax
 1001919:	25 00 f0 ff ff       	and    $0xfffff000,%eax
 100191e:	a3 44 49 00 01       	mov    %eax,0x1004944
 1001923:	eb 0f                	jmp    1001934 <mem_init+0x38>
 1001925:	a1 44 49 00 01       	mov    0x1004944,%eax
 100192a:	05 00 10 00 00       	add    $0x1000,%eax
 100192f:	a3 44 49 00 01       	mov    %eax,0x1004944
 1001934:	a1 44 49 00 01       	mov    0x1004944,%eax
 1001939:	c7 44 24 04 00 10 00 	movl   $0x1000,0x4(%esp)
 1001940:	00 
 1001941:	89 04 24             	mov    %eax,(%esp)
 1001944:	e8 b3 0e 00 00       	call   10027fc <new_pages>
 1001949:	85 c0                	test   %eax,%eax
 100194b:	75 d8                	jne    1001925 <mem_init+0x29>
 100194d:	a1 44 49 00 01       	mov    0x1004944,%eax
 1001952:	05 00 10 00 00       	add    $0x1000,%eax
 1001957:	a3 48 49 00 01       	mov    %eax,0x1004948
 100195c:	c9                   	leave  
 100195d:	c3                   	ret    

0100195e <mem_sbrk>:
 100195e:	55                   	push   %ebp
 100195f:	89 e5                	mov    %esp,%ebp
 1001961:	83 ec 28             	sub    $0x28,%esp
 1001964:	a1 44 49 00 01       	mov    0x1004944,%eax
 1001969:	89 45 f4             	mov    %eax,-0xc(%ebp)
 100196c:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
 1001970:	78 11                	js     1001983 <mem_sbrk+0x25>
 1001972:	8b 45 08             	mov    0x8(%ebp),%eax
 1001975:	89 c2                	mov    %eax,%edx
 1001977:	03 55 f4             	add    -0xc(%ebp),%edx
 100197a:	a1 40 49 00 01       	mov    0x1004940,%eax
 100197f:	39 c2                	cmp    %eax,%edx
 1001981:	76 07                	jbe    100198a <mem_sbrk+0x2c>
 1001983:	b8 00 00 00 00       	mov    $0x0,%eax
 1001988:	eb 77                	jmp    1001a01 <mem_sbrk+0xa3>
 100198a:	8b 45 08             	mov    0x8(%ebp),%eax
 100198d:	89 c2                	mov    %eax,%edx
 100198f:	03 55 f4             	add    -0xc(%ebp),%edx
 1001992:	a1 48 49 00 01       	mov    0x1004948,%eax
 1001997:	39 c2                	cmp    %eax,%edx
 1001999:	76 53                	jbe    10019ee <mem_sbrk+0x90>
 100199b:	8b 45 08             	mov    0x8(%ebp),%eax
 100199e:	03 45 f4             	add    -0xc(%ebp),%eax
 10019a1:	89 c2                	mov    %eax,%edx
 10019a3:	a1 48 49 00 01       	mov    0x1004948,%eax
 10019a8:	89 d1                	mov    %edx,%ecx
 10019aa:	29 c1                	sub    %eax,%ecx
 10019ac:	89 c8                	mov    %ecx,%eax
 10019ae:	89 45 f0             	mov    %eax,-0x10(%ebp)
 10019b1:	81 45 f0 ff 0f 00 00 	addl   $0xfff,-0x10(%ebp)
 10019b8:	81 65 f0 00 f0 ff ff 	andl   $0xfffff000,-0x10(%ebp)
 10019bf:	a1 48 49 00 01       	mov    0x1004948,%eax
 10019c4:	8b 55 f0             	mov    -0x10(%ebp),%edx
 10019c7:	89 54 24 04          	mov    %edx,0x4(%esp)
 10019cb:	89 04 24             	mov    %eax,(%esp)
 10019ce:	e8 29 0e 00 00       	call   10027fc <new_pages>
 10019d3:	85 c0                	test   %eax,%eax
 10019d5:	74 07                	je     10019de <mem_sbrk+0x80>
 10019d7:	b8 00 00 00 00       	mov    $0x0,%eax
 10019dc:	eb 23                	jmp    1001a01 <mem_sbrk+0xa3>
 10019de:	8b 15 48 49 00 01    	mov    0x1004948,%edx
 10019e4:	8b 45 f0             	mov    -0x10(%ebp),%eax
 10019e7:	01 d0                	add    %edx,%eax
 10019e9:	a3 48 49 00 01       	mov    %eax,0x1004948
 10019ee:	8b 15 44 49 00 01    	mov    0x1004944,%edx
 10019f4:	8b 45 08             	mov    0x8(%ebp),%eax
 10019f7:	01 d0                	add    %edx,%eax
 10019f9:	a3 44 49 00 01       	mov    %eax,0x1004944
 10019fe:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1001a01:	c9                   	leave  
 1001a02:	c3                   	ret    
 1001a03:	90                   	nop

01001a04 <savechar>:
 1001a04:	55                   	push   %ebp
 1001a05:	89 e5                	mov    %esp,%ebp
 1001a07:	83 ec 10             	sub    $0x10,%esp
 1001a0a:	8b 45 08             	mov    0x8(%ebp),%eax
 1001a0d:	89 45 fc             	mov    %eax,-0x4(%ebp)
 1001a10:	8b 45 fc             	mov    -0x4(%ebp),%eax
 1001a13:	8b 40 08             	mov    0x8(%eax),%eax
 1001a16:	83 f8 ff             	cmp    $0xffffffff,%eax
 1001a19:	74 10                	je     1001a2b <savechar+0x27>
 1001a1b:	8b 45 fc             	mov    -0x4(%ebp),%eax
 1001a1e:	8b 50 04             	mov    0x4(%eax),%edx
 1001a21:	8b 45 fc             	mov    -0x4(%ebp),%eax
 1001a24:	8b 40 08             	mov    0x8(%eax),%eax
 1001a27:	39 c2                	cmp    %eax,%edx
 1001a29:	74 28                	je     1001a53 <savechar+0x4f>
 1001a2b:	8b 45 fc             	mov    -0x4(%ebp),%eax
 1001a2e:	8b 40 04             	mov    0x4(%eax),%eax
 1001a31:	8d 50 01             	lea    0x1(%eax),%edx
 1001a34:	8b 45 fc             	mov    -0x4(%ebp),%eax
 1001a37:	89 50 04             	mov    %edx,0x4(%eax)
 1001a3a:	8b 45 fc             	mov    -0x4(%ebp),%eax
 1001a3d:	8b 00                	mov    (%eax),%eax
 1001a3f:	8b 55 0c             	mov    0xc(%ebp),%edx
 1001a42:	88 10                	mov    %dl,(%eax)
 1001a44:	8b 45 fc             	mov    -0x4(%ebp),%eax
 1001a47:	8b 00                	mov    (%eax),%eax
 1001a49:	8d 50 01             	lea    0x1(%eax),%edx
 1001a4c:	8b 45 fc             	mov    -0x4(%ebp),%eax
 1001a4f:	89 10                	mov    %edx,(%eax)
 1001a51:	eb 01                	jmp    1001a54 <savechar+0x50>
 1001a53:	90                   	nop
 1001a54:	c9                   	leave  
 1001a55:	c3                   	ret    

01001a56 <vsprintf>:
 1001a56:	55                   	push   %ebp
 1001a57:	89 e5                	mov    %esp,%ebp
 1001a59:	83 ec 38             	sub    $0x38,%esp
 1001a5c:	c7 45 f4 ff ff ff ff 	movl   $0xffffffff,-0xc(%ebp)
 1001a63:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 1001a6a:	8b 45 08             	mov    0x8(%ebp),%eax
 1001a6d:	89 45 ec             	mov    %eax,-0x14(%ebp)
 1001a70:	8d 45 ec             	lea    -0x14(%ebp),%eax
 1001a73:	89 44 24 10          	mov    %eax,0x10(%esp)
 1001a77:	c7 44 24 0c 04 1a 00 	movl   $0x1001a04,0xc(%esp)
 1001a7e:	01 
 1001a7f:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
 1001a86:	00 
 1001a87:	8b 45 10             	mov    0x10(%ebp),%eax
 1001a8a:	89 44 24 04          	mov    %eax,0x4(%esp)
 1001a8e:	8b 45 0c             	mov    0xc(%ebp),%eax
 1001a91:	89 04 24             	mov    %eax,(%esp)
 1001a94:	e8 c8 01 00 00       	call   1001c61 <_doprnt>
 1001a99:	8b 45 ec             	mov    -0x14(%ebp),%eax
 1001a9c:	c6 00 00             	movb   $0x0,(%eax)
 1001a9f:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1001aa2:	c9                   	leave  
 1001aa3:	c3                   	ret    

01001aa4 <vsnprintf>:
 1001aa4:	55                   	push   %ebp
 1001aa5:	89 e5                	mov    %esp,%ebp
 1001aa7:	83 ec 38             	sub    $0x38,%esp
 1001aaa:	8b 45 0c             	mov    0xc(%ebp),%eax
 1001aad:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1001ab0:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 1001ab7:	8b 45 08             	mov    0x8(%ebp),%eax
 1001aba:	89 45 ec             	mov    %eax,-0x14(%ebp)
 1001abd:	8d 45 ec             	lea    -0x14(%ebp),%eax
 1001ac0:	89 44 24 10          	mov    %eax,0x10(%esp)
 1001ac4:	c7 44 24 0c 04 1a 00 	movl   $0x1001a04,0xc(%esp)
 1001acb:	01 
 1001acc:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
 1001ad3:	00 
 1001ad4:	8b 45 14             	mov    0x14(%ebp),%eax
 1001ad7:	89 44 24 04          	mov    %eax,0x4(%esp)
 1001adb:	8b 45 10             	mov    0x10(%ebp),%eax
 1001ade:	89 04 24             	mov    %eax,(%esp)
 1001ae1:	e8 7b 01 00 00       	call   1001c61 <_doprnt>
 1001ae6:	8b 45 ec             	mov    -0x14(%ebp),%eax
 1001ae9:	c6 00 00             	movb   $0x0,(%eax)
 1001aec:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1001aef:	c9                   	leave  
 1001af0:	c3                   	ret    

01001af1 <sprintf>:
 1001af1:	55                   	push   %ebp
 1001af2:	89 e5                	mov    %esp,%ebp
 1001af4:	83 ec 28             	sub    $0x28,%esp
 1001af7:	8d 45 0c             	lea    0xc(%ebp),%eax
 1001afa:	83 c0 04             	add    $0x4,%eax
 1001afd:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1001b00:	8b 45 0c             	mov    0xc(%ebp),%eax
 1001b03:	8b 55 f4             	mov    -0xc(%ebp),%edx
 1001b06:	89 54 24 08          	mov    %edx,0x8(%esp)
 1001b0a:	89 44 24 04          	mov    %eax,0x4(%esp)
 1001b0e:	8b 45 08             	mov    0x8(%ebp),%eax
 1001b11:	89 04 24             	mov    %eax,(%esp)
 1001b14:	e8 3d ff ff ff       	call   1001a56 <vsprintf>
 1001b19:	89 45 f0             	mov    %eax,-0x10(%ebp)
 1001b1c:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1001b1f:	c9                   	leave  
 1001b20:	c3                   	ret    

01001b21 <snprintf>:
 1001b21:	55                   	push   %ebp
 1001b22:	89 e5                	mov    %esp,%ebp
 1001b24:	83 ec 28             	sub    $0x28,%esp
 1001b27:	8d 45 10             	lea    0x10(%ebp),%eax
 1001b2a:	83 c0 04             	add    $0x4,%eax
 1001b2d:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1001b30:	8b 45 10             	mov    0x10(%ebp),%eax
 1001b33:	8b 55 f4             	mov    -0xc(%ebp),%edx
 1001b36:	89 54 24 0c          	mov    %edx,0xc(%esp)
 1001b3a:	89 44 24 08          	mov    %eax,0x8(%esp)
 1001b3e:	8b 45 0c             	mov    0xc(%ebp),%eax
 1001b41:	89 44 24 04          	mov    %eax,0x4(%esp)
 1001b45:	8b 45 08             	mov    0x8(%ebp),%eax
 1001b48:	89 04 24             	mov    %eax,(%esp)
 1001b4b:	e8 54 ff ff ff       	call   1001aa4 <vsnprintf>
 1001b50:	89 45 f0             	mov    %eax,-0x10(%ebp)
 1001b53:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1001b56:	c9                   	leave  
 1001b57:	c3                   	ret    

01001b58 <simicsPrint>:
 1001b58:	55                   	push   %ebp
 1001b59:	89 e5                	mov    %esp,%ebp
 1001b5b:	53                   	push   %ebx
 1001b5c:	50                   	push   %eax
 1001b5d:	53                   	push   %ebx
 1001b5e:	8b 45 08             	mov    0x8(%ebp),%eax
 1001b61:	bb 0f d0 ad 1b       	mov    $0x1badd00f,%ebx
 1001b66:	66 87 db             	xchg   %bx,%bx
 1001b69:	5b                   	pop    %ebx
 1001b6a:	58                   	pop    %eax
 1001b6b:	5b                   	pop    %ebx
 1001b6c:	5d                   	pop    %ebp
 1001b6d:	c3                   	ret    

01001b6e <lprintf_kern>:
 1001b6e:	55                   	push   %ebp
 1001b6f:	89 e5                	mov    %esp,%ebp
 1001b71:	81 ec 28 01 00 00    	sub    $0x128,%esp
 1001b77:	8d 45 0c             	lea    0xc(%ebp),%eax
 1001b7a:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1001b7d:	8b 45 08             	mov    0x8(%ebp),%eax
 1001b80:	8b 55 f4             	mov    -0xc(%ebp),%edx
 1001b83:	89 54 24 0c          	mov    %edx,0xc(%esp)
 1001b87:	89 44 24 08          	mov    %eax,0x8(%esp)
 1001b8b:	c7 44 24 04 ff 00 00 	movl   $0xff,0x4(%esp)
 1001b92:	00 
 1001b93:	8d 85 f4 fe ff ff    	lea    -0x10c(%ebp),%eax
 1001b99:	89 04 24             	mov    %eax,(%esp)
 1001b9c:	e8 03 ff ff ff       	call   1001aa4 <vsnprintf>
 1001ba1:	8d 85 f4 fe ff ff    	lea    -0x10c(%ebp),%eax
 1001ba7:	89 04 24             	mov    %eax,(%esp)
 1001baa:	e8 a9 ff ff ff       	call   1001b58 <simicsPrint>
 1001baf:	c9                   	leave  
 1001bb0:	c3                   	ret    

01001bb1 <lprintf>:
 1001bb1:	55                   	push   %ebp
 1001bb2:	89 e5                	mov    %esp,%ebp
 1001bb4:	81 ec 28 01 00 00    	sub    $0x128,%esp
 1001bba:	8d 45 0c             	lea    0xc(%ebp),%eax
 1001bbd:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1001bc0:	8b 45 08             	mov    0x8(%ebp),%eax
 1001bc3:	8b 55 f4             	mov    -0xc(%ebp),%edx
 1001bc6:	89 54 24 0c          	mov    %edx,0xc(%esp)
 1001bca:	89 44 24 08          	mov    %eax,0x8(%esp)
 1001bce:	c7 44 24 04 ff 00 00 	movl   $0xff,0x4(%esp)
 1001bd5:	00 
 1001bd6:	8d 85 f4 fe ff ff    	lea    -0x10c(%ebp),%eax
 1001bdc:	89 04 24             	mov    %eax,(%esp)
 1001bdf:	e8 c0 fe ff ff       	call   1001aa4 <vsnprintf>
 1001be4:	8d 85 f4 fe ff ff    	lea    -0x10c(%ebp),%eax
 1001bea:	89 04 24             	mov    %eax,(%esp)
 1001bed:	e8 66 ff ff ff       	call   1001b58 <simicsPrint>
 1001bf2:	c9                   	leave  
 1001bf3:	c3                   	ret    

01001bf4 <printnum>:
 1001bf4:	55                   	push   %ebp
 1001bf5:	89 e5                	mov    %esp,%ebp
 1001bf7:	57                   	push   %edi
 1001bf8:	56                   	push   %esi
 1001bf9:	53                   	push   %ebx
 1001bfa:	83 ec 4c             	sub    $0x4c,%esp
 1001bfd:	8b 4d 08             	mov    0x8(%ebp),%ecx
 1001c00:	8b 7d 0c             	mov    0xc(%ebp),%edi
 1001c03:	8d 45 c8             	lea    -0x38(%ebp),%eax
 1001c06:	8d 58 1f             	lea    0x1f(%eax),%ebx
 1001c09:	89 fe                	mov    %edi,%esi
 1001c0b:	89 c8                	mov    %ecx,%eax
 1001c0d:	ba 00 00 00 00       	mov    $0x0,%edx
 1001c12:	f7 f6                	div    %esi
 1001c14:	89 d0                	mov    %edx,%eax
 1001c16:	0f b6 80 19 49 00 01 	movzbl 0x1004919(%eax),%eax
 1001c1d:	88 03                	mov    %al,(%ebx)
 1001c1f:	83 eb 01             	sub    $0x1,%ebx
 1001c22:	89 7d c4             	mov    %edi,-0x3c(%ebp)
 1001c25:	89 c8                	mov    %ecx,%eax
 1001c27:	ba 00 00 00 00       	mov    $0x0,%edx
 1001c2c:	f7 75 c4             	divl   -0x3c(%ebp)
 1001c2f:	89 c1                	mov    %eax,%ecx
 1001c31:	85 c9                	test   %ecx,%ecx
 1001c33:	75 d4                	jne    1001c09 <printnum+0x15>
 1001c35:	eb 15                	jmp    1001c4c <printnum+0x58>
 1001c37:	0f b6 03             	movzbl (%ebx),%eax
 1001c3a:	0f be c0             	movsbl %al,%eax
 1001c3d:	89 44 24 04          	mov    %eax,0x4(%esp)
 1001c41:	8b 45 14             	mov    0x14(%ebp),%eax
 1001c44:	89 04 24             	mov    %eax,(%esp)
 1001c47:	8b 45 10             	mov    0x10(%ebp),%eax
 1001c4a:	ff d0                	call   *%eax
 1001c4c:	83 c3 01             	add    $0x1,%ebx
 1001c4f:	8d 45 c8             	lea    -0x38(%ebp),%eax
 1001c52:	83 c0 20             	add    $0x20,%eax
 1001c55:	39 c3                	cmp    %eax,%ebx
 1001c57:	75 de                	jne    1001c37 <printnum+0x43>
 1001c59:	83 c4 4c             	add    $0x4c,%esp
 1001c5c:	5b                   	pop    %ebx
 1001c5d:	5e                   	pop    %esi
 1001c5e:	5f                   	pop    %edi
 1001c5f:	5d                   	pop    %ebp
 1001c60:	c3                   	ret    

01001c61 <_doprnt>:
 1001c61:	55                   	push   %ebp
 1001c62:	89 e5                	mov    %esp,%ebp
 1001c64:	57                   	push   %edi
 1001c65:	56                   	push   %esi
 1001c66:	53                   	push   %ebx
 1001c67:	81 ec 8c 00 00 00    	sub    $0x8c,%esp
 1001c6d:	8b 45 08             	mov    0x8(%ebp),%eax
 1001c70:	89 45 b8             	mov    %eax,-0x48(%ebp)
 1001c73:	e9 9f 07 00 00       	jmp    1002417 <_doprnt+0x7b6>
 1001c78:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001c7b:	0f b6 00             	movzbl (%eax),%eax
 1001c7e:	3c 25                	cmp    $0x25,%al
 1001c80:	74 21                	je     1001ca3 <_doprnt+0x42>
 1001c82:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001c85:	0f b6 00             	movzbl (%eax),%eax
 1001c88:	0f be c0             	movsbl %al,%eax
 1001c8b:	83 45 b8 01          	addl   $0x1,-0x48(%ebp)
 1001c8f:	89 44 24 04          	mov    %eax,0x4(%esp)
 1001c93:	8b 45 18             	mov    0x18(%ebp),%eax
 1001c96:	89 04 24             	mov    %eax,(%esp)
 1001c99:	8b 45 14             	mov    0x14(%ebp),%eax
 1001c9c:	ff d0                	call   *%eax
 1001c9e:	e9 74 07 00 00       	jmp    1002417 <_doprnt+0x7b6>
 1001ca3:	83 45 b8 01          	addl   $0x1,-0x48(%ebp)
 1001ca7:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
 1001cae:	c7 45 e0 ff ff ff ff 	movl   $0xffffffff,-0x20(%ebp)
 1001cb5:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
 1001cbc:	c6 45 db 20          	movb   $0x20,-0x25(%ebp)
 1001cc0:	c7 45 cc 00 00 00 00 	movl   $0x0,-0x34(%ebp)
 1001cc7:	c7 45 c8 00 00 00 00 	movl   $0x0,-0x38(%ebp)
 1001cce:	c7 45 c4 00 00 00 00 	movl   $0x0,-0x3c(%ebp)
 1001cd5:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001cd8:	0f b6 00             	movzbl (%eax),%eax
 1001cdb:	3c 23                	cmp    $0x23,%al
 1001cdd:	75 0d                	jne    1001cec <_doprnt+0x8b>
 1001cdf:	c7 45 c4 01 00 00 00 	movl   $0x1,-0x3c(%ebp)
 1001ce6:	83 45 b8 01          	addl   $0x1,-0x48(%ebp)
 1001cea:	eb e9                	jmp    1001cd5 <_doprnt+0x74>
 1001cec:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001cef:	0f b6 00             	movzbl (%eax),%eax
 1001cf2:	3c 2d                	cmp    $0x2d,%al
 1001cf4:	75 0d                	jne    1001d03 <_doprnt+0xa2>
 1001cf6:	c7 45 dc 01 00 00 00 	movl   $0x1,-0x24(%ebp)
 1001cfd:	83 45 b8 01          	addl   $0x1,-0x48(%ebp)
 1001d01:	eb d2                	jmp    1001cd5 <_doprnt+0x74>
 1001d03:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001d06:	0f b6 00             	movzbl (%eax),%eax
 1001d09:	3c 2b                	cmp    $0x2b,%al
 1001d0b:	75 0d                	jne    1001d1a <_doprnt+0xb9>
 1001d0d:	c7 45 cc 2b 00 00 00 	movl   $0x2b,-0x34(%ebp)
 1001d14:	83 45 b8 01          	addl   $0x1,-0x48(%ebp)
 1001d18:	eb bb                	jmp    1001cd5 <_doprnt+0x74>
 1001d1a:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001d1d:	0f b6 00             	movzbl (%eax),%eax
 1001d20:	3c 20                	cmp    $0x20,%al
 1001d22:	75 13                	jne    1001d37 <_doprnt+0xd6>
 1001d24:	83 7d cc 00          	cmpl   $0x0,-0x34(%ebp)
 1001d28:	75 07                	jne    1001d31 <_doprnt+0xd0>
 1001d2a:	c7 45 cc 20 00 00 00 	movl   $0x20,-0x34(%ebp)
 1001d31:	83 45 b8 01          	addl   $0x1,-0x48(%ebp)
 1001d35:	eb 9e                	jmp    1001cd5 <_doprnt+0x74>
 1001d37:	90                   	nop
 1001d38:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001d3b:	0f b6 00             	movzbl (%eax),%eax
 1001d3e:	3c 30                	cmp    $0x30,%al
 1001d40:	75 08                	jne    1001d4a <_doprnt+0xe9>
 1001d42:	c6 45 db 30          	movb   $0x30,-0x25(%ebp)
 1001d46:	83 45 b8 01          	addl   $0x1,-0x48(%ebp)
 1001d4a:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001d4d:	0f b6 00             	movzbl (%eax),%eax
 1001d50:	3c 2f                	cmp    $0x2f,%al
 1001d52:	7e 45                	jle    1001d99 <_doprnt+0x138>
 1001d54:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001d57:	0f b6 00             	movzbl (%eax),%eax
 1001d5a:	3c 39                	cmp    $0x39,%al
 1001d5c:	7f 3b                	jg     1001d99 <_doprnt+0x138>
 1001d5e:	eb 23                	jmp    1001d83 <_doprnt+0x122>
 1001d60:	8b 55 e4             	mov    -0x1c(%ebp),%edx
 1001d63:	89 d0                	mov    %edx,%eax
 1001d65:	c1 e0 02             	shl    $0x2,%eax
 1001d68:	01 d0                	add    %edx,%eax
 1001d6a:	01 c0                	add    %eax,%eax
 1001d6c:	89 c2                	mov    %eax,%edx
 1001d6e:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001d71:	0f b6 00             	movzbl (%eax),%eax
 1001d74:	0f be c0             	movsbl %al,%eax
 1001d77:	83 e8 30             	sub    $0x30,%eax
 1001d7a:	01 d0                	add    %edx,%eax
 1001d7c:	89 45 e4             	mov    %eax,-0x1c(%ebp)
 1001d7f:	83 45 b8 01          	addl   $0x1,-0x48(%ebp)
 1001d83:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001d86:	0f b6 00             	movzbl (%eax),%eax
 1001d89:	3c 2f                	cmp    $0x2f,%al
 1001d8b:	7e 3f                	jle    1001dcc <_doprnt+0x16b>
 1001d8d:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001d90:	0f b6 00             	movzbl (%eax),%eax
 1001d93:	3c 39                	cmp    $0x39,%al
 1001d95:	7e c9                	jle    1001d60 <_doprnt+0xff>
 1001d97:	eb 33                	jmp    1001dcc <_doprnt+0x16b>
 1001d99:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001d9c:	0f b6 00             	movzbl (%eax),%eax
 1001d9f:	3c 2a                	cmp    $0x2a,%al
 1001da1:	75 2a                	jne    1001dcd <_doprnt+0x16c>
 1001da3:	83 45 0c 04          	addl   $0x4,0xc(%ebp)
 1001da7:	8b 45 0c             	mov    0xc(%ebp),%eax
 1001daa:	8b 40 fc             	mov    -0x4(%eax),%eax
 1001dad:	89 45 e4             	mov    %eax,-0x1c(%ebp)
 1001db0:	83 45 b8 01          	addl   $0x1,-0x48(%ebp)
 1001db4:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
 1001db8:	79 13                	jns    1001dcd <_doprnt+0x16c>
 1001dba:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
 1001dbe:	0f 94 c0             	sete   %al
 1001dc1:	0f b6 c0             	movzbl %al,%eax
 1001dc4:	89 45 dc             	mov    %eax,-0x24(%ebp)
 1001dc7:	f7 5d e4             	negl   -0x1c(%ebp)
 1001dca:	eb 01                	jmp    1001dcd <_doprnt+0x16c>
 1001dcc:	90                   	nop
 1001dcd:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001dd0:	0f b6 00             	movzbl (%eax),%eax
 1001dd3:	3c 2e                	cmp    $0x2e,%al
 1001dd5:	75 78                	jne    1001e4f <_doprnt+0x1ee>
 1001dd7:	83 45 b8 01          	addl   $0x1,-0x48(%ebp)
 1001ddb:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001dde:	0f b6 00             	movzbl (%eax),%eax
 1001de1:	3c 2f                	cmp    $0x2f,%al
 1001de3:	7e 4c                	jle    1001e31 <_doprnt+0x1d0>
 1001de5:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001de8:	0f b6 00             	movzbl (%eax),%eax
 1001deb:	3c 39                	cmp    $0x39,%al
 1001ded:	7f 42                	jg     1001e31 <_doprnt+0x1d0>
 1001def:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
 1001df6:	eb 23                	jmp    1001e1b <_doprnt+0x1ba>
 1001df8:	8b 55 e0             	mov    -0x20(%ebp),%edx
 1001dfb:	89 d0                	mov    %edx,%eax
 1001dfd:	c1 e0 02             	shl    $0x2,%eax
 1001e00:	01 d0                	add    %edx,%eax
 1001e02:	01 c0                	add    %eax,%eax
 1001e04:	89 c2                	mov    %eax,%edx
 1001e06:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001e09:	0f b6 00             	movzbl (%eax),%eax
 1001e0c:	0f be c0             	movsbl %al,%eax
 1001e0f:	83 e8 30             	sub    $0x30,%eax
 1001e12:	01 d0                	add    %edx,%eax
 1001e14:	89 45 e0             	mov    %eax,-0x20(%ebp)
 1001e17:	83 45 b8 01          	addl   $0x1,-0x48(%ebp)
 1001e1b:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001e1e:	0f b6 00             	movzbl (%eax),%eax
 1001e21:	3c 2f                	cmp    $0x2f,%al
 1001e23:	7e 29                	jle    1001e4e <_doprnt+0x1ed>
 1001e25:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001e28:	0f b6 00             	movzbl (%eax),%eax
 1001e2b:	3c 39                	cmp    $0x39,%al
 1001e2d:	7e c9                	jle    1001df8 <_doprnt+0x197>
 1001e2f:	eb 1d                	jmp    1001e4e <_doprnt+0x1ed>
 1001e31:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001e34:	0f b6 00             	movzbl (%eax),%eax
 1001e37:	3c 2a                	cmp    $0x2a,%al
 1001e39:	75 14                	jne    1001e4f <_doprnt+0x1ee>
 1001e3b:	83 45 0c 04          	addl   $0x4,0xc(%ebp)
 1001e3f:	8b 45 0c             	mov    0xc(%ebp),%eax
 1001e42:	8b 40 fc             	mov    -0x4(%eax),%eax
 1001e45:	89 45 e0             	mov    %eax,-0x20(%ebp)
 1001e48:	83 45 b8 01          	addl   $0x1,-0x48(%ebp)
 1001e4c:	eb 01                	jmp    1001e4f <_doprnt+0x1ee>
 1001e4e:	90                   	nop
 1001e4f:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001e52:	0f b6 00             	movzbl (%eax),%eax
 1001e55:	3c 6c                	cmp    $0x6c,%al
 1001e57:	75 04                	jne    1001e5d <_doprnt+0x1fc>
 1001e59:	83 45 b8 01          	addl   $0x1,-0x48(%ebp)
 1001e5d:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%ebp)
 1001e64:	8b 45 b8             	mov    -0x48(%ebp),%eax
 1001e67:	0f b6 00             	movzbl (%eax),%eax
 1001e6a:	0f be c0             	movsbl %al,%eax
 1001e6d:	83 f8 7a             	cmp    $0x7a,%eax
 1001e70:	0f 87 79 05 00 00    	ja     10023ef <_doprnt+0x78e>
 1001e76:	8b 04 85 38 2a 00 01 	mov    0x1002a38(,%eax,4),%eax
 1001e7d:	ff e0                	jmp    *%eax
 1001e7f:	83 45 0c 04          	addl   $0x4,0xc(%ebp)
 1001e83:	8b 45 0c             	mov    0xc(%ebp),%eax
 1001e86:	8b 40 fc             	mov    -0x4(%eax),%eax
 1001e89:	89 45 d0             	mov    %eax,-0x30(%ebp)
 1001e8c:	83 45 0c 04          	addl   $0x4,0xc(%ebp)
 1001e90:	8b 45 0c             	mov    0xc(%ebp),%eax
 1001e93:	8b 58 fc             	mov    -0x4(%eax),%ebx
 1001e96:	0f b6 03             	movzbl (%ebx),%eax
 1001e99:	0f be c0             	movsbl %al,%eax
 1001e9c:	89 45 bc             	mov    %eax,-0x44(%ebp)
 1001e9f:	83 c3 01             	add    $0x1,%ebx
 1001ea2:	8b 45 18             	mov    0x18(%ebp),%eax
 1001ea5:	89 44 24 0c          	mov    %eax,0xc(%esp)
 1001ea9:	8b 45 14             	mov    0x14(%ebp),%eax
 1001eac:	89 44 24 08          	mov    %eax,0x8(%esp)
 1001eb0:	8b 45 bc             	mov    -0x44(%ebp),%eax
 1001eb3:	89 44 24 04          	mov    %eax,0x4(%esp)
 1001eb7:	8b 45 d0             	mov    -0x30(%ebp),%eax
 1001eba:	89 04 24             	mov    %eax,(%esp)
 1001ebd:	e8 32 fd ff ff       	call   1001bf4 <printnum>
 1001ec2:	83 7d d0 00          	cmpl   $0x0,-0x30(%ebp)
 1001ec6:	0f 84 3d 05 00 00    	je     1002409 <_doprnt+0x7a8>
 1001ecc:	c7 45 b4 00 00 00 00 	movl   $0x0,-0x4c(%ebp)
 1001ed3:	e9 37 01 00 00       	jmp    100200f <_doprnt+0x3ae>
 1001ed8:	0f b6 03             	movzbl (%ebx),%eax
 1001edb:	3c 20                	cmp    $0x20,%al
 1001edd:	0f 8f ad 00 00 00    	jg     1001f90 <_doprnt+0x32f>
 1001ee3:	83 7d b4 00          	cmpl   $0x0,-0x4c(%ebp)
 1001ee7:	74 15                	je     1001efe <_doprnt+0x29d>
 1001ee9:	c7 44 24 04 2c 00 00 	movl   $0x2c,0x4(%esp)
 1001ef0:	00 
 1001ef1:	8b 45 18             	mov    0x18(%ebp),%eax
 1001ef4:	89 04 24             	mov    %eax,(%esp)
 1001ef7:	8b 45 14             	mov    0x14(%ebp),%eax
 1001efa:	ff d0                	call   *%eax
 1001efc:	eb 1a                	jmp    1001f18 <_doprnt+0x2b7>
 1001efe:	c7 44 24 04 3c 00 00 	movl   $0x3c,0x4(%esp)
 1001f05:	00 
 1001f06:	8b 45 18             	mov    0x18(%ebp),%eax
 1001f09:	89 04 24             	mov    %eax,(%esp)
 1001f0c:	8b 45 14             	mov    0x14(%ebp),%eax
 1001f0f:	ff d0                	call   *%eax
 1001f11:	c7 45 b4 01 00 00 00 	movl   $0x1,-0x4c(%ebp)
 1001f18:	0f b6 03             	movzbl (%ebx),%eax
 1001f1b:	0f be f8             	movsbl %al,%edi
 1001f1e:	83 c3 01             	add    $0x1,%ebx
 1001f21:	eb 16                	jmp    1001f39 <_doprnt+0x2d8>
 1001f23:	0f be 45 af          	movsbl -0x51(%ebp),%eax
 1001f27:	89 44 24 04          	mov    %eax,0x4(%esp)
 1001f2b:	8b 45 18             	mov    0x18(%ebp),%eax
 1001f2e:	89 04 24             	mov    %eax,(%esp)
 1001f31:	8b 45 14             	mov    0x14(%ebp),%eax
 1001f34:	ff d0                	call   *%eax
 1001f36:	83 c3 01             	add    $0x1,%ebx
 1001f39:	0f b6 03             	movzbl (%ebx),%eax
 1001f3c:	88 45 af             	mov    %al,-0x51(%ebp)
 1001f3f:	80 7d af 20          	cmpb   $0x20,-0x51(%ebp)
 1001f43:	7f de                	jg     1001f23 <_doprnt+0x2c2>
 1001f45:	8d 47 ff             	lea    -0x1(%edi),%eax
 1001f48:	8b 55 d0             	mov    -0x30(%ebp),%edx
 1001f4b:	89 55 80             	mov    %edx,-0x80(%ebp)
 1001f4e:	8b 55 80             	mov    -0x80(%ebp),%edx
 1001f51:	89 c1                	mov    %eax,%ecx
 1001f53:	d3 ea                	shr    %cl,%edx
 1001f55:	89 55 84             	mov    %edx,-0x7c(%ebp)
 1001f58:	89 f0                	mov    %esi,%eax
 1001f5a:	29 f8                	sub    %edi,%eax
 1001f5c:	ba 02 00 00 00       	mov    $0x2,%edx
 1001f61:	89 d6                	mov    %edx,%esi
 1001f63:	89 c1                	mov    %eax,%ecx
 1001f65:	d3 e6                	shl    %cl,%esi
 1001f67:	89 f0                	mov    %esi,%eax
 1001f69:	83 e8 01             	sub    $0x1,%eax
 1001f6c:	8b 55 84             	mov    -0x7c(%ebp),%edx
 1001f6f:	21 c2                	and    %eax,%edx
 1001f71:	8b 45 18             	mov    0x18(%ebp),%eax
 1001f74:	89 44 24 0c          	mov    %eax,0xc(%esp)
 1001f78:	8b 45 14             	mov    0x14(%ebp),%eax
 1001f7b:	89 44 24 08          	mov    %eax,0x8(%esp)
 1001f7f:	8b 45 bc             	mov    -0x44(%ebp),%eax
 1001f82:	89 44 24 04          	mov    %eax,0x4(%esp)
 1001f86:	89 14 24             	mov    %edx,(%esp)
 1001f89:	e8 66 fc ff ff       	call   1001bf4 <printnum>
 1001f8e:	eb 7f                	jmp    100200f <_doprnt+0x3ae>
 1001f90:	8d 46 ff             	lea    -0x1(%esi),%eax
 1001f93:	ba 01 00 00 00       	mov    $0x1,%edx
 1001f98:	89 d6                	mov    %edx,%esi
 1001f9a:	89 c1                	mov    %eax,%ecx
 1001f9c:	d3 e6                	shl    %cl,%esi
 1001f9e:	89 f0                	mov    %esi,%eax
 1001fa0:	23 45 d0             	and    -0x30(%ebp),%eax
 1001fa3:	85 c0                	test   %eax,%eax
 1001fa5:	74 60                	je     1002007 <_doprnt+0x3a6>
 1001fa7:	83 7d b4 00          	cmpl   $0x0,-0x4c(%ebp)
 1001fab:	74 15                	je     1001fc2 <_doprnt+0x361>
 1001fad:	c7 44 24 04 2c 00 00 	movl   $0x2c,0x4(%esp)
 1001fb4:	00 
 1001fb5:	8b 45 18             	mov    0x18(%ebp),%eax
 1001fb8:	89 04 24             	mov    %eax,(%esp)
 1001fbb:	8b 45 14             	mov    0x14(%ebp),%eax
 1001fbe:	ff d0                	call   *%eax
 1001fc0:	eb 32                	jmp    1001ff4 <_doprnt+0x393>
 1001fc2:	c7 44 24 04 3c 00 00 	movl   $0x3c,0x4(%esp)
 1001fc9:	00 
 1001fca:	8b 45 18             	mov    0x18(%ebp),%eax
 1001fcd:	89 04 24             	mov    %eax,(%esp)
 1001fd0:	8b 45 14             	mov    0x14(%ebp),%eax
 1001fd3:	ff d0                	call   *%eax
 1001fd5:	c7 45 b4 01 00 00 00 	movl   $0x1,-0x4c(%ebp)
 1001fdc:	eb 16                	jmp    1001ff4 <_doprnt+0x393>
 1001fde:	0f be 45 af          	movsbl -0x51(%ebp),%eax
 1001fe2:	89 44 24 04          	mov    %eax,0x4(%esp)
 1001fe6:	8b 45 18             	mov    0x18(%ebp),%eax
 1001fe9:	89 04 24             	mov    %eax,(%esp)
 1001fec:	8b 45 14             	mov    0x14(%ebp),%eax
 1001fef:	ff d0                	call   *%eax
 1001ff1:	83 c3 01             	add    $0x1,%ebx
 1001ff4:	0f b6 03             	movzbl (%ebx),%eax
 1001ff7:	88 45 af             	mov    %al,-0x51(%ebp)
 1001ffa:	80 7d af 20          	cmpb   $0x20,-0x51(%ebp)
 1001ffe:	7f de                	jg     1001fde <_doprnt+0x37d>
 1002000:	eb 0d                	jmp    100200f <_doprnt+0x3ae>
 1002002:	83 c3 01             	add    $0x1,%ebx
 1002005:	eb 01                	jmp    1002008 <_doprnt+0x3a7>
 1002007:	90                   	nop
 1002008:	0f b6 03             	movzbl (%ebx),%eax
 100200b:	3c 20                	cmp    $0x20,%al
 100200d:	7f f3                	jg     1002002 <_doprnt+0x3a1>
 100200f:	0f b6 03             	movzbl (%ebx),%eax
 1002012:	0f be f0             	movsbl %al,%esi
 1002015:	85 f6                	test   %esi,%esi
 1002017:	0f 95 c0             	setne  %al
 100201a:	83 c3 01             	add    $0x1,%ebx
 100201d:	84 c0                	test   %al,%al
 100201f:	0f 85 b3 fe ff ff    	jne    1001ed8 <_doprnt+0x277>
 1002025:	83 7d b4 00          	cmpl   $0x0,-0x4c(%ebp)
 1002029:	0f 84 dd 03 00 00    	je     100240c <_doprnt+0x7ab>
 100202f:	c7 44 24 04 3e 00 00 	movl   $0x3e,0x4(%esp)
 1002036:	00 
 1002037:	8b 45 18             	mov    0x18(%ebp),%eax
 100203a:	89 04 24             	mov    %eax,(%esp)
 100203d:	8b 45 14             	mov    0x14(%ebp),%eax
 1002040:	ff d0                	call   *%eax
 1002042:	e9 c5 03 00 00       	jmp    100240c <_doprnt+0x7ab>
 1002047:	83 45 0c 04          	addl   $0x4,0xc(%ebp)
 100204b:	8b 45 0c             	mov    0xc(%ebp),%eax
 100204e:	83 e8 04             	sub    $0x4,%eax
 1002051:	8b 00                	mov    (%eax),%eax
 1002053:	88 45 af             	mov    %al,-0x51(%ebp)
 1002056:	0f be 45 af          	movsbl -0x51(%ebp),%eax
 100205a:	89 44 24 04          	mov    %eax,0x4(%esp)
 100205e:	8b 45 18             	mov    0x18(%ebp),%eax
 1002061:	89 04 24             	mov    %eax,(%esp)
 1002064:	8b 45 14             	mov    0x14(%ebp),%eax
 1002067:	ff d0                	call   *%eax
 1002069:	e9 a5 03 00 00       	jmp    1002413 <_doprnt+0x7b2>
 100206e:	83 7d e0 ff          	cmpl   $0xffffffff,-0x20(%ebp)
 1002072:	75 07                	jne    100207b <_doprnt+0x41a>
 1002074:	c7 45 e0 ff ff ff 7f 	movl   $0x7fffffff,-0x20(%ebp)
 100207b:	83 45 0c 04          	addl   $0x4,0xc(%ebp)
 100207f:	8b 45 0c             	mov    0xc(%ebp),%eax
 1002082:	8b 58 fc             	mov    -0x4(%eax),%ebx
 1002085:	85 db                	test   %ebx,%ebx
 1002087:	75 05                	jne    100208e <_doprnt+0x42d>
 1002089:	bb 30 2a 00 01       	mov    $0x1002a30,%ebx
 100208e:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
 1002092:	7e 4a                	jle    10020de <_doprnt+0x47d>
 1002094:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
 1002098:	75 44                	jne    10020de <_doprnt+0x47d>
 100209a:	c7 45 d4 00 00 00 00 	movl   $0x0,-0x2c(%ebp)
 10020a1:	89 de                	mov    %ebx,%esi
 10020a3:	eb 07                	jmp    10020ac <_doprnt+0x44b>
 10020a5:	83 45 d4 01          	addl   $0x1,-0x2c(%ebp)
 10020a9:	83 c3 01             	add    $0x1,%ebx
 10020ac:	0f b6 03             	movzbl (%ebx),%eax
 10020af:	84 c0                	test   %al,%al
 10020b1:	74 08                	je     10020bb <_doprnt+0x45a>
 10020b3:	8b 45 d4             	mov    -0x2c(%ebp),%eax
 10020b6:	3b 45 e0             	cmp    -0x20(%ebp),%eax
 10020b9:	7c ea                	jl     10020a5 <_doprnt+0x444>
 10020bb:	89 f3                	mov    %esi,%ebx
 10020bd:	eb 17                	jmp    10020d6 <_doprnt+0x475>
 10020bf:	c7 44 24 04 20 00 00 	movl   $0x20,0x4(%esp)
 10020c6:	00 
 10020c7:	8b 45 18             	mov    0x18(%ebp),%eax
 10020ca:	89 04 24             	mov    %eax,(%esp)
 10020cd:	8b 45 14             	mov    0x14(%ebp),%eax
 10020d0:	ff d0                	call   *%eax
 10020d2:	83 45 d4 01          	addl   $0x1,-0x2c(%ebp)
 10020d6:	8b 45 d4             	mov    -0x2c(%ebp),%eax
 10020d9:	3b 45 e4             	cmp    -0x1c(%ebp),%eax
 10020dc:	7c e1                	jl     10020bf <_doprnt+0x45e>
 10020de:	c7 45 d4 00 00 00 00 	movl   $0x0,-0x2c(%ebp)
 10020e5:	eb 24                	jmp    100210b <_doprnt+0x4aa>
 10020e7:	83 45 d4 01          	addl   $0x1,-0x2c(%ebp)
 10020eb:	8b 45 d4             	mov    -0x2c(%ebp),%eax
 10020ee:	3b 45 e0             	cmp    -0x20(%ebp),%eax
 10020f1:	7f 21                	jg     1002114 <_doprnt+0x4b3>
 10020f3:	0f b6 03             	movzbl (%ebx),%eax
 10020f6:	0f be c0             	movsbl %al,%eax
 10020f9:	83 c3 01             	add    $0x1,%ebx
 10020fc:	89 44 24 04          	mov    %eax,0x4(%esp)
 1002100:	8b 45 18             	mov    0x18(%ebp),%eax
 1002103:	89 04 24             	mov    %eax,(%esp)
 1002106:	8b 45 14             	mov    0x14(%ebp),%eax
 1002109:	ff d0                	call   *%eax
 100210b:	0f b6 03             	movzbl (%ebx),%eax
 100210e:	84 c0                	test   %al,%al
 1002110:	75 d5                	jne    10020e7 <_doprnt+0x486>
 1002112:	eb 01                	jmp    1002115 <_doprnt+0x4b4>
 1002114:	90                   	nop
 1002115:	8b 45 d4             	mov    -0x2c(%ebp),%eax
 1002118:	3b 45 e4             	cmp    -0x1c(%ebp),%eax
 100211b:	0f 8d ee 02 00 00    	jge    100240f <_doprnt+0x7ae>
 1002121:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
 1002125:	0f 84 e4 02 00 00    	je     100240f <_doprnt+0x7ae>
 100212b:	eb 17                	jmp    1002144 <_doprnt+0x4e3>
 100212d:	c7 44 24 04 20 00 00 	movl   $0x20,0x4(%esp)
 1002134:	00 
 1002135:	8b 45 18             	mov    0x18(%ebp),%eax
 1002138:	89 04 24             	mov    %eax,(%esp)
 100213b:	8b 45 14             	mov    0x14(%ebp),%eax
 100213e:	ff d0                	call   *%eax
 1002140:	83 45 d4 01          	addl   $0x1,-0x2c(%ebp)
 1002144:	8b 45 d4             	mov    -0x2c(%ebp),%eax
 1002147:	3b 45 e4             	cmp    -0x1c(%ebp),%eax
 100214a:	7c e1                	jl     100212d <_doprnt+0x4cc>
 100214c:	e9 be 02 00 00       	jmp    100240f <_doprnt+0x7ae>
 1002151:	a1 4c 49 00 01       	mov    0x100494c,%eax
 1002156:	89 45 c0             	mov    %eax,-0x40(%ebp)
 1002159:	c7 45 bc 08 00 00 00 	movl   $0x8,-0x44(%ebp)
 1002160:	e9 cd 00 00 00       	jmp    1002232 <_doprnt+0x5d1>
 1002165:	a1 4c 49 00 01       	mov    0x100494c,%eax
 100216a:	89 45 c0             	mov    %eax,-0x40(%ebp)
 100216d:	c7 45 bc 0a 00 00 00 	movl   $0xa,-0x44(%ebp)
 1002174:	e9 87 00 00 00       	jmp    1002200 <_doprnt+0x59f>
 1002179:	a1 4c 49 00 01       	mov    0x100494c,%eax
 100217e:	89 45 c0             	mov    %eax,-0x40(%ebp)
 1002181:	c7 45 bc 0a 00 00 00 	movl   $0xa,-0x44(%ebp)
 1002188:	e9 a5 00 00 00       	jmp    1002232 <_doprnt+0x5d1>
 100218d:	c6 45 db 30          	movb   $0x30,-0x25(%ebp)
 1002191:	c7 45 e4 08 00 00 00 	movl   $0x8,-0x1c(%ebp)
 1002198:	c7 44 24 04 30 00 00 	movl   $0x30,0x4(%esp)
 100219f:	00 
 10021a0:	8b 45 18             	mov    0x18(%ebp),%eax
 10021a3:	89 04 24             	mov    %eax,(%esp)
 10021a6:	8b 45 14             	mov    0x14(%ebp),%eax
 10021a9:	ff d0                	call   *%eax
 10021ab:	c7 44 24 04 78 00 00 	movl   $0x78,0x4(%esp)
 10021b2:	00 
 10021b3:	8b 45 18             	mov    0x18(%ebp),%eax
 10021b6:	89 04 24             	mov    %eax,(%esp)
 10021b9:	8b 45 14             	mov    0x14(%ebp),%eax
 10021bc:	ff d0                	call   *%eax
 10021be:	a1 4c 49 00 01       	mov    0x100494c,%eax
 10021c3:	89 45 c0             	mov    %eax,-0x40(%ebp)
 10021c6:	c7 45 bc 10 00 00 00 	movl   $0x10,-0x44(%ebp)
 10021cd:	eb 63                	jmp    1002232 <_doprnt+0x5d1>
 10021cf:	a1 4c 49 00 01       	mov    0x100494c,%eax
 10021d4:	89 45 c0             	mov    %eax,-0x40(%ebp)
 10021d7:	c7 45 bc 10 00 00 00 	movl   $0x10,-0x44(%ebp)
 10021de:	eb 20                	jmp    1002200 <_doprnt+0x59f>
 10021e0:	a1 4c 49 00 01       	mov    0x100494c,%eax
 10021e5:	89 45 c0             	mov    %eax,-0x40(%ebp)
 10021e8:	8b 45 10             	mov    0x10(%ebp),%eax
 10021eb:	89 45 bc             	mov    %eax,-0x44(%ebp)
 10021ee:	eb 10                	jmp    1002200 <_doprnt+0x59f>
 10021f0:	a1 4c 49 00 01       	mov    0x100494c,%eax
 10021f5:	89 45 c0             	mov    %eax,-0x40(%ebp)
 10021f8:	8b 45 10             	mov    0x10(%ebp),%eax
 10021fb:	89 45 bc             	mov    %eax,-0x44(%ebp)
 10021fe:	eb 32                	jmp    1002232 <_doprnt+0x5d1>
 1002200:	83 45 0c 04          	addl   $0x4,0xc(%ebp)
 1002204:	8b 45 0c             	mov    0xc(%ebp),%eax
 1002207:	8b 40 fc             	mov    -0x4(%eax),%eax
 100220a:	89 45 d4             	mov    %eax,-0x2c(%ebp)
 100220d:	83 7d d4 00          	cmpl   $0x0,-0x2c(%ebp)
 1002211:	78 0e                	js     1002221 <_doprnt+0x5c0>
 1002213:	8b 45 d4             	mov    -0x2c(%ebp),%eax
 1002216:	89 45 d0             	mov    %eax,-0x30(%ebp)
 1002219:	8b 45 cc             	mov    -0x34(%ebp),%eax
 100221c:	89 45 c8             	mov    %eax,-0x38(%ebp)
 100221f:	eb 1f                	jmp    1002240 <_doprnt+0x5df>
 1002221:	8b 45 d4             	mov    -0x2c(%ebp),%eax
 1002224:	f7 d8                	neg    %eax
 1002226:	89 45 d0             	mov    %eax,-0x30(%ebp)
 1002229:	c7 45 c8 2d 00 00 00 	movl   $0x2d,-0x38(%ebp)
 1002230:	eb 0e                	jmp    1002240 <_doprnt+0x5df>
 1002232:	83 45 0c 04          	addl   $0x4,0xc(%ebp)
 1002236:	8b 45 0c             	mov    0xc(%ebp),%eax
 1002239:	8b 40 fc             	mov    -0x4(%eax),%eax
 100223c:	89 45 d0             	mov    %eax,-0x30(%ebp)
 100223f:	90                   	nop
 1002240:	8d 45 8f             	lea    -0x71(%ebp),%eax
 1002243:	8d 58 1f             	lea    0x1f(%eax),%ebx
 1002246:	c7 45 b0 00 00 00 00 	movl   $0x0,-0x50(%ebp)
 100224d:	83 7d c0 00          	cmpl   $0x0,-0x40(%ebp)
 1002251:	83 7d d0 00          	cmpl   $0x0,-0x30(%ebp)
 1002255:	74 22                	je     1002279 <_doprnt+0x618>
 1002257:	83 7d c4 00          	cmpl   $0x0,-0x3c(%ebp)
 100225b:	74 1c                	je     1002279 <_doprnt+0x618>
 100225d:	83 7d bc 08          	cmpl   $0x8,-0x44(%ebp)
 1002261:	75 09                	jne    100226c <_doprnt+0x60b>
 1002263:	c7 45 b0 31 2a 00 01 	movl   $0x1002a31,-0x50(%ebp)
 100226a:	eb 0d                	jmp    1002279 <_doprnt+0x618>
 100226c:	83 7d bc 10          	cmpl   $0x10,-0x44(%ebp)
 1002270:	75 07                	jne    1002279 <_doprnt+0x618>
 1002272:	c7 45 b0 33 2a 00 01 	movl   $0x1002a33,-0x50(%ebp)
 1002279:	8b 4d bc             	mov    -0x44(%ebp),%ecx
 100227c:	8b 45 d0             	mov    -0x30(%ebp),%eax
 100227f:	ba 00 00 00 00       	mov    $0x0,%edx
 1002284:	f7 f1                	div    %ecx
 1002286:	89 d0                	mov    %edx,%eax
 1002288:	0f b6 80 08 49 00 01 	movzbl 0x1004908(%eax),%eax
 100228f:	88 03                	mov    %al,(%ebx)
 1002291:	83 eb 01             	sub    $0x1,%ebx
 1002294:	8b 55 bc             	mov    -0x44(%ebp),%edx
 1002297:	89 55 80             	mov    %edx,-0x80(%ebp)
 100229a:	8b 45 d0             	mov    -0x30(%ebp),%eax
 100229d:	ba 00 00 00 00       	mov    $0x0,%edx
 10022a2:	f7 75 80             	divl   -0x80(%ebp)
 10022a5:	89 45 d0             	mov    %eax,-0x30(%ebp)
 10022a8:	83 6d e0 01          	subl   $0x1,-0x20(%ebp)
 10022ac:	83 7d d0 00          	cmpl   $0x0,-0x30(%ebp)
 10022b0:	75 c7                	jne    1002279 <_doprnt+0x618>
 10022b2:	89 d8                	mov    %ebx,%eax
 10022b4:	8d 55 8f             	lea    -0x71(%ebp),%edx
 10022b7:	83 c2 1f             	add    $0x1f,%edx
 10022ba:	29 d0                	sub    %edx,%eax
 10022bc:	01 45 e4             	add    %eax,-0x1c(%ebp)
 10022bf:	83 7d c8 00          	cmpl   $0x0,-0x38(%ebp)
 10022c3:	74 04                	je     10022c9 <_doprnt+0x668>
 10022c5:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
 10022c9:	83 7d b0 00          	cmpl   $0x0,-0x50(%ebp)
 10022cd:	74 0e                	je     10022dd <_doprnt+0x67c>
 10022cf:	8b 45 b0             	mov    -0x50(%ebp),%eax
 10022d2:	89 04 24             	mov    %eax,(%esp)
 10022d5:	e8 32 02 00 00       	call   100250c <strlen>
 10022da:	29 45 e4             	sub    %eax,-0x1c(%ebp)
 10022dd:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
 10022e1:	7e 06                	jle    10022e9 <_doprnt+0x688>
 10022e3:	8b 45 e0             	mov    -0x20(%ebp),%eax
 10022e6:	29 45 e4             	sub    %eax,-0x1c(%ebp)
 10022e9:	80 7d db 20          	cmpb   $0x20,-0x25(%ebp)
 10022ed:	75 25                	jne    1002314 <_doprnt+0x6b3>
 10022ef:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
 10022f3:	75 1f                	jne    1002314 <_doprnt+0x6b3>
 10022f5:	eb 13                	jmp    100230a <_doprnt+0x6a9>
 10022f7:	c7 44 24 04 20 00 00 	movl   $0x20,0x4(%esp)
 10022fe:	00 
 10022ff:	8b 45 18             	mov    0x18(%ebp),%eax
 1002302:	89 04 24             	mov    %eax,(%esp)
 1002305:	8b 45 14             	mov    0x14(%ebp),%eax
 1002308:	ff d0                	call   *%eax
 100230a:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
 100230e:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
 1002312:	79 e3                	jns    10022f7 <_doprnt+0x696>
 1002314:	83 7d c8 00          	cmpl   $0x0,-0x38(%ebp)
 1002318:	74 12                	je     100232c <_doprnt+0x6cb>
 100231a:	8b 45 c8             	mov    -0x38(%ebp),%eax
 100231d:	89 44 24 04          	mov    %eax,0x4(%esp)
 1002321:	8b 45 18             	mov    0x18(%ebp),%eax
 1002324:	89 04 24             	mov    %eax,(%esp)
 1002327:	8b 45 14             	mov    0x14(%ebp),%eax
 100232a:	ff d0                	call   *%eax
 100232c:	83 7d b0 00          	cmpl   $0x0,-0x50(%ebp)
 1002330:	74 3d                	je     100236f <_doprnt+0x70e>
 1002332:	eb 1c                	jmp    1002350 <_doprnt+0x6ef>
 1002334:	8b 45 b0             	mov    -0x50(%ebp),%eax
 1002337:	0f b6 00             	movzbl (%eax),%eax
 100233a:	0f be c0             	movsbl %al,%eax
 100233d:	83 45 b0 01          	addl   $0x1,-0x50(%ebp)
 1002341:	89 44 24 04          	mov    %eax,0x4(%esp)
 1002345:	8b 45 18             	mov    0x18(%ebp),%eax
 1002348:	89 04 24             	mov    %eax,(%esp)
 100234b:	8b 45 14             	mov    0x14(%ebp),%eax
 100234e:	ff d0                	call   *%eax
 1002350:	8b 45 b0             	mov    -0x50(%ebp),%eax
 1002353:	0f b6 00             	movzbl (%eax),%eax
 1002356:	84 c0                	test   %al,%al
 1002358:	75 da                	jne    1002334 <_doprnt+0x6d3>
 100235a:	eb 13                	jmp    100236f <_doprnt+0x70e>
 100235c:	c7 44 24 04 30 00 00 	movl   $0x30,0x4(%esp)
 1002363:	00 
 1002364:	8b 45 18             	mov    0x18(%ebp),%eax
 1002367:	89 04 24             	mov    %eax,(%esp)
 100236a:	8b 45 14             	mov    0x14(%ebp),%eax
 100236d:	ff d0                	call   *%eax
 100236f:	83 6d e0 01          	subl   $0x1,-0x20(%ebp)
 1002373:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
 1002377:	79 e3                	jns    100235c <_doprnt+0x6fb>
 1002379:	80 7d db 30          	cmpb   $0x30,-0x25(%ebp)
 100237d:	75 36                	jne    10023b5 <_doprnt+0x754>
 100237f:	eb 13                	jmp    1002394 <_doprnt+0x733>
 1002381:	c7 44 24 04 30 00 00 	movl   $0x30,0x4(%esp)
 1002388:	00 
 1002389:	8b 45 18             	mov    0x18(%ebp),%eax
 100238c:	89 04 24             	mov    %eax,(%esp)
 100238f:	8b 45 14             	mov    0x14(%ebp),%eax
 1002392:	ff d0                	call   *%eax
 1002394:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
 1002398:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
 100239c:	79 e3                	jns    1002381 <_doprnt+0x720>
 100239e:	eb 15                	jmp    10023b5 <_doprnt+0x754>
 10023a0:	0f b6 03             	movzbl (%ebx),%eax
 10023a3:	0f be c0             	movsbl %al,%eax
 10023a6:	89 44 24 04          	mov    %eax,0x4(%esp)
 10023aa:	8b 45 18             	mov    0x18(%ebp),%eax
 10023ad:	89 04 24             	mov    %eax,(%esp)
 10023b0:	8b 45 14             	mov    0x14(%ebp),%eax
 10023b3:	ff d0                	call   *%eax
 10023b5:	83 c3 01             	add    $0x1,%ebx
 10023b8:	8d 45 8f             	lea    -0x71(%ebp),%eax
 10023bb:	83 c0 20             	add    $0x20,%eax
 10023be:	39 c3                	cmp    %eax,%ebx
 10023c0:	75 de                	jne    10023a0 <_doprnt+0x73f>
 10023c2:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
 10023c6:	74 4a                	je     1002412 <_doprnt+0x7b1>
 10023c8:	eb 13                	jmp    10023dd <_doprnt+0x77c>
 10023ca:	c7 44 24 04 20 00 00 	movl   $0x20,0x4(%esp)
 10023d1:	00 
 10023d2:	8b 45 18             	mov    0x18(%ebp),%eax
 10023d5:	89 04 24             	mov    %eax,(%esp)
 10023d8:	8b 45 14             	mov    0x14(%ebp),%eax
 10023db:	ff d0                	call   *%eax
 10023dd:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
 10023e1:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
 10023e5:	79 e3                	jns    10023ca <_doprnt+0x769>
 10023e7:	eb 29                	jmp    1002412 <_doprnt+0x7b1>
 10023e9:	83 6d b8 01          	subl   $0x1,-0x48(%ebp)
 10023ed:	eb 24                	jmp    1002413 <_doprnt+0x7b2>
 10023ef:	8b 45 b8             	mov    -0x48(%ebp),%eax
 10023f2:	0f b6 00             	movzbl (%eax),%eax
 10023f5:	0f be c0             	movsbl %al,%eax
 10023f8:	89 44 24 04          	mov    %eax,0x4(%esp)
 10023fc:	8b 45 18             	mov    0x18(%ebp),%eax
 10023ff:	89 04 24             	mov    %eax,(%esp)
 1002402:	8b 45 14             	mov    0x14(%ebp),%eax
 1002405:	ff d0                	call   *%eax
 1002407:	eb 0a                	jmp    1002413 <_doprnt+0x7b2>
 1002409:	90                   	nop
 100240a:	eb 07                	jmp    1002413 <_doprnt+0x7b2>
 100240c:	90                   	nop
 100240d:	eb 04                	jmp    1002413 <_doprnt+0x7b2>
 100240f:	90                   	nop
 1002410:	eb 01                	jmp    1002413 <_doprnt+0x7b2>
 1002412:	90                   	nop
 1002413:	83 45 b8 01          	addl   $0x1,-0x48(%ebp)
 1002417:	8b 45 b8             	mov    -0x48(%ebp),%eax
 100241a:	0f b6 00             	movzbl (%eax),%eax
 100241d:	84 c0                	test   %al,%al
 100241f:	0f 85 53 f8 ff ff    	jne    1001c78 <_doprnt+0x17>
 1002425:	81 c4 8c 00 00 00    	add    $0x8c,%esp
 100242b:	5b                   	pop    %ebx
 100242c:	5e                   	pop    %esi
 100242d:	5f                   	pop    %edi
 100242e:	5d                   	pop    %ebp
 100242f:	c3                   	ret    

01002430 <exit>:
 1002430:	55                   	push   %ebp
 1002431:	89 e5                	mov    %esp,%ebp
 1002433:	83 ec 18             	sub    $0x18,%esp
 1002436:	8b 45 08             	mov    0x8(%ebp),%eax
 1002439:	89 04 24             	mov    %eax,(%esp)
 100243c:	e8 df 02 00 00       	call   1002720 <set_status>
 1002441:	e8 ea 02 00 00       	call   1002730 <vanish>
 1002446:	90                   	nop
 1002447:	90                   	nop

01002448 <panic>:
 1002448:	55                   	push   %ebp
 1002449:	89 e5                	mov    %esp,%ebp
 100244b:	83 ec 28             	sub    $0x28,%esp
 100244e:	8d 45 0c             	lea    0xc(%ebp),%eax
 1002451:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1002454:	8b 45 08             	mov    0x8(%ebp),%eax
 1002457:	8b 55 f4             	mov    -0xc(%ebp),%edx
 100245a:	89 54 24 04          	mov    %edx,0x4(%esp)
 100245e:	89 04 24             	mov    %eax,(%esp)
 1002461:	e8 9a 01 00 00       	call   1002600 <vprintf>
 1002466:	c7 04 24 24 2c 00 01 	movl   $0x1002c24,(%esp)
 100246d:	e8 e6 01 00 00       	call   1002658 <printf>
 1002472:	c7 04 24 ff ff ff ff 	movl   $0xffffffff,(%esp)
 1002479:	e8 b2 ff ff ff       	call   1002430 <exit>
 100247e:	90                   	nop
 100247f:	90                   	nop

01002480 <bcopy>:
 1002480:	55                   	push   %ebp
 1002481:	89 e5                	mov    %esp,%ebp
 1002483:	57                   	push   %edi
 1002484:	56                   	push   %esi
 1002485:	8b 75 08             	mov    0x8(%ebp),%esi
 1002488:	8b 7d 0c             	mov    0xc(%ebp),%edi

0100248b <bcopy_common>:
 100248b:	8b 55 10             	mov    0x10(%ebp),%edx
 100248e:	8d 04 16             	lea    (%esi,%edx,1),%eax
 1002491:	39 c7                	cmp    %eax,%edi
 1002493:	73 20                	jae    10024b5 <bcopy_common+0x2a>
 1002495:	8d 04 17             	lea    (%edi,%edx,1),%eax
 1002498:	39 c6                	cmp    %eax,%esi
 100249a:	73 19                	jae    10024b5 <bcopy_common+0x2a>
 100249c:	39 f7                	cmp    %esi,%edi
 100249e:	72 15                	jb     10024b5 <bcopy_common+0x2a>
 10024a0:	74 0c                	je     10024ae <bcopy_common+0x23>
 10024a2:	01 d6                	add    %edx,%esi
 10024a4:	4e                   	dec    %esi
 10024a5:	01 d7                	add    %edx,%edi
 10024a7:	4f                   	dec    %edi
 10024a8:	fd                   	std    
 10024a9:	89 d1                	mov    %edx,%ecx
 10024ab:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
 10024ad:	fc                   	cld    
 10024ae:	8b 45 08             	mov    0x8(%ebp),%eax
 10024b1:	5e                   	pop    %esi
 10024b2:	5f                   	pop    %edi
 10024b3:	c9                   	leave  
 10024b4:	c3                   	ret    
 10024b5:	fc                   	cld    
 10024b6:	89 d1                	mov    %edx,%ecx
 10024b8:	c1 f9 02             	sar    $0x2,%ecx
 10024bb:	78 09                	js     10024c6 <bcopy_common+0x3b>
 10024bd:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
 10024bf:	89 d1                	mov    %edx,%ecx
 10024c1:	83 e1 03             	and    $0x3,%ecx
 10024c4:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
 10024c6:	8b 45 08             	mov    0x8(%ebp),%eax
 10024c9:	5e                   	pop    %esi
 10024ca:	5f                   	pop    %edi
 10024cb:	c9                   	leave  
 10024cc:	c3                   	ret    
 10024cd:	8d 76 00             	lea    0x0(%esi),%esi

010024d0 <memcpy>:
 10024d0:	55                   	push   %ebp
 10024d1:	89 e5                	mov    %esp,%ebp
 10024d3:	57                   	push   %edi
 10024d4:	56                   	push   %esi
 10024d5:	8b 7d 08             	mov    0x8(%ebp),%edi
 10024d8:	8b 75 0c             	mov    0xc(%ebp),%esi
 10024db:	eb ae                	jmp    100248b <bcopy_common>
 10024dd:	90                   	nop
 10024de:	90                   	nop
 10024df:	90                   	nop

010024e0 <bzero>:
 10024e0:	57                   	push   %edi
 10024e1:	8b 7c 24 08          	mov    0x8(%esp),%edi
 10024e5:	8b 54 24 0c          	mov    0xc(%esp),%edx
 10024e9:	fc                   	cld    
 10024ea:	31 c0                	xor    %eax,%eax
 10024ec:	83 fa 10             	cmp    $0x10,%edx
 10024ef:	72 15                	jb     1002506 <L1>
 10024f1:	89 f9                	mov    %edi,%ecx
 10024f3:	f7 d9                	neg    %ecx
 10024f5:	83 e1 03             	and    $0x3,%ecx
 10024f8:	29 ca                	sub    %ecx,%edx
 10024fa:	f3 aa                	rep stos %al,%es:(%edi)
 10024fc:	89 d1                	mov    %edx,%ecx
 10024fe:	c1 e9 02             	shr    $0x2,%ecx
 1002501:	83 e2 03             	and    $0x3,%edx
 1002504:	f3 ab                	rep stos %eax,%es:(%edi)

01002506 <L1>:
 1002506:	89 d1                	mov    %edx,%ecx
 1002508:	f3 aa                	rep stos %al,%es:(%edi)
 100250a:	5f                   	pop    %edi
 100250b:	c3                   	ret    

0100250c <strlen>:
 100250c:	55                   	push   %ebp
 100250d:	89 e5                	mov    %esp,%ebp
 100250f:	83 ec 10             	sub    $0x10,%esp
 1002512:	8b 45 08             	mov    0x8(%ebp),%eax
 1002515:	89 45 fc             	mov    %eax,-0x4(%ebp)
 1002518:	90                   	nop
 1002519:	8b 45 08             	mov    0x8(%ebp),%eax
 100251c:	0f b6 00             	movzbl (%eax),%eax
 100251f:	84 c0                	test   %al,%al
 1002521:	0f 95 c0             	setne  %al
 1002524:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 1002528:	84 c0                	test   %al,%al
 100252a:	75 ed                	jne    1002519 <strlen+0xd>
 100252c:	8b 45 08             	mov    0x8(%ebp),%eax
 100252f:	83 e8 01             	sub    $0x1,%eax
 1002532:	89 c2                	mov    %eax,%edx
 1002534:	8b 45 fc             	mov    -0x4(%ebp),%eax
 1002537:	89 d1                	mov    %edx,%ecx
 1002539:	29 c1                	sub    %eax,%ecx
 100253b:	89 c8                	mov    %ecx,%eax
 100253d:	c9                   	leave  
 100253e:	c3                   	ret    
 100253f:	90                   	nop

01002540 <flush>:
 1002540:	55                   	push   %ebp
 1002541:	89 e5                	mov    %esp,%ebp
 1002543:	83 ec 18             	sub    $0x18,%esp
 1002546:	8b 55 08             	mov    0x8(%ebp),%edx
 1002549:	8b 45 08             	mov    0x8(%ebp),%eax
 100254c:	8b 80 80 00 00 00    	mov    0x80(%eax),%eax
 1002552:	89 54 24 04          	mov    %edx,0x4(%esp)
 1002556:	89 04 24             	mov    %eax,(%esp)
 1002559:	e8 7e 01 00 00       	call   10026dc <print>
 100255e:	8b 45 08             	mov    0x8(%ebp),%eax
 1002561:	c7 80 80 00 00 00 00 	movl   $0x0,0x80(%eax)
 1002568:	00 00 00 
 100256b:	c9                   	leave  
 100256c:	c3                   	ret    

0100256d <printf_char>:
 100256d:	55                   	push   %ebp
 100256e:	89 e5                	mov    %esp,%ebp
 1002570:	83 ec 28             	sub    $0x28,%esp
 1002573:	8b 45 08             	mov    0x8(%ebp),%eax
 1002576:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1002579:	83 7d 0c 0a          	cmpl   $0xa,0xc(%ebp)
 100257d:	75 2a                	jne    10025a9 <printf_char+0x3c>
 100257f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1002582:	8b 80 80 00 00 00    	mov    0x80(%eax),%eax
 1002588:	8b 55 f4             	mov    -0xc(%ebp),%edx
 100258b:	c6 04 02 00          	movb   $0x0,(%edx,%eax,1)
 100258f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1002592:	89 04 24             	mov    %eax,(%esp)
 1002595:	e8 06 01 00 00       	call   10026a0 <puts>
 100259a:	8b 45 f4             	mov    -0xc(%ebp),%eax
 100259d:	c7 80 80 00 00 00 00 	movl   $0x0,0x80(%eax)
 10025a4:	00 00 00 
 10025a7:	eb 55                	jmp    10025fe <printf_char+0x91>
 10025a9:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 10025ad:	74 0e                	je     10025bd <printf_char+0x50>
 10025af:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10025b2:	8b 80 80 00 00 00    	mov    0x80(%eax),%eax
 10025b8:	83 f8 7f             	cmp    $0x7f,%eax
 10025bb:	76 18                	jbe    10025d5 <printf_char+0x68>
 10025bd:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10025c0:	89 04 24             	mov    %eax,(%esp)
 10025c3:	e8 78 ff ff ff       	call   1002540 <flush>
 10025c8:	8b 45 0c             	mov    0xc(%ebp),%eax
 10025cb:	89 04 24             	mov    %eax,(%esp)
 10025ce:	e8 ad 00 00 00       	call   1002680 <putchar>
 10025d3:	eb 29                	jmp    10025fe <printf_char+0x91>
 10025d5:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10025d8:	8b 80 80 00 00 00    	mov    0x80(%eax),%eax
 10025de:	8b 55 0c             	mov    0xc(%ebp),%edx
 10025e1:	89 d1                	mov    %edx,%ecx
 10025e3:	8b 55 f4             	mov    -0xc(%ebp),%edx
 10025e6:	88 0c 02             	mov    %cl,(%edx,%eax,1)
 10025e9:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10025ec:	8b 80 80 00 00 00    	mov    0x80(%eax),%eax
 10025f2:	8d 50 01             	lea    0x1(%eax),%edx
 10025f5:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10025f8:	89 90 80 00 00 00    	mov    %edx,0x80(%eax)
 10025fe:	c9                   	leave  
 10025ff:	c3                   	ret    

01002600 <vprintf>:
 1002600:	55                   	push   %ebp
 1002601:	89 e5                	mov    %esp,%ebp
 1002603:	81 ec b8 00 00 00    	sub    $0xb8,%esp
 1002609:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 1002610:	8d 85 74 ff ff ff    	lea    -0x8c(%ebp),%eax
 1002616:	89 44 24 10          	mov    %eax,0x10(%esp)
 100261a:	c7 44 24 0c 6d 25 00 	movl   $0x100256d,0xc(%esp)
 1002621:	01 
 1002622:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
 1002629:	00 
 100262a:	8b 45 0c             	mov    0xc(%ebp),%eax
 100262d:	89 44 24 04          	mov    %eax,0x4(%esp)
 1002631:	8b 45 08             	mov    0x8(%ebp),%eax
 1002634:	89 04 24             	mov    %eax,(%esp)
 1002637:	e8 25 f6 ff ff       	call   1001c61 <_doprnt>
 100263c:	8b 45 f4             	mov    -0xc(%ebp),%eax
 100263f:	85 c0                	test   %eax,%eax
 1002641:	74 0e                	je     1002651 <vprintf+0x51>
 1002643:	8d 85 74 ff ff ff    	lea    -0x8c(%ebp),%eax
 1002649:	89 04 24             	mov    %eax,(%esp)
 100264c:	e8 ef fe ff ff       	call   1002540 <flush>
 1002651:	b8 00 00 00 00       	mov    $0x0,%eax
 1002656:	c9                   	leave  
 1002657:	c3                   	ret    

01002658 <printf>:
 1002658:	55                   	push   %ebp
 1002659:	89 e5                	mov    %esp,%ebp
 100265b:	83 ec 28             	sub    $0x28,%esp
 100265e:	8d 45 0c             	lea    0xc(%ebp),%eax
 1002661:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1002664:	8b 45 08             	mov    0x8(%ebp),%eax
 1002667:	8b 55 f4             	mov    -0xc(%ebp),%edx
 100266a:	89 54 24 04          	mov    %edx,0x4(%esp)
 100266e:	89 04 24             	mov    %eax,(%esp)
 1002671:	e8 8a ff ff ff       	call   1002600 <vprintf>
 1002676:	89 45 f0             	mov    %eax,-0x10(%ebp)
 1002679:	8b 45 f0             	mov    -0x10(%ebp),%eax
 100267c:	c9                   	leave  
 100267d:	c3                   	ret    
 100267e:	90                   	nop
 100267f:	90                   	nop

01002680 <putchar>:
 1002680:	55                   	push   %ebp
 1002681:	89 e5                	mov    %esp,%ebp
 1002683:	83 ec 18             	sub    $0x18,%esp
 1002686:	8d 45 08             	lea    0x8(%ebp),%eax
 1002689:	89 44 24 04          	mov    %eax,0x4(%esp)
 100268d:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
 1002694:	e8 43 00 00 00       	call   10026dc <print>
 1002699:	8b 45 08             	mov    0x8(%ebp),%eax
 100269c:	c9                   	leave  
 100269d:	c3                   	ret    
 100269e:	90                   	nop
 100269f:	90                   	nop

010026a0 <puts>:
 10026a0:	55                   	push   %ebp
 10026a1:	89 e5                	mov    %esp,%ebp
 10026a3:	83 ec 18             	sub    $0x18,%esp
 10026a6:	eb 15                	jmp    10026bd <puts+0x1d>
 10026a8:	8b 45 08             	mov    0x8(%ebp),%eax
 10026ab:	0f b6 00             	movzbl (%eax),%eax
 10026ae:	0f be c0             	movsbl %al,%eax
 10026b1:	89 04 24             	mov    %eax,(%esp)
 10026b4:	e8 c7 ff ff ff       	call   1002680 <putchar>
 10026b9:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 10026bd:	8b 45 08             	mov    0x8(%ebp),%eax
 10026c0:	0f b6 00             	movzbl (%eax),%eax
 10026c3:	84 c0                	test   %al,%al
 10026c5:	75 e1                	jne    10026a8 <puts+0x8>
 10026c7:	c7 04 24 0a 00 00 00 	movl   $0xa,(%esp)
 10026ce:	e8 ad ff ff ff       	call   1002680 <putchar>
 10026d3:	b8 00 00 00 00       	mov    $0x0,%eax
 10026d8:	c9                   	leave  
 10026d9:	c3                   	ret    
 10026da:	90                   	nop
 10026db:	90                   	nop

010026dc <print>:
 10026dc:	55                   	push   %ebp
 10026dd:	89 e5                	mov    %esp,%ebp
 10026df:	56                   	push   %esi
 10026e0:	53                   	push   %ebx
 10026e1:	83 ec 14             	sub    $0x14,%esp
 10026e4:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 10026eb:	8b 45 08             	mov    0x8(%ebp),%eax
 10026ee:	89 45 e8             	mov    %eax,-0x18(%ebp)
 10026f1:	8b 45 0c             	mov    0xc(%ebp),%eax
 10026f4:	89 45 ec             	mov    %eax,-0x14(%ebp)
 10026f7:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 10026fe:	8d 75 e8             	lea    -0x18(%ebp),%esi
 1002701:	89 75 e4             	mov    %esi,-0x1c(%ebp)
 1002704:	8b 75 e4             	mov    -0x1c(%ebp),%esi
 1002707:	cd 4e                	int    $0x4e
 1002709:	89 c3                	mov    %eax,%ebx
 100270b:	89 5d f0             	mov    %ebx,-0x10(%ebp)
 100270e:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1002711:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1002714:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1002717:	83 c4 14             	add    $0x14,%esp
 100271a:	5b                   	pop    %ebx
 100271b:	5e                   	pop    %esi
 100271c:	5d                   	pop    %ebp
 100271d:	c3                   	ret    
 100271e:	90                   	nop
 100271f:	90                   	nop

01002720 <set_status>:
 1002720:	55                   	push   %ebp
 1002721:	89 e5                	mov    %esp,%ebp
 1002723:	56                   	push   %esi
 1002724:	8b 45 08             	mov    0x8(%ebp),%eax
 1002727:	89 c6                	mov    %eax,%esi
 1002729:	cd 59                	int    $0x59
 100272b:	5e                   	pop    %esi
 100272c:	5d                   	pop    %ebp
 100272d:	c3                   	ret    
 100272e:	90                   	nop
 100272f:	90                   	nop

01002730 <vanish>:
 1002730:	55                   	push   %ebp
 1002731:	89 e5                	mov    %esp,%ebp
 1002733:	83 ec 10             	sub    $0x10,%esp
 1002736:	c7 45 fc a6 ee ff ff 	movl   $0xffffeea6,-0x4(%ebp)
 100273d:	cd 60                	int    $0x60
 100273f:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 1002746:	8b 45 fc             	mov    -0x4(%ebp),%eax
 1002749:	89 c2                	mov    %eax,%edx
 100274b:	c1 fa 1f             	sar    $0x1f,%edx
 100274e:	f7 7d fc             	idivl  -0x4(%ebp)
 1002751:	89 45 fc             	mov    %eax,-0x4(%ebp)
 1002754:	8b 45 fc             	mov    -0x4(%ebp),%eax
 1002757:	8b 55 fc             	mov    -0x4(%ebp),%edx
 100275a:	89 10                	mov    %edx,(%eax)
 100275c:	c9                   	leave  
 100275d:	c3                   	ret    
 100275e:	90                   	nop
 100275f:	90                   	nop

01002760 <yield>:
 1002760:	55                   	push   %ebp
 1002761:	89 e5                	mov    %esp,%ebp
 1002763:	56                   	push   %esi
 1002764:	53                   	push   %ebx
 1002765:	83 ec 14             	sub    $0x14,%esp
 1002768:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 100276f:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 1002776:	8b 75 08             	mov    0x8(%ebp),%esi
 1002779:	89 75 e4             	mov    %esi,-0x1c(%ebp)
 100277c:	8b 75 e4             	mov    -0x1c(%ebp),%esi
 100277f:	cd 45                	int    $0x45
 1002781:	89 c3                	mov    %eax,%ebx
 1002783:	89 5d f0             	mov    %ebx,-0x10(%ebp)
 1002786:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1002789:	89 45 f4             	mov    %eax,-0xc(%ebp)
 100278c:	8b 45 f4             	mov    -0xc(%ebp),%eax
 100278f:	83 c4 14             	add    $0x14,%esp
 1002792:	5b                   	pop    %ebx
 1002793:	5e                   	pop    %esi
 1002794:	5d                   	pop    %ebp
 1002795:	c3                   	ret    
 1002796:	90                   	nop
 1002797:	90                   	nop

01002798 <gettid>:
 1002798:	55                   	push   %ebp
 1002799:	89 e5                	mov    %esp,%ebp
 100279b:	53                   	push   %ebx
 100279c:	83 ec 10             	sub    $0x10,%esp
 100279f:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%ebp)
 10027a6:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 10027ad:	cd 48                	int    $0x48
 10027af:	89 c3                	mov    %eax,%ebx
 10027b1:	89 5d f4             	mov    %ebx,-0xc(%ebp)
 10027b4:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10027b7:	89 45 f8             	mov    %eax,-0x8(%ebp)
 10027ba:	8b 45 f8             	mov    -0x8(%ebp),%eax
 10027bd:	83 c4 10             	add    $0x10,%esp
 10027c0:	5b                   	pop    %ebx
 10027c1:	5d                   	pop    %ebp
 10027c2:	c3                   	ret    
 10027c3:	90                   	nop

010027c4 <sleep>:
 10027c4:	55                   	push   %ebp
 10027c5:	89 e5                	mov    %esp,%ebp
 10027c7:	56                   	push   %esi
 10027c8:	53                   	push   %ebx
 10027c9:	83 ec 14             	sub    $0x14,%esp
 10027cc:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 10027d3:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 10027da:	8b 75 08             	mov    0x8(%ebp),%esi
 10027dd:	89 75 e4             	mov    %esi,-0x1c(%ebp)
 10027e0:	8b 75 e4             	mov    -0x1c(%ebp),%esi
 10027e3:	cd 4b                	int    $0x4b
 10027e5:	89 c3                	mov    %eax,%ebx
 10027e7:	89 5d f0             	mov    %ebx,-0x10(%ebp)
 10027ea:	8b 45 f0             	mov    -0x10(%ebp),%eax
 10027ed:	89 45 f4             	mov    %eax,-0xc(%ebp)
 10027f0:	8b 45 f4             	mov    -0xc(%ebp),%eax
 10027f3:	83 c4 14             	add    $0x14,%esp
 10027f6:	5b                   	pop    %ebx
 10027f7:	5e                   	pop    %esi
 10027f8:	5d                   	pop    %ebp
 10027f9:	c3                   	ret    
 10027fa:	90                   	nop
 10027fb:	90                   	nop

010027fc <new_pages>:
 10027fc:	55                   	push   %ebp
 10027fd:	89 e5                	mov    %esp,%ebp
 10027ff:	56                   	push   %esi
 1002800:	53                   	push   %ebx
 1002801:	83 ec 14             	sub    $0x14,%esp
 1002804:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 100280b:	8b 45 08             	mov    0x8(%ebp),%eax
 100280e:	89 45 e8             	mov    %eax,-0x18(%ebp)
 1002811:	8b 45 0c             	mov    0xc(%ebp),%eax
 1002814:	89 45 ec             	mov    %eax,-0x14(%ebp)
 1002817:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 100281e:	8d 75 e8             	lea    -0x18(%ebp),%esi
 1002821:	89 75 e4             	mov    %esi,-0x1c(%ebp)
 1002824:	8b 75 e4             	mov    -0x1c(%ebp),%esi
 1002827:	cd 49                	int    $0x49
 1002829:	89 c3                	mov    %eax,%ebx
 100282b:	89 5d f0             	mov    %ebx,-0x10(%ebp)
 100282e:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1002831:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1002834:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1002837:	83 c4 14             	add    $0x14,%esp
 100283a:	5b                   	pop    %ebx
 100283b:	5e                   	pop    %esi
 100283c:	5d                   	pop    %ebp
 100283d:	c3                   	ret    
