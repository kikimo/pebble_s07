#!/usr/bin/env ruby
# symtabgen.rb: Generates a table of function names & argument types.
#
# Author: Mike Kasick <mkasick@andrew.cmu.edu>
# Date:   Sat, 20 Jan 2007 23:10:32 -0500
#
# Based on Michael Ashley-Rollman's symtabgen.pl for 15-410 S07.
#
# As mpa states:
#     This program presumes that there is a global variable
#     called functions and that it is an array of functsym_t
#     in the executable on which it is called. It fills in this
#     array with information from the stabs.
#
# Warning:
#     File names should not contain any "special" characters, otherwise
#     they'll be eaten by the shell and undefined behavior will result.  If
#     this is a real problem, use the open3 or open4 libraries instead.
#
# Hacking:
# - The stabs format has some oddities.  I've commented the stabs parser
#   with hints of strange behavior that I've discovered working with stabs
#   data.
# - Adding additional basic types or reference types is easy, just add them
#   to the TYPES hash.
# - Adding typedef support requires modification of the stabs parser,
#   removing the self-referential requirement should do it.
# - There is no support in the parser for enums, structs, etc.  They use
#   a more complicated stab string format that could be parsed with some
#   pain & effort.
# - Stabs documentation:
#   http://sourceware.org/gdb/current/onlinedocs/stabs_toc.html

# We need to run with -W0 to get rid of
#   ./symtabgen.rb:142: warning: Insecure world writable dir /afs/andrew.cmu.edu/usr2 in PATH, mode 040777
# But this next line overrides -W0.
#   BEGIN {$VERBOSE = true}

# OBJDUMP      = "/usr/local/bin/objdump"   # Path to objdump executable.
OBJDUMP      = "/usr/bin/objdump"   # Path to objdump executable.
TABLE_SYMBOL = 'functions' # Symbol name of the function table in .rodata.

# Type mapping:
# Contains basic types (i.e., not typedefs) to recognize when parsing the
# stabs.  Types must not have spaces between the type name and any '*'s.
# It is OK to have spaces in the type name itself (e.g., "unsigned int*" but
# not "unsigned int *").
TYPES = Hash.new(-1).merge!({
	'char'   => 0,
	'int'    => 1,
	'float'  => 2,
	'double' => 3,
	'char*'  => 4,
	'char**' => 5,
#	'void*'  => 6,
})

class String
	# Returns the integer value of a string of eight hexadecimal characters
	# interpreted as a signed 32-bit integer.  Used for interpreting file
	# offsets which may be negative.
	def hexs32
		return *[reverse].pack('h8').unpack('i')
	end
end

class Argsym
	attr_writer :type
	attr_reader :name

	def initialize(type, offset, name)
		raise ArgumentError,
                      "Argument name \"#{name}\" is too long: #{name.length}/#{self.class.max_name} chars/max." \
		      if name.length >= self.class.max_name

		@type   = type
		@offset = offset
		@name   = name
	end

	# Packs an Argsym into the C structure representation padded for proper
	# alignment.
	def pack
		packed = [@type, @offset, @name].pack("iia#{self.class.max_name}")
		return packed + "\0" * (self.class.sizeof - packed.length)
	end

	class << self
		attr_accessor :sizeof   # Size of Argsym struct including padding.
		attr_accessor :max_name # Maximum length of an argument name.
	end
end

class Functsym
	attr_reader :addr, :name, :args

	def initialize(addr, name)
		raise ArgumentError,
                      "Function name \"#{name}\" is too long: #{name.length}/#{self.class.max_name} chars/max." \
		      if name.length >= self.class.max_name

		@addr = addr
		@name = name
		@args = []
	end

	# Compares two Functsyms by their addresses.  This is used to sort the
	# function table by address.
	def <=>(func)
		return @addr <=> func.addr
	end

	# Packs a Functsym into the C structure representation padded for proper
	# alignment.
	def pack
		packed = [@addr, @name].pack("Ia#{self.class.max_name}") +
		         args.inject('') {|memo, arg| memo + arg.pack}

		return packed + "\0" * (self.class.sizeof - packed.length)
	end

	class << self
		attr_accessor :sizeof   # Size of Functsym struct including padding.
		attr_accessor :max_name # Maximum length of a function name.

		# Returns a null string of the same length as the Functsym structure.  This
		# is used to terminate the functions table.
		def zero
			return "\0" * @sizeof
		end
	end
end

class ParseError < StandardError; end

if ARGV.empty?
	puts "Usage: #{File.basename($0)} file ..."
	exit
end

ARGV.each do |fname|
	rodata_addr = rodata_offset = table_addr = nil

	# Parse the section headers to find the location of the .rodata
	# section
	IO.foreach("| #{OBJDUMP} -h -- #{fname}") do |line|
		# Regexp match example:
		# Idx Name          Size      VMA       LMA       File off  Algn
		#   5 .rodata       0011392e  08091e40  08091e40  00049e40  2**5
		if line =~ /^\s*\d+\s+(\S+)\s+\w+\s+(\w+)\s+\w+\s+(\w+)\s+\S+$/
			name, vma, file_off = $~.captures

			if name == '.rodata'
				rodata_addr   = vma.hex
				rodata_offset = file_off.hex
			end
		end
	end

	raise ParseError, "No .rodata section found in section headers." \
	      if rodata_addr.nil?

	symbols = []

	# Parse the symbol table for the function table symbol and find its
	# address.  Also do a quick parse of function symbol entries to avoid an
	# additional pass later.
	IO.foreach("| #{OBJDUMP} -t -- #{fname}") do |line|
		# Regexp match example:
		# 08091f80 g     O .rodata        00100000 functions
		if line =~ /^(\w+)\s+\w+\s+(\w)\s+(\S+)\s+\w+\s+(?:\S+\s+)?(\w+)$/
			value, type, section, name = $~.captures

			if section == '.rodata' && name == TABLE_SYMBOL
				# Function table symbol entry
				table_addr = value.hex
			elsif type == 'F'
				# A function symbol entry
				addr = value.hex
				symbols << [addr, name]
			end
		end
	end

	raise ParseError,
	      "No \"#{TABLE_SYMBOL}\" symbol found in .rodata section." \
	      if table_addr.nil?

	table_offset = table_addr - rodata_addr + rodata_offset

	max_num_funcs = max_num_args = nil

	# Read useful constants stored as the first entry in the preprocessed
	# function table.  See traceback_globals.c for details.
	File.open(fname, 'rb') do |file|
		file.seek(table_offset)
		Functsym.sizeof   = *file.read(4).unpack('I')
		max_num_funcs     = *file.read(2).unpack('S')
		max_num_args      = *file.read(1).unpack('C')
		Argsym.sizeof     = *file.read(1).unpack('C')
		Functsym.max_name = *file.read(1).unpack('C')
		Argsym.max_name   = *file.read(1).unpack('C')
	end

	types = {}
	funcs = {}
	func  = nil

	# Parse the stab entries for three things:
	# 1. Basic types emitted by GCC at the top of the stabs.
	# 2. Reference types that are defined nearly anywhere, but always before
	#    they are used.
	# 3. Function (FUN) entries, associated parameter variables (PSYM), and
	#    stack variables (LSYM--but only those that share an identifier with a
	#    PSYM).
	IO.foreach("| #{OBJDUMP} -G -- #{fname}") do |line|
		# Regexp match example:
		# Symnum n_type n_othr n_desc n_value  n_strx String
		# 145    FUN    0      0      0804822e 5750   main:F(0,1)
		if line =~ /^-?\d+\s+(\w+)\s+\d+\s+\d+\s+(\w+)\s+\d+\s+(.*)$/
			type, value, string = $~.captures

			# Parse a basic type emitted by GCC:
			#
			# GCC emits type numbers of the format (A,B).  Integer basic types are
			# self-referential (i.e., t(A,B)=(A,B), while floating point types refer
			# to an integer range type.
			#
			# Regexp match examples:
			# Symnum n_type n_othr n_desc n_value  n_strx String
			# 13     LSYM   0      0      00000000 448    float:t(0,12)=r(0,1);4;0;
			# 20     LSYM   0      0      00000000 690    void:t(0,19)=(0,19)
			if string =~ /^([\w ]+):t(\(\d+,\d+\))=r\(\d+,\d+\)/ ||
			   string =~ /^([\w ]+):t(\(\d+,\d+\))=\w?\2/
				name, type_num  = $~.captures
				types[type_num] = name if TYPES.key?(name)
				next
			end

			# Scan for reference types:
			#
			# They may be defined anywhere prior to their usage, but usually the first
			# time a typedef or a variable of that type first appears in the stabs,
			# including structure definitions.
			#
			# Warning: Type numbers are sometimes reused (GCC bug?).  When a previously
			# defined type number references an unknown type, it must be removed from
			# the types hash or misidentification occurs.
			#
			# Regexp match example:
			# __builtin_va_list:t(0,20)=*(0,2)
			string.scan(/(\(\d+,\d+\))=\*(\(\d+,\d+\))/).each do |type_num, ref_num|
				if !types[ref_num].nil?
					name = "#{types[ref_num]}*"

					if TYPES.key?(name)
						types[type_num] = name
					else
						types.delete(type_num)
					end
				else
					types.delete(type_num)
				end
			end

			case type

			# Parse a function entry:
			# Type number associated with a function entry is its return type.
			# Currently they are ignored.
			when 'FUN'
				raise ParseError, "Exceeded maximum number of functions." \
				      if funcs.size >= max_num_funcs

				addr = value.hex

				raise ParseError,
				      "More than one FUN entry exists at address 0x#{"%08x" % addr}." \
				      if funcs.key?(addr)

				name = string.split(':')[0]
				func = Functsym.new(addr, name)

				funcs[addr] = func

			# Parse a parameter variable entry:
			# These should immediately follow a function entry.  The types specified by
			# the type number may actually be a larger type for alignment purposes
			# (e.g., a char may appear as an integer type to maintain 4-byte
			# alignment).  If so, immediately following the function's source line
			# numbers should be a stack variable entry with the proper type number.
			when 'PSYM'
				raise ParseError, "Encountered PSYM entry with no associated FUN entry." \
				      if func.nil?

				raise ParseError,
				      "Exceeded maximum number of arguments for function \"#{func.name}\"." \
				      if func.args.size >= max_num_args

				# Regexp match example:
				# argc:p(0,1)
				if string =~ /^(\w+):p(\(\d+,\d+\))/
					name, type_num = $~.captures

					type   = TYPES[types[type_num]]
					offset = value.hexs32
					arg    = Argsym.new(type, offset, name)

					func.args << arg
				end

			# Don't parse line number entries, but maintain the func reference for
			# corrective stack variable entries that may follow a parameter variable.
			when 'SLINE'

			# Parse a stack variable entry:
			#
			# In general stack variables (e.g., local variables) are not of interest.
			# Furthermore, LSYM entries are used for basic types (parsed above),
			# typedefs, structure definitions, and perhaps others.  We don't parse
			# these.
			#
			# However, stack variables that immediately follow a function entry (and
			# its PSYMs and SLINEs) may correct an inaccurate type number used in a
			# previous parameter variable entry.  These are parsed.
			when 'LSYM'
				next if func.nil?

				# Regexp match example:
				# c:(0,2)
				if string =~ /^(\w+):(\(\d+,\d+\))/
					name, type_num = $~.captures

					arg      = func.args.find {|a| a.name == name}
					arg.type = TYPES[types[type_num]] unless arg.nil?
				end

			# FUN entries are followed by PSYM, SLINE, and LSYM entries (may be a
			# GCCism only).  Entries of other types mark the end a function
			# specification.  Avoid unnecessary (contentious?) LSYM parses by unsetting
			# func.
			else
				func = nil
			end
		end
	end

	# Add previously parsed function symbol entries to the funcs array:
	# Often multiple function symbols point to the same address, so function
	# addresses are not unique.  Entries in the function table should be unique
	# to avoid confusion, so eliminate duplicate entries.  Fortunately, it
	# appears that common names (i.e., weak symbol names) appear first.
	symbols.each do |addr, name|
		if !funcs.key?(addr)
			raise ParseError, "Exceeded maximum number of functions." \
			      if funcs.size >= max_num_funcs

			funcs[addr] = Functsym.new(addr, name)
		end
	end

	# Write the function table to the file and terminate with a null
	# table entry.
	File.open(fname, 'r+b') do |file|
		file.seek(table_offset)
		funcs.values.sort.each {|f| file << f.pack}
		file << Functsym.zero
	end
end
