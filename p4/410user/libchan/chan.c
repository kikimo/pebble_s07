/**
 * @file chan.c
 * @brief Implementation of the unbuffered channel library for P4 s18
 */

#include <chan.h> // the channel interface we're implementing
#include <stdlib.h>
#include <assert.h> // assert()
#include <thread.h> // thr_getid()

/**
 * @brief Initialize an unbuffered channel
 * @param c Pointer to the channel to be initialized
 * @return 0 on success, negative error code on failure
 */
int chan_init(chan_t *c){

    if(mutex_init(&c->mtx) < 0){
        goto mtx_fail;
    }
    if(cond_init(&c->has_space) < 0){
        goto has_space_fail;
    }
    if(cond_init(&c->has_data) < 0){
        goto has_data_fail;
    }
    if(cond_init(&c->was_received) < 0){
        goto was_received_fail;
    }
    if(cond_init(&c->close_cvar) < 0){
        goto close_cvar_fail;
    }

    c->data = NULL;
    c->sender = -1;
    c->state = OPEN;
    c->refcount = 0;

    return 0;

 close_cvar_fail:
    cond_destroy(&c->was_received);
 was_received_fail:
    cond_destroy(&c->has_data);
 has_data_fail:
    cond_destroy(&c->has_space);
 has_space_fail:
    mutex_destroy(&c->mtx);
 mtx_fail:
   return -1;
}

/**
 * @brief Close a channel, signal all waiters and send them an error
 * @note Closing is not equivalent to destroying!
 * @param c Pointer to the channel to be closed
 */
void chan_close(chan_t *c){

    assert(c->state != DESTROYED);

    mutex_lock(&c->mtx);

    c->state = CLOSED;
    c->data = NULL;
    c->sender = -1;

    cond_broadcast(&c->has_data);
    cond_broadcast(&c->has_space);
    cond_signal(&c->was_received);

    while(c->refcount > 0){
        cond_wait(&c->close_cvar, &c->mtx);
    }

    mutex_unlock(&c->mtx);
    return;
}

/**
 * @brief Destroys a channel
 * @param c The channel to be destroyed
 */
void chan_destroy(chan_t *c){
    assert(c->refcount == 0);
    c->state = DESTROYED;
    mutex_destroy(&c->mtx);
    cond_destroy(&c->has_space);
    cond_destroy(&c->has_data);
    cond_destroy(&c->was_received);
    return;
}

/**
 * @brief Sends data through a channel
 * @param c The channel to send through
 * @param data The data to send
 * @return 0 on success, a negative error code on failure (possibly a closed channel)
 */
int chan_send(chan_t *c, void *data){

    assert(c->state != DESTROYED);

    mutex_lock(&c->mtx);
    c->refcount++;

    while(c->sender != -1 && c->state == OPEN){
        cond_wait(&c->has_space, &c->mtx);
    }

    if(c->state != OPEN){
        goto closed;
    }

    int tid = thr_getid();
    c->data = data;

    c->sender = tid;

    cond_signal(&c->has_data);

    while(c->sender == tid && c->state == OPEN){
        cond_wait(&c->was_received, &c->mtx);
    }

    if(c->state != OPEN){
        goto closed;
    }
    c->refcount--;
    cond_signal(&c->close_cvar);
    mutex_unlock(&c->mtx);
    return 0;

closed:
    c->refcount--;
    cond_signal(&c->close_cvar);
    mutex_unlock(&c->mtx);
    return -1;
}

/**
 * @brief Receive data through a channel
 * @param c The channel to receive through
 * @param data Out argument pointer for the received data
 * @return 0 on success, a negative error code on failure
 */
int chan_recv(chan_t *c, void **data){

    assert(c->state != DESTROYED);
    assert(data != NULL);

    mutex_lock(&c->mtx);
    c->refcount++;

    while(c->sender == -1 && c->state == OPEN){
        cond_wait(&c->has_data, &c->mtx);
    }

    if(c->state != OPEN){
        c->refcount--;
        cond_signal(&c->close_cvar);
        mutex_unlock(&c->mtx);
        return -1;
    }

    *data = c->data;

    c->data = NULL;
    c->sender = -1;

    cond_signal(&c->has_space);
    cond_signal(&c->was_received);

    c->refcount--;
    cond_signal(&c->close_cvar);
    mutex_unlock(&c->mtx);

    return 0;
}
