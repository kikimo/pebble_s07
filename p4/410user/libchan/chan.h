/**
 * @file chan.h
 * @brief Interface for the unbuffered channel lbirary
 */

#ifndef _LIBCHAN_CHAN_H_
#define _LIBCHAN_CHAN_H_

#include <cond.h>
#include <mutex.h>
#include <stdbool.h>
#include <stddef.h>

typedef enum {
    OPEN,
    CLOSED,
    DESTROYED
} chan_state_t;

typedef struct {
    mutex_t mtx;
    cond_t has_space;
    cond_t has_data;
    cond_t was_received;
    cond_t close_cvar;
    void *data;
    int sender;
    chan_state_t state;
    int refcount;
} chan_t;

int chan_init(chan_t *c);
void chan_close(chan_t *c);
void chan_destroy(chan_t *c);
int chan_send(chan_t *c, void *data);
int chan_recv(chan_t *c, void **data);

#endif // _LIBCHAN_CHAN_H_
