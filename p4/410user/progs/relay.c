/**
 * @file relay.c
 * @brief A relay race involving some runner threads.
 *
 * @author Axel Feldmann (asfeldma)
 * @author Zachary Snow (zsnow)
 * @author Stanley Zhang (szz)
 *
 * @public yes
 * @for p4-goroutines
 * @covers preemptibility,thr_create(),thr_exit(),thr_getid(),thr_join()
 *
 * @status ?
 */

// libc includes
#include <stdlib.h>     // NULL, atoi(), calloc()
#include <stdio.h>      // snprintf(), puts()

// spec includes
#include <syscall.h>    // PAGE_SIZE, set_status(), task_vanish()

// library includes
#include <thread.h>     // thread library
#include <mutex.h>      // mutexes
#include <410_tests.h>  // testing infrastructure

DEF_TEST_NAME("relay:");

/** @brief Stack size. */
#define STACK_SIZE PAGE_SIZE

/** @brief Timer period. */
#define TIMER_PERIOD 1

/** @brief Formatting buffer length. */
#define BUFLEN 64

/** @brief Default number of runners in the relay. */
#define DEFAULT_RUNNERS 20

/** @brief Default number of kernel threads. */
#define DEFAULT_NTHREAD 4

/** @brief The total number of runners. */
unsigned int runners = DEFAULT_RUNNERS;

/** @brief Array of runner thread IDs. */
int *tids;

/** @brief The current runner. */
unsigned int runner;

/**
 * @brief Runs a leg of the race.
 *
 * @param arg Our relay number.
 * @return Does not return.
 */
void *run(void *arg) {
    char buf[BUFLEN];

    unsigned int me = (unsigned int) arg;
    if (me >= runners) {
        snprintf(buf, sizeof(buf), "Bogus runner (%u >= %u)", me, runners);
        report_misc(buf); puts(buf);
        REPORT_END_FAIL;
        task_vanish(-1);
    }

    snprintf(buf, sizeof(buf), "runner = %u", me);
    report_misc(buf); puts(buf);
    runner = me;

    if (me > 0) {
        unsigned int next = me - 1;
        REPORT_FAILOUT_ON_ERR(tids[next] = thr_create(run, (void *) next));

        while (runner > next) {
            continue;   // Spinning is bad, but this is a test.
        }
    }

    snprintf(buf, sizeof(buf), "%u finished", me);
    report_misc(buf); puts(buf);
    thr_exit(NULL);

    return NULL;    // placate compiler
}

/**
 * @brief Starts the race.
 *
 * @param argc Number of command line arguments.
 * @param argv Command line arguments.
 */
int main(int argc, char *argv[]) {
    unsigned int nthread = DEFAULT_NTHREAD;

    if (argc > 1) {
        nthread = atoi(argv[1]);
    }
    if (argc > 2) {
        runners = atoi(argv[2]);
    }

    REPORT_START_CMPLT;

    if (
            runners < 1
            || thr_init_multi(STACK_SIZE, nthread, TIMER_PERIOD) < 0
            || (tids = calloc(runners, sizeof(tids[0]))) == NULL
       ) {
        report_misc("Initialization failure!");
        REPORT_END_FAIL;
        task_vanish(-1);
    }

    runner = runners - 1;
    REPORT_FAILOUT_ON_ERR(tids[runner] = thr_create(run, (void *) runner));

    while (runner != 0) {
        continue;   // Spinning is bad, but this is a test.
    }

    for (unsigned int i = 0; i < runners; i++) {
        REPORT_FAILOUT_ON_ERR(thr_join(tids[i], NULL));
    }

    REPORT_END_SUCCESS;
    set_status(0);
    thr_exit(NULL);
}

