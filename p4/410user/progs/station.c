/**
 * @file station.c
 * @brief Exercises clock interrupts in a charming fashion.
 *
 * @author de0u
 *
 * @note Modified for P4 S2018 by Stanley Zhang (szz).
 */


// libc includes
#include <stdlib.h>     // NULL, atoi()
#include <stdio.h>      // puts()

// spec includes
#include <syscall.h>    // PAGE_SIZE, swexn(), task_vanish()

// library includes
#include <410_tests.h>  // testing infrastructure

DEF_TEST_NAME("station:");

// If you make this much larger, what happens next will shock you!
#define DELAY_TICKS 10

#define DEFAULT_PERIOD 1

#define SWEXN_STACK_SIZE PAGE_SIZE

/* https://classictrainsongs.com/down_by_station/down_by_the_station.htm */
static char *lyrics[] = {
    "Down by the station early in the morning,",
    "See the little puffer bellies all in a row.",
    "See the station master turn the little handle,",
    "Chug, chug, toot, toot, off we go.",
    NULL
};

static void twiddle(void) {
    volatile int accumulator = 0;
    while (accumulator != -1) {
        ++accumulator;
    }

    const char *overflow = "Uh oh, overflow, population, common group";
    report_misc(overflow); puts(overflow);
    REPORT_END_FAIL;
    task_vanish(-1);
}


static void tickback(void *, ureg_t *);

static void tickback_install(ureg_t *ureg) {
    static char stack[SWEXN_STACK_SIZE];

    REPORT_FAILOUT_ON_ERR(
            swexn(stack + sizeof(stack), tickback, NULL, ureg)
            );
}

static void tickback(void *arg, ureg_t *ureg) {
    static volatile int tick = 0;
    static volatile int line = 0;

    (void) arg;

    if (++tick == DELAY_TICKS) {
        tick = 0;
        report_misc(lyrics[line]); puts(lyrics[line]);
        ++line;
        if (lyrics[line] == NULL) {
            REPORT_END_SUCCESS;
            task_vanish(0);
        }
    }

    tickback_install(ureg);
}

int main(int argc, char *argv[]) {
    int period = DEFAULT_PERIOD;

    if (argc > 1) {
        period = atoi(argv[1]);
    }

    REPORT_START_CMPLT;

    tickback_install(NULL);
    set_timer(period);
    twiddle();
}

