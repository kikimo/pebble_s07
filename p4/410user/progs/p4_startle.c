/**
 * @file p4_startle.c
 *
 * @brief simple test of thread creation and preemption
 *
 * @author Dave Eckhardt
 *
 * @note Updated for P4 S2018 by Stanley Zhang
 */

// libc includes
#include <stdlib.h>         // NULL, atoi()
#include <stdio.h>          // printf()

// spec includes
#include <syscall.h>        // task_vanish()

// library includes
#include <thread.h>         // thr_init_multi(), thr_create(), thr_getid(),
                            // thr_yield()
#include <410_tests.h>      // testing infrastructure

DEF_TEST_NAME("p4_startle:");

/** @brief Thread stack size. */
#define STACK_SIZE 3072

/** @brief Timer period. */
#define TIMER_PERIOD 1

/** @brief Default number of child threads to create. */
#define DEFAULT_NCHILD 15

/** @brief Default number of kernel threads. */
#define DEFAULT_THR_THREADS 4

/** @brief Child ID array. */
int *child_ids;

/** @brief Root thread ID. */
int roottid;

/**
 * @brief Declare that we have run, then twiddle thumbs.
 *
 * @param param The destination for our ID.
 */
static void *child(void *param) {
    int *slot = param;

    int id = thr_getid();
    printf("%d started\n", id);
    *slot = id;

    while (1) {
        // Spinning is a bad idea, but this is a test.
        thr_yield(roottid);
    }

    return NULL;    // placate compiler
}

/** @brief Create a bunch of threads, and test that they all run. */
int main(int argc, char *argv[]) {
    int nchild = DEFAULT_NCHILD;
    int thr_threads = DEFAULT_THR_THREADS;

    if (argc > 1) {
        thr_threads = atoi(argv[2]);
    }
    if (argc > 2) {
        nchild = atoi(argv[1]);
    }

    REPORT_START_CMPLT;

    if (
            thr_init_multi(STACK_SIZE, thr_threads, TIMER_PERIOD) < 0
            || !(child_ids = calloc(nchild, sizeof(child_ids[0])))
       ) {
        report_misc("Initialization failure!");
        REPORT_END_FAIL;
        task_vanish(-1);
    }

    roottid = thr_getid();

    for (int t = 0; t < nchild; ++t) {
        REPORT_FAILOUT_ON_ERR(thr_create(child, child_ids + t));
    }

    while (1) {
        int nregistered, slot;

        for (nregistered = 0, slot = 0; slot < nchild; ++slot) {
            if (child_ids[slot] != 0) {
                ++nregistered;
            }
        }

        if (nregistered == nchild) {
            break;
        }

        // I hope something startling happens!
        continue;
    }

    REPORT_END_SUCCESS;
    task_vanish(0);
}

