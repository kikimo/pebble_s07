/**
 * @file unfair.c
 * @brief Tests a situation with "unfair" threads.
 *
 * @author Stanley Zhang (szz)
 *
 * @public yes
 * @for p4-goroutines
 * @covers m-to-n,thr_create(),thr_exit(),thr_yield()
 *
 * @status ?
 *
 * This test explicitly deschedules the kernel thread that happens
 * to run each of the "unfair" user threads.
 *
 * Many syscalls deal with kernel thread lifecycles and scheduling.
 * However, if the user threads are not 1-to-1 with the kernel
 * threads, performing syscalls that block kernel threads for "a
 * long time" (maybe even forever!) will severely hamper the utility
 * of the thread library.  Some previous P2 tests, such as `largetest`,
 * also have this problem, albeit involving other syscall(s).
 *
 * Programs should generally be written to avoid the very problem
 * which is exercised in this test: the thread management interface
 * is provided by the thread library and its accompanying
 * synchronization primitives, abstracting away the scheduling and
 * execution of those threads.  For further illustration, one might
 * consider why `thr_yield()` is part of the thread library "even
 * though" there is a `yield()` syscall.
 */

// libc includes
#include <stdlib.h>     // NULL, atoi()

// spec includes
#include <syscall.h>    // PAGE_SIZE, deschedule(), set_status(), task_vanish()

// library includes
#include <thread.h>     // thread library
#include <410_tests.h>  // testing infrastructure

DEF_TEST_NAME("unfair:");

/** @brief Stack size. */
#define STACK_SIZE PAGE_SIZE

/** @brief Timer period. */
#define TIMER_PERIOD 1

/** @brief Formatting buffer length. */
#define BUFLEN 64

/** @brief Default number of kernel threads. */
#define DEFAULT_NTHREAD 4

/**
 * @brief Deschedules the kernel thread running the thread.
 *
 * This is very much illegitimate behavior for "typical" thread
 * library consumers!  The thread library should be in charge of
 * the scheduling and execution of threads; the "leakage" of kernel
 * thread management functionality might be considered an unfortunate
 * side effect of their exposure as system calls.  See comments
 * above for more discussion.
 *
 * @param arg Destination (int *) for writing a 1 upon start.
 * @return Does not return.
 */
static void *unfair(void *arg) {
    int *flagp = arg;
    *flagp = 1;

    int reject = 0;
    REPORT_FAILOUT_ON_ERR(deschedule(&reject));

    report_misc("How did you get my number?");
    REPORT_END_FAIL;
    task_vanish(-1);
}

/**
 * @brief Starts the test.
 *
 * @param argc Number of command line arguments.
 * @param argv Command line arguments.
 */
int main(int argc, char *argv[]) {
    unsigned int nthread = DEFAULT_NTHREAD;

    if (argc > 1) {
        nthread = atoi(argv[1]);
    }

    REPORT_START_CMPLT;

    if (
            nthread < 2
            || thr_init_multi(STACK_SIZE, nthread, TIMER_PERIOD) < 0
        ) {
        report_misc("Initialization failure!");
        REPORT_END_FAIL;
        task_vanish(-1);
    }

    // In a task with `nthread` kernel threads, create `nthread - 1`
    // threads that unfairly "clog" the system by descheduling their
    // kernel threads.
    for (unsigned int i = 0; i < nthread - 1; i++) {
        volatile int started = 0;
        int tid;
        REPORT_FAILOUT_ON_ERR(tid = thr_create(unfair, (int *) &started));
        while (!started) {
            thr_yield(tid);
        }
    }

    // Hopefully, we still have one kernel thread available to run this part...
    REPORT_END_SUCCESS;
    task_vanish(0);
}
