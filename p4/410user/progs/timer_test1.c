/**
 * @file timer_test1.c
 * @brief A test to check the swexn timer rate.
 *
 * **> Public: Yes
 * **> Covers: swexn,set_timer,get_ticks
 * @author Stanley Zhang <szz>
 */

// libc includes
#include <stdlib.h>     // atoi()
#include <stdio.h>      // snprintf(), puts()

// spec includes
#include <syscall.h>    // swexn(), set_timer(), get_ticks(), task_vanish()

// library includes
#include "410_tests.h"  // testing infrastructure

DEF_TEST_NAME("timer_test1:");

/** @brief Formatting buffer length. */
#define BUFLEN 256

/** @brief Default timer period. */
#define PERIOD_DEFAULT 1

/** @brief Default number of periods to wait. */
#define PERIOD_COUNT_DEFAULT 10

/** @brief Size of the swexn stack. */
#define SWEXN_STACK_SIZE 1024

/** @brief Number of timer ticks to wait before declaring completion. */
#define TICKS_DONE 5

/** @brief (Top of) swexn stack. */
char swexn_stack[SWEXN_STACK_SIZE];

/** @brief 1 upon completion of timing; 0 otherwise. */
volatile int timing_done = 0;

/**
 * @brief Handles swexns.
 *
 * @param arg The destination (volatile unsigned int *) for the number of ticks.
 * @param regs The registers at the time of the exception.
 * @return Does not return.
 */
static void swexn_handler(void *arg, ureg_t *regs) {
    if (regs->cause != SWEXN_CAUSE_TIMER) {
        char buf[BUFLEN];
        snprintf(
                buf, sizeof(buf), "swexn_handler() got non-timer cause: %d",
                regs->cause
                );
        report_misc(buf);
        REPORT_END_FAIL;
        task_vanish(-1);
    }

    if (timing_done) {
        report_misc("swexn_handler() called after timing done");
        REPORT_END_FAIL;
        task_vanish(-1);
    }

    volatile unsigned int *tp = arg;
    *tp = get_ticks();

    if (swexn(
                swexn_stack + sizeof(swexn_stack), swexn_handler,
                arg, regs
             ) < 0) {
        report_misc("swexn_handler() swexn() failed");
        REPORT_END_FAIL;
        task_vanish(-1);
    }

    report_misc("swexn() returned after restoring regs");
    REPORT_END_FAIL;
    task_vanish(-1);
}

/**
 * @brief Program entry point.
 *
 * Sets up the handler, starts the timer, and waits for the time.
 *
 * @param argc Command-line argument length.
 * @param argv Command-line arguments.
 */
int main(int argc, char *argv[]) {
    int period = PERIOD_DEFAULT;
    int period_count = PERIOD_COUNT_DEFAULT;
    if (argc > 1) {
        period = atoi(argv[1]);
    }
    if (argc > 2) {
        period_count = atoi(argv[2]);
    }

    if (period <= 0 || period_count <= 0) {
        printf(
                "Usage: %s [period=%d] [period_count=%d]\n"
                "`period` and `period_count` must be positive.\n",
                argv[0], PERIOD_DEFAULT, PERIOD_COUNT_DEFAULT
              );
        return -1;
    }

    REPORT_START_CMPLT;

    volatile unsigned int t = 0;

    if (swexn(
                swexn_stack + sizeof(swexn_stack), swexn_handler,
                (void *) &t, NULL
             ) < 0) {
        report_misc("main() swexn() failed");
        REPORT_END_FAIL;
        return -1;
    }

    set_timer(period);

    unsigned int t_start = get_ticks();
    for (int i = 0; i < period_count; i++) {
        unsigned int t_curr;
        while ((t_curr = t) <= t_start) {
            continue;   // Wait for our handler to write the time in.
        }
        unsigned int ticks = t_curr - t_start;

        char buf[BUFLEN];
        snprintf(buf, sizeof(buf), "%d: %u ticks", i, ticks);
        report_misc(buf); puts(buf);

        if (ticks < (unsigned int) period) {
            report_misc("tick came in too early!");
            REPORT_END_FAIL;
            return -1;
        }

        t_start = t_curr;
    }

    set_timer(0);
    timing_done = 1;

    unsigned int t_done = get_ticks() + TICKS_DONE;
    while (get_ticks() < t_done) {
        continue;   // Wait around for a bit; maybe a rogue swexn will appear...
    }

    REPORT_END_SUCCESS;
    return 0;
}

