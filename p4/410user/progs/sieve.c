/**
 * @file sieve.c
 * @brief "A concurrent prime sieve"
 * Ported (loosely) from: https://golang.org/doc/play/sieve.go
 *
 * @author Zachary Snow (zsnow)
 * @author Axel Feldmann (asfeldma)
 * @bug no known bugs
 */

#include <assert.h> // assert()
#include <chan.h>   // chan_*()
#include <thread.h> // thr_*()
#include <stdio.h>  // printf()
#include <stdlib.h> // NULL, atoi()

#define STACK_SIZE 4096
#define TIMER_PERIOD 1
#define DEFAULT_NTHREAD 4
#define DEFAULT_MAX_NUMBER 60

static void *filter(void *arg) {
    chan_t *in = arg;
    int prime;

    if (chan_recv(in, (void **)&prime) < 0){
        thr_exit(NULL);
    }

    chan_t out;
    assert(chan_init(&out) == 0);

    int child = thr_create(filter, &out);
    assert(child >= 0);

    if (prime != 37) {
        printf("%d is prime\n", prime);
    } else {
        printf("%d is the best prime\n", prime);
    }

    while (1) {
        int i, res = chan_recv(in, (void **)&i);
        if (res != 0) {
            break;
        }
        if (i % prime == 0) {
            continue;
        }
        chan_send(&out, (void *)i);
    }

    chan_close(&out);
    thr_join(child, NULL);
    chan_destroy(&out);
    thr_exit(NULL);

    return NULL;
}

int main(int argc, char *argv[]) {
    int nthread = DEFAULT_NTHREAD;
    int max_number = DEFAULT_MAX_NUMBER;

    if (argc > 1) {
        nthread = atoi(argv[1]);
    }
    if (argc > 2) {
        max_number = atoi(argv[2]);
    }

    if (thr_init_multi(STACK_SIZE, nthread, TIMER_PERIOD) < 0) {
        return -1;
    }

    chan_t out;
    assert(chan_init(&out) == 0);

    int child = thr_create(filter, &out);
    assert(child >= 0);

    for (int i = 2; i <= max_number; i++) {
        chan_send(&out, (void *)i);
    }

    chan_close(&out);
    thr_join(child, NULL);
    chan_destroy(&out);
    thr_exit(NULL);
}

