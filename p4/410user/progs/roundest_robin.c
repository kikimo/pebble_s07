/**
 * @file roundest_robin.c
 * @brief
 *
 * @author Axel Feldmann (asfeldma)
 * @author Zachary Snow (zsnow)
 * @author Stanley Zhang (szz)
 *
 * @public yes
 * @for p4-goroutines
 * @covers preemptibility,thr_create(),thr_exit()
 *
 * @status ?
 */

// libc includes
#include <stdlib.h>     // NULL, atoi()
#include <stdio.h>      // snprintf(), puts()

// spec includes
#include <syscall.h>    // PAGE_SIZE, set_status(), task_vanish()

// library includes
#include <thread.h>     // thread library
#include <410_tests.h>  // testing infrastructure

DEF_TEST_NAME("roundest_robin:");

/** @brief Stack size. */
#define STACK_SIZE PAGE_SIZE

/** @brief Timer period. */
#define TIMER_PERIOD 1

/** @brief Formatting buffer length. */
#define BUFLEN 64

/** @brief Default number of child threads to create. */
#define DEFAULT_CHILDREN 5

/** @brief Default number of rounds. */
#define DEFAULT_ROUNDS 2

/** @brief Default number of kernel threads. */
#define DEFAULT_NTHREAD 4

/** @brief The total number of child threads. */
unsigned int children = DEFAULT_CHILDREN;

/** @brief The count so far. */
unsigned int count = 0;

/** @brief The target value to reach. */
unsigned int target;

/**
 * @brief Child thread function.
 *
 * @param arg The child's creation ID.
 */
void *robin(void *arg) {
    unsigned int id = (unsigned int) arg;

    unsigned int seen;
    while ((seen = count) < target) {
        if (seen % children == id) {
            char buf[BUFLEN];
            snprintf(buf, sizeof(buf), "%u: incr %u", id, seen);
            report_misc(buf); puts(buf);
            count = seen + 1;
        }

        continue;   // Don't try this at home!
    }

    thr_exit(NULL);
    return NULL;    // placate compiler
}

/**
 * @brief Begins the round robin.
 *
 * @param argc Number of command line arguments.
 * @param argv Command line arguments.
 */
int main(int argc, char *argv[]) {
    unsigned int nthread = DEFAULT_NTHREAD;
    unsigned int rounds = DEFAULT_ROUNDS;

    if (argc > 1) {
        nthread = atoi(argv[1]);
    }
    if (argc > 2) {
        children = atoi(argv[2]);
    }
    if (argc > 3) {
        rounds = atoi(argv[3]);
    }

    target = children * rounds;

    REPORT_START_CMPLT;

    char buf[BUFLEN];
    snprintf(buf, sizeof(buf), "target: %u", target);
    report_misc(buf); puts(buf);

    if (thr_init_multi(STACK_SIZE, nthread, TIMER_PERIOD) < 0) {
        report_misc("Initialization failure!");
        REPORT_END_FAIL;
        task_vanish(-1);
    }

    for (unsigned int i = 0; i < children; i++) {
        if (thr_create(robin, (void *) i) < 0) {
            report_misc("Failed to create child!");
            REPORT_END_FAIL;
            task_vanish(-1);
        }
    }

    while (count < target) {
        continue;   // Don't try this at home!
    }

    REPORT_END_SUCCESS;
    set_status(0);
    thr_exit(NULL);
}

