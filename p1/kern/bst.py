class Node(object):
    def __init__(self, key):
        self.key = key
        self.parent = None
        self.left = None
        self.right = None

    def __unicode__(self):
        return str(self.key)

class BST(object):
    def __init__(self):
        self.root = None

    def insert(self, node):
        if self.root is None:
            self.root = node

        else:
            self._do_insert(self.root, node)

    def _do_insert(self, root, node):
        if root.key > node:
            if root.left is None:
                root.left = node
                node.parent = root
            else:
                self._do_insert(self.root.left, node)

        else:
            if root.right is None:
                root.right = node
                node.parent = node
            else:
                self._do_insert(self.root.right, node)

    def delete(self, node):
        # TODO implement
        pass

    def successor(self, node):
        # TODO implement
        pass

    def precessor(self, node):
        # TODO implement
        pass

    def find(self, node):
        return self._do_find(self.root, node)

    def _do_find(self, root, node):
        if root is None:
            return None

        if root.key == node.key:
            return root             

        elif root.key > node.key:
            return self._do_find(root.left, node)

        else:
            return self._do_find(root.right, node)



