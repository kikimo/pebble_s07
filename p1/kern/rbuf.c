#include <rbuf.h>

/**
 * init rbuf.
 * return 0 on success otherwise -1.
 */
int rbuf_init(rbuf_t *rbuf, int capacity)
{
    rbuf->head = rbuf->buf;
    rbuf->tail = rbuf->buf;
    rbuf->capacity = capacity;
    rbuf->count = 0;

    return 0;
}

/**
 * put an element into rbuf.
 * return 0 on success, -1 if rbuf is full.
 */
int rbuf_put(rbuf_t *rbuf, char e)
{
    if (rbuf_is_full(rbuf)) {
        return -1;
    }

    *rbuf->tail = e;
    rbuf->tail++;
    // if we reach to the end of rbuf->buf
    if (rbuf->tail == rbuf->buf + rbuf->capacity) {
        rbuf->tail = rbuf->buf;
    }

    rbuf->count++;
    return 0;
}

/**
 * get an element from rbuf.
 * the element in rbuf will be stored in *val,
 * return 0 on success, -1 if rbuf is empty.
 */
int rbuf_get(rbuf_t *rbuf, char *e)
{
    if (rbuf_is_empty(rbuf)) {
        return -1;
    }

    *e = *rbuf->head;
    rbuf->head++;
    // if we reach to the end of rbuf->buf
    if (rbuf->head == rbuf->buf + rbuf->capacity) {
        rbuf->head = rbuf->buf;
    }

    rbuf->count--;

    return 0;
}

/**
 * return non-zeof value if rbuf is empty
 * otherwise 0
 */
int rbuf_is_empty(rbuf_t *rbuf)
{
    return rbuf->count == 0;
}

/**
 * return non-zero value if rbuf is full
 * otherwise 0
 */
int rbuf_is_full(rbuf_t *rbuf)
{
    return rbuf->count == rbuf->capacity;
}
