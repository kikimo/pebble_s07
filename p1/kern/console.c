/** @file console.c 
 *  @brief A console driver.
 *
 *  @author Harry Q. Bovik (hbovik) <-- change this
 *  @bug None known
 */

#include <console.h>
#include <string.h>
#include <x86/pio.h>

char term_color = FGND_WHITE | BGND_BLACK;
int logic_row = 0, logic_col = 0;
int cursor_row = 0, cursor_col = 0;
int is_cursor_visible = 1;

static inline void set_hardware_cursor(int row, int col)
{
    // TODO test
    outb(CRTC_IDX_REG, CRTC_CURSOR_LSB_IDX);
    outb(CRTC_DATA_REG, (unsigned char) col);
    outb(CRTC_IDX_REG, CRTC_CURSOR_MSB_IDX);
    outb(CRTC_DATA_REG, (unsigned char) row);
}

static inline void update_cursor()
{
    if (is_cursor_visible) {
        set_hardware_cursor(logic_row, logic_col);
    }
}

static inline char *get_pos(int row, int col)
{
    return (char *) (row * CONSOLE_WIDTH * 2 + col * 2 + CONSOLE_MEM_BASE);
}

static void scroll_up_one_line()
{
    int i;

    for (i = 0; i < CONSOLE_HEIGHT - 1; i++) {
        char *curr_row = get_pos(i, 0);
        char *next_row = get_pos(i + 1, 0);
        memcpy(curr_row, next_row, CONSOLE_WIDTH * 2);
    }

    memset(get_pos(CONSOLE_HEIGHT - 1, 0), 0, CONSOLE_WIDTH * 2);
}

static void goto_next_row()
{
    if (logic_row < CONSOLE_HEIGHT - 1) {
        logic_row++;
    } else {
        scroll_up_one_line();
    }

    logic_col = 0;
}

static void goto_last_pos()
{
    if (logic_col > 0) {
        logic_col--;
    } else {
        if (logic_row > 0) {
            logic_row--;
            logic_col = CONSOLE_WIDTH - 1;
        }
    }
}

int putbyte( char ch )
{
    switch(ch) {
        case '\n':
            goto_next_row();
            break;

        case '\r':
            logic_col = 0;
            break;

        case '\b': {
            char * ptr;

            goto_last_pos();
            ptr = get_pos(logic_row, logic_col);
            ptr[0] = 0;  // char
            ptr[1] = 0;  // color

            break;
        }
        
        default: {
            char *addr;

            if (logic_col == CONSOLE_WIDTH - 1) {
                goto_next_row();
            }

            addr = get_pos(logic_row, logic_col);
            addr[0] = ch;
            addr[1] = term_color;
            logic_col++;

            break;
        }
    }

    update_cursor();

    return (int) ch;
}

void 
putbytes( const char *s, int len )
{
    int i;

    for (i = 0; i < len; i++) {
        putbyte(s[i]);
    }
}

int
set_term_color( int color )
{
    // TODO check color
    term_color = color;
    return 0;
}

void
get_term_color( int *color )
{
    // TODO check int *color pointer
    *color = term_color;
}

int
set_cursor( int row, int col )
{
    if (row >= 0 && row < CONSOLE_HEIGHT && col >= 0 && col <= CONSOLE_WIDTH) {
        logic_row = row;
        logic_col = col;

        update_cursor();

        return 0;
    }

    return -1;
}

void
get_cursor( int *row, int *col )
{
    *row = logic_row;
    *col = logic_col;
}

void
hide_cursor()
{
    is_cursor_visible = 0;
    set_hardware_cursor(CONSOLE_HEIGHT, 0);
}

void
show_cursor()
{
    is_cursor_visible = 1;
    set_hardware_cursor(logic_row, logic_col);
}

void 
clear_console()
{
    int i;

    for (i = 0; i < CONSOLE_HEIGHT; i++) {
        char *ptr = get_pos(i, 0);
        memset(ptr, 0, CONSOLE_WIDTH * 2);
    }

    logic_col = 0;
    logic_row = 0;

    update_cursor();
}

void
draw_char( int row, int col, int ch, int color )
{
    int origin_color = term_color;

    // check row, col
    if (row < 0 || row >= CONSOLE_HEIGHT ||
        col < 0 ||col >= CONSOLE_WIDTH) {
            return;
    }

    logic_row = row;
    logic_col = col;
    term_color = color;
    putbyte(ch);
    term_color = origin_color;
}

char
get_char( int row, int col )
{
    char *ptr;

    if (row >= 0 && row < CONSOLE_HEIGHT && col >= 0 && col < CONSOLE_WIDTH) {
        ptr = get_pos(row, col);
        return ptr[0];
    }

    return -1;
}
