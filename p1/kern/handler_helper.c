#include <x86/seg.h>
#include <x86/pio.h>
#include <interrupts.h>
#include <handler_helper.h>

void set_interrupt_handler(int entry, void* handler)
{
    uint64_t *idt_entry = (uint64_t *) sidt() + entry;
    uint16_t *idt_ptr = (uint16_t *) idt_entry;

    // 1. set handler offset
    idt_ptr[0] = ((uint32_t) handler) & 0xffff;
    idt_ptr[3] = (((uint32_t) handler) & 0xfff0000) >> 16;

    // 2. set segment selector
    idt_ptr[1] = KERNEL_CS_SEGSEL;

    // 3. set option = [1 00 0] [1110] [000 0] [0000]
    idt_ptr[2] = 0x8e00;
}

inline void ack_interrupt()
{
    outb(INT_CTL_REG, INT_CTL_DONE);
}
