#include <stdio.h>
#include <stdlib.h>
#include <timer_defines.h>
#include <410_reqs.h>
#include <interrupts.h>
#include <x86/seg.h>
#include <x86/pio.h>
#include <stdint.h>
#include <handler_helper.h>

// timer tick interval, one tick ervery 10ms
#define TIMER_TICK_INTERVAL   (TIMER_RATE / 100)

/**
 * Defined in handler_install.c
 */
extern void timer_handler_wrapper();
typedef void (*tickback_t)(unsigned int);

tickback_t tickback_p = NULL;
unsigned int ticks;

void timer_handler()
{
    ack_interrupt();
    tickback_p(ticks);
    printf("%d\n", ticks);
    ticks++;
}

int install_timer_handler(void (*tickback)(unsigned int))
{
    // 1. first set timer interrupt entry
    tickback_p = tickback;

    // 2. set interrupt handler
    set_interrupt_handler(TIMER_IDT_ENTRY, timer_handler_wrapper);

    // 3. config timer

    // 3.1 set timmer mode to square wave
    outb(TIMER_MODE_IO_PORT, TIMER_SQUARE_WAVE);

    // 3.2 set timer period
    outb(TIMER_PERIOD_IO_PORT, TIMER_TICK_INTERVAL & 0xff);
    outb(TIMER_PERIOD_IO_PORT, TIMER_TICK_INTERVAL >> 8);

    return 0;
}
