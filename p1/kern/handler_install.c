/** @file handler_install.c
 * 
 *  @brief Handler installation function
 *
 *  Edit this file to allow your kernel to initialize and install handlers
 *  
 *  Declared in 410_reqs.h
 *
 *  @author Harry Q. Bovik (hbovik) <-- change this 
 *  @bug None known
 **/

#include <410_reqs.h>
#include <timer.h>
#include <keyboard.h>

int handler_install(void) 
{
    int ret;

    // timer handler
    if ((ret = install_timer_handler(tick)) != 0) {
        return ret;
    }

    // keyboard handler
    if ((ret = install_keyboard_handler()) != 0) {
        return ret;
    }

    return 0;
}
