#include <keyhelp.h>
#include <stdio.h>
#include <stdint.h>
#include <x86/proc_reg.h>
#include <x86/pio.h>
#include <410_reqs.h>

#include <keyboard.h>
#include <handler_helper.h>
#include <rbuf.h>

rbuf_t rbuf;

/**
 * Defined in handler_install.c
 */
extern void keyboard_handler_wrapper();

void keyboard_handler()
{
    int keypress = inb(KEYBOARD_PORT);
    // int c;

    rbuf_put(&rbuf, (char) keypress);
    // if ((c = readchar()) != -1) {
    //     putchar(c);
    // }

    ack_interrupt();
}

int install_keyboard_handler()
{
    // 1. init rbuf for keyboard device
    rbuf_init(&rbuf, RBUF_SIZE);

    // 2. set interrupt handler
    set_interrupt_handler(KEY_IDT_ENTRY, keyboard_handler_wrapper);

    return 0;
}

int readchar(void)
{
    int ret, keypress, augchar;

    while (1) {
        if ((ret = rbuf_get(&rbuf, (char *) &keypress)) != 0) {
            return -1;
        }

        augchar = process_scancode(keypress);
        if (KH_HASDATA(augchar) && KH_ISMAKE(augchar)) {
            return KH_GETCHAR(augchar);
        }
    }

    return -1;
}
