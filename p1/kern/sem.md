# semarphor

* syscall
    + int cas_runflag(int *old_val, int *expected_val, int *new_val)

* sem
    + void sem_init(int cnt)

* cont
    + cont_wait(cond_t *cv, mutex_t *mp)
        1. put current thread into cv wait list
        2. unclock(mp)
        3. goto sleep
        4. wait to be waked up
        4. remove current thread from cv wait list
        5. lock(mp)

    + cond_signal(cond_t *cv)
        1. remove one thread from cv waitl list
        2. wait the thread

    + cond_broadcast(cond_t *cv)

* mutex
    + mutex_lock(mutex_t *mp)

    + mutex_unlock()

```C
typedef struct {
    spinlock_t  wq_lock;
    queue_t     wq;
    int         lock;
} mutex_t;

int mutex_lock(mutext_t *mp)
{
    spin_lock(mp->wq_lock);
    queue_append(mp->wq, current_thread);
    spin_unlock(mp->wq_lock);

    while (test_set_lock(mp->lock) == FAILED) {
        goto sleep
    }

    spin_lock(mp->wq_lock);
    queue_remove(mp->wq, current_thread);
    spin_unlock(mp->wq_lock);

    return 0;
}

int mutex_unlock(mutext_t *mp)
{
    spin_lock(mp->wq_lock);

    mp->lock = UNLOCKED;
    if (len(mp->wq) == 0) {
        goto end;
    }

    while (mp->lock == UNLOCKED) {
        for (waiter in mp->wq) {
            wake_up(waiter);
        }
    }

end:
    spin_unlock(mp->wq_lock);

    return 0;
}
```