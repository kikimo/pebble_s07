#ifndef _TIMER_H
#define _TIMER_H

int install_timer_handler(void (*tickback)(unsigned int));

#endif  // _TIMER_H
