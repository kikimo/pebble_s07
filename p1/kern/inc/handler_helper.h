#ifndef _HANDLER_HELPER_H
#define _HANDLER_HELPER_H

#include <stdint.h>

/**
 * Install interrupt handler at IDT[entry].
 */
void set_interrupt_handler(int entry, void* handler);

/**
 * Acknowledge interrupt.
 */
inline void ack_interrupt();

#endif  // _HANDLER_HELPER_H
