#ifndef _KEYBOARD_H
#define _KEYBOARD_H

/**
 * Install keyboard handler
 * return
 *  0 on success
 *  -1 on error
 */
int install_keyboard_handler();

#endif  // _KEYBOARD_H
