#ifndef _RBUF_H
#define _RBUF_H

#include <stdint.h>

#define RBUF_SIZE   1024

typedef struct {
    char    *head;
    char    *tail;
    int     capacity;
    int     count;
    char    buf[RBUF_SIZE];
} rbuf_t;

/**
 * init rbuf.
 * return 0 on success otherwise -1.
 */
int rbuf_init(rbuf_t *rbuf, int capacity);

/**
 * put an element into rbuf.
 * return 0 on success, -1 if rbuf is full.
 */
int rbuf_put(rbuf_t *rbuf, char e);

/**
 * get an element from rbuf.
 * the element in rbuf will be stored in *val,
 * return 0 on success, -1 if rbuf is empty.
 */
int rbuf_get(rbuf_t *rbuf, char *e);

/**
 * return non-zeof value if rbuf is empty
 * otherwise 0
 */
int rbuf_is_empty(rbuf_t *rbuf);

/**
 * return non-zero value if rbuf is full
 * otherwise 0
 */
int rbuf_is_full(rbuf_t *rbuf);

#endif  // _RBUF_H