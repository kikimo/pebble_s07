/** @file tick.c
 *  @brief Tick function, to be called by the timer interrupt handler
 * 
 *  Fill in this tick function with any processing your game needs
 *  to do on each timer interrupt. This function should be called from your
 *  timer interrupt handler, even if it is empty.
 *
 *  Function declared in 410_reqs.h
 *
 *  @author Harry Q. Bovik (hbovik) <-- change this
 *  @bug None known
 **/

#include <stdio.h>
#include <410_reqs.h>

void tick(unsigned int numTicks)
{
    // printf("%u\n", numTicks);
}
