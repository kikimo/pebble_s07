# 自动化 Web 页面开发

1. 团队人员现状

    + 运维 + 开发
    + 开发大多后端出身，前端不熟悉，开发前端页面非常耗时

2. 团队 Web 页面开发需求
    + 大部分页面结构
        - 三级结构
            + 页面搜索添加 panel
            + 页面主题内容 table
            + 分页 panel

3. 自动化 Web 开发
    + 相当一部分的页面是针对某一资源的 CRUD，OPS 中如服务器页面、用户信息页面、Salt 安装页面
    + 只要提供相应资源的 CRUD 操作接口，操作接口的命名标准化，可以实现该类型 Web 页面的自动化开发
        - 方法：代码生成
        - 提供资源的字段描述，也就是数据库表的描述，Django model

    + 应用实例
        - tddns 服务器组页面、服务器页面、域名管理页面、记录管理页面、外网管理页面
        - 外网管理页面，How？
            + fake a model
4. 实现
    + Django app
        - custom Django app uigen
        - custom Django command uigen
        - Django model meta programming
            - model help_text as field label
            - model name as field name

    + DRF + Swagger API
        - 插播一个 Swagger 推广
            + 代码即文档
                - 根据代码自动生成带注释的 API 文档
            + Open API 规范生成代码
                - 根据 Open API 规范自动生成客户端调用代码: Python, Java, Go, JS...
                - 根据 Open API 规范自动生成服务端脚手架代码
            + 超赞

    + Django template for code generation
    + 类似 Django admin? Why not just Django admin?
        1. Django admin 没法拿到线上直接用
        2. Django admin 要同我们的技术栈上整合有困难
            + Django admin 使用模板技术
            + 我们的应用，Ajax，富前端

5. 使用推广
    + 当前要推广使用过的话，有困难
        - Django + Swagger 依赖
