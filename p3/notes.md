# notes

## user mode/kernel mode switching

1. priviledge level
    + 运行特权指令
    + 内存访问

Even if the program or task requesting access to a segment has sufficient privilege to access the
segment, access is denied if the RPL is not of sufficient privilege level. 

That is, if the RPL of a segment selector is numerically greater than the CPL, the RPL overrides the CPL, and
 vice versa. 

The RPL can be used to insure that privileged code does not access a segment
on behalf of an application program unless the program itself has access privileges for that segment.

The conforming (C) flag in the segment descriptor for the destination code segment, which
determines whether the segment is a conforming (C flag is set) or nonconforming (C flag is
clear) code segment. (See Section 3.4.3.1., “Code- and Data-Segment Descriptor Types”,
for more information about this flag.)

    + conforming and nonconforming segment
        - intel-sys 3.4.3.1

Code segments can be either conforming or nonconforming. A transfer of execution into a moreprivileged conforming segment allows execution to continue at the current privilege level. A
transfer into a nonconforming segment at a different privilege level results in a general-protection exception (#GP), unless a call gate or task gate is used (see Section 4.8.1., “Direct Calls or
Jumps to Code Segments”, for more information on conforming and nonconforming code
segments). 

System utilities that do not access protected facilities and handlers for some types of
exceptions (such as, divide error or overflow) may be loaded in conforming code segments. 
Utilities that need to be protected from less privileged programs and procedures should be placed in
nonconforming code segments.

Interrupt and trap gates are very similar to call gates (see Section 4.8.3., “Call Gates”). They
contain a far pointer (segment selector and offset) that the processor uses to transfer program
execution to a handler procedure in an exception- or interrupt-handler code segment. These
gates differ in the way the processor handles the IF flag in the EFLAGS register (see Section
5.10.1.2., “Flag Usage By Exception- or Interrupt-Handler Procedure”)

The CPL is treated slightly differently when
accessing conforming code segments. Conforming code segments can be accessed from
any privilege level that is equal to or numerically greater (less privileged) than the DPL of
the conforming code segment. Also, the CPL is not changed when the processor accesses a
conforming code segment that has a different privilege level than the CPL.

https://pdos.csail.mit.edu/6.828/2017/readings/i386/s06_03.htm

2. user mode priviledge level
3. kernel mode priviledge level
4. switching from kernel mode to user mode
    + iret

5. switching from user mode to kernel mode
    + interrupt
    + system call

6. stack switching
    + intel-sys.pdf 4.8.5 stack switching

## run the first program

1. 内存分页
2. 加载二进制程序
3. 跳转到程序执行入口
4. 程序内核态，用户态切换

## finding process/thread


## pL

* Restricting Access to Data

Instructions may load a data-segment register (and subsequently use the target segment) only if the DPL of the target segment is numerically greater than or equal to the maximum of the CPL and the selector's RPL. In other words, a procedure can only access data that is at the same or less privileged level.

* Restricting Control Transfers

The "near" forms of JMP, CALL, and RET transfer within the current code segment, and therefore are subject only to limit checking. 

* The operands of the "far" forms of JMP and CALL refer to other segments; therefore, the processor performs privilege checking. There are two ways a JMP or CALL can refer to another segment:

    1. The operand selects the descriptor of another executable segment.
    2. The operand selects a call gate descriptor. This gated form of transfer is discussed in a later section on call gates.

Normally the CPL is equal to the DPL of the segment that the processor is currently executing. CPL may, however, be greater than DPL if the conforming bit is set in the descriptor of the current executable segment. The processor keeps a record of the CPL cached in the CS register; this value can be different from the DPL in the descriptor of the code segment.

The processor permits a JMP or CALL directly to another segment only if one of the following privilege rules is satisfied:

DPL of the target is equal to CPL.

The conforming bit of the target code-segment descriptor is set, and the DPL of the target is less than or equal to CPL.

An executable segment whose descriptor has the conforming bit set is called a conforming segment. The conforming-segment mechanism permits sharing of procedures that may be called from various privilege levels but should execute at the privilege level of the calling procedure. Examples of such procedures include math libraries and some exception handlers. When control is transferred to a conforming segment, the CPL does not change. This is the only case when CPL may be unequal to the DPL of the current executable segment.

Most code segments are not conforming. 
The basic rules of privilege above mean that, for nonconforming segments, control can be transferred without a gate only to executable segments at the same level of privilege. 
There is a need, however, to transfer control to (numerically) smaller privilege levels; this need is met by the CALL instruction when used with call-gate descriptors, which are explained in the next section. 
The JMP instruction may never transfer control to a nonconforming segment whose DPL does not equal CPL.

* stack switching


If the destination code segment of the call gate is at a different privilege level than the CPL, an interlevel transfer is being requested.

To maintain system integrity, each privilege level has a separate stack.

The processor locates these stacks via the task state segment (see Figure 6-8). Each task has a separate TSS, thereby permitting tasks to have separate stacks. Systems software is responsible for creating TSSs and placing correct stack pointers in them. The initial stack pointers in the TSS are strictly read-only values. The processor never changes them during the course of execution.

When a call gate is used to change privilege levels, a new stack is selected by loading a pointer value from the Task State Segment (TSS). The processor uses the DPL of the target code segment (the new CPL) to index the initial stack pointer for PL 0, PL 1, or PL 2.

